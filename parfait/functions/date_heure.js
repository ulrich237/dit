import _ from 'underscore'
import React, { Component } from 'react'

export default class Date_heure extends Component {

    constructor(props) {
        super(props);
        this.state = {
         
        }
    }



    time(dateHours) {
        var d = new Date(Date.now()), f = new Date(dateHours);
        var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);
        var twoDaysAgo = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24 * 2);
        var i = f.getMonth() + 1

        if (f.getDate() == d.getDate() && f.getMonth() == d.getMonth() && f.getFullYear() == d.getFullYear()) {
            return f.getHours() + ":" + f.getMinutes()
        } else {
            if (f.getDate() == yesterday.getDate() && f.getMonth() == yesterday.getMonth() && f.getFullYear() == yesterday.getFullYear()) {
                return "Hier"
            }
            else {
                if (f.getDate() == twoDaysAgo.getDate() && f.getMonth() == twoDaysAgo.getMonth() && f.getFullYear() == twoDaysAgo.getFullYear()) {
                    return "Avant-Hier"
                } else {
                    return f.getDate() + "/" + i + "/" + f.getFullYear()
                }
            }
        }
    }

    format_date(dateHours){
        var d = new Date(Date.now()), f = new Date(dateHours);
        var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);
        var twoDaysAgo = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24 * 2);
        var i = f.getMonth() + 1

        return f.getDate() + "/" + i + "/" + f.getFullYear()

    }

    getDate_publication = (debut) => {
        let d = new Date(Date.now()); let f = new Date(debut); let diff = {}; let take = ""
        let tmp = d - f;  //Nombre de millisecondes entre les dates
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        diff.days = tmp;

        let seconds = 0;
        if ((diff.min == 0) && (diff.seconds >= 0) && (diff.seconds < 60)) {
            seconds = diff.seconds
            take = "Il y a moins d'une minute"
        }

        if ((diff.hour == 0) && (diff.min >= 1) && (diff.min < 60)) {
            var minutes = diff.min
            if (minutes == 1) {
                take = "Il y a " + minutes + " min"
            }
            else {
                take = "Il y a " + minutes + " min"
            }
        }

        if ((diff.days == 0) && (diff.hour >= 1) && (diff.hour < 23)) {
            var hours = diff.hour
            if (hours == 1) {
                take = "Il y a " + hours + " h"
            }
            else if (diff.min !== 1) {
                take = "Il y a " + hours + " h " + diff.min + " min"
            }
            else {
                take = "Il y a " + hours + " h"
            }
        }

        //1 day to 30 days
        if ((diff.days >= 1) && (diff.days < 30)) {
            var days = diff.days
            if (days == 1) {
                take = "Il y a " + days + " jour"
            }
            else if (diff.hour !== 1) {
                take = "Il y a " + days + " jours " //+ diff.hour + ' h'
            }
            else {
                take = "Il y a " + days + " jours"
            }
        }

        if (diff.days >= 30 && diff.days < 365) {
            var months = Math.floor(diff.days / 30)
            if (months == 1 && (diff.days - 30) > 0) {
                take = "Il y a " + months + " mois " //+ (diff.days - 30) + "jour(s)"
            }
            else if (months == 1) {
                take = "Il y a " + months + " mois"
            }
            else {
                take = "Il y a " + months + " mois"
            }
        }

        if (diff.days >= 365) {
            var years = Math.floor(diff.days / 365)
            if (years == 1) {
                take = "Il y  a " + years + " an"
            }
            else {
                take = "Il y a " + years + " ans"
            }
        }

        return take
    }


}
