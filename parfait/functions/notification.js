import _ from 'underscore'
import React, { Component } from 'react'

var my_fir_ser_key =
    'AAAA2dC4Mq8:APA91bHA0z322vvsu8J7XOrmhHtc0nnTRJ9CLeXxzYZbULtddZoNW_0UlyaQlt47OZURIGNBF9L13G8bEhjY48RuZNvqh6dpqNitt7flcb4GzZXu5l2y1z1gghv4HGbmYAGBrQvUzFFY';


var test_tokk =
    'dSWtQX1LTAGgb58XwJvqqW:APA91bF9Vrh2PVNm0Ptitl5oopiEIoP6_5E9xuTV9mhYVWThbDzQMMdwmMGAKu7yQ9xWGI1eZdvM03-mgM_A6FVEJo4tCVV7_CZ4MrImV5CfqibZPLNsG-USFkgFBYIinY0fVxh839xn';

export default class Notification extends Component {

    
    constructor(props) {
        super(props);
        this.state = {         

        }
    }

    sendNotitfications = async (test_tok, title, body, data) => {
        const message = {
            to: test_tok,
            notification: {
                title: title,
                body: body,
                vibrate: 1,
                sound: 1,
                show_in_foreground: true,
                priority: "high",
                content_available: true,
                color: '#dc7e27', 
            },
            data: {
                link: data
            }
        }

        const headers = {
            "Content-Type": "application/json",
            Authorization: "key=" + my_fir_ser_key,
        }
        
        let response = await fetch("https://fcm.googleapis.com/fcm/send", {
            method: "POST",
            headers,
            body: JSON.stringify(message),
        }
        ).catch(error => console.log(error)
        )
        response = await response.json()


    }


    sendNotitficationsAll = async (test_tok, title, body, data) => {
        const message = {
            registration_ids: test_tok,
            notification: {
                title: title,
                body: body,
                vibrate: 1,
                sound: 1,
                show_in_foreground: true,
                priority: "high",
                content_available: true,
                color: '#dc7e27',
            },
            data: {
                link: data
            }
        }

        const headers = {
            "Content-Type": "application/json",
            Authorization: "key=" + my_fir_ser_key,
        }
        let response = await fetch("https://fcm.googleapis.com/fcm/send", {
            method: "POST",
            headers,
            body: JSON.stringify(message),
        }
        ).catch(error => console.log(error)
        )
        response = await response.json()


    }




}
