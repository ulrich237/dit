import axios from 'axios'
import _ from 'underscore'
import React, { Component } from 'react'

export default class Touched extends Component {


  hoursDate(dateHours) {
    var d = new Date(Date.now()), f = new Date(dateHours), diff = {};
    var tmp = d - f;  //Nombre de millisecondes entre les dates

    tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
    diff.seconds = tmp % 60;                    // Extraction du nombre de secondes

    tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
    diff.min = tmp % 60;                    // Extraction du nombre de minutes

    tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)

    return tmp;
  }

  delete_touched = (what, touched, object_id) => {

    var touched_ = []
    for (let i = 0; i < touched.length; i++) {

      if (touched[i].touched.includes(object_id)) {
        touched[i].touched.map(id => {
          id == object_id ? null : touched_.push(id)
        })

        switch (what) {
          case 'house':
            axios.patch("https://imobbis.pythonanywhere.com/wen/touch_house" + touched[i].id, {
              'touched': touched_
            }).catch(error => console.log(error)).then(response => {

            })
              .catch(error => console.log(error))
            break

          case 'landlord':
            axios.patch("https://imobbis.pythonanywhere.com/touched/touchedlandlords" + touched[i].id, {
              'touched': touched_
            }).catch(error => console.log(error)).then(response => {

            })
              .catch(error => console.log(error))

            break
        }
      }

    }
  }


  touched_it(what, object_id, user) {

    var deal_array = []
   
    switch (what) {
      case 'house':
        axios.get(`https://imobbis.pythonanywhere.com/wen/touch_house`,)
          .then(res => {
            var user_in = _.filter(res.data, { user: user })[0]
            this.delete_touched('house', _.filter(res.data, { user: user }), object_id)

            user_in == null ? (
              axios.post("https://imobbis.pythonanywhere.com/wen/touch_house", { 'user': user, 'touched': [object_id] }).then(response => {

              })
                .catch(error => console.log(error))
            ) : (
                this.hoursDate(user_in.timestamp) <= 24 ? (

                  user_in.touched.map(touched_ => deal_array.push(touched_)),
                  !deal_array.includes(object_id) ? deal_array.push(object_id) : null,
                  deal_array.length >= 11 ? deal_array.shift() : null,

                  axios.patch("https://imobbis.pythonanywhere.com/wen/touch_house" + user_in.id, {
                    'touched': deal_array
                  }).catch(error => console.log(error)).then(response => {

                  })
                    .catch(error => console.log(error))

                ) : (
                    axios.post("https://imobbis.pythonanywhere.com/wen/touch_house", { 'user': user, 'touched': [object_id] }).then(response => {

                    })
                      .catch(error => console.log(error))

                  )
              )
          }).catch(error => this.setState({ error, isLoading: false }));

        break

    }
  }


}
