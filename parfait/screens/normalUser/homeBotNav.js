import React, { Component } from 'react'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator, DrawerItem, DrawerContentScrollView } from '@react-navigation/drawer';
import AsyncStorage from '@react-native-async-storage/async-storage'
import UserNavigation from './homeNavigation/navvigation'
import BookingNavigation from './bookNavigation/navigation';
import Liked from './liked'
import LandHome from '../landlord/landHome'
import NavigationLand from '../landlord/NavigationLand'
import RentHome from '../renter/rentHome'
import axios from 'axios'
import AgentHome from '../agent/agentHome'
import _ from 'underscore';
import Draw from './Draw';
import MeNavigation from './meNavigation/navigation';
import NavigationA from './abonnement/navigationA'
import AgentNavigation from '../agent/navigation';
import NavigationMessage from './navigationMessage';
import SpecialisteNavigation from '../specialiste/navigation';
import MessagesI from './Messages';
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default class HomeBottomTabs extends Component {
    constructor(props) {
        super(props)
        this.state = {
            who: null, users: {}
        }
    }
    componentDidMount() {

        this.displayData()

        axios.get("https://imobbis.pythonanywhere.com/user/").then(res => {
            this.setState({ users: _.find(res.data, { id: this.state.who }) })
        }).catch(err => console.log(err));

    }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ who: json_id })
        } catch { error => alert(error) };
    }


    render() {
        const { who, users } = this.state

        //activeBackgroundColor: '#7c3325', 
        return (
            <Drawer.Navigator initialRouteName="imobbis"
               //defaultStatus="open"
                screenOptions={{
                    drawerStyle: {
                        backgroundColor: '#c6cbef',
                        width: "70%",
                    }
                }}
                drawerStyle={{ width: '80%' }}
                drawerContent={props => <Draw {...props} />}
            >
                <Drawer.Screen name="imobbis" component={UserNavigation}
                    options={{ drawerLabel: 'Accueil',headerShown: false }}
                />
                <Drawer.Screen name="specialiste" component={SpecialisteNavigation}
                    options={{ drawerLabel: 'Nos spécialistes', headerShown: false }}
                />
                <Drawer.Screen name="NavigationMessage" component={NavigationMessage}
                    options={{ drawerLabel: 'Mes messages', headerShown: false }}
                />
        
                <Drawer.Screen name="locataire" component={RentHome}
                    options={{ drawerLabel: 'Locataire', headerShown: false }}
                />

                <Drawer.Screen name="agent" component={AgentNavigation}
                    options={{ drawerLabel: 'Agent', headerShown: false }}
                />

                <Drawer.Screen name="landhome" component={NavigationLand}
                    options={{ drawerLabel: 'Bailleur', headerShown: false }}
                />
                <Drawer.Screen name="liked" component={Liked}
                    options={{ drawerLabel: 'Mes favoris', headerShown: false }}
                />
                <Drawer.Screen name="booking" component={BookingNavigation}
                    options={{ drawerLabel: 'Réservation', headerShown: false }}
                />
                <Drawer.Screen name="me" component={MeNavigation}
                    options={{ drawerLabel: 'Mon compte', headerShown: false }}
                />
                 <Drawer.Screen name="NavigationA" component={NavigationA}
                    options={{ drawerLabel: 'Abonnement', headerShown: false }}
                />
            </Drawer.Navigator>
        );
    }
}