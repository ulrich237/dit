import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Houses from './homeNavigation/houses'
import Home from './home';
import DetailUser from './DetailUser';
import AglaProfile from '../profiles/aglaProfile';
import HouseDetails from './homeNavigation/houseDetails';
import Messages from './Messages';
import MessagesIni from './Messages_in';
import PostI from './PostI';




const Stack = createStackNavigator();

export default class NavigationMessage extends Component {
    constructor(props){
        super(props)
    }

    //<Stack.Screen name='Profil' component={Profil} options={{headerShown: false}}/>
    
    render(){
        return(
        <Stack.Navigator>
           
            <Stack.Screen name='Messages' component={Messages} options={{headerShown: false}}/>
            <Stack.Screen name='MessagesIni' component={MessagesIni} options={{headerShown: false}}/>
            <Stack.Screen name='home' component={Home} options={{headerShown: false}}/>
            <Stack.Screen name='houses' component={Houses} options={{headerShown: false}}/>
            <Stack.Screen name='AglaProfile' component={AglaProfile} options={{headerShown: false}}/>
            <Stack.Screen name='HouseDetails' component={HouseDetails} options={{headerShown: false}}/>
            <Stack.Screen name='PostI' component={PostI} options={{headerShown: false}}/>
          

        </Stack.Navigator>
        )
    }
    
}
