import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  Modal,
  View,
  Dimensions,
} from 'react-native';
import _ from 'underscore';
import axios from 'axios';
import { Avatar } from 'react-native-paper'
import Fontisto from "react-native-vector-icons/Fontisto";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Badge, Divider } from 'react-native-elements';
import moment from 'moment';


var width_ = Math.round(Dimensions.get('window').width)
export default class Draw extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}, user_id: null, senrec: [], data: {}
    }
  }


  componentDidMount() {
    this.displayData()

    axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/"+ this.state.user_id).then(resu => {
      this.setState({user:resu.data})
    }).catch(err => console.log(err));


  }


  componentDidUpdate(prevProps, prevState, snapshot) {

    if (prevState.senrec !== this.state.senrec) {
      axios.get('https://imobbis.pythonanywhere.com/new/messageListeUser/' + this.state.user_id)
        .then(response => {
         /* try {
            AsyncStorage.setItem('messages', JSON.stringify(response.data));
          } catch { error => alert(error) }*/
          this.setState({
            senrec: response.data,
            isLoading: false,
          })

        })
        .catch(error => this.setState({ error, isLoading: false }));
    }

    if(prevState.user !== this.state.user){
      axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/"+ this.state.user_id).then(resu => {
        this.setState({user:resu.data})
      }).catch(err => console.log(err));
    }


  }


  You_received = () => {
    const { senrec } = this.state
    return _.size(_.filter(senrec, { user_receiver: this.state.user_id, view: false }))
  }

  displayData = async () => {
    try {

      let user = await AsyncStorage.getItem('user_data');
      let json_id = JSON.parse(user)

      axios.get('https://imobbis.pythonanywhere.com/new/messageListeUser/' + json_id)
        .then(response => {
          try {
            AsyncStorage.setItem('messages', JSON.stringify(response.data));
          } catch { error => alert(error) }
          this.setState({
            senrec: response.data,
            isLoading: false,
          })
        })
        .catch(error => this.setState({ error, isLoading: false }));

      let use_ = await AsyncStorage.getItem('myuser');
      let json_ = JSON.parse(use_)

      this.setState({
        user_id: json_id, user: json_,
      })

    } catch { error => alert(error) };
  }


  render() {
    const { user, senrec, data } = this.state;

  

    return (
      <View style={{ felex: 1 }}>
        <View style={{
          backgroundColor: '#d97d2a', marginLeft: -10, marginTop: -200,
          //transform: [{ rotate: '-12deg' }], 
          height: '50%', width: '100%',
          borderRadius: 10, borderBottomRightRadius: 100,
          //borderBottomRightRadius: 85, borderBottomLeftRadius: 150 
          //borderTopStartRadius: 0, borderTopEndRadius: 0,
        }}>

        </View>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('me')
          }}>
          <View style={{ marginTop: -140, marginBottom: '25%', flexDirection: 'row', marginLeft: '10%' }}>
            <View style={{ width: '30%' }} >
              <Avatar.Image rounded='10' source={{ uri: user?.profile_image }} size={90} />
            </View>
            <View style={{ width: '60%', marginLeft: '10%', marginTop: '5%' }}>
              <Text numberOfLines={1} style={{ fontSize: 20, fontWeight: 'bold', color: 'black' }}>{user?.username}</Text>
              <Text numberOfLines={1} style={{ fontWeight: 'bold', fontSize: 18, color: '#7c3225', marginTop: '1%' }} >
                {
                  user?.typ_user?.toLowerCase() == 'agent' ? user?.typ_user + ' immobilier' : user?.typ_user}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', }}
          onPress={() => {
            this.props.navigation.navigate('imobbis')
          }}>
          <Entypo name='home' size={30} color='#7c3225' />
          <Text style={{ marginLeft: '4%', fontSize: 20 }}>Accueil</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
          onPress={() => {
            this.props.navigation.navigate('specialiste')
          }}>
          <Entypo name='users' size={30} color='#7c3225' />
          <Text style={{ marginLeft: '4%', fontSize: 20 }}>Nos spécialistes</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
          onPress={() => {
            user?.date_experiration == null || user?.typ_user?.toLowerCase() == 'client' || moment(user?.date_experiration) >= moment(new Date()) ?
              this.props.navigation.navigate('NavigationMessage')
              : this.props.navigation.navigate('NavigationA')
          }}>
          <Entypo name='message' size={30} color={user?.typ_user?.toLowerCase() == 'client' || moment(user?.date_experiration) >= moment(new Date()) ? '#7c3225' : '#8e8e8e'} />
          <Text style={{
            'color': user?.date_experiration == null || user?.typ_user?.toLowerCase() == 'bailleur' || moment(user?.date_experiration) >= moment(new Date()) ? 'black' : '#8e8e8e',
            marginLeft: '4%', fontSize: 20
          }}>
            Mes réservations</Text>
          <Text style={{ marginTop: 5, marginLeft: 10 }}>
            {
              _.size(_.filter(senrec, { user_receiver: this.state.user_id, view: false })) != 0 ?
                <Badge
                  status="error"
                  value={_.size(_.filter(senrec, { user_receiver: this.state.user_id, view: false }))}
                  size={20}
                  containerStyle={{}}
                />
                : null
            }
          </Text>
        </TouchableOpacity>

        {user?.typ_user?.toLowerCase() == 'bailleur' ||
          user?.typ_user?.toLowerCase() == 'agence immobiliere' ||
          user?.typ_user?.toLowerCase() == 'agent' ?
          <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
            onPress={() => {
              user?.typ_user?.toLowerCase() == 'bailleur' ? this.props.navigation.navigate('landhome')
                : user?.typ_user?.toLowerCase() == 'agence immobiliere' || user?.typ_user?.toLowerCase() == 'agent' ?
                  this.props.navigation.navigate('agent') : null
            }}>
            <Entypo name='user' size={30} color='#7c3225' />
            <Text style={{ marginLeft: '4%', fontSize: 20 }}>Gcel immobilier</Text>
          </TouchableOpacity> : null
        }

        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '12%', marginTop: '5%' }}
          onPress={() => {
            this.props.navigation.navigate('liked')
          }}>
          <Fontisto name='favorite' size={30} color='#7c3225' />
          <Text style={{ marginLeft: '6%', fontSize: 20 }}>Mes Favoris</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
          onPress={() => {
            this.props.navigation.navigate('booking')
          }}>
          <Entypo name='save' size={30} color='#7c3225' />
          <Text style={{ marginLeft: '4%', fontSize: 20 }}>Mes fréquentations</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
          onPress={() => {
            this.props.navigation.navigate('me')
          }}>
          <MaterialCommunityIcons name='account-cog' size={30} color='#7c3225' />
          <Text style={{ marginLeft: '4%', fontSize: 20 }}>Mon Compte</Text>
        </TouchableOpacity>

        {
          user?.typ_user?.toLowerCase() != 'client' ?
            <TouchableOpacity style={{ flexDirection: 'row', marginLeft: '10%', marginTop: '5%' }}
              onPress={() => {
                this.props.navigation.navigate('NavigationA')
              }}>
              <Entypo name='colours' size={30} color='#7c3225' />
              <Text style={{ marginLeft: '4%', fontSize: 20 }}>Abonnement</Text>
            </TouchableOpacity>
            : null
        }

        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity style={{
            flexDirection: 'row',
            alignItems: 'center', justifyContent: 'center',
            marginTop: '10%', height: "40%", width: "80%", borderRadius: 10
          }}
            onPress={() => {

            }}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20, color: 'white' }}>Déconnexion</Text>
          </TouchableOpacity>
        </View>


      </View>
    );
  }
}



