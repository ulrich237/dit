import React, {Component} from 'react'
import {Text, View, StyleSheet, Dimensions, Image, ScrollView, TouchableOpacity} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class BookList extends Component{
    constructor(props){
        super(props)
        this.state = {
            
        }
    }

    getDaysFromDays = (date1, date2) => {
        var end_date = new Date(date2?.substring(0, 10).replace('-', '/').replace('-', '/'))
        var current_date = new Date(date1?.substring(0, 10).replace('-', '/').replace('-', '/'))
        var time_left = end_date.getTime() - current_date.getTime();
        var days_left = time_left / (1000 * 3600 * 24);
        return days_left
    }

    render(){
        const {city, houses, bookings} = this.props.route.params
        return(
            <View style={{height: height_, backgroundColor: 'white', paddingHorizontal: 9, paddingBottom: 120}}>
                <View style={{flexDirection: 'row',}}>
                    <TouchableOpacity style={{marginLeft: -10, width: '14%'}}  onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before'  size={45} style={{marginTop: -5}}/>
                    </TouchableOpacity>
                </View>
                <ScrollView>
                    <Text style={{fontWeight: '500', fontSize: 25, marginBottom: 15}}>
                        Vos reservations de <Text style={{color: 'orange'}}>{city}</Text>
                    </Text>
                    {
                        houses.map(hr => {return(
                            bookings.map(bk => {return(
                                hr.id == bk.house_book ? 
                                    <View style={{marginVertical: 8, flexDirection:"row", borderBottomWidth: 2,
                                         borderBottomColor: '#e6e6e6', paddingBottom: 20}}
                                    >
                                        <Image source={{uri: hr.photo_1}} style={{height: 85, width: '25%', borderRadius: 8}}/>
                                        <View style={{marginLeft: 10}}>
                                            <Text style={{fontWeight: 'bold', fontSize: 20}}>{hr.name}</Text>
                                            <Text style={{color: 'grey', fontSize: 15}}>
                                                De {bk.checkin_time?.substring(0, 10)} a {bk.checkout_time?.substring(0, 10)}
                                            </Text>
                                            <View style={{flexDirection: 'row'}}>
                                                <View style={{width: '50%'}}>
                                                    <Text style={{color: 'grey', fontSize: 15, marginVertical: 2}}>
                                                    Complete: {this.getDaysFromDays(bk.checkin_time, bk.checkout_time)} jours
                                                    </Text>
                                                    <Text style={{color: 'red', fontSize: 14, fontWeight: 'bold'}}>
                                                        {this.getDaysFromDays(bk.checkin_time, bk.checkout_time)*hr} XAF
                                                    </Text>
                                                </View>
                                                <TouchableOpacity style={{...styles.btn}}>
                                                    <Text style={{fontSize: 18, fontWeight: '500'}}>Contact</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                : null
                            )})
                        )})
                    }
                </ScrollView>
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    btn: {
        padding: 5, borderColor: '#05375a', borderWidth: 2, borderRadius: 7, height: 35, width: '35%',
        alignItems: 'center', justifyContent: 'center', marginTop: 5, 
    }
})