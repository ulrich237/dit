import React, {Component} from 'react'
import {Text, View, StyleSheet, Dimensions, Image, TouchableOpacity} from 'react-native'
import axios from 'axios';
import _ from 'underscore';
import AsyncStorage from '@react-native-async-storage/async-storage';
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);
var user_id = 7
var url = 'https://imobbis.pythonanywhere.com'
export default class Booking extends Component{
    constructor(props){
        super(props)
        this.state = {
            bookings: [], cities: [], houses: [], user_id:null
        }
    }

    componentDidMount(){
        this.displayData()
        axios.get('https://imobbis.pythonanywhere.com/house/house_booking')
        .then(res => this.setState({bookings: _.filter(res.data, {user:this.state.user_id})}))

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
        .then(res => {this.setState({cities: res.data.city })});

        axios.get("https://imobbis.pythonanywhere.com/house/")
        .then(res => this.setState({houses: res.data}))
    }

    displayData = async () => {
        try{
            let user = await  AsyncStorage.getItem('user_data');  
            let json_id = JSON.parse(user)
            
            this.setState({user_id: json_id})
        } catch{error => alert(error)};
    }

    render(){
        const {bookings, cities, houses} = this.state
        var houses_ = []
        var booked_house = []
        var cities_ = []

        bookings.map(book => houses_.push(book.house_book))
        houses_.map(hous => houses.map(hou => hous == hou.id ? booked_house.push(hou) : null ))
        booked_house.map(bo_ho => cities.map(cit => bo_ho.city == cit.id ? cities_.push(cit): null))
        return(
            <View style={{height: height_, backgroundColor: 'white', padding: 8}}>
                <Text style={{fontWeight: '500', fontSize: 25, marginVertical: 7}}>Vous étiez déjà ici</Text>
                <Text style={{color: 'orange', fontSize: 20, marginTop: 5}}>{_.size(_.uniq(cities_))} villes, {_.size(bookings)} reservations</Text>
                <View style={{borderBottomColor: 'orange', borderBottomWidth: 2.5, marginVertical: 7, marginBottom: 20}}/>
                {
                    _.uniq(cities_).map(city_ =>{return(
                        book = _.size(_.filter(booked_house, {city: city_.id })),
                        book_ = _.size(_.filter(_.uniq(booked_house), {city: city_.id })),
                        <TouchableOpacity style={{margin: 10, flexDirection: 'row'}}
                            onPress={() => this.props.navigation.navigate('BookList', 
                            {city: city_.name, houses: _.filter(_.uniq(booked_house), {city: city_.id }), bookings: bookings})}
                        >
                            <Image style={{height: 85, width: 90, borderRadius: 10}} source={{uri: url+city_.image}}/>
                            <View style={{marginLeft: 10}}>
                                <Text style={{fontWeight: '500', fontSize: 22}}>{city_.name}</Text>
                                <Text style={{color: 'grey', fontSize: 19, marginTop: 5}}>{book} reservations</Text>
                                <Text style={{color: 'grey', fontSize: 19, marginTop: 5}}>{book_} maisons</Text>
                            </View>
                        </TouchableOpacity>
                    )
                    })
                }
            </View>
        )
    }
    
}
