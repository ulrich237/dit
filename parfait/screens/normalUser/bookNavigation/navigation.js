import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Booking from './booking';
import BookList from './bookList';

const Stack = createStackNavigator();

export default class BookingNavigation extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
        <Stack.Navigator>
            <Stack.Screen name='booking' component={Booking} options={{headerShown: false}}/>
            <Stack.Screen name='BookList' component={BookList} options={{headerShown: false}}/>
        </Stack.Navigator>
        )
    }
    
}
