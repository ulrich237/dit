import React, { Component } from "react";
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform, Pressable,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Easing
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import { Avatar, Divider } from "react-native-elements";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from 'react-native-vector-icons/AntDesign'
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import NumericInput from 'react-native-numeric-input'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import axios from 'axios'
import _ from 'underscore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import call from 'react-native-phone-call';
import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';
//import ZoomImage from 'react-native-zoom-image';
import ImageViewer from 'react-native-image-zoom-viewer';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);



export default class DetailUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            house: null,
            categorys: [],
            users: [],
            outil_houses: [],
            countrys: [], neighbours: [],
            provinces: [],
            citys: [],
            likes: [],
            reviews: [],
            nbre_personne: 0,
            modalVisible2: false,
            selectedStartDate: null,
            selectedEndDate: null,
            dates_occupers: [],
            searchelt: '',
            houses: [],
            userid: null,
            houseid: Number(this.props.route.params.data),
            imageshouses: [], activeSlide: 1, etat: false, images_: []
        }
    }

    componentDidMount() {
        this.displayData()  //https://imobbis.pythonanywhere.com/new/hous


        axios.get(`https://imobbis.pythonanywhere.com/new/hous`,)
            .then(response => this.setState({ houses: _.filter(response.data, { user: this.state.userid }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: _.filter(response.data, { house: this.state.houseid }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(resct => {
            this.setState({ categorys: resct.data })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            this.setState({ users: resu.data })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    countrys: res.data.country,
                    citys: res.data.city
                })
            });

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }



    calling = (args) =>{
        call(args).catch(console.error)
  }


    pagination(entries) {
        const { activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'white' }}
                dotStyle={{
                    width: 5,
                    height: 5,
                    borderRadius: 2,
                    marginHorizontal: 2,
                    backgroundColor: '#dc7e27'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.4}
            />
        );
    }



    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ userid: json_id })
        } catch { error => alert(error) };
    }






    temps_restant = (debut) => {

        let take = "";

        let d = new Date(Date.now()); let f = new Date(debut); let diff = {};
        let tmp = d - f;  //Nombre de millisecondes entre les dates
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        diff.days = tmp;

        let seconds = 0;
        if ((diff.days == 0) && (diff.seconds >= 0) && (diff.seconds < 60)) {
            seconds = diff.seconds
            take = " moins d'une minute"
        }

        if ((diff.days == 0) && (diff.seconds >= 60) && (diff.seconds < 3600)) {
            var minutes = Math.floor(diff.seconds / 60)
            if (minutes == 1) {
                take = minutes + " minute"
            }
            else {
                take = minutes + " minutes"
            }
        }

        if ((diff.days == 0) && (diff.seconds >= 3600) && (diff.seconds < 86400)) {
            var hours = Math.floor(diff.seconds / 3600)
            if (hours == 1) {
                take = hours + " heure"
            }
            else {
                take = hours + " heures"
            }
        }

        //1 day to 30 days
        if ((diff.days >= 1) && (diff.days < 30)) {
            var days = diff.days
            if (days == 1) {
                take = days + " jour"
            }
            else {
                take = days + " jours"
            }
        }

        if (diff.days >= 30 && diff.days < 365) {
            var months = Math.floor(diff.days / 30)
            if (months == 1) {
                take = months + " mois"
            }
            else {
                take = months + " mois"
            }
        }

        if (diff.days >= 365) {
            var years = Math.floor(diff.days / 365)
            if (years == 1) {
                take = years + " an"
            }
            else {
                take = years + " ans"
            }
        }

        return take;
    }




    render() {
        const { imageshouses,
            users, userid, houses, outil_houses, countrys,
            citys,
        } = this.state;

        const { data } = this.props.route.params

        console.log(data)
        var image = []
        imageshouses.map(ima_ => image.push({ url: ima_.image, props: {} })
        )

        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        <View>
                            <FastImage
                                source={{ uri: data?.profile_image }}
                                style={styles.images}
                                enableScaling={false}
                                easingFunc={Easing.ease}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', bottom: height_ * 0.37, }} >
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                                style={{ ...styles.heart, margin: 5, marginTop: 15 }}>
                                <Entypo name='cross' size={32} />
                            </TouchableOpacity>

                        </View>

                        <View style={{}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: -65, marginLeft: 5 }}>
                                {data?.username} {data?.typ_user?.toLowerCase() == 'agent' ? data?.typ_user + ' immobilier' : data?.typ_user}
                            </Text>
                            <Text style={{color: '#dc7e27', fontSize: 11,  marginTop: -60,}}>
                                membre depuis {this.temps_restant(data?.date_joined)}
                            </Text>
                        </View>
                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ flexDirection: "row", margin: 6,}} >
                            <View style={{ }}>

                                <Text style={{ fontWeight: "bold", color: 'red', fontSize: 18, textAlign: 'center' }}>
                                    A déjà fait {_.size(houses)} publication(s) </Text>
                            </View>

                        </View >
                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ bottom: 15, marginLeft: 5, marginRight: '10%', marginTop: 10 }}>
                            <Text style={{ fontWeight: '500', fontSize: 20, marginBottom: 5 }}>Description</Text>
                            <Text style={{ color: 'grey' }} >"{data?.descriptions}"</Text>
                        </View>



                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                       

                        {
                            data?.is_specialiste ?
                                <View style={{ bottom: 12, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10 }}>
                                     <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: "500", fontSize: 20 }} >Spécialiste</Text>
                                    <View style={{ bottom: 15, marginLeft: 5, marginRight: '10%', marginTop: 10 }}>
                                        <Text style={{ fontWeight: '500', fontSize: 20, marginBottom: 5 }}>Description</Text>
                                        <Text style={{ color: 'grey' }} >"{data?.description_specialiste}"</Text>
                                    </View>

                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>

                                    </View>
                                </View> : null

                        }




                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ marginLeft: 5, marginBottom: 10, marginTop: 7 }}>
                            <Text style={{ fontSize: 16, fontWeight: "500" }} >Vit à {_.find(citys, { id: data?.citie })?.name + ", " + _.find(countrys, { id: data?.countrie })?.name}</Text>
                        </View>
                    </View>
                    <View style={{ height: 70 }} ></View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={{
                        borderRadius: 10, textAlign: "center", marginRight: 10,
                        backgroundColor: 'red', alignItems: "center"
                        , flexDirection: 'row', justifyContent: "center",
                        shadowColor: "black",
                        shadowOffset: {
                            width: 0, height: 6,
                        }, paddingBottom: 10, paddingTop: 5,
                        shadowOpacity: 0.2, width: "28%", height: 40,
                    }}
                        onPress={() => this.calling(
                            {
                                number:'+' + _.find(countrys, { id: data?.countrie })?.indicatif?.toString() + data?.phone_number.toString(),
                                prompt:false
                            }
                        )}
                    >
                        <Text style={{ color: "black", fontWeight: "bold", fontSize: 15 }}>Contacter</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    footer: {
        //padding: 5,
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'center',
        bottom: -10, backgroundColor: 'white',
        height: 60,
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: Platform.OS === 'ios' ? 10 : 5,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    modalView: {
        backgroundColor: "white",
        borderTopRightRadius: 15, borderTopLeftRadius: 15,
        padding: 8, marginTop: height_ * 0.63,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '100%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    textStyle: {
        color: "#010102",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    buttonstyle1: {
        width: '30%',
        borderRadius: 7,
        backgroundColor: 'white',
        borderWidth: 2,
        // borderColor: '#dc7227',
        textAlign: 'center',
        height: 30,
        justifyContent: 'center'
    },
    buttonstyle: {
        borderRadius: 10, textAlign: "center",
        backgroundColor: 'orange', alignItems: "center"
        , flexDirection: 'row', justifyContent: "center",
        shadowColor: "black",
        shadowOffset: {
            width: 0, height: 6,
        }, paddingBottom: 10, paddingTop: 5,
        shadowOpacity: 0.2, width: "27%", height: 35,
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 24,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    images: {
        width: width_, height: height_ * 0.5,
        borderColor: "white", borderWidth: 0.5,
    }
})

//  https://fireship.io/lessons/pwa-to-play-store/




