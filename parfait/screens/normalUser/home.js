import React, { Component, Suspense, PureComponent } from "react";
import {
    View, FlatList, Text, Picker, TouchableOpacity, Alert, Platform, ActivityIndicator, Pressable,
    TextInput, SafeAreaView, Linking, Button, Modal, ScrollView, StyleSheet, Dimensions, Share, Image
} from "react-native";
import { Avatar, } from 'react-native-paper';
import Entypo from 'react-native-vector-icons/Entypo';
import { Divider, } from 'react-native-elements'
import axios from 'axios'
import Icon from "react-native-vector-icons/FontAwesome";
//import VersionNumber from 'react-native-version-number';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import _ from 'lodash';
import messaging from '@react-native-firebase/messaging';


import AsyncStorage from '@react-native-async-storage/async-storage';

import FontAwesome from "react-native-vector-icons/FontAwesome";
import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Touched from '../../functions/touched';
import AnimatedEllipsis from 'react-native-animated-ellipsis';

import firebase from '@react-native-firebase/app';

import Notification from '../../functions/notification';
import Date_heure from '../../functions/date_heure';
import PostData from '../../functions/postData';
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';
import moment from 'moment';

import call from 'react-native-phone-call';




var width_ = Math.round(Dimensions.get('window').width)
var height_ = Math.round(Dimensions.get('window').height)

var dat = {}


var test_tok =
    'enOV5MJ7TyqVetq3CYI5ow:APA91bFq-vwAXDmGr9qJTVOJcVfhkZ3_dYc2HDfE_PxJEXZoO6rOEFJuv7w_X8jXPmFHg2NA1UVnECHGxlzCnC61T7_Wcre3Bg4cxH-L2tHmlgiKMy3YRNQpdml2EgmSehc8gqI1Y471';



export default class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            cats: [], houses: [], user: [], imageshouses: [], userid: null,
            neighbors: [], citys: [], likes: [], modiflike: false, comment: [],
            scrollPosition: 0, isLoading1: false, charge: false, failed: [],
            pubId: [], mes_likes: [], all_likes: [], countrys: [], isloadin: false,
            activeSlide: 0, isLoading_: false, currentPage: 1, refresh: false, seed: 1,
            url: null, partage: {}, allHouses: [], prestataire: [], user_: {}
        }


    }

    generateLink = async (param, value, url, user, token) => {
        var lien = null
        //value?.lien == null || value?.lien == '' ?
        link_ = await dynamicLinks()
            .buildShortLink(
                {
                    link: 'https://imobbis.page.link/message/' + value?.id,
                    domainUriPrefix: 'https://imobbis.page.link',
                    android: {
                        packageName: 'com.imobbis.app',
                        minimumVersion: '23'
                        //version minmale ici
                        //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                    },
                    /*ios: {
                         bundleId: AppConfig.getIOSBundle(),
                         appStoreId: AppConfig.getIOSAppId(),
                     }, */
                    social: {
                        title: value?.category + ' à ' + value?.publish_type,
                        descriptionText: value?.description,
                        imageUrl: url
                    },
                    navigation: {
                        forcedRedirectEnabled: true,
                    }
                },
                //firebase.dynamicLinks.ShortLinkType.SHORT
            )
            .then((link) => {
                //DynamicLinkStore.currentUserProfileLink = link;
                console.log(link)
                console.log(value?.id)

                lien = link.slice(link.lastIndexOf('/') + 1)
                console.log(lien)

                new PostData().partageur(value?.partageurs, user, value?.id)
                new Notification().sendNotitfications(token, this.state.user_?.username,
                    'A partagé votre publication.', 'hotboy://message/' + value?.id)

                this.onShare(link)

                /* lien ?
                     axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + value?.id, {
                         'lien': lien
                     }).catch(error => console.log(error)).then(response => {
                         this.onShare(link),
                             new PostData().partageur(value?.partageurs, user, value?.id)

                     })
                         .catch(error => console.log(error))
                     : null*/
            })
            .catch((err) => {
                console.log("0000000")
                console.log(err);
                //DynamicLinkStore.profileLinkLoading = false;
            })
        //  : (new PostData().partageur(value?.partageurs, user, value?.id), this.onShare('https://imobbis.page.link/' + value?.lien))

    }



    calling = (args) =>{
        call(args).catch(console.error)
  }





    componentDidMount() {

        this.displayData()

        this.requestUserPermission()

        this.display()


        axios.get(`https://imobbis.pythonanywhere.com/house/cat`,)
            .then(response => {
                this.setState({ cats: response.data })
            })
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/review`,)
            .then(response => this.setState({ comment: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/house4`,)
            .then(response => this.setState({ allHouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            var data = resu.data.filter((value) => value.typ_user.toLowerCase() != 'client'.toLowerCase())
            this.setState({ user: resu.data, prestataire: data })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => { //https://imobbis.pythonanywhere.com/location/
                this.setState({
                    citys: res.data.city,
                    countrys: res.data.country
                })
            });

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.comment !== this.state.comment) {
            axios.get(`https://imobbis.pythonanywhere.com/new/review`,)
                .then(response => this.setState({ comment: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
        }

    }

    refresh_depart = () => {
        let houses = []
        axios.get(`https://imobbis.pythonanywhere.com/new/sitedBra`,)
            .then(response => {
                houses = response.data
                this.setState({ houses: houses, refresh: false })
            })
            .catch(error => this.setState({ error, refresh: false }));
    }




    requestUserPermission = async () => {
        messaging()
            .hasPermission()
            .then((enabled) => {
                if (enabled) {
                    messaging()
                        .getToken()
                        .then((fcmToken) => {
                            if (fcmToken) {
                                axios
                                    .patch('https://imobbis.pythonanywhere.com/new/utilisateur/' + this.state.userid, {
                                        tokens: fcmToken,
                                    })
                                    .catch((error) => console.log("error"));
                            } else {

                            }
                        });
                } else {
                    firebase
                        .messaging()
                        .requestPermission()
                        .then(() => {
                            messaging()
                                .getToken()
                                .then((fcmToken) => {
                                    if (fcmToken) {
                                        axios
                                            .patch(
                                                'https://imobbis.pythonanywhere.com/new/utilisateur/' + this.state.userid,
                                                {
                                                    tokens: fcmToken,
                                                },
                                            )
                                            .catch((error) => console.log(error));
                                    } else {

                                    }
                                });
                        })
                        .catch((error) => { });
                }
            });
    }





    displayData = async () => {

        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user);
            this.actua(json_id)
            this.getLike(json_id)
            this.setState({ userid: JSON.parse(user), })
            var url_ = await AsyncStorage.getItem('url_pub');
            var json_url = JSON.parse(url_);
            this.pageGet(json_url)

        } catch { error => alert(error) };
    }


    display = async () => {

        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user);

            let users = await AsyncStorage.getItem('myuser');

            let hous = await AsyncStorage.getItem('house');

            this.setState({ //houses: JSON.parse(hous), 
                user_: JSON.parse(users)
            })

            this.versionAt(JSON.parse(user))

        } catch { error => alert(error) };
    }


    getLike(id) {
        axios.get(`https://imobbis.pythonanywhere.com/new/like`)
            .then(res => {
                let mes_likes = []
                let da = []
                res.data.map(us => da.push({ user: us.user, house: us.house, number: _.size(_.filter(res.data, { user: us.user, house: us.house })) }))
                _.filter(res.data, { user: id }).map(id_ => mes_likes.push(id_.house))
                this.setState({ likes: res.data, mes_likes: mes_likes, all_likes: da })
            });
    }



    renderLoader = () => {
        return (
            this.state.isLoading_ ?
                <View style={{ height: 170, backgroundColor: '#d0d0d0' }}>
                    <ActivityIndicator size="large" color="#7c3325" />
                    <Text style={{ textAlign: 'center', fontWeight: "bold", color: "#dc7e27" }}>
                        Chargement des publications...
                    </Text>
                </View> : null
        );
    };



    loadMoreItem = () => {
        this.setState({ currentPage: _.size(this.state.houses) > 35 ? 1 : this.state.currentPage + 1, }
            , () => { this.actua(this.state.userid) })
    };


    handleRefesh = () => {
        this.setState({ seed: this.state.seed, refresh: true, currentPage: 1 }
            , () => { this.refresh_depart() })
    };


    actua = (userid) => {
        const { currentPage } = this.state
        axios.get(`https://imobbis.pythonanywhere.com/new/recomm/${userid}`,)
            .then(response => {
                var data = this.state.houses.concat(response.data)
                this.setState({
                    houses: currentPage === 1 || _.size(data) > 35 ? response.data : data,
                    isLoading_: true
                })
            })
            .catch(error => this.setState({ error, isLoading: false }));
    }

    postLien = (user, data) => {
        //   axios.get("https://imobbis.pythonanywhere.com/new/messagin").then((reslh) => {
        //    _.find(reslh.data, { user_receiver: user?.id, house: data?.id, user_sender: this.state.userid }) ? null :
        axios.post("https://imobbis.pythonanywhere.com/new/messagin",
            {
                'user_receiver': user?.id,
                'user_sender': this.state.userid,
                'house': data?.id,
                'view': false,
            })
            .then(response => {
                new Notification().sendNotitfications(user?.tokens, this.state.user_?.username,
                    'Vous a envoyé un message pour resérvation.', 'hotboy://reservation')
            })
            .catch(error => alert(error))
        // }).catch(err => console.log(err));
    }



    save_like = (homeid, user, prop) => {
        let mes_likes = this.state.mes_likes;
        let exist = _.find(this.state.likes, { user: user, house: homeid }) ? true : false;

        exist == true ? (mes_likes = _.filter(this.state.likes, (value) => value.house != homeid))
            : (mes_likes.push(homeid))
        this.setState({ mes_likes: mes_likes });
        let item = exist == false ? {
            house: Number(homeid),
            user: Number(user),
        }
            : { id: _.find(this.state.likes, { user: user, house: homeid })?.id };
        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/like`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/like/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                    .then(res => {
                        exist == false ? new Notification().sendNotitfications(prop.tokens, this.state.user_?.username, 'A aimé votre publication.', 'hotboy://message/' + homeid) : null
                        let mes_likes = []
                        _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.house))
                        this.setState({ likes: res.data, mes_likes: mes_likes })
                    });

            }).catch(error => {
                console.log("error " + error)
            })
    }


    monnaie = (a) => {
        return a.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    }


    onShare = async (data) => {
        try {
            const result = await Share.share({
                message: data,
            });


            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    pagination(entries) {
        const { activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'white' }}
                dotStyle={{
                    width: 5,
                    height: 5,
                    borderRadius: 5,
                    marginHorizontal: 4,
                    backgroundColor: '#dc7e27'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
            //inactiveDotOpacity={0.4}
            //inactiveDotScale={0.4}
            />
        );
    }


    firstCarousel = ({ item, index }) => {
        return (
            <View
                activeOpacity={0.8}
                enabled={false}
            >
                <FastImage
                    style={styles.images
                    }
                    source={{
                        uri: item.image_url || item.image,
                    }}
                />
            </View>

        );
    }

    pageGet = (url_) => {
        var dat_ = this.state.houses
        var lien = null
        lien = url_.slice(url_.lastIndexOf('/') + 1)

        lien ?
            (
                axios.get(`https://imobbis.pythonanywhere.com/new/houseLien2/${lien}`)
                    .then(resp => {
                        dat_.unshift(resp?.data)
                        this.setState({
                            houses: dat_,
                            partage: resp.data

                        })
                        this.removeUrl()
                    })
                    .catch(error => this.setState({ error, refresh: false }))
            )
            : null
    }



    removeUrl = async () => {
        try {
            await AsyncStorage.removeItem('url_pub');
        } catch { error => alert(error) };
    }



    /*versionAt = (id) => {
        axios.get('https://imobbis.pythonanywhere.com/bsend/versionUser/' + id,)
            .then(response => {
                // cette version declenche un evenement dans le serveur, la version 2.5

                response.data?.version == "2.5" || response.data?.version_ios == "2.5" ? null :
                    (
                        axios
                            .patch('https://imobbis.pythonanywhere.com/bsend/version/' + response.data?.id, {
                                "version": Platform.OS === 'ios' ? null : VersionNumber.appVersion,
                                "version_ios": Platform.OS === 'ios' ? VersionNumber.appVersion : null,
                            })
                            .then(res => {

                            }).catch(err => {

                            })
                    )
            }
            )
            .catch(error => {

                if (error.toString() == 'Error: Request failed with status code 500') {

                    axios.post('https://imobbis.pythonanywhere.com/bsend/version',
                        {
                            "version": Platform.OS === 'ios' ? null : VersionNumber.appVersion.toString(),
                            "version_ios": Platform.OS === 'ios' ? VersionNumber.appVersion.toString() : null,
                            "user": id

                        })
                        .then(res => {

                        }).catch(err => {
                            console.log(err)
                        })



                        .catch(error => alert(error))

                }
            });

    }*/

    ima(data) {
        var d = []

        data?.map(im => d.push({ source: { uri: im?.image_url } }))

        return d
    }




    render() {
        const { cats, houses, user, userid
            , comment, allHouses } = this.state;

        let user_; let imageshouse_;



        return (

            <SafeAreaView style={{ backgroundColor: 'white' }}>
                <View>
                    <View style={{
                        width: '100%', justifyContent: 'space-between', flexDirection:
                            'row', backgroundColor: '#dc7e27'
                    }}>
                        <View style={{ paddingLeft: 7 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                                <TouchableOpacity style={{ marginLeft: 20, marginTop: 14 }}
                                    onPress={() => this.props.navigation.openDrawer()}
                                >
                                    <FontAwesome name='list' color='black' size={20} />
                                </TouchableOpacity>
                                <Text style={{
                                    marginLeft: 50, color: 'black',
                                    marginTop: 10, fontSize: 20, fontWeight: 'bold'
                                }}>IMOBBIS</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{
                            marginTop: 10, marginRight: 15
                        }}
                            onPress={() => this.props.navigation.navigate('filter_Feed', { data: allHouses, prestataire: this.state.prestataire })}
                        >
                            <FontAwesome name='search' color='black' size={20} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 10, }}>
                        {
                            cats.map(cat => {
                                return (
                                    <TouchableOpacity style={{ marginVertical: 5, width: width_ / 5, alignItems: 'center' }}
                                        onPress={() => this.props.navigation.navigate('houses', { 'cat': cat })}
                                    >
                                        <Ionicons name={cat.icon} color="grey" size={19} />
                                        <Text numberOfLines={1} style={{ color: 'grey', fontWeight: 'bold', marginTop: 5 }}>{cat.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </ScrollView>
                </View>
                {
                    <FlatList
                        data={houses}
                        numColumns={1}
                        renderItem={({ item, index, separators }) => (
                            user_ = item.user,
                            imageshouse_ = item.image,
                            <View>
                                <View>
                                    <View style={{ flexDirection: 'row', justifyContent: item?.disponible ? "space-between" : 'center' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center' }}>
                                                {item?.category?.toUpperCase()}
                                                {!item?.colocation ? " À " + item?.publish_type?.toUpperCase() : ' EN COLLOCATION'}</Text>
                                        </View>
                                        {item?.disponible ?
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='warning' color="red" size={20} style={{ marginTop: 2 }} />
                                                <Text style={{ fontSize: 15, fontWeight: "bold", textAlign: 'center', color: 'red' }}>
                                                    Indisponible
                                                </Text>

                                            </View>
                                            : null
                                        }
                                    </View>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('AglaProfile', {
                                            agla: item.user,
                                            countrie: item.user?.countrie,
                                            houses: _.filter(allHouses, (value) => value?.user?.id == item?.user?.id),
                                            content_ct: 15
                                        })
                                    }}>
                                    <View style={{
                                        padding: 10, flexDirection: "row", margin: '4%',
                                        justifyContent: "space-between", alignItem: "center",
                                    }}>
                                        <View style={{ flexDirection: "row", width: width_ * 0.6, }}>
                                            <Avatar.Image
                                                size={49}
                                                rounded
                                                avatarStyle={{ backgroundColor: 'orange' }}
                                                source={{ uri: user_?.profile_url }} />
                                            <View style={{ marginLeft: '5%', }}>
                                                <Text numberOfLines={1} style={{ fontWeight: "bold", }}>{user_?.username}</Text>
                                                <Text style={{ fontSize: 12 }}>
                                                    {user_?.typ_user?.toLowerCase() == 'agent' ? user_?.typ_user + ' immobilier' : user_?.typ_user}
                                                </Text>
                                            </View>
                                        </View >
                                        <Text style={{ fontSize: 11, }}>{new Date_heure().getDate_publication(item.date_created)} </Text>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ marginLeft: '3%', }}>
                                    <Text>{item.description}</Text>
                                </View>

                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    enabled={false}
                                    onPress={() => {
                                        new Touched().touched_it('house', item?.id, this.state.userid),
                                            this.props.navigation.navigate('HouseDetails',
                                                {
                                                    'id': item.id, 'userid': this.state.userid, 'data': item,
                                                    'imageshouses': item.image,
                                                })
                                    }}>
                                    <Carousel
                                        ref={c => {
                                            this.numberCarousel = c;
                                        }}
                                        //styleName="large-square"
                                        data={item.image}
                                        renderItem={this.firstCarousel}
                                        sliderWidth={width_}
                                        itemWidth={width_}
                                    //style={styles.images}
                                    />
                                    {this.pagination(item.image)}
                                </TouchableOpacity>
                                <View style={{ marginBottom: '3%', marginLeft: '2%' }}>
                                    <View style={{ flexDirection: "row", margin: '4%', justifyContent: "space-between", marginTop: '0%' }}>
                                        <View style={{ flexDirection: "row", width: '60%' }}>
                                            <Icon name='map-marker' color="grey" size={19} />
                                            <Text numberOfLines={1} style={{ fontSize: 13, marginLeft: 10, color: 'grey' }}>
                                                {item.neighbor}
                                                {item.city?.name ? ", " + item.city?.name : null} </Text>
                                        </View>
                                        <Text style={{ fontSize: 13, color: "red" }}>
                                            {item.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}
                                            {' ' + item.country?.monnaie}
                                            {item?.category?.toLowerCase() == "terrain" ? ' / m²' : item.publish_type == 'vendre' ? '' : item.is_immeuble == true ? ' / jour' : ' / mois'}</Text>
                                    </View>
                                    <View style={{ marginTop: '1%', margin: '5%', flexDirection: 'row', justifyContent: "space-between" }}>
                                        <TouchableOpacity
                                            onPress={() => this.save_like(item.id, userid, user_)} //lineHeight: 32,
                                            style={{ ...styles.heart }}>
                                            <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                <Icon style={{ top: 8, color: "#7c3325" }} size={22} name={this.state.mes_likes.includes(item.id) == false ? 'heart-o' : 'heart'} />
                                                {
                                                    _.filter(this.state.likes, { house: item.id }).length !== 0 ?
                                                        <Text style={{
                                                            fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                            borderRadius: 100, color: "black"
                                                        }}>{_.filter(this.state.likes, { house: item.id }).length} </Text>
                                                        : null
                                                }
                                            </View>
                                            <Text style={{ marginRight: 5, fontSize: 8, fontWeight: 'bold' }}>Aimer</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{ ...styles.heart }}
                                            onPress={() => {
                                                new PostData().reservation(item.reserveurs, userid, item.id),
                                                    user_?.date_experiration == null || moment(user_?.date_experiration) >= moment(new Date()) ?
                                                        this.calling(
                                                            {
                                                                number:'+' + item?.user?.countrie?.indicatif?.toString() + item.user?.phone_number?.toString(),
                                                                prompt:false
                                                            }
                                                        )
                                                        : new Notification().sendNotitfications(user_?.tokens, this.state.user_?.username, 'A essayé de vous joindre, veuillez payer votre abonnement.', 'hotboy://message/' + item?.id)
                                            }
                                            }
                                        >
                                            <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                <Icon style={{ top: 8, }} name='phone' size={22} color='grey' />
                                                {
                                                    item.nb_reserveur !== 0 ?
                                                        <Text style={{
                                                            fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                            borderRadius: 100, color: "black"
                                                        }}>{item.nb_reserveur} </Text>
                                                        : null
                                                }
                                            </View>
                                            <Text style={{
                                                color: user_?.date_experiration == null || moment(user_?.date_experiration) >= moment(new Date()) ? '' : '#8e8e8e',
                                                marginRight: 5, fontSize: 8, fontWeight: 'bold'
                                            }}>
                                                Appeler</Text>
                                        </TouchableOpacity>
                                        {
                                            item?.user.id !== userid ?
                                                <TouchableOpacity
                                                    style={{ ...styles.heart }}
                                                    onPress={() => {
                                                        this.postLien(item.user, item)
                                                        this.props.navigation.navigate(
                                                            'MessagesIni', {
                                                            data: item,
                                                            user: item.user
                                                        })
                                                    }}>
                                                    <Entypo name='message' color='grey' size={22} />
                                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>{item?.publish_type == 'louer' ? ' Réserver ' : 'Acheter'}</Text>
                                                </TouchableOpacity>
                                                : <View></View>
                                        }
                                        <TouchableOpacity
                                            style={{ ...styles.heart }}
                                            onPress={() => {
                                                this.props.navigation.navigate(
                                                    'PostI', {
                                                    child_id: item.id,
                                                    pers: item,
                                                    user_: item.user,
                                                }
                                                )
                                            }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='wechat' size={22} color='grey' />
                                                {
                                                    _.filter(comment, { house: item.id }).length !== 0 ?
                                                        <Text style={{
                                                            fontSize: 10, left: 3, top: 7, fontWeight: 'bold',
                                                            borderRadius: 100, color: "black"
                                                        }}> {_.filter(comment, { house: item.id }).length} </Text>
                                                        : null
                                                }
                                            </View>
                                            <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Commenter</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{ ...styles.heart }}
                                            onPress={() => { this.generateLink('id', item, _.first(item.image)?.image_url, userid, item?.user?.tokens) }
                                            }
                                        >
                                            <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                <Entypo style={{ top: 8 }} name='forward' color='grey' size={22} />
                                                {
                                                    item.nb_partageur !== 0 ?
                                                        <Text style={{
                                                            fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                            borderRadius: 100, color: "black"
                                                        }}>{item.nb_partageur} </Text>
                                                        : null
                                                }
                                            </View>
                                            <Text style={{ marginRight: 5, fontSize: 8, fontWeight: 'bold' }}>Partager</Text>
                                        </TouchableOpacity>

                                    </View>
                                    {
                                        item?.category?.toLowerCase() !== "terrain" && item?.category !== "" && item?.category?.toLowerCase() !== "boutique"
                                            && item?.category?.toLowerCase() !== "local administratif" && item?.category?.toLowerCase() !== "bureau"
                                            && item?.category?.toLowerCase() !== "autre" ?
                                            <>
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Icon name='bed' color='grey' size={20} />
                                                        <Text style={{ fontSize: 12, color: 'grey' }}>{item.nb_room} Chambres </Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                        <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{item.nb_toilet} douche(s)</Text>
                                                    </View>
                                                    {item.nb_parlour != 0 ?
                                                        <View style={{ flexDirection: "row" }}>
                                                            <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                            <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{item.nb_parlour} salon(s) </Text>
                                                        </View> : null
                                                    }
                                                </View>
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: 'space-between' }}>
                                                    {item.nb_kitchen != 0 ?
                                                        <View style={{ flexDirection: "row" }}>
                                                            <MaterialIcons name='kitchen' color='grey' size={20} style={{ marginLeft: '2%' }} />
                                                            <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{item.nb_kitchen} Cuisine(s)</Text>
                                                        </View> : null
                                                    }
                                                    {item.fence == true ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon name='slideshare' color='grey' size={20} />
                                                            <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>Dans la barrière</Text>
                                                        </View>) : null
                                                    }
                                                    {item.garden == true ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon name='pied-piper-alt' color='grey' size={20} />
                                                            <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>jardin</Text>
                                                        </View>) : null
                                                    }
                                                    {item.parking == true ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon name='car' color='grey' size={20} />
                                                            <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>parking</Text>
                                                        </View>) : null
                                                    }

                                                </View>
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: 'space-between' }}>

                                                </View>
                                            </> :
                                            item?.category?.toLowerCase() == "terrain" ?
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: "row", justifyContent: 'center' }}>
                                                        <Icon name='stop' color='grey' size={20} />
                                                        <Text style={{ fontSize: 13, color: 'grey', textAlign: 'center' }}>Dimension: {item.dimension} mètres carrés </Text>
                                                    </View>


                                                </View> : null
                                    }
                                </View>
                                <Divider style={styles.divider} />
                            </View >
                        )
                        }
                        ListFooterComponent={this.renderLoader}
                        onEndReached={this.loadMoreItem}
                        onEndReachedThreshold={0.5}
                        keyExtractor={item => item.id}
                        ListEmptyComponent={
                            <View style={{ alignItems: 'center', alignSelf: 'center', }}>
                                <ActivityIndicator style={{ marginTop: '15%' }} size="large" color="#7c3325" />
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: "35%" }}>
                                    <Text style={{ marginTop: 30 }}>Chargement </Text>
                                    <AnimatedEllipsis numberOfDots={3}
                                        style={{
                                            color: '#7c3325',
                                            fontSize: 50,
                                        }}
                                    />
                                </View>

                            </View>}
                        refreshing={this.state.refresh}
                        //onEndThreshold={5}
                        initialNumToRender={10}
                        onRefresh={this.handleRefesh}
                    />

                }

                {
                    /*isLoading == true ?
                        <View style={{ alignItems: 'center', marginLeft: '5%', marginBottom: '10%' }}>
                            <Text style={{ fontWeight: "bold", color: "#dc7e27" }}>Chargement des publications ... </Text>
                        </View> :
                        <TouchableOpacity style={{ borderColor: '#dc7e27', borderWidth: 2, marginBottom: '5%', marginTop: '2%', marginLeft: '10%', marginRight: '10%' }}
                            onPress={() => (isLoading = true, this.refresh())}
                        >
                            <Text style={{ fontSize: 13, textAlign: "center", color: "#dc7e27" }}>voir publications </Text>
                        </TouchableOpacity> ici*/
                }
                {
                    /*  isLoading1 == true ?
                          <View style={{ alignItems: 'center', marginLeft: '5%', marginBottom: '10%' }}>
                              <Text style={{ fontWeight: "bold", color: "#dc7e27" }}>Chargement des publications ... </Text>
                          </View> :
                          <TouchableOpacity style={{ borderColor: '#dc7e27', borderWidth: 2, marginBottom: '5%', marginTop: '2%', marginLeft: '10%', marginRight: '10%' }}
                              onPress={() => (isLoading1 = true, this.refresh())}
                          >
                              <Text numberOfLines={1} style={{ fontSize: 13, textAlign: "center", color: "#dc7e27" }}>Veuillez être plus actif... </Text>
                          </TouchableOpacity>
                          
                           ici*/
                }

            </SafeAreaView >




        )

    }


}

const styles = StyleSheet.create({
    heart: {
        //textAlign: 'right',
        alignItems: 'center',
        backgroundColor: "#fff6e6", // width: '20%' ,
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    espace: { height: 100 },
    divider: {
        backgroundColor: 'grey',
        height: 15,
        marginTop: '3%',
        width: width_
    },
    preloader: {
        //margintop: '50%',
        //bottom: 0,
        //position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    images: {
        width: width_, height: height_ * 0.6,
        borderColor: "white", borderWidth: 0.5,
    }
})