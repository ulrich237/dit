import axios from 'axios'
import _ from 'lodash'
import React, { Component } from 'react'
import { Avatar } from 'react-native-paper'
import {
    View, Text, ScrollView, Image, StyleSheet, Dimensions, Modal, ActivityIndicator,
    TextInput, KeyboardAvoidingView, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native'
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
//import ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { Badge, Divider } from 'react-native-elements';
import AutogrowInput from 'react-native-autogrow-input';
import AsyncStorage from '@react-native-async-storage/async-storage'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { isNull } from 'underscore'
import FastImage from 'react-native-fast-image';
import Textarea from 'react-native-textarea';
import Notification from '../../functions/notification';

import Date_heure from '../../functions/date_heure';
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

var dat = 'hotboy://reservation'

export default class MessagesIni extends Component {


    _isMounted = false;

    constructor(props) {
        super(props)
        this.listRef = React.createRef();
        this.state = {
            Details: [],
            filePath: '',
            content: '',
            senrecs: [], tutors: [],
            agent: [], user: {},
            landlord: [], user_id: null,
            envoyer: true, post: false,
            house: [], carts: [],
            foods: [], users: [], photo: {},
            size_col_qty: [], company: [],
            openImage: '', modalImage: false, imageshouses: [],
            ancienSenrec: [], reservation: [], save: false, getHous: false,
            getMes: false, user_: {}

        }
    }
    //restomenue

    componentDidMount() {
        this._isMounted = true;

        this.dataFetcher()
        this.displayData()

        this.WillFocusSubscription = this.props.navigation.addListener(
            'focus',
            () => {
                this.dataFetcher();
            }
        );

        const { user, data } = this.props.route.params

        axios.get(`https://imobbis.pythonanywhere.com/new/hous`,)
            .then(response =>
                this.setState({
                    house: response.data,
                    isLoading: false, getHous: true
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));


        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({
                imageshouses: _.filter(response.data, { house: data?.id }),
                imageshouses_: response.data
            }))
            .catch(error => this.setState({ error, isLoading: false }));


        axios.get(`https://imobbis.pythonanywhere.com/house/house_booking`,)
            .then(response =>
                this.setState({
                    reservation: response.data,
                    isLoading: false,
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));


    }

    componentWillUnmount() {

        this._isMounted = false;
        this.WillFocusSubscription() ?
            this.WillFocusSubscription.remove()
            : null

    }



    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.senrecs !== this.state.senrecs) {
            this.dataFetcher()
        }

    }


    displayData = async () => {

        try {

            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            let users = await AsyncStorage.getItem('myuser');

            this.setState({ user_id: json_id, user_: JSON.parse(users) })

            axios.get('https://imobbis.pythonanywhere.com/new/messageListe4/' + json_id + "/" + this.props.route.params?.user?.id,)
                .then(response => {
                    this.setState({
                        ancienSenrec: response.data,
                        getMes: true, senrecs: response.data
                    })

                })
                .catch(error => this.setState({ error, isLoading: false }));


        } catch { error => alert(error) };
    }


    view = () => {

        const { senrecs } = this.state
        // je recupere les messages qu'on m'a envoyé et que jai pas lu je signale alors que j'ai vue 
        _.filter(senrecs, (value) => (value?.user_receiver.id == this.state.user_id && value?.view == false)).map(senrec_ => {
            axios.patch("https://imobbis.pythonanywhere.com/new/messagin/" + senrec_.id, {
                'view': true
            }).catch(error => { }).then(response => {

            })
                .catch(error => { })
        })
    }

    recently = (val) => {
        var tab = _.filter(val, (num) => (
            num.view == false && num.user_receiver == this.state.user_id))
        return tab

    }

    dataFetcher = () => {
        axios.get('https://imobbis.pythonanywhere.com/new/messageListe4/' + this.state.user_id + "/" + this.props.route.params?.user?.id,)
            .then(response => {
                this.setState({
                    senrecs: response.data
                })

            })
            .catch(error => this.setState({ error, isLoading: false }));
    }

    handlerChoosePhoto = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
        }).then(images => {


            this.setState({ photo: images, post: true })
        });

    }

    postLien = () => {

        const { photo } = this.state
        const { user, data } = this.props.route.params

        var data_ = this.state.senrecs

        var et = {
            "context": null,
            "time_sent": new Date(),
            "image": null,
            "view": false,
            "user_sender": this.state.user_,
            "user_receiver": user,
            "house": data,
        }


        data_.push(et)

        this.setState({ senrecs: data_ })

        this.setState({ envoyer: false, save: false })

        axios.post("https://imobbis.pythonanywhere.com/new/messagin",
            {
                'user_receiver': user,
                'user_sender': this.state.user_,
                'house': data?.id,
                'view': false,
            })
            .then(response => {
                new Notification().sendNotitfications(user?.tokens,
                    this.state.user_?.username,
                    'Vous a envoyé un message pour resérvation.', dat)
                //this.setState({ envoyer: false, save: false })
            })
            .catch(error => alert(error))

    }



    postImage = () => {
        var data = this.state.senrecs

        const { photo } = this.state
        const { user } = this.props.route.params
        const form = new FormData();
        var photos = _.toArray(photo)

        var et = {
            "context": this.state.content,
            "time_sent": new Date(),
            "image": photo?.path,
            "view": false,
            "user_sender": this.state.user_,
            "user_receiver": user,
            "house": null
        }

        data.push(et)
        this.setState({ senrecs: data })


        this.setState({ post: false, save: false, content: '', photo: {} })

        form.append('user_receiver', user?.id);
        form.append('user_sender', this.state.user_id);
        form.append('view', false);
        form.append('context', this.state.content);

        for (let i = 0; i < photos.length; i++) {
            form.append('image', {
                name: photo.filename,
                type: photo.mime,
                uri: Platform.OS === 'android' ? photo.path : photo.path,
            }
            );
        }
       
        fetch('https://imobbis.pythonanywhere.com/new/messagin', {
            method: 'post',
            headers: {
                'Content-type': 'multipart/form-data',
            },
            body: form
        })
            .then(res => {
                new Notification().sendNotitfications(
                    user?.tokens,
                    this.state.user_?.username,
                    'Vous a envoyé un message: ' + this.state.content, dat)

                this.setState({ post: false, save: false, content: '', photo: {} })

            }).catch(err => {
                console.log(err)
            })
    }




    post = (content) => {
        const { photo } = this.state
        const { user } = this.props.route.params

        var data = this.state.senrecs

        var et = {
            "context": content,
            "time_sent": new Date(),
            "image": null,
            "view": false,
            "user_sender": this.state?.user_,
            "user_receiver": user,
            "house": null

        }

        data.push(et)

        this.setState({ senrecs: data, post: true, save: true, })

        this.setState({ post: false, save: false, content: '', })
        axios.post('https://imobbis.pythonanywhere.com/new/messagin', {
            'user_receiver': user?.id,
            'user_sender': this.state.user_id,
            'context': content,
            'view': false,

        })
            .then(res => {
                new Notification().sendNotitfications(
                    user?.tokens,
                    this.state.user_?.username,
                    'Vous a envoyé un message: ' + content, 'hotboy://reservation')

                // this.setState({ post: false, save: false, content: '', })

            }).catch(err => {
                console.log(err)
            })
    }





    render() {
        const { senrecs, envoyer, reservation, getMes,
            photo, user_id, house, imageshouses_, getHous, } = this.state
        const { data, id, target_ct, order_, user, sms } = this.props.route.params
        var envoyer_ = envoyer

        this.view()
        this.props.route.params?.data == undefined && this.props.route.params?.order_ == undefined ? envoyer_ = false : null

        var notif = _.differenceWith(this.state.senrecs, this.state.ancienSenrec, _.isEqual)
        notif = this.recently(notif)


        return (
            <View style={{ flex: 1, }}>
                <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#dc7e27' }}>
                    <TouchableOpacity style={{ marginLeft: -10, width: '14%' }} onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before' size={45} style={{ marginTop: 3 }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: 'row', width: '80%', alignItems: 'center', marginTop: 3 }}
                        onPress={() => { getHous ? this.props.navigation.navigate('AglaProfile', { 'agla': user, 'countrie': user?.countrie, 'houses': _.filter(house, { user: user?.id }) }) : null }}>
                        <Avatar.Image size={55} source={{ uri: user?.profile_url || user?.profile_image }} />
                        <Text style={{ textAlign: 'center', fontWeight: "bold", }}> {user?.username}  ({user?.typ_user?.toLowerCase() == 'agent' ? user?.typ_user + ' immobilier' : user?.typ_user}) </Text>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.post}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <TouchableOpacity style={{ flexDirection: 'row', marginTop:50}} onPress={() => this.setState({ post: false })}>
                            <FontAwesome style={{ marginLeft: '90%', }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <ScrollView>

                            <FastImage
                                style={{
                                    width: width_, height: height_ * 0.8
                                }}
                                source={{
                                    uri: photo?.path,
                                    headers: { Authorization: 'someAuthToken' },
                                    priority: FastImage.priority.normal,
                                    // cache:FastImage.cacheControl.cacheOnly
                                }}
                            //resizeMode={FastImage.resizeMode.cover}
                            />
                        </ScrollView>
                    </View>
                    <KeyboardAvoidingView style={styles.footer}>
                        <View style={styles.input_}>
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={styles.textarea}
                                autoFocus={true}
                                value={this.state.content}
                                onChangeText={(val) => { this.setState({ content: val }) }}
                                placeholder={'Taper un message'}
                                placeholderTextColor={'#c7c7c7'}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                        <Entypo size={35} style={{ margin: 3, marginLeft: 10, marginTop: 25 }} name='image' color='#7c3325'
                            onPress={() => this.handlerChoosePhoto()}
                        />
                        {
                            //this.state.content != '' ?
                            <TouchableOpacity
                                style={{ marginLeft: 4, marginTop: 21 }}
                                onPress={() => !this.state.save ? this.postImage() : null}>
                                <Ionicons size={30} name='send-sharp' color={!this.state.save || this.state.content == '' ? '#7c3325' : '#dcca8a'} style={{ marginTop: 6 }}

                                />
                            </TouchableOpacity>
                            // : null
                        }
                    </KeyboardAvoidingView>

                </Modal>
                {
                    // click from image
                }
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.modalImage}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <TouchableOpacity style={{ flexDirection: 'row', marginTop:50}}
                            onPress={() => {
                                this.setState({ modalImage: false, openImage: '' })
                            }}>
                            <FontAwesome style={{ marginLeft: '90%', }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <FastImage
                            style={{
                                width: width_, height: height_ + 50,
                            }}
                            source={{ uri: this.state.openImage }}
                        //resizeMode={FastImage.resizeMode.cover}
                        />

                    </View>
                </Modal>

                <ScrollView style={{ marginBottom: 80 }}
                    ref={ref => { this.scrollView = ref }}
                    onContentSizeChange={() => this.scrollView.scrollToEnd({ animated: false })}
                >
                    <View>
                        {
                            !getMes ?
                                <View style={{ alignItems: 'center', alignSelf: 'center', }}>
                                    <ActivityIndicator style={{ marginTop: '15%' }} size="large" color="#7c3325" />
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: "35%" }}>
                                        <Text style={{ marginTop: 30 }}>Chargement </Text>
                                        <AnimatedEllipsis numberOfDots={3}
                                            style={{
                                                color: '#7c3325',
                                                fontSize: 50,
                                            }}
                                        />
                                    </View>

                                </View> :
                                senrecs.map(senrece => {
                                    return (
                                        <View style={{ flexDirection: senrece.user_sender?.id == this.state.user_id ? 'row-reverse' : 'row' }}>

                                            {
                                                senrece.user_sender?.id != this.state.user_id ?

                                                    <View>
                                                        <Avatar.Image size={45} source={{ uri: user?.profile_url }} />
                                                    </View>

                                                    : null
                                            }
                                            <View style={senrece.user_sender?.id == this.state.user_id ? styles.store_row : styles.store_row4}>
                                                {
                                                    <View>
                                                        {senrece.context != null || senrece.image != null ?
                                                            <View>
                                                                <Text style={{ fontSize: 14 }}>{senrece.context}</Text>
                                                                {senrece.image != null ?
                                                                    <TouchableOpacity
                                                                        onPress={() => {
                                                                            this.setState({ modalImage: true, openImage: senrece.image })
                                                                        }}>

                                                                        <FastImage
                                                                            source={{ uri: senrece.image }}
                                                                            style={{ height: 110, resizeMode: 'stretch', width: 100 }}
                                                                        />

                                                                    </TouchableOpacity> : null
                                                                }
                                                            </View> : senrece.house != null ?
                                                                <View>

                                                                    {// house

                                                                        <TouchableOpacity
                                                                            onPress={() => {
                                                                                this.props.navigation.navigate('HouseDetails', { 'data': senrece?.house, })
                                                                            }}>
                                                                            <View>
                                                                                <View style={{ flexWrap: 'wrap' }}>
                                                                                    <Text style={[styles.row, { textAlign: 'center' }]}>Bien.{senrece?.house.name}</Text>
                                                                                </View>
                                                                                <View style={{ alignItems: 'center', marginTop: 3, }}>

                                                                                    <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                                                                                        {senrece?.house.image.map(images => {
                                                                                            return (

                                                                                                <FastImage
                                                                                                    style={{ height: 40, resizeMode: 'stretch', width: 30, margin: 2 }}
                                                                                                    source={{ uri: images.image_url }}
                                                                                                />
                                                                                            )
                                                                                        })

                                                                                        }
                                                                                    </View>
                                                                                </View>

                                                                                <Text style={{ color: "red", fontSize: 15, fontWeight: '300' }}>{senrece?.house.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                                                                            </View>
                                                                        </TouchableOpacity>

                                                                    }
                                                                </View> : senrece?.bouking != null ?
                                                                    <TouchableOpacity
                                                                        onPress={() => {
                                                                            this.props.navigation.navigate('reservation', { 'data': senrece.content_id })
                                                                        }}>
                                                                        <View style={{ marginTop: 3, }}>
                                                                            <View style={{ flexDirection: "row-reverse", flexWrap: 'wrap' }}>

                                                                                { // reservation
                                                                                    _.filter(reservation, { id: senrece.content_id }).map(order_ => {
                                                                                        return (
                                                                                            <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                                                                                                <View>
                                                                                                    <Text>La reservation de depuis {order_.checkin_time.toLocaleString('en-GB', { timeZone: 'UTC' })}</Text>
                                                                                                </View>
                                                                                                {
                                                                                                    _.filter(house, { id: order_.house_book }).map(lien_ => {
                                                                                                        return (
                                                                                                            <View>
                                                                                                                <View>
                                                                                                                    <View style={{ flexWrap: 'wrap' }}>
                                                                                                                        <Text style={[styles.row, { textAlign: 'center' }]}>Bien.{lien_.name}</Text>
                                                                                                                    </View>

                                                                                                                    <FastImage
                                                                                                                        source={{
                                                                                                                            uri: lien_.photo_1
                                                                                                                        }}
                                                                                                                        style={{ height: 10, resizeMode: 'stretch', width: 10 }}
                                                                                                                    />

                                                                                                                    <Text style={{ color: "red", fontSize: 20, fontWeight: '300' }}>{lien_.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                                                                                                                </View>
                                                                                                            </View>
                                                                                                        )
                                                                                                    })

                                                                                                }
                                                                                                <View style={{ flexDirection: 'row' }}>
                                                                                                    <Text>Mode de payement: {order_.payment_mode}</Text>
                                                                                                </View>
                                                                                            </View>
                                                                                        )
                                                                                    })
                                                                                }

                                                                            </View>
                                                                        </View>
                                                                    </TouchableOpacity>

                                                                    : null
                                                        }
                                                    </View>
                                                }
                                                <View style={{ flexDirection: 'row-reverse', justifyContent: 'space-between' }}>
                                                    {senrece.user_sender?.id == this.state.user_id ?
                                                        <View style={{ flexDirection: 'row-reverse', justifyContent: 'space-between' }}>
                                                            <FontAwesome5 style={{}}
                                                                name='check-double'
                                                                size={10}
                                                                color={!senrece.view ? 'black' : '#dc7e27'}
                                                            />
                                                            <Text style={{ fontSize: 9 }}>{!senrece.view ? 'Pas encore lu' : 'Déjà lu'}</Text>
                                                        </View>
                                                        : null
                                                    }
                                                    <Text style={{ fontSize: 9, }}>{new Date_heure().time(senrece.time_sent)}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    )
                                })
                        }
                    </View>
                    {
                        envoyer_ ? (
                            <View>
                                {
                                    this.props.route.params?.data != undefined ?
                                        <View style={[styles.row3]}>
                                            <View style={{ flexDirection: 'row', justifyContent: "flex-end" }}>
                                                <TouchableOpacity

                                                    onPress={() => {
                                                        this.setState({ envoyer: false })
                                                    }}
                                                >
                                                    <FontAwesome style={{}}
                                                        name='close'
                                                        size={25}
                                                        color='red'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{}}>
                                                <Text style={styles.row}>{data.name}</Text>
                                                <View style={{ alignItems: 'center', marginTop: 3, }}>

                                                    <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                                                        {data?.image.map(images => {
                                                            return (
                                                                <FastImage
                                                                    style={{
                                                                        width: 50, position: 'relative', marginRight: 1,
                                                                        height: 50, borderRadius: 2
                                                                    }} source={{ uri: images.image_url }}
                                                                />
                                                            )
                                                        })
                                                        }
                                                    </View>
                                                </View>
                                                <View style={{
                                                    textAlign: "center", justifyContent: 'center',
                                                    alignItems: "flex-start"
                                                }}>

                                                    <Text style={{ color: "red", fontSize: 20, fontWeight: '300', textAlign: 'center' }}>{data.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF </Text>
                                                    <View style={{ position: 'relative', }}>

                                                        <TouchableOpacity
                                                            style={[styles.singnIn, {
                                                                backgroundColor: 'green',
                                                                borderColor: 'green',
                                                            }]}
                                                            onPress={() => {
                                                                this.postLien();
                                                            }}
                                                        >
                                                            <Text style={[styles.textSign, {
                                                                color: '#fff', fontSize: 17
                                                            }]}> Resérver
                                                            </Text>
                                                        </TouchableOpacity>

                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                        : <View style={[styles.row3]}>
                                            <View style={{ flexDirection: 'row-reverse', justifyContent: "flex-end" }}>
                                                <TouchableOpacity
                                                    style={[styles.singnIn, {

                                                    }]}
                                                    onPress={() => {
                                                        this.setState({ envoyer: false })
                                                    }}
                                                >
                                                    <FontAwesome style={{}}
                                                        name='close'
                                                        size={25}
                                                        color='red'
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{ alignItems: 'center', marginTop: 3, }}>
                                                <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                                                    {
                                                        _.filter(reservation, { id: order_.id }).map(order_ => {
                                                            return (
                                                                <View style={{ flexDirection: "row", flexWrap: 'wrap' }}>
                                                                    <View>
                                                                        <Text>La reservation de depuis {order_.checkin_time.toLocaleString('en-GB', { timeZone: 'UTC' })}</Text>
                                                                    </View>
                                                                    {
                                                                        _.filter(house, { id: order_.house_book }).map(lien_ => {
                                                                            return (
                                                                                <View>
                                                                                    <View>
                                                                                        <View style={{ flexWrap: 'wrap' }}>
                                                                                            <Text style={[styles.row, { textAlign: 'center' }]}>Bien.{lien_.name}</Text>
                                                                                        </View>
                                                                                        <Image
                                                                                            source={{
                                                                                                uri: lien_.photo_1
                                                                                            }}
                                                                                            style={{ height: 10, resizeMode: 'stretch', width: 10 }}
                                                                                        />

                                                                                        <Text style={{ color: "red", fontSize: 20, fontWeight: '300' }}>
                                                                                            {lien_.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                                                                                    </View>
                                                                                </View>
                                                                            )
                                                                        })

                                                                    }
                                                                    <View style={{ flexDirection: 'row' }}>
                                                                        <Text>Mode de payement: {order_.payment_mode}</Text>

                                                                    </View>
                                                                </View>
                                                            )
                                                        })
                                                    }
                                                </View>
                                            </View>
                                            <View style={{ position: 'relative', }}>
                                                <TouchableOpacity
                                                    style={[styles.singnIn, {
                                                        backgroundColor: 'green',
                                                        borderColor: 'green',
                                                    }]}
                                                    onPress={() => {
                                                        this.postLien(this.state.user_id, target_ct, id, 29, order_?.id);
                                                    }}
                                                >
                                                    <Text style={[styles.textSign, {
                                                        color: '#fff', fontSize: 17
                                                    }]}> Envoyer
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                }
                            </View>
                        ) : null
                    }
                    <View style={{ height: 30 }}></View>
                </ScrollView>

                <View style={{ position: "absolute", padding: 7, }}>
                    {
                        _.size(notif) !== 0 ?
                            <Badge
                                status="error"
                                value={_.size(notif)}
                                size={20}
                                containerStyle={{ marginLeft: width_ * 0.85, marginTop: height_ * 0.65 }}
                            />

                            : null
                    }
                </View>

                <KeyboardAvoidingView style={styles.footer}>
                    <View style={styles.input_}>
                        {/*<AutogrowInput
                            defaultHeight={10}
                            style={styles.textInput}
                            multiline={true}
                            numberOfLines={100}
                            placeholder="Taper un message"
                            autoFocus={true}
                            placeholderTextColor="#666666"
                            autoCapitalize='none'
                            value={this.state.content}
                            onChangeText={(val) => this.setState({ content: val })}
                        />*/}

                        <Textarea
                            containerStyle={styles.textareaContainer}
                            style={styles.textarea}
                            autoFocus={true}
                            onChangeText={(val) => { this.setState({ content: val }) }}
                            value={this.state.content}
                            placeholder={'Taper un message'}
                            placeholderTextColor={'black'}
                            underlineColorAndroid={'transparent'}
                        />
                    </View>
                    <Entypo size={35} style={{ margin: 3, marginLeft: 10, marginTop: 25 }} name='image' color='#7c3325'
                        onPress={() => this.handlerChoosePhoto()}
                    />
                    {
                        this.state.content != '' ?
                            <TouchableOpacity
                                style={{ marginLeft: 4, marginTop: 21 }}
                                onPress={() => !this.state.save ? this.post(this.state.content) : null}>
                                <Ionicons size={30} name='send-sharp' color={!this.state.save || this.state.content == '' ? '#7c3325' : '#dcca8a'} style={{ marginTop: 6 }}

                                />
                            </TouchableOpacity>
                            : null
                    }
                </KeyboardAvoidingView>
            </View>
        )
    }


}

const styles = StyleSheet.create({
    store_row4: {
        padding: 10,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5, marginTop: 5,
        borderRadius: 15, margin: 1,
        width: width_ * 2 / 3

    },
    singnIn: {
        width: 150,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 10, borderRadius: 7,// marginTop: '5%'
    },
    row3: {
        backgroundColor: '#fff',
        shadowColor: "black",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.36,
        paddingBottom: 10,
        width: width_ * 0.9,
        margin: 10,
        borderRadius: 10,
    },
    modal: {
        backgroundColor: "white",
        //  borderRadius: 20,
        // padding: 10,
        // shadowColor: "#000",

        width: width_,
        height: height_,
        //marginBottom: height_ * 0.005,
        // marginTop: height_ * 0.12,

    },
    name_user: {
        padding: 5,
        backgroundColor: "#9259c9",
        shadowColor: "#ded1f4",

        elevation: 1, marginTop: 1,
        borderRadius: 15, margin: 10

    },
    store_row: {
        padding: 10,
        backgroundColor: "#919191",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5, marginTop: 5,
        borderRadius: 15, margin: 1,
        width: width_ * 2 / 3
    },
    row: {
        marginBottom: 5,
        borderBottomWidth: 0.8,
        borderBottomColor: '#d9d9d9',
        padding: 4,
    },
    footer: {
        //borderTopColor: '#05375a',
        // padding: 7, borderTopWidth: 0.3,
        position: 'absolute',
        bottom: 10,
        width: '100%',
        height: 80,
        //marginBottom: 20,
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    textInput: {
        // borderWidth: 1, 
        //height: 80,
        backgroundColor: '#F5FCFF',
        //marginBottom: 10,
        //backgroundColor:'red',
        //minHeight: 50, padding: 5,
        color: '#7c3325',
        // borderRadius: 7, borderColor: '#05375a', borderWidth: 1.5,
    },
    input_: {
        //borderWidth: 1, 
        width: '77%', //minHeight: 40,
        backgroundColor: '#F5FCFF',
        //padding: 5,
        // borderRadius: 7, borderColor: '#05375a', borderWidth: 1.5,
    },
    textareaContainer: {
        height: 85,
        //padding: 5,
        backgroundColor: '#F5FCFF',

    },
    textarea: {
        //textAlignVertical: 'top',  // hack android
        height: 80,
        fontSize: 14,
        color: '#7c3325',
    },
})