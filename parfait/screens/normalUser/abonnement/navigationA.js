import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Bsend from './bsend';
import NavigationAbon from './navigationAbon';
import Abonnement from './abonnement'


const Stack = createStackNavigator();

export default class NavigationA extends Component {
    constructor(props){
        super(props)
    }

    //<Stack.Screen name='Profil' component={Profil} options={{headerShown: false}}/>
    
    render(){
        return(
        <Stack.Navigator>
            
            <Stack.Screen name='NavigationAbon' component={NavigationAbon} options={{headerShown: false}}/>
            <Stack.Screen name='Bsend' component={Bsend} options={{headerShown: false}}/>
            <Stack.Screen name='Abonnement' component={Abonnement} options={{headerShown: false}}/>
           
        </Stack.Navigator>
        )
    }
    
}
