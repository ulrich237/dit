import React, { Component } from 'react';
import { Text, Dimensions, SafeAreaView } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Abonnement from './abonnement';
import MesAbonnement from './mesabonnement';
import Feather from 'react-native-vector-icons/Feather'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'


const TopTab = createMaterialTopTabNavigator();

var height_ = Math.round(Dimensions.get('window').height)


export default class NavigationAbon extends Component {
    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#fdf7e8', flex: 1 }}>
                <Text style={{ fontWeight: '700', color: '#7c3325', fontSize: 30, margin: 10 }}>Je m'abonne</Text>
                <TopTab.Navigator
                    tabBarOptions={{
                        activeTintColor: '#7c3325', inactiveTintColor: '#e9deb5',
                        indicatorStyle: { backgroundColor: '#7c3325'}
                    }}
                >
                    <TopTab.Screen name="Abonnement" children={props =>
                        <Abonnement navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Abonné',
                            tabBarIcon: ({ color, size }) => (
                                <Feather name="list" color={color} size={21} />
                            ),
                        }}
                    />
                    <TopTab.Screen name="MesAbonnement" children={props =>
                        <MesAbonnement navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Mes abonnements',
                            tabBarIcon: ({ color, size }) => (
                                <MaterialIcons name="local-mall" color={color} size={26} />
                            ),
                        }}
                    />
                </TopTab.Navigator>
            </SafeAreaView>
        )
    }
}


