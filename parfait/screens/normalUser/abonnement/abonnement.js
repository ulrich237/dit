import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Share, Alert, Linking,
    TouchableOpacity, ScrollView, FlatList, Dimensions,
} from 'react-native'
import _ from 'underscore'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import moment from 'moment';
import { sha256 } from 'react-native-sha256';



var width_ = Math.round(Dimensions.get('window').width)

export default class Abonnement extends Component {

    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: {}, landlords: {},
            user_id: null, abonnement: [], refresh: false, idPack: {},
            country: [],
        }
    }



    componentDidMount() {
        this.displayData()

        axios.get(`https://imobbis.pythonanywhere.com/bsend/abonnement`,)
            .then(response => this.setState({ abonnement: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/location/`,)
            .then(response => this.setState({ country: response.data.country }),)
            .catch(error => this.setState({ error, isLoading: false }));

    }



    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.renters !== this.state.renters) {
            axios.get(`https://imobbis.pythonanywhere.com/bsend/souscription`,)
                .then(response => this.setState({ renters: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
        }
        if (prevState.users !== this.state.users) {
            axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/' + this.state.user_id)
                .then(response => this.setState({ users: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
        }

    }




    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })

    };


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ user_id: json_id })

            axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/' + json_id)
                .then(response => this.setState({ users: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
        } catch { error => alert(error) };

    }

    getDaysFromDays = (dateout) => {
        let d = new Date(dateout); let f = new Date(Date.now()); let diff = {};

        let tmp = d - f;  // uttilise

        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        diff.days = tmp;

        return tmp

    }


    addMonths(date, months) {
        var f = new Date(date)
        var d = f.getDate();
        f.setMonth(f.getMonth() + +months);
        if (f.getDate() != d) {
            f.setDate(0);
        }
        return f;
    }



    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }





    abonn = (idPack) => {

        const { navigate } = this.props

        const { users, country } = this.state

        var pays = _.find(country, { id: users?.countrie });

        var nom = pays?.name?.toLowerCase() == 'cameroon' ? 'CM' :
            pays?.name?.toLowerCase() == 'gabon' ? 'GA' :
                pays?.name?.toLowerCase() == 'togo' ? 'TG' : 'CM'



        Alert.alert(
            'Confirmation',
            'Etes vous sûr de vouloir souscrire au ' + idPack?.nom,
            [
                {
                    text: 'OUI',
                    onPress: () => {
                        // prendre l argent avant de faire la transaction
                        sha256(this.makeid(10) + new Date().getUTCMilliseconds() + new Date().getTime() + users?.phone_number).then(hash => {
                            axios.post("https://bsend-op.com/api/v1.0/payment/control", {
                                "amount": idPack?.prix,
                                "phone": users?.phone_number.toString(),
                                "email": "imobbis.sar@gmail.com",
                                "first_name": users?.username,
                                "cmd": "BS_PAY",
                                "currency": pays?.monnaie,
                                "description": "achat des biens",
                                "langue": "fr",
                                "payment_ref": hash,
                                "public_key": "REF_yk0tED4tuFeV4wuW5QUf",
                                "country": pays?.name?.toLowerCase(),
                                "country_ccode": nom,
                                "country_cdial": pays?.indicatif
                            }).then(response => {
                                response.data.response.toUpperCase() == 'SUCCESS' ?
                                    (
                                        axios.post("https://imobbis.pythonanywhere.com/bsend/souscription",
                                            {
                                                "user": users?.id,
                                                "payment_ref": hash,
                                                "abonnement": idPack?.id,
                                                "nbMois": idPack?.nb_jr_mois
                                            }).then(res => {
                                                navigate('Bsend', { 'item': response.data?.payment_url })

                                                /* axios.patch("https://imobbis.pythonanywhere.com/new/utilisateur/" + users?.id, {
                                                  'date_experiration': this.addMonths(users?.date_experiration, idPack?.nb_jr_mois)
                                                }).then(response => {
                                                
                                                }).catch(error => { console.log(' 2 ' + error) })*/

                                            }).catch(error => { console.log('1 ' + error) })
                                    )
                                    : Alert.alert('Attention', response.data?.message)
                            }).catch(error => { console.log(' 2 ' + error) })
                        })
                    }
                },
                {
                    text: 'NON',
                    onPress: () => console.log('Close Pressed'),
                    style: 'cancel'
                }
            ]
        )

    }

    verif = (data) => {
        var id = []
        moment(this.addMonths(this.state.users?.date_prestataire, 2)) < moment(new Date()) ?
            id = _.filter(data, (value) => value.id != 1)
            : id = data
        return id

    }


    render() {
        const { navigate } = this.props
        const { abonnement, users } = this.state





        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                    <View style={{ marginTop: 20, marginBottom: 10 }}>
                        <View style={styles.view}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                <MaterialIcons style={{ marginTop: -2 }} name={'auto-awesome'} size={25} color='#664200' />
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    <Text style={styles.text}>Votre abonnement prend fin: </Text>
                                    <Text style={{ color: 'red', fontWeight: 'bold' }}>{users?.date_experiration?.split('T')[0]}</Text>
                                </View>
                            </View>
                            <View>
                               
                                <Text style={{textAlign: 'center', fontSize:11}}>{this.getDaysFromDays(users?.date_experiration)} jour(s) restant(s)</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{}}>
                        {
                            _.find(this.verif(abonnement), { id: 1 }) ?
                                <View style={{ marginTop: 10, backgroundColor: "pink", marginBottom: 10 }}>
                                    <View style={{ alignItems: 'center' }}><Text style={{ textAlign: 'center', fontWeight: "bold", margin: 3 }}><AntDesign name={'earth'} color='#664200' size={20} style={{ marginLeft: '2%' }} />  Pack Promo  <AntDesign name={'earth'} color='#664200 ' size={20} style={{ marginLeft: '2%' }} />
                                    </Text></View>
                                    <View style={{ margin: 10, flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center' }}> Sera disponible pendant 02 mois durant la periode d'essai, ainsi se termine le {users?.date_prestataire ? moment(this.addMonths(users?.date_prestataire, 2)).format()?.split('T')[0] : null}</Text>
                                       
                                    </View>
                                    <Text style={{textAlign: 'center', fontSize:11}}>{this.getDaysFromDays(moment(this.addMonths(users?.date_prestataire, 2)).format())} jour(s) restant(s)</Text>
                                </View> : null
                        }
                    </View>
                    {
                        this.verif(abonnement).map(abon => {
                            return (
                                //this.addMonths(users?.date_experiration, 2)
                                <View>
                                    <View style={{
                                        alignItems: 'center', marginTop: 10, borderWidth: 1,
                                        borderColor: 'grey', padding: 4, borderRadius: 10,
                                    }}>
                                        <TouchableOpacity
                                            onPress={() => { this.setState({ idPack: abon }), this.abonn(abon) }} style={{}}>
                                            <Text style={{ textAlign: 'center', fontWeight: "bold", margin: 10 }}>
                                                <AntDesign name={abon.id == 1 ? 'earth' : 'star'} color={abon.id == 1 ? '#664200' : '#dc7e27'} size={20} style={{ marginLeft: '2%' }} />  {abon.nom}  <AntDesign name={abon.id == 1 ? 'earth' : 'star'} color={abon.id == 1 ? '#664200' : '#dc7e27'} size={20} style={{ marginLeft: '2%' }} />
                                            </Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ color: 'red', textAlign: 'center' }}>{abon.prix?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}</Text>
                                                <Text style={{ textAlign: 'center' }}>{' XAF pour ' + abon.nb_jr_mois + ' mois'}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })
                    }
                </ScrollView>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        }, width: '47%', margin: 6,
        shadowOpacity: 0.2,
        shadowRadius: 4,
        padding: 6, elevation: 9,
        borderRadius: 20
    },
    imgContainer: {
        backgroundColor: '#dc7e27',
        //alignItems: 'center',
        justifyContent: 'center', margin: 6,
        height: '30%', borderRadius: 15,
        shadowOffset: {
            width: 0, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8.30,
        shadowColor: '#05375a'
    },
    view: {
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 15,
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        margin: 5
    },
    bt1: {
        backgroundColor: '#7c3325', width: '46%',
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 5, marginTop: 4, marginLeft: 3
    },
    row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
})
