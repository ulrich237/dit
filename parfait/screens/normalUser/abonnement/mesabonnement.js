import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, FlatList, ActivityIndicator,
    TouchableOpacity, ScrollView, Alert, Dimensions,
} from 'react-native'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import AntDesign from "react-native-vector-icons/AntDesign";

import AsyncStorage from '@react-native-async-storage/async-storage';

var width_ = Math.round(Dimensions.get('window').width)


export default class MesAbonnement extends Component {

    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: {}, transac: [], houses: [], user_id: null,
            abonnement: [], gcel: [], touch: [], refresh: false, countrys: [], idPack: {}
        }
    }

    componentDidMount() {
        this.displayData()


        axios.get(`https://imobbis.pythonanywhere.com/bsend/abonnement`,)
            .then(response => this.setState({ abonnement: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

    }


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/' + json_id)
                .then(response => this.setState({ users: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
            this.setState({ user_id: json_id })

            axios.get('https://imobbis.pythonanywhere.com/bsend/transaction_et_Sous/' + json_id,)
                .then(response => this.setState({ renters: response.data.souscription, transac: response.data.transaction }),)
                .catch(error => this.setState({ error, isLoading: false }));

        } catch { error => alert(error) };
    }


    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })
    };


    render() {

        const { countrys, users, transac, idPack, renters, user_id, abonnement, touch } = this.state
        const { navigate } = this.props
        let trans
        let renter_


        return (
            <ScrollView style={{ backgroundColor: 'white' }}>
                <Text style={{ textAlign: "center", marginTop: "3%", fontWeight: 'bold', color: 'grey', fontSize: 12 }}>
                    Vos differents abonnement éffectués.
                </Text>
                <FlatList
                    numColumns={1}
                    data={renters}
                    showsVerticalScrollIndicator={false}
                    onMomentumScrollBegin={() => this.setState({ mom_beg: false })}
                    renderItem={({ item, index, separators }) => (
                        renter_ = _.find(abonnement, { id: item.abonnement }),

                        <View
                            style={{

                                borderWidth: 1, borderColor: 'grey', padding: 4, borderRadius: 10, marginTop: 10
                            }}
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                                <Text style={{ fontSize: 16, color: 'red' }}>{renter_?.nom}</Text>
                                <Text style={{ fontSize: 11, }}>
                                    {item?.date_created.split('T')[0]}</Text>
                            </View>

    
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 5 }}>
                                <Text style={{ fontSize: 11, }}>{renter_?.prix?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ") + ' XAF pour ' + renter_?.nb_jr_mois + ' mois'}</Text>
                                <Text style={{ fontSize: 11, color: '' }}> {_.find(transac, (value) => value.payment_ref == item?.payment_ref) ? " Effectivement payé" : " Non payé "} </Text>
                            </View>
                        </View>



                    )}
                    //ListFooterComponent={this.renderLoader}
                    onEndReachedThreshold={0.7}
                    keyExtractor={item => item.id}
                    //onEndReached={this.itemsToRender2}
                    refreshing={this.state.refresh}
                    onRefresh={this.handleRefesh}
                />
                <View style={{ height: 30 }}></View>
            </ScrollView>
        )
    }

}



const styles = StyleSheet.create({
    row3: {
        marginTop: 2,
        backgroundColor: '#fdf7e8',
        shadowColor: "orange",
        shadowOffset: {
            width: 1,
            height: 1,
        }, marginVertical: 3,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7, elevation: 3,
        marginVertical: 2,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: 80,
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    },
    imgContainer: {
        backgroundColor: '#dc7e27',
        alignItems: 'center',
        justifyContent: 'center', margin: 6,
        height: '40%', borderRadius: 15,
        shadowOffset: {
            width: 0, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8.30,
        shadowColor: '#05375a'
    },
})
