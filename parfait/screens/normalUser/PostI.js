import React from 'react';
import axios from 'axios';
import _ from 'underscore';
import Entypo from 'react-native-vector-icons/Entypo';
import ImagePicker from 'react-native-image-crop-picker';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ModalSelector from 'react-native-modal-selector';
import Icon from "react-native-vector-icons/FontAwesome";
import { Divider } from 'react-native-elements'
import DeviceInfo from 'react-native-device-info'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Form from 'react-native-form';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Button,
    TextInput, Modal,
    Text, Share,
    StatusBar,
     Dimensions, KeyboardAvoidingView

} from 'react-native';
import { Checkbox, Avatar } from 'react-native-paper';
import StepIndicator from 'react-native-step-indicator'
import AutogrowInput from 'react-native-autogrow-input';
import { Rating, AirbnbRating } from 'react-native-ratings';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import Textarea from 'react-native-textarea';
import Notification from '../../functions/notification';
import Date_heure from '../../functions/date_heure';
import PostData from '../../functions/postData';
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';

import call from 'react-native-phone-call';

//import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);
var dat = {}
var test_tok =
    'enOV5MJ7TyqVetq3CYI5ow:APA91bFq-vwAXDmGr9qJTVOJcVfhkZ3_dYc2HDfE_PxJEXZoO6rOEFJuv7w_X8jXPmFHg2NA1UVnECHGxlzCnC61T7_Wcre3Bg4cxH-L2tHmlgiKMy3YRNQpdml2EgmSehc8gqI1Y471';

export default class PostI extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            likes: [], save: false,
            title: '', content: '', score: 0.0, currentPosition: 0, modalI: false,
            photo: {}, user_id: null, imageshouses: [], neighbors: [], citys: [],
            houses: [], comment: [], users: [], focus: true, mes_likes: [],
            all_likes: []
        };

    }

    generateLink = async (param, value, url, user, token) => {
        var lien = null
        // value?.lien == null || value?.lien == '' ?
        link_ = await dynamicLinks()
            .buildShortLink(
                {
                    link: 'https://imobbis.page.link/message/' + value?.id,
                    domainUriPrefix: 'https://imobbis.page.link',
                    android: {
                        packageName: 'com.imobbis.app',
                        minimumVersion: '19'
                        //version minmale ici
                        //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                    },
                    /*ios: {
                         bundleId: AppConfig.getIOSBundle(),
                         appStoreId: AppConfig.getIOSAppId(),
                     }, */
                    social: {
                        title: value?.category + ' à ' + value?.publish_type,
                        descriptionText: value?.description,
                        imageUrl: url
                    }
                    /* navigation: {
                         forcedRedirectEnabled: true,
                     }*/
                },
                //firebase.dynamicLinks.ShortLinkType.SHORT
            )
            .then((link) => {
                //DynamicLinkStore.currentUserProfileLink = link;
                console.log(link)
                console.log(value?.id)

                new PostData().partageur(value?.partageurs, user, value?.id)

                new Notification().sendNotitfications(token, _.find(this.state.users, { id: this.state.user_id })?.username,
                    'A partagé votre publication.', 'hotboy://message/' + value?.id)


                this.onShare(link)
                //lien = link.slice(link.lastIndexOf('/') + 1)
                //console.log(lien)


                /* lien ?
                     axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + value?.id, {
                         'lien': lien
                     }).catch(error => console.log(error)).then(response => {


                     })
                         .catch(error => console.log(error))
                     : null*/

            })

            .catch((err) => {
                console.log(err);
                //DynamicLinkStore.profileLinkLoading = false;
            })
        //  : (this.onShare('https://imobbis.page.link/' + value?.lien))

    }

    componentDidMount() {

        const { pers } = this.props.route.params

        this.displayData()

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            this.setState({ users: resu.data })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/new/review`,)
            .then(response => this.setState({ comment: _.filter(response.data, { house: pers?.id }) }),)
            .catch(error => this.setState({ error, isLoading: false }));



        axios.get(`https://imobbis.pythonanywhere.com/new/like`)
            .then(res => {
                let mes_likes = []
                let da = []
                res.data.map(us => da.push({ user: us.user, house: us.house, number: _.size(_.filter(res.data, { user: us.user, house: us.house })) }))
                _.filter(res.data, { user: this.state.user_id }).map(id_ => mes_likes.push(id_.house))
                this.setState({ likes: res.data, mes_likes: mes_likes, all_likes: da })
            });


    }


    calling = (args) =>{
        call(args).catch(console.error)
  }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        const { pers } = this.props.route.params
        if (prevState.likes !== this.state.likes) {
            axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                .then(res => {
                    this.setState({ likes: res.data })
                });
        }


        if (prevState.comment !== this.state.comment) {
            axios.get(`https://imobbis.pythonanywhere.com/new/review`,)
                .then(response => this.setState({ comment: _.filter(response.data, { house: pers?.id }) }),)
                .catch(error => this.setState({ error, isLoading: false }));
        }

    }


    save_like = (homeid, user, prop) => {
        let mes_likes = this.state.mes_likes;

        let exist = _.find(this.state.likes, { user: user, house: homeid }) ? true : false;

        exist == true ? (mes_likes = _.filter(this.state.likes, (value) => value.house != homeid))
            : (mes_likes.push(homeid))

        this.setState({ mes_likes: mes_likes });

        let item = exist == false ? {
            house: Number(homeid),
            user: Number(user),
        }
            : { id: _.find(this.state.likes, { user: user, house: homeid })?.id };
        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/like`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/like/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                    .then(res => {
                        exist == false ? new Notification().sendNotitfications(prop?.tokens, _.find(this.state.users, { id: this.state.user_id })?.username, 'A aimé votre publication.', 'hotboy://message/' + homeid) : null
                        let mes_likes = []
                        _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.house))
                        this.setState({ likes: res.data, mes_likes: mes_likes })
                    });
                console.log("le post a été bien effectué !")
            }).catch(error => {
                console.log("error " + error)
            })
    }


    onShare = async (data) => {
        try {

            const result = await Share.share({
                message: data
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {

                    // shared with activity type of result.activityType

                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            //alert(error.message);
        }
    };

    addPost = () => {

        const { content, photo, score, } = this.state;
        const { pers, user_ } = this.props.route.params

        var photos = _.toArray(photo)
        const form = new FormData();

        form.append('house', pers?.id);
        form.append('content', content);
        form.append('user', this.state.user_id);
        form.append('note', score);

        

      

        for (let i = 0; i < photos.length; i++) {
            form.append('image', {
                name: photo.filename,
                type: photo.mime,
                uri: Platform.OS === 'android' ? photo.path : photo.path
            }
            );
        }

        console.log(form)
        this.setState({ save: true })
       fetch('https://imobbis.pythonanywhere.com/new/review', {
            method: 'post',
            headers: {
                'Content-type': 'multipart/form-data',
            },
            body:form
        })
            .then(res => {
                console.log('enfin')
                new Notification().sendNotitfications(_.find(this.state.users, { id: user_?.id })?.tokens, _.find(this.state.users, { id: this.state.user_id })?.username, 'A commenté votre publication: ' + content, 'hotboy://message/' + pers?.id)
                this.setState({ modalI: false, content: '', save: false, score: 0, photo: null })
            }).catch(err => {
                console.log(err)
                this.setState({ save: false, photo: null })
            })
    }





    handlerChoosePhoto = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
        }).then(images => {
           
            this.setState({ photo: images, currentPosition: 3, modalI: true })
        });

    }


    onPageChange(position) {
        this.setState({ currentPosition: position });

    }



    render() {
        const { pers, user_, } = this.props.route.params
        const { photo, imageshouses, neighbors, citys, likes, user_id, comment, users, mes_likes } = this.state;



        return (
            <View style={{ flex: 1, }}>
                <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#dc7e27' }}>
                    <TouchableOpacity style={{ marginLeft: -10, width: '14%' }} onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before' size={45} style={{ marginTop: -5 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', width: '80%', alignItems: 'center', marginTop: 3 }}>
                        <Text style={{ fontWeight: "600", fontSize: 12 }}>Publié par </Text>
                        <Text style={{ textAlign: 'center', fontWeight: "bold", }}> {user_?.username}  ({user_?.typ_user?.toLowerCase() == 'agent' ? user_?.typ_user + ' immobilier' : user_?.typ_user})</Text>
                    </View>
                </View>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.modalI}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => this.setState({ modalI: false })}>
                            <FontAwesome style={{ marginLeft: '90%', }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <ScrollView>
                            <Image
                                source={{ uri: photo?.path }}
                                style={{
                                    width: width_, height: height_ * 0.70, marginTop: 10,

                                }} />
                        </ScrollView>
                    </View>


                    <KeyboardAvoidingView style={styles.footer}>
                        <AirbnbRating
                            count={5}
                            reviews={["Terrible", "Bad", "Good", "Very Good", "Unbelievable"]}
                            defaultRating={this.state.score}
                            size={25}
                            ratingCount={this.state.score}
                            startingValue={this.state.score}
                            onFinishRating={score => this.setState({ score: score })}
                        />
                        <View style={{ flexDirection: 'row' }}>
                            <View style={styles.input_}>
                                <Textarea
                                    containerStyle={styles.textareaContainer}
                                    style={styles.textarea}
                                    autoFocus={true}
                                    onChangeText={(val) => { this.setState({ content: val }) }}
                                    value={this.state.content}
                                    placeholder={'Taper un message'}
                                    placeholderTextColor={'black'}
                                    underlineColorAndroid={'transparent'}
                                />
                            </View>
                            <Entypo size={35} style={{ margin: 3, marginLeft: 10, marginTop: 25 }} name='image' color='#7c3325'
                                onPress={() => this.handlerChoosePhoto()}
                            />
                            {//this.content != '' ?
                                <TouchableOpacity
                                    style={{ marginLeft: 8, marginLeft: 4, marginTop: 21 }}
                                    onPress={() => !this.state.save ? this.addPost() : null}>
                                    <Ionicons size={30} name='send-sharp' color={!this.state.save ? '#7c3325' : '#dcca8a'} style={{ marginTop: 6 }}
                                    />
                                </TouchableOpacity>
                                // : null
                            }
                        </View>
                    </KeyboardAvoidingView>
                </Modal>

                <ScrollView style={{}}>

                    <View>
                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center' }}>
                                        {pers?.category?.toUpperCase()} A
                                        {" " + pers?.publish_type?.toUpperCase()}</Text>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity
                        >
                            <View style={{
                                padding: 10, flexDirection: "row",
                                justifyContent: "space-between", alignItem: "center",
                            }}>
                                <View style={{ flexDirection: "row" }} >
                                    <Avatar.Image
                                        size={49}
                                        rounded
                                        avatarStyle={{ backgroundColor: 'orange' }}
                                        source={{ uri: user_?.profile_url }} />

                                    <View style={{ marginLeft: '5%', }}>
                                        <Text style={{ fontWeight: "bold", }}>{user_?.username} </Text>
                                        <Text style={{ fontSize: 12 }}>{user_?.typ_user}</Text>
                                    </View>
                                </View >
                                <Text style={{ fontSize: 11, }}>{new Date_heure().getDate_publication(pers?.date_created)} </Text>
                            </View>

                        </TouchableOpacity >
                        <View style={{ marginLeft: '3%', }}>
                            <Text>{pers?.description}</Text></View>

                        <ScrollView horizontal={true} >
                            {
                                pers?.image?.map(image_ =>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.props.navigation.navigate('HouseDetails', {
                                                'id': pers.id, 'userid': this.state.user_id, 'data': pers,
                                                'imageshouses': pers.image,
                                            })
                                        }}
                                    >
                                        <Image source={{ uri: image_.image_url }} style={{ width: width_, height: width_+50, borderColor: "white", borderWidth: 5 }} />
                                    </TouchableOpacity>
                                )
                            }
                        </ScrollView>
                        <View style={{ marginBottom: '3%', marginLeft: '2%' }}>
                            <View style={{ flexDirection: "row", margin: '4%', justifyContent: "space-between", marginTop: '0%' }}>
                                <View style={{ flexDirection: "row", width: '60%' }}>
                                    <Icon name='map-marker' color="grey" size={19} />
                                    <Text style={{ fontSize: 12, marginLeft: 10, color: 'grey' }}>
                                        {pers?.neighbor}
                                        {", " + pers?.city?.name} </Text>
                                </View>
                                <Text style={{ fontSize: 12, color: "red" }}>{pers?.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF  {pers.category.toLowerCase() == "terrain" ? '/ m²' : pers.publish_type == 'vendre' ? '' : pers.is_immeuble == true ? '/ jour' : '/ mois'}</Text>
                            </View>
                            <View style={{ marginTop: '1%', margin: '5%', flexDirection: 'row', justifyContent: "space-between" }}>
                                <TouchableOpacity
                                    onPress={() => { this.save_like(pers?.id, user_id, user_) }} //lineHeight: 32,
                                    style={{
                                        ...styles.heart
                                        //   backgroundColor: "#fff6e6",
                                        // shadowColor: "black", //margin: '2%', textAlign: "center",
                                        // height: 32, opacity: 0.75,
                                        // width: 55, borderRadius: 24,
                                        //bottom: height_ * 0.58, left: width_ * 0.75,
                                    }}>
                                    <View style={{ flexDirection: 'row', }}>
                                        <Icon name={this.state.mes_likes.includes(pers?.id) == false ? 'heart-o' : 'heart'} size={22}
                                            style={{ top: 6, color: "#7c3325" }}
                                        />
                                        <Text style={{
                                            fontSize: 10, left: 3, top: 7, fontWeight: 'bold',
                                            borderRadius: 100, color: "black"
                                        }}> {_.filter(likes, { house: pers?.id }).length} </Text>
                                    </View>
                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Aimer</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styles.heart }}
                                    onPress={() => {
                                        new PostData().reservation(pers.reserveurs, this.state.user_id, pers.id),
                                            user_?.date_experiration == null || moment(user_?.date_experiration) >= moment(new Date()) ?
                                                this.calling(
                                                    {
                                                        number:'+' + pers?.user?.countrie?.indicatif?.toString() + pers.user?.phone_number?.toString(),
                                                        promt:false
                                                    }
                                                )
                                                : new Notification().sendNotitfications(user_?.tokens, _.find(this.state.users, { id: this.state.user_id })?.username, 'A essayé de vous joindre, veuillez payer votre abonnement.', 'hotboy://message/' + pers?.id)

                                    }
                                    }
                                >
                                    <Icon name='phone' size={22} color='grey' />
                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Appeler</Text>
                                </TouchableOpacity>

                                {
                                    pers?.user?.id !== user_id ?
                                        <TouchableOpacity
                                            style={{ ...styles.heart }}
                                            onPress={() => {
                                                this.props.navigation.navigate(
                                                    'MessagesIni', {
                                                    data: pers,
                                                    user: _.find(users, { id: pers?.user })
                                                })
                                            }}>
                                            <Entypo name='message' color='grey' size={22} />
                                            <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Ecrire</Text>
                                        </TouchableOpacity>
                                        : null
                                }

                                <TouchableOpacity
                                    style={{ ...styles.heart }}
                                    onPress={() => {
                                        this.setState({ focus: true })
                                    }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Icon name='wechat' size={22} color='grey' />
                                        <Text style={{
                                            fontSize: 10, left: 3, top: 7, fontWeight: 'bold',
                                            borderRadius: 100, color: "black"
                                        }}>  {_.size(comment)} </Text>
                                    </View>
                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Commenter</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styles.heart }}
                                    onPress={() => this.generateLink('id', pers, _.first(pers.image)?.image_url, user_id, pers?.user?.tokens)}
                                >
                                    <Entypo name='forward' color='grey' size={22} />
                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Partager</Text>
                                </TouchableOpacity>

                            </View>
                            {
                                pers?.category.toLowerCase() !== "terrain" && pers?.category !== "" && pers?.category.toLowerCase() !== "boutique"
                                    && pers?.category.toLowerCase() !== "local administratif" && pers?.category.toLowerCase() !== "bureau"
                                    && pers?.category.toLowerCase() !== "autre" ?
                                    <>
                                        <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                            <View style={{ flexDirection: "row" }}>
                                                <Icon name='bed' color='grey' size={20} />
                                                <Text style={{ fontSize: 13, color: 'grey' }}>{pers?.nb_room} Chambres </Text>
                                            </View>
                                            <View style={{ flexDirection: "row" }}>
                                                <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{pers?.nb_toilet} douches</Text>
                                            </View>
                                            <View style={{ flexDirection: "row" }}>
                                                <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{pers?.nb_parlour} salons </Text>
                                            </View>

                                        </View>
                                        <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%' }}>
                                            {pers?.nb_kitchen != 0 ?
                                                <View style={{ flexDirection: "row" }}>
                                                    <MaterialIcons name='kitchen' color='grey' size={20} style={{ marginLeft: '2%' }} />
                                                    <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{pers?.nb_kitchen} Cuisine(s)</Text>
                                                </View> : null
                                            }
                                            {pers?.fence == true ? (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon name='slideshare' color='grey' size={20} />
                                                    <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>Dans la barrière</Text>
                                                </View>) : null
                                            }
                                            {pers?.garden == true ? (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon name='pied-piper-alt' color='grey' size={20} />
                                                    <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>jardin</Text>
                                                </View>) : null
                                            }
                                            {pers?.parking == true ? (
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon name='car' color='grey' size={20} />
                                                    <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>parking</Text>
                                                </View>) : null
                                            }

                                        </View>
                                    </> :
                                    pers?.category.toLowerCase() == "terrain" ?
                                        <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                            <View style={{ flexDirection: "row" }}>
                                                <Icon name='stop' color='grey' size={20} />
                                                <Text style={{ fontSize: 13, color: 'grey' }}>Dimension: {pers?.dimension} mètres carrés </Text>
                                            </View>


                                        </View> : null
                            }
                        </View>


                    </View>


                    <View style={{
                        backgroundColor: 'white', //height: hp('60%'), marginBottom: height_ / 2.5
                    }}>
                        {_.size(comment) == 0 ?
                            <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: height_ * 0.4, alignSelf: 'center' }}>
                                Soyez la première personne à commenter cette publication
                            </Text>
                            :
                            comment.map(comment_ => {
                                return (
                                    <View style={{ flexDirection: 'row', margin: 10, marginLeft: 25 }}>
                                        <Avatar.Image size={35} source={{ uri: _.find(users, { id: comment_.user })?.profile_image }} />
                                        <View style={styles.store_row}>
                                            <View style={{ flexDirection: 'row', }}>
                                                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{_.find(users, { id: comment_.user })?.username}</Text>

                                            </View>

                                            <Rating
                                                type='star'
                                                ratingCount={comment_.note}
                                                imageSize={12}
                                                startingValue={comment_.note}
                                                readonly={true}
                                                //ratingBackgroundColor = {'#919191'}
                                                tintColor={'#919191'}
                                                style={{ marginTop: 5, backgroundColor: '#919191' }}
                                            />
                                            <Text style={{ fontSize: 12 }}>{comment_.content}</Text>
                                            {
                                                !comment_.image ? null :
                                                    <Image
                                                        source={{ uri: comment_.image }}
                                                        resizeMode='stretch'
                                                        style={{
                                                            width: width_ / 3, height: 100,
                                                            borderRadius: 10, marginLeft: 5,
                                                        }}
                                                    />
                                            }


                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{ fontSize: 11 }}>{new Date_heure().getDate_publication(comment_.date)} </Text>
                                                <TouchableOpacity style={{ marginLeft: 25 }}>
                                                    <Text style={{ fontSize: 11 }}>Repondre </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>


                                    </View>
                                )
                            })

                        }
                    </View>
                    <View style={{ height: 165, backgroundColor: 'white' }} ></View>

                </ScrollView>

                <KeyboardAvoidingView style={styles.footer}>
                    <AirbnbRating
                        count={5}
                        reviews={["Terrible", "Bad", "Good", "Very Good", "Unbelievable"]}
                        defaultRating={this.state.score}
                        size={25}
                        ratingCount={this.state.score}
                        startingValue={this.state.score}
                        onFinishRating={score => this.setState({ score: score })}
                    />
                    <View style={{ flexDirection: 'row' }}>
                        <View style={styles.input_}>
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={styles.textarea}
                                autoFocus={true}
                                onChangeText={(val) => { this.setState({ content: val }) }}
                                value={this.state.content}
                                placeholder={'Taper un message'}
                                placeholderTextColor={'black'}
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                        <Entypo size={35} style={{ margin: 3, marginLeft: 10, marginTop: 25 }} name='image' color='#7c3325'
                            onPress={() => this.handlerChoosePhoto()}
                        />
                        {this.state.content != '' ?
                            <TouchableOpacity
                                style={{ marginLeft: 4, marginTop: 21 }}
                                onPress={() => !this.state.save ? this.addPost() : null}>
                                <Ionicons size={30} name='send-sharp' color={!this.state.save ? '#7c3325' : '#dcca8a'} style={{ marginTop: 6 }}
                                />
                            </TouchableOpacity>
                            : null
                        }
                    </View>
                </KeyboardAvoidingView>
            </View >
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "azure",
        alignItems: "center",
        justifyContent: "center",
    },
    textInputt: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        marginTop: -13
    },
    input_: {
        // borderWidth: 1, 
        //height: 15,
        width: '77%',
        backgroundColor: '#F5FCFF',
        //minHeight: 50, padding: 5, color: '#7c3325',
        // borderRadius: 7, borderColor: '#05375a', borderWidth: 1.5,
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    store_row: {
        padding: 10,
        backgroundColor: "#919191",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5, marginTop: 3,
        borderRadius: 15, margin: 1,
        width: width_ * 0.75

    },
    footer: {
        // borderTopColor: '#05375a',
        // padding: 7,
        //borderTopWidth: 0.5,
        position: 'absolute',
        bottom: 10,
        width: '100%',
        height: 150,
        // marginBottom: 5,

        backgroundColor: 'white'

    },
    textInput: {
        //flex: 1,
        margin: 12,
        color: '#05375a',
        borderWidth: 1,
        width: '90%', minHeight: 40, padding: 5,
        borderRadius: 7, borderColor: '#05375a', borderWidth: 1.5,
    },
    singnIn: {
        width: 80,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },

    button: {
        height: 45, flexDirection: 'row',
        padding: 8, backgroundColor: '#7c3325',
        marginTop: 20, marginLeft: 50
        // justifyContent: 'center',
        // alignItems: 'center', alignSelf: 'flex-end',
        // margin: 10, borderRadius: 7
    },
    modal: {
        backgroundColor: "white",
        // borderRadius: 20,
        // padding: 10,
        // shadowColor: "#000",

        width: width_,
        height: height_,
        //marginBottom: height_ * 0.005,
        // marginTop: height_ * 0.12,
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },

    textareaContainer: {
        height: 85,
        //padding: 5,
        backgroundColor: '#F5FCFF',

    },
    textarea: {
        //textAlignVertical: 'top',  // hack android
        height: 80,
        fontSize: 14,
        color: '#7c3325',
    },

});