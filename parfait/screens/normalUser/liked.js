import React from 'react';
import {
    View, Text, Image, ScrollView, StyleSheet, ImageBackground,
    TouchableOpacity, Button, TextInput, Modal, Dimensions
} from 'react-native';
import axios from 'axios';

import _ from 'underscore';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Icon from "react-native-vector-icons/FontAwesome";
import AsyncStorage from '@react-native-async-storage/async-storage';
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Liked extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            querry: '', agent: [], users: [], company: {}, houses: [],
            likes: [],
            name: '', description: '', photo: null,
            user: null, 

        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.likes !== this.state.likes) {
            axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                .then(res => {
                    this.setState({ likes: _.filter(res.data, { user: this.state.user, }) })
                });
        }
    }

    componentDidMount() {
        this.displayData()




    }


    save_like = (content_id, user) => {

        id_ = _.find(this.state.likes, { user: user, content_id: content_id, content_type: 14 })?.id
        axios.delete(`https://imobbis.pythonanywhere.com/feature/like/${id_}`)

    }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ user: json_id }) //https://imobbis.pythonanywhere.com/new/likeDunUser/8

            axios.get("https://imobbis.pythonanywhere.com/new/likeDunUser/" + json_id).then(res => {
                this.setState({ houses: res.data });
                console.log(res.data)
            })

        } catch { error => alert(error) };
    }
    render() {
        const { houses} = this.state;


        return (
            <View style={{ backgroundColor: 'white', }}>
                <Text style={{ fontWeight: 'bold', fontSize: 20, marginVertical: 20, marginLeft: 20 }}>J'ai aimé</Text>
                <ScrollView>
                    {
                        _.size(houses) == 0 ?
                            <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: height_ * 0.4, alignSelf: 'center' }}>
                                Pas de j'aime pour le moment
                            </Text>
                            :
                            <View>
                                {

                                    <View style={{ padding: 5 }}>
                                        {
                                            houses.map(houses_ =>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.props.navigation.navigate('HouseDetails', { id: houses_.house.id, data:houses_.house })
                                                    }}>
                                                    <View style={{ ...styles.row3 }}>
                                                        <Text style={{ fontSize: 18, fontWeight: '700' }}> {houses_.house.name}</Text>
                                                        <ImageBackground source={{ uri: _.first(houses_?.house?.image)?.image_url }} resizeMode='stretch'
                                                            style={{ width: '100%', height: 200, borderRadius: 15 }}
                                                        >
                                                            <TouchableOpacity onPress={() => this.save_like(pers.content_id, this.state.user)}
                                                                style={{ alignSelf: 'flex-end', margin: 7 }}
                                                            >

                                                                <FontAwesome
                                                                    name={'heart'}
                                                                    size={30} color='#7c3325'
                                                                />
                                                            </TouchableOpacity>
                                                        </ImageBackground>
                                                        <View style={{ marginLeft: '2%', paddingVertical: 7 }}>
                                                            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                                                <View style={{ flexDirection: "row" }}>
                                                                    <View style={{ flexDirection: 'row' }}>
                                                                        <Icon name='map-marker' color="grey" size={19} />
                                                                        <Text style={{ fontSize: 17, marginLeft: 5, color: 'grey' }}>{ houses_.house.city?.name} </Text>
                                                                    </View>

                                                                </View>
                                                            </View>
                                                            <Text style={{ fontSize: 17, marginVertical: 4 }}>
                                                                <Text style={{ fontWeight: 'bold', color: 'red' }}>{houses_.house.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                                                                <Text style={{ fontWeight: 'bold', color: 'red' }}> par  {houses_.house.category.toLowerCase() == "terrain" ? '/ m²' : houses_.house.publish_type == 'vendre' ? '' : houses_.house.is_immeuble == true ? '/ jour' : '/ mois'}</Text>
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        }
                                    </View>

                                }
                            </View>
                    }
                </ScrollView>
                <View style={{ height: '35%' }}>

                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        }, margin: 6,
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 9,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: '46%',
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 5, marginTop: 4, marginLeft: 3
    }
})
