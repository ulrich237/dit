import React, { Component } from "react";
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform, Pressable, FlatList,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Share, ActivityIndicator, Linking
} from "react-native";
import { Checkbox, Avatar } from 'react-native-paper';
import Entypo from 'react-native-vector-icons/Entypo';
import { Divider } from 'react-native-elements'
import axios from 'axios'
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import _ from 'lodash';
import MultiSlider from '@ptomasroos/react-native-multi-slider'

import AsyncStorage from '@react-native-async-storage/async-storage'

import FastImage from 'react-native-fast-image';
import ModalSelector from 'react-native-modal-selector';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import Toast from 'react-native-simple-toast';
import Touched from '../../../functions/touched';
import Date_heure from '../../../functions/date_heure';
import Notification from '../../../functions/notification';
import PostData from "../../../functions/postData";
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';

import call from 'react-native-phone-call';
import moment from "moment";

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);
var dat = {}
export default class Houses extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //ville selectionnée
            villeid: 1,//Number(this.props.route.params.id),
            quartierid: 108,
            house: [],
            user: [],
            category: [],

            countrys: [],
            provinces: [],
            citys: [],
            likes: [],
            modalVisible2: false,
            //outils maison

            searchelt: null,

            //
            //categorie maison



            userid: null,
            //
            // like
            liked: {
                id: null,
                content_id: null,
                user: null,
                content_type: null
            },
            modif: false,
            modif_supprimer: '', data: {},
            select: {}, quartiers: [], imageshouses: [], isloading: false,
            mes_likes: [], all_likes: [],
            meuble: false, moderne: false, vendre: false, louer: true, disc: false,
            hs: [], init_city: 'Ville', city_id: null, cityHouse: [], pays: null, modalP: false,
            activeSlide: 0, colloc: false, isLoading: false,
            isLoading_: false, currentPage: 1, refresh: false, seed: 1,
            allHouses: [], prixMax: 0, modalC: false, ville: {}, prixMin: 0, fil: false
        }
    }



    calling = (args) =>{
        call(args).catch(console.error)
  }


    generateLink = async (param, value, url, user, token) => {

        var lien = null
        //value?.lien == null || value?.lien == '' ?
            link_ = await dynamicLinks()
                .buildShortLink(
                    {
                        link: 'https://imobbis.page.link/message' + value?.id,
                        domainUriPrefix: 'https://imobbis.page.link',
                        android: {
                            packageName: 'com.imobbis.app',
                            minimumVersion: '20'
                            //version minmale ici
                            //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                        },
                        /*ios: {
                             bundleId: AppConfig.getIOSBundle(),
                             appStoreId: AppConfig.getIOSAppId(),
                         }, */
                        social: {
                            title: value?.category + ' à ' + value?.publish_type,
                            descriptionText: value?.description,
                            imageUrl: url
                        }
                        /* navigation: {
                             forcedRedirectEnabled: true,
                         }*/
                    },
                    //firebase.dynamicLinks.ShortLinkType.SHORT
                )
                .then((link) => {
                    //DynamicLinkStore.currentUserProfileLink = link;
                    console.log(link)
                    console.log(value?.id)

                    lien = link.slice(link.lastIndexOf('/') + 1)
                    console.log(lien)

                    new PostData().partageur(value?.partageurs, user, value?.id)

                    new Notification().sendNotitfications(token, _.find(this.state.user, { id: this.state.userid })?.username,
                    'A partagé votre publication.', 'hotboy://message/'+ value?.id)

                    this.onShare(link)
                    

                   /* lien ?
                        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + value?.id, {
                            'lien': lien
                        }).catch(error => console.log(error)).then(response => {
                           // this.onShare(link),
                            //    new PostData().partageur(value?.partageurs, user, value?.id)
                        })
                            .catch(error => console.log(error))
                        : null*/

                })

                .catch((err) => {
                    console.log(err);
                    //DynamicLinkStore.profileLinkLoading = false;
                })
            //: (new PostData().partageur(value?.partageurs, user, value?.id), this.onShare('https://imobbis.page.link/' + value?.lien))

    }

    componentDidMount() {
        this.displayData()
        const { cat } = this.props.route.params;
        axios.get(`https://imobbis.pythonanywhere.com/new/house_category2/${cat?.name}`).then(res => {
            this.setState({
                house: res.data,
                isloading: true,
            })

        }).catch(err => { err.Error == 'Network Error' ? (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null });
        axios.get(`https://imobbis.pythonanywhere.com/new/categoryVille/${cat?.name}`,)
            .then(response => {
                this.setState({ cityHouse: response.data })
            })

            .catch(error => this.setState({ error, isLoading: false }));
        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(resloc => {
                this.setState({
                    countrys: resloc.data.country, citys: resloc.data.city,
                })
            })

        axios.get("https://imobbis.pythonanywhere.com/new/hous/").then(res => {
            this.setState({
                allHouses: res.data
            })
        }).catch(err => console.log(err))

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            this.setState({ user: resu.data })
        }).catch(err => err.Error == 'Network Error' ? (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null);

        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(resct => {
            this.setState({ category: resct.data })
        }).catch(err => err.Error == 'Network Error' ? (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null);

        axios.get(`https://imobbis.pythonanywhere.com/new/like`)
            .then(res => {
                let mes_likes = []
                _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.house))
                this.setState({ likes: res.data, mes_likes: mes_likes })
            });

    }


    renderLoader = () => {
        return (
            this.state.isLoading_ ?
                <View style={{ height: 310, backgroundColor: '#d0d0d0' }}>
                    <ActivityIndicator size="large" color="#7c3325" />
                    <Text style={{ textAlign: 'center', fontWeight: "bold", color: "#dc7e27" }}>
                        Chargement des publications...
                    </Text>
                </View> : null
        );
    };

    loadMoreItem = () => {
        this.setState({ currentPage: _.size(this.state.house) > 25 ? 1 : this.state.currentPage + 1 }
            , () => { this.actua(this.state.userid) })
    };

    handleRefesh = () => {
        this.setState({ seed: this.state.seed, refresh: true, currentPage: 1 }
            , () => { this.actua(this.state.userid) })

    };

    actua = (userid) => {
        const { cat } = this.props.route.params;
        const { currentPage } = this.state
        axios.get(`https://imobbis.pythonanywhere.com/new/house_category2/${cat?.name}`,)
            .then(response => {
                var data = this.state.house.concat(response.data)
                this.setState({
                    house: currentPage === 1 || _.size(data) > 25 ? response.data : data,
                    isLoading_: true, refresh: false, init_city: 'Ville', city_id: null
                }),
                    this.reinitialiser()
            })
            .catch(error => this.setState({ error, isLoading: false }));
    }



    onShare = async (data) => {
        try {
            const result = await Share.share({
                message: data
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            //alert(error.message);
        }
    };


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            let city = await AsyncStorage.getItem('cities');
            let count = await AsyncStorage.getItem('countries');

            this.setState({
                userid: json_id,
                //countrys: JSON.parse(count), citys:JSON.parse(city)
            })

            this.actua(json_id)
        } catch { error => alert(error) };
    }


    postLien = (user, data) => {
        //axios.get("https://imobbis.pythonanywhere.com/new/messagin").then((reslh) => {
          //  _.find(reslh.data, { user_receiver: user?.id, house: data?.id, user_sender: this.state.userid }) ? null :
                axios.post("https://imobbis.pythonanywhere.com/new/messagin",
                    {
                        'user_receiver': user?.id,
                        'user_sender': this.state.userid,
                        'house': data?.id,
                        'view': false,
                    })
                    .then(response => {
                        new Notification().sendNotitfications(user?.tokens, _.find(this.state.user, { id: this.state.userid })?.username, 'Vous a envoyé un message pour resérvation.', 'hotboy://message/'+ data?.id)
                    })
                    .catch(error => alert(error))
       // }).catch(err => console.log(err));
    }




    save_like = (homeid, user, prop) => {
        let mes_likes = this.state.mes_likes;
        let exist = _.find(this.state.likes, { user: user, house: homeid }) ? true : false;
        exist == true ? mes_likes = _.filter(this.state.likes, (value) => value.house != homeid) : mes_likes.push(homeid)
        this.setState({ mes_likes: mes_likes });
        let item = exist == false ? {
            house: Number(homeid),
            user: Number(user),
        } : { id: _.find(this.state.likes, { user: user, house: homeid })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/like`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/like/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                    .then(res => {
                        exist == false ? new Notification().sendNotitfications(prop?.tokens, _.find(this.state.user, { id: this.state.userid })?.username, 'A aimé votre publication.', 'hotboy://message/'+ homeid) : null
                        let mes_likes = []
                        _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.house))
                        this.setState({ likes: res.data, mes_likes: mes_likes })
                    });

            }).catch(error => {

            })
    }



    rechercher() {

        const { cat } = this.props.route.params;

        const {
            meuble, vendre, louer, prixMin, fil,
            pays, ville, colloc, prixMax

        } = this.state;

        let etat = vendre ? 'vendre' : 'louer'
        let colloc_ = colloc ? 'True' : 'False'
        let meuble_ = meuble ? 'True' : 'False'


        if (pays) {
            if (ville === null) {

                Alert.alert("Attention", "Veuillez mettre la ville pour filtrer")
                this.setState({ modalVisible2: true })

            } else {
                if (prixMax === 0) {

                    Alert.alert("Attention", "Veuillez mettre le prix maximum pour filtrer.")
                    this.setState({ modalVisible2: true })
                } else {

                    if (Number(prixMax) <= Number(prixMin)) {
                        console.log(prixMax + "/ " + prixMin)
                        Alert.alert("Attention", "Veuillez mettre le prix maximum strictement supérieure au prix minimum pour filtrer.")
                        this.setState({ modalVisible2: true })

                    } else {

                        console.log(pays?.id + '/' + ville?.id + '/' + etat + '/' + prixMin + '/' + prixMax + '/' + cat?.name + "/" + colloc_ + '/' + meuble_)

                        this.setState({ modalVisible2: false }),
                            axios.get('https://imobbis.pythonanywhere.com/new/filtre/' + pays?.id + '/' + ville?.id + '/' + etat + '/' + prixMin + '/' + prixMax + '/' + cat?.name + "/" + colloc_ + '/' + meuble_,)
                                .then(response => {

                                    _.size(response.data) == 0 ?
                                    Toast.shadowColor(
                                            "Rien ne correspond à votre filtre.",
                                            Toast.LONG,
                                            Toast.CENTER,
                                          
                                        ) : this.setState({ fil: true, house: response.data })
                                },

                                )

                                .catch(error => this.setState({ error, isLoading: false }));


                    }


                }
            }
        } else {

            Alert.alert("Attention", "Veuillez mettre le pays pour filtrer.")
            this.setState({ modalVisible2: true })

        }

        //
    }


    reinitialiser() {
        this.setState({
            colloc: false, init_city: 'Ville', fil: false,
            moderne: false, meuble: false, vendre: false, louer: false,
            city_id: null, prixMax: 0, ville: null, pays: null, prixMin: 0
        })
    }


    setModalVisible2 = (visible) => {
        this.setState({ modalVisible2: visible });
    }


    pagination(entries) {
        const { activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'white' }}
                dotStyle={{
                    width: 5,
                    height: 5,
                    borderRadius: 5,
                    marginHorizontal: 4,
                    backgroundColor: '#dc7e27'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
            //inactiveDotOpacity={0.4}
            //inactiveDotScale={0.4}
            />
        );
    }


    firstCarousel = ({ item, index }) => {
        return (
            <View
                activeOpacity={0.8}
                enabled={false}
                onPress={() => {
                }}>
                <FastImage
                    style={styles.images
                    }
                    source={{
                        uri: item.image_url || item.image,

                    }}

                />
            </View>
            //)
        );
    }


    filterCity = (id) => {

        const { cat } = this.props.route.params;
        console.log(id)
        axios.get(`https://imobbis.pythonanywhere.com/new/AllHouseVille4/${cat?.name}/${id}`,)
            .then(response => {
                this.setState({ house: response.data, currentPage: 1 }),
                    console.log(response.data)
            })
            .catch(error => this.setState({ error, isLoading: false }));

    }

    render() {

        const { modalP, pays, init_city, cityHouse, allHouses, meuble, modalC, prixMin, prixMax,
            ville, vendre, louer, disc, house, likes, imageshouses, quartiers, user, fil,
            modalVisible2, city_id, colloc, userid, citys, countrys } = this.state;
        const { cat } = this.props.route.params
        let user_; let imageshouse_;
        var dict = {}

        const data = cityHouse.map(city => dict = { key: city.id, label: city.name });
        let isLoading = this.state.isLoading;


        let etat = vendre ? 'vendre' : 'louer'



        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalP}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}>

                    <View style={[styles.modalView]}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={{
                                    ...styles.openButton,
                                    shadowColor: "black",
                                    shadowOffset: {
                                        width: 0, height: 6,
                                    },
                                    shadowOpacity: 0.1, marginTop: 20
                                }}
                                onPress={() => {
                                    this.setState({ modalP: false });
                                }}
                            >
                                <Icon name="times" size={25} style={{ marginLeft: 2, color: 'orangered' }} />
                            </TouchableOpacity>
                            <Text style={{ width: '20%' }}></Text>
                        </View>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                        <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Sélectionner un pays</Text>
                        <ScrollView>
                            {
                                countrys.map(city => {
                                    return (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ modalC: true, modalP: false, pays: city });
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>{city.name}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalC}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}>
                    <View style={[styles.modalView]}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={{
                                    ...styles.openButton,
                                    shadowColor: "black",
                                    shadowOffset: {
                                        width: 0, height: 6,
                                    },
                                    shadowOpacity: 0.1, marginTop: 20
                                }}
                                onPress={() => {
                                    this.setState({ modalC: false });
                                }}
                            >
                                <Icon name="times" size={25} style={{ marginLeft: 2, color: 'orangered' }} />
                            </TouchableOpacity>
                            <Text style={{ width: '20%' }}></Text>
                        </View>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                        <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Sélectionner une ville</Text>
                        <ScrollView>
                            {
                                pays?.cities?.map(pays_ => {
                                    return (
                                        <View style={{ alignItems: 'center' }}>
                                            {
                                                _.filter(citys, { id: pays_ }).map(city => {
                                                    return (
                                                        <TouchableOpacity
                                                            style={{ alignItems: 'center' }}
                                                            onPress={() => {
                                                                this.setState({ modalC: false, ville: city });
                                                            }}
                                                        >
                                                            <Text style={{ fontSize: 15, alignSelf: "center", textAlign: 'center', margin: 5 }}>{city.name}</Text>
                                                        </TouchableOpacity>
                                                    )
                                                })
                                            }
                                        </View>
                                    )
                                })
                            }
                            <View style={{ height: 100 }}></View>
                        </ScrollView>
                    </View>
                </Modal>





                <Modal animationType="slide"
                    transparent={true}
                    visible={modalVisible2}
                    onRequestClose={() => {
                        this.setModalVisible2(!modalVisible2);
                    }}>
                    <View style={{ ...styles.centeredView, marginTop: this.state.searchelt !== "filtre" ? height_ * 0.25 : 53 }}>
                        <View style={styles.modalView}>
                            <View style={{ flexDirection: 'row' }} >
                                <TouchableOpacity
                                    style={{ width: '10%' }}
                                    onPress={() => this.setModalVisible2(!modalVisible2)}
                                >
                                    <Icon name="times" size={25} style={{ marginLeft: 2, color: 'orangered' }} />
                                </TouchableOpacity>
                                <Text style={{ width: '20%' }}></Text>
                            </View>
                            <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                            <ScrollView>
                                <TouchableOpacity
                                    style={{ margin: 10 }}
                                    onPress={() => this.setState({ modalP: true })}
                                >
                                    <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>
                                        Sélectionner le pays: {pays?.name} </Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{ margin: 10 }}
                                    onPress={() => { pays ? this.setState({ modalC: true }) : this.setState({ modalP: true }) }}
                                >
                                    <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>
                                        Sélectionner la ville: {ville?.name} </Text>
                                </TouchableOpacity>


                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                {
                                    cat?.name.toUpperCase() != 'TERRAIN' && cat?.name.toUpperCase() != 'AUTRES' && cat?.name.toUpperCase() != 'AUTRE' ?
                                        <>
                                            <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>Les biens meublés </Text>
                                            <Checkbox.Item style={{ width: "75%", height: 37 }} label="Meublé" status={this.state.meuble == true ? 'checked' : 'unchecked'}
                                                onPress={val => this.setState({ meuble: !meuble })} />

                                            <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                        </>
                                        : null
                                }
                                <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>A vendre </Text>

                                <Checkbox.Item style={{ width: "75%", height: 36 }} label={'vendre'} status={this.state.vendre == true ? 'checked' : 'unchecked'}
                                    onPress={val => this.setState({ vendre: !vendre, louer: false })} />


                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />

                                <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>A louer </Text>

                                <Checkbox.Item style={{ width: "75%", height: 36 }} label={'louer'} status={this.state.louer == true ? 'checked' : 'unchecked'}
                                    onPress={val => this.setState({ louer: !louer, vendre: false })} />


                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>Les biens en collocation</Text>
                                <Checkbox.Item style={{ width: "75%", height: 37 }} label="Collocation" status={this.state.colloc == true ? 'checked' : 'unchecked'}
                                    onPress={val => this.setState({ colloc: !colloc, vendre: false })} />

                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>Prix minimum: </Text>
                                <View style={{ margin: 10, height: 50, justifyContent: "center" }}>


                                    <TextInput
                                        style={{ color: "black" }}
                                        onChangeText={val => this.setState({ prixMin: val })}
                                        value={this.state.prixMin}
                                        placeholder="Taper le prix min ici..."
                                        placeholderTextColor="#dc7e27"
                                        keyboardType="numeric"
                                    />
                                </View>

                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                <Text style={{ fontSize: 16, marginLeft: 6, fontWeight: "bold" }}>Prix maximum:</Text>
                                <View style={{ margin: 10, height: 50, justifyContent: "center" }}>


                                    <TextInput
                                        style={{ color: "black" }}
                                        onChangeText={val => this.setState({ prixMax: val })}
                                        value={this.state.prixMax}
                                        placeholder="Taper le prix max ici..."
                                        placeholderTextColor="#dc7e27"
                                        keyboardType="numeric"
                                    />
                                </View>

                                <Divider style={{ backgroundColor: 'grey', height: 2, marginTop: '3%' }} />
                                <View style={{ flexDirection: 'row', marginTop: 10, }}>
                                    <TouchableOpacity style={styles.buttonstyle1}
                                        onPress={() => { this.setState({ modalVisible2: false }), this.reinitialiser() }}>
                                        <Text style={{ fontSize: 12, textAlign: "center" }}>Réinitialiser</Text>
                                    </TouchableOpacity>
                                    <View style={{ width: "10%" }}></View>
                                    <TouchableOpacity style={styles.buttonstyle} onPress={() => { this.rechercher() }}>
                                        <Text style={{ fontSize: 12, textAlign: "center", color: "white" }}>Rechercher</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 100 }} ></View>
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
                {
                    <View>
                        <View style={{}}>
                            <View style={{
                                width: '100%', flexDirection:
                                    'row', backgroundColor: '#dc7e27'
                            }}>
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                                    style={{ margin: 10 }}>
                                    <MaterialIcons name='navigate-before' size={45} style={{ marginTop: -5 }} />
                                </TouchableOpacity>
                                <View style={{ flexDirection: 'row', marginTop: 3, width: '80%', alignItems: 'center' }}>

                                    <Ionicons name={cat?.icon} color="black" size={35} />
                                    <Text style={{
                                        color: 'black', marginTop: 2,
                                        fontSize: 18, fontWeight: 'bold'
                                    }}>{cat?.name}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row', justifyContent: 'space-around',
                            height: 50, padding: 4, backgroundColor: "#7c3325", borderColor: "#dc7e27",
                            borderRadius: 50, margin: 5, marginTop: 10
                        }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <View style={{ flexDirection: "row", alignItems: 'center' }}>
                                    <EvilIcons name='location' size={30} color='white' />
                                    <ModalSelector
                                        data={data}
                                        cancelText='Cancel'
                                        optionTextStyle={{ color: '#dc7e27', fontSize: 20, fontWeight: '700' }}
                                        optionContainerStyle={{ height: 300, backgroundColor: 'white', width: '60%', alignSelf: 'center' }}
                                        cancelStyle={{ backgroundColor: 'red', width: '60%', alignSelf: 'center', martop: 5 }}
                                        cancelTextStyle={{ color: '#dc7e27', fontSize: 20 }}
                                        initValueTextStyle={{ color: '#dc7e27', fontSize: 12 }}
                                        animationType='slide'
                                        initValue={init_city}
                                        accessible={true}
                                        scrollViewAccessibilityLabel={'Scrollable options'}
                                        cancelButtonAccessibilityLabel={'Cancel Button'}
                                        onChange={(option) => {
                                            this.filterCity(option.key), this.setState({ init_city: option.label, city_id: option.key })
                                        }}>
                                        <TextInput
                                            style={{ fontSize: 12, color: 'white', fontWeight: 'bold' }}
                                            editable={false}
                                            placeholder={init_city}
                                            value={init_city}
                                        />
                                    </ModalSelector>
                                </View>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }} >
                                <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }}
                                    onPress={() => this.setState({ modalVisible2: true, searchelt: "filtre" })}>
                                    <Text style={{ color: 'white', fontSize: 12 }}>Filtrer</Text>
                                    <Ionicons name="md-options-outline" size={30} style={{ marginLeft: 8, marginRight: 15, color: "white" }} />
                                </TouchableOpacity>
                            </View>

                        </View>

                        {

                            fil ?

                                <View>
                                    <Text style={{
                                        "color": "red",
                                        "fontSize": 8,
                                        "fontWeight": "bold",
                                        textAlign: "center"
                                    }}>
                                        {_.size(house)} résutat(s) || {pays?.name} / {ville?.name} / {etat} / {prixMin?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} à {prixMax?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} / {colloc ? 'collocation /' : ''} {meuble ? 'meublé' : ''}
                                    </Text>
                                </View>

                            : null

                        }


                        <FlatList
                            data={house}
                            numColumns={1}
                            renderItem={({ item, index, separators }) => (
                                user_ = item.user,
                                imageshouse_ = item.image,
                                <View>
                                    <View>
                                        <View style={{ flexDirection: 'row', justifyContent: item?.disponible ? "space-between" : 'center' }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center' }}>
                                                    {item?.category?.toUpperCase()}
                                                    {!item?.colocation ? " A " + item?.publish_type?.toUpperCase() : ' EN COLLOCATION'}</Text>
                                            </View>
                                            {item?.disponible ?
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Icon name='warning' color="red" size={20} style={{ marginTop: 2 }} />
                                                    <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center', color: 'red' }}>
                                                        Indisponible
                                                    </Text>

                                                </View>
                                                : null
                                            }
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        enabled={false}
                                        onPress={() => {
                                            this.props.navigation.navigate('AglaProfile', {
                                                agla: item.user,
                                                countrie:item.user?.countrie
                                                ,houses: _.filter(allHouses, (value) => value?.user?.id == item.user?.id),
                                                content_ct: 15
                                            })
                                        }}>
                                        <View style={{ padding: 10, flexDirection: "row", margin: '4%', justifyContent: "space-between", alignItem: "center", }}>
                                            <View style={{ flexDirection: "row", width: width_ * 0.6, }} >
                                                <Avatar.Image
                                                    size={49}
                                                    rounded
                                                    avatarStyle={{ backgroundColor: 'orange' }}
                                                    source={{ uri: user_?.profile_url || user_?.profile_image }} />
                                                <View style={{ marginLeft: '5%', }}>
                                                    <Text numberOfLines={1} style={{ fontWeight: "bold", }}>{user_?.username} </Text>
                                                    <Text style={{ fontSize: 12 }}>
                                                        {user_?.typ_user?.toLowerCase() == 'agent' ? user_?.typ_user + ' immobilier' : user_?.typ_user}</Text>

                                                </View>
                                            </View>
                                            <Text style={{ fontSize: 11, }}>{new Date_heure().getDate_publication(item.date_created)} </Text>
                                        </View>
                                    </TouchableOpacity >
                                    <View style={{ marginBottom: '1%' }}><Text>{item.description}</Text></View>
                                    <TouchableOpacity
                                        activeOpacity={0.8}
                                        enabled={false}
                                        onPress={() => {
                                            new Touched().touched_it('house', item?.id, this.state.userid),
                                                this.props.navigation.navigate('HouseDetails',
                                                    {
                                                        id: item.id, userid: userid, data: item,
                                                        imageshouses: item.image
                                                    })
                                        }}
                                    >
                                        <Carousel
                                            ref={(c) => { this._carousel = c; }}
                                            data={item.image}
                                            renderItem={this.firstCarousel}
                                            sliderWidth={width_}
                                            itemWidth={width_}


                                        />
                                        {this.pagination(item.image)}
                                    </TouchableOpacity>
                                    <View style={{ marginBottom: '3%', marginLeft: '2%' }}>
                                        <View style={{ flexDirection: "row", margin: '4%', justifyContent: "space-between", marginTop: '0%' }}>
                                            <View style={{ flexDirection: "row", width: '60%' }}>
                                                <Icon name='map-marker' color="grey" size={19} />
                                                <Text numberOfLines={1} style={{ fontSize: 13, marginLeft: 10, color: 'grey' }}>
                                                    {item.neighbor}
                                                    {item.city?.name ? ', ' + item.city?.name : null} </Text>
                                            </View>
                                            <Text style={{ fontSize: 13, color: "red", }}>{item.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} {item.country?.monnaie}  {item.category.toLowerCase() == "terrain" ? '/ m²' : item.publish_type == 'vendre' ? '' : item.is_immeuble == true ? '/ jour' : '/ mois'}</Text>
                                        </View>
                                        <View style={{ marginTop: '1%', margin: '5%', flexDirection: 'row', justifyContent: "space-between" }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.save_like(item.id, userid, user_)
                                                }} //lineHeight: 32,
                                                style={{
                                                    ...styles.heart
                                                }}>
                                                <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                    <Icon style={{ top: 8, color: "#7c3325" }} name={!_.find(likes, { house: item.id, user: userid }) ? 'heart-o' : 'heart'} size={22}

                                                    />
                                                    {
                                                        _.filter(likes, { house: item.id }).length !== 0 ?
                                                            <Text style={{
                                                                fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                                borderRadius: 100, color: "black"
                                                            }}> {_.filter(likes, { house: item.id }).length} </Text>
                                                            : null
                                                    }
                                                </View>
                                                <Text style={{ marginRight: 5, fontSize: 8, fontWeight: 'bold' }}>Aimer</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ ...styles.heart }}
                                                onPress={() => {
                                                   
                                                    new PostData().reservation(item.reserveurs, userid, item.id),
                                                        user_?.date_experiration == null || moment(user_?.date_experiration) >= moment(new Date()) ?
                                                            this.calling(
                                                                {
                                                                    number:'+' + item?.user?.countrie?.indicatif?.toString() + item.user?.phone_number.toString(),
                                                                    prompt:false
                                                                }
                                                            )
                                                            : new Notification().sendNotitfications(user_?.tokens, _.find(this.state.user, { id: this.state.userid })?.username, 'A essayé de vous joindre, veuillez payer votre abonnement.', 'hotboy://message/'+ item?.id)
                                                }}
                                            >
                                                <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                    <Icon style={{ top: 8, }} name='phone' size={22} color='grey' />
                                                    {
                                                        item.nb_reserveur !== 0 ?
                                                            <Text style={{
                                                                fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                                borderRadius: 100, color: "black"
                                                            }}>{_.size(item.nb_reserveur)} </Text>
                                                            : null
                                                    }
                                                </View>
                                                <Text style={{
                                                    color: user_?.date_experiration == null || moment(user_?.date_experiration) >= moment(new Date()) ? '' : '#8e8e8e',
                                                    fontSize: 8, fontWeight: 'bold'
                                                }}>
                                                    Appeler</Text>
                                            </TouchableOpacity>
                                            {item?.user?.id != userid ?
                                                <TouchableOpacity
                                                    style={{ ...styles.heart }}
                                                    onPress={() => {
                                                        this.postLien(_.find(user, { id: item.user }), item),
                                                            this.props.navigation.navigate(
                                                                'MessagesIni', {
                                                                data: item,
                                                                user: item.user
                                                            })
                                                    }}>
                                                    <Entypo name='message' color='grey' size={22} />
                                                    <Text style={{ fontSize: 8, fontWeight: 'bold' }}>{item?.publish_type == 'louer' ? ' Réserver ' : 'Acheter'}</Text>
                                                </TouchableOpacity>
                                                : <View></View>
                                            }
                                            <TouchableOpacity
                                                style={{ ...styles.heart }}
                                                onPress={() => {
                                                    this.props.navigation.navigate(
                                                        'PostI', {
                                                        child_id: item.id,
                                                        pers: item,
                                                        user_: item.user,
                                                    }
                                                    )
                                                }}>
                                                <Icon name='wechat' size={22} color='grey' />
                                                <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Commenter</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ ...styles.heart }}
                                                onPress={() => { this.generateLink('id', item, _.first(item.image)?.image_url, userid, item?.user?.tokens) }}
                                            >
                                                <View style={{ flexDirection: 'row', marginBottom: 6 }}>
                                                    <Entypo style={{ top: 8 }} name='forward' color='grey' size={22} />
                                                    {
                                                        item.nb_partageur !== 0 ?
                                                            <Text style={{
                                                                fontSize: 10, left: 3, bottom: -12, fontWeight: 'bold',
                                                                borderRadius: 100, color: "black"
                                                            }}>{item.nb_partageur} </Text>
                                                        : null
                                                    }
                                                </View>
                                                <Text style={{ fontSize: 8, fontWeight: 'bold' }}>Partager</Text>
                                            </TouchableOpacity>
                                        </View>
                                        {
                                            item.category.toLowerCase() !== "terrain" && item.category !== "" && item.category.toLowerCase() !== "boutique"
                                                && item.category.toLowerCase() !== "local administratif" && item.category.toLowerCase() !== "bureau"
                                                && item.category.toLowerCase() !== "autre" ?
                                                <>
                                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <Icon name='bed' color='grey' size={20} />
                                                            <Text style={{ fontSize: 13, color: 'grey' }}>{item.nb_room} Chambres </Text>
                                                        </View>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{item.nb_toilet} douches</Text>
                                                        </View>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{item.nb_parlour} salons </Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%' }}>
                                                        {item.nb_kitchen != 0 ?
                                                            <View style={{ flexDirection: "row" }}>
                                                                <MaterialIcons name='kitchen' color='grey' size={20} style={{ marginLeft: '2%' }} />
                                                                <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{item.nb_kitchen} Cuisine(s)</Text>
                                                            </View> : null
                                                        }
                                                        {item.fence == true ? (
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Icon name='slideshare' color='grey' size={20} />
                                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>Dans la barrière</Text>
                                                            </View>) : null
                                                        }
                                                        {item.garden == true ? (
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Icon name='pied-piper-alt' color='grey' size={20} />
                                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>jardin</Text>
                                                            </View>) : null
                                                        }
                                                        {item.parking == true ? (
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Icon name='car' color='grey' size={20} />
                                                                <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>parking</Text>
                                                            </View>) : null
                                                        }

                                                    </View>
                                                </> :
                                                item.category.toLowerCase() == "terrain" ?
                                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <Icon name='stop' color='grey' size={20} />
                                                            <Text style={{ fontSize: 13, color: 'grey' }}>Dimension: {item.dimension} mètres carrés</Text>
                                                        </View>
                                                    </View> : null
                                        }
                                    </View>
                                    <Divider style={{ backgroundColor: 'grey', height: 15, marginTop: '3%', width: width_ }} />
                                </View>
                            )}
                            ListFooterComponent={this.renderLoader}
                            onEndReached={this.loadMoreItem}
                            onEndReachedThreshold={0.5}
                            keyExtractor={item => item.id}
                            ListEmptyComponent={
                                <View style={{ alignItems: 'center', alignSelf: 'center', }}>
                                    <ActivityIndicator style={{ marginTop: '15%' }} size="large" color="#7c3325" />
                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: "35%" }}>
                                        <Text style={{ marginTop: 30 }}>Chargement </Text>
                                        <AnimatedEllipsis numberOfDots={3}
                                            style={{
                                                color: '#7c3325',
                                                fontSize: 50,
                                            }}
                                        />

                                    </View>
                                </View>
                            }
                            refreshing={this.state.refresh}
                            initialNumToRender={10}
                            onRefresh={this.handleRefesh}
                        />
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        borderColor: 'orange',
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderTopWidth: 2,
        padding: 15,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '95%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    textStyle: {
        color: "#010102",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    buttonstyle1: {
        width: '30%',
        borderRadius: 7,
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: '#dc7227',
        textAlign: 'center',
        height: 30,
        justifyContent: 'center'
    },
    buttonstyle: {
        width: '40%',
        borderRadius: 7,
        backgroundColor: '#dc7227',
        textAlign: 'center',
        height: 30,
        justifyContent: 'center'
    },
    images: {
        width: width_, height: height_ * 0.6,
        borderColor: "white", borderWidth: 1,
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    preloader: {
        left: 0,
        right: 0,
        top: height_ / 2,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    }
});