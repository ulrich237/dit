import React, { Component } from "react";
import {
    View, Share, Image, Text, Picker, TouchableOpacity, Alert, Platform, Pressable,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Easing, ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import { Avatar, Divider } from "react-native-elements";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from 'react-native-vector-icons/AntDesign'
import CalendarPicker from 'react-native-calendar-picker';
import moment from 'moment';
import NumericInput from 'react-native-numeric-input'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import axios from 'axios'
import _ from 'underscore';
import AsyncStorage from '@react-native-async-storage/async-storage';

import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import ImageViewer from 'react-native-image-zoom-viewer';
import Notification from '../../../functions/notification';
import DeviceInfo from 'react-native-device-info';
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';
import PostData from '../../../functions/postData'
import { Examples, InlineGallery } from '@shoutem/ui';
import call from 'react-native-phone-call';


var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

var dat = {}

export default class HouseDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            house: this.props.route.params?.data,
            categorys: [],
            users: [],
            outil_houses: [],
            neighbours: [],
            provinces: [],
            likes: [],
            reviews: [],
            nbre_personne: 0,
            modalVisible2: false,
            selectedStartDate: null,
            selectedEndDate: null,
            dates_occupers: [],
            searchelt: '',
            reservation_user: {
                pending: false,
                confirm: false,
                error: false,
                checkin_time: null,
                checkout_time: null,
                user: null,
                house_book: null
            },
            userid: null,
            houseid: Number(this.props.route.params?.id),
            imageshouses: this.props.route.params?.data?.image, activeSlide: 1, etat: false, images_: [],
            allHouses: [], user_:{}
        }
    }



    generateLink = async (param, value, url, user, token) => {
        var lien = null
       // value?.lien == null || value?.lien == '' ?
            link_ = await dynamicLinks()
                .buildShortLink(
                    {
                        link:'https://imobbis.page.link/message/' + value?.id,
                        domainUriPrefix: 'https://imobbis.page.link',
                        android: {
                            packageName: 'com.imobbis.app',
                            minimumVersion: '19'
                            //version minmale ici
                            //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                        },
                        /*ios: {
                             bundleId: AppConfig.getIOSBundle(),
                             appStoreId: AppConfig.getIOSAppId(),
                         }, */
                        social: {
                            title: value?.category + ' à ' + value?.publish_type,
                            descriptionText: value?.description,
                            imageUrl: url
                        }
                        /* navigation: {
                             forcedRedirectEnabled: true,
                         }*/
                    },
                    //firebase.dynamicLinks.ShortLinkType.SHORT
                )
                .then((link) => {

                    //DynamicLinkStore.currentUserProfileLink = link;
                    console.log(link)
                    console.log(value?.id)
                    lien = link.slice(link.lastIndexOf('/') + 1)
                    console.log(lien)
                  
                    new PostData().partageur(value?.partageurs, user, value?.id)

                    new Notification().sendNotitfications(token, this.state.user_?.username,
                    'A partagé votre publication.', 'hotboy://message/'+ value?.id)

                    this.onShare(link)

                   /* lien ?
                        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + value?.id, {
                            'lien': lien
                        }).catch(error => console.log(error)).then(response => {
                           // this.onShare(link),
                             //   new PostData().partageur(value?.partageurs, user, value?.id)
                        })
                            .catch(error => console.log(error))
                    : null*/
                })

                .catch((err) => {
                    console.log(err);
                    //DynamicLinkStore.profileLinkLoading = false;
                })
          //  : (new PostData().partageur(value?.partageurs, user, value?.id), this.onShare('https://imobbis.page.link/' + value?.lien))

    }


    onShare = async (data) => {
        try {
            const result = await Share.share({
                message: data,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };


    calling = (args) =>{
        call(args).catch(console.error)
  }



    componentDidMount() {

        this.displayData()
        axios.get("https://imobbis.pythonanywhere.com/new/house4").then(res => {
            this.setState({
                /*house: this.props.route.params?.data == undefined ?
                    _.find(res.data, { id: this.state.houseid }) :
                    this.props.route.params?.data,*/
                 allHouses: res.data
            })
        }).catch(err => console.log(err))


        axios.get("https://imobbis.pythonanywhere.com/new/house4un/" + this.state.houseid).then(res => {
            this.setState({
                house: res.data,
                imageshouses:res.data?.image

            })
        }).catch(err => console.log(err))



        /*axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: _.filter(response.data, { house: this.state.houseid }) }),)
            .catch(error => this.setState({ error, isLoading: false }));*/

        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(resct => {
            this.setState({ categorys: resct.data })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            this.setState({ users: resu.data })
        }).catch(err => console.log(err));


        axios.get(`https://imobbis.pythonanywhere.com/new/like`)
            .then(res => {
                this.setState({ likes: res.data })
            });

        axios.get(`https://imobbis.pythonanywhere.com/new/house_bookin`)
            .then(res => {

                axios.get(`https://imobbis.pythonanywhere.com/new/personal_us`)
                    .then(resus => {
                        let dates_occupers = [];
                        _.filter(res.data, { house_book: this.state.houseid, pending: false, confirm: false, error: false }).map(reserv => dates_occupers.push({ checkin_time: reserv.checkin_time, checkout_time: reserv.checkout_time }));
                        _.filter(resus.data, { house: this.state.houseid }).map(reserv => dates_occupers.push({ checkin_time: reserv.checkin_time, checkout_time: reserv.checkout_time }));
                        this.setState({ dates_occupers })
                    });
            })
            .catch(err => { console.log(err) });

    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.likes !== this.state.likes) {
            axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                .then(res => {
                    this.setState({ likes: res.data })
                });
        }
    }


    pagination(entries) {
        const { activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries?.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'white' }}
                dotStyle={{
                    width: 5,
                    height: 5,
                    borderRadius: 2,
                    marginHorizontal: 2,
                    backgroundColor: '#dc7e27'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.4}
            />
        );
    }



    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            let users = await AsyncStorage.getItem('myuser');

            this.setState({ userid: json_id, user_: JSON.parse(users)})

        } catch { error => alert(error) };
    }


    save_like = (homeid, user, prop) => {
        let exist = _.find(this.state.likes, { user: user, house: homeid }) ? true : false;
        let item = exist == false ? {
            house: Number(homeid),
            user: Number(user),
        } : { id: _.find(this.state.likes, { user: user, house: homeid })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/like`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/like/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                    .then(res => {
                        exist == false ? new Notification().sendNotitfications(prop?.tokens, 
                            this.state.user_?.username, 'A aimé votre publication.', 'hotboy://message/'+ homeid) : null
                        this.setState({ likes: res.data })
                    });

            }).catch(error => {
                console.log("error " + error)
            })
    }

    onDateChange = (date, type) => {
        if (type === 'END_DATE' && date !== null) {
            let exist = false; let datefin = date;
            if (this.state.dates_occupers.length !== 0) {
                while (new Date(date).getTime() >= new Date(this.state.selectedStartDate).getTime()) {
                    this.state.dates_occupers.map(da_ =>
                        moment(date).isBetween(new Date(new Date(da_.checkin_time).getTime() - (86400 * 1000)).toISOString(), da_.checkout_time) ? (exist = true, console.log('je suis à true')) : null
                    );
                    date = new Date(new Date(date).getTime() - (86400 * 1000)).toISOString()
                }
            }
            let reservation_user = {
                pending: false,
                confirm: false,
                error: false,
                checkin_time: this.state.selectedStartDate,
                checkout_time: datefin,
                user: this.state.userid,
                house_book: this.state.houseid,
                identification: [],
            }
            exist == false ?
                this.setState({
                    selectedEndDate: datefin,
                    reservation_user: reservation_user
                    //modalVisible2: false
                }) : (this.setState({ searchelt: "guest" }),
                    alert("la plage selectionnée contient des dates déjà prise par un client !"));
        }

        if (type === 'START_DATE') {
            let reservation_user = {
                pending: false,
                confirm: false,
                error: false,
                checkin_time: null,
                checkout_time: null,
                user: this.state.userid,
                house_book: this.state.houseid,
                identification: [],
            }
            this.setState({
                selectedStartDate: date,
                reservation_user: reservation_user
            });
        }
    }


    postLien = (user, data) => {
       // axios.get("https://imobbis.pythonanywhere.com/new/messagin").then((reslh) => {
            //_.find(reslh.data, { user_receiver: user?.id, house: data?.id, user_sender: this.state.userid }) ? null :
                axios.post("https://imobbis.pythonanywhere.com/new/messagin",
                    {
                        'user_receiver': user?.id,
                        'user_sender': this.state.userid,
                        'house': data?.id,
                        'view': false,
                    })
                    .then(response => {
                        new Notification().sendNotitfications(user?.tokens, 
                            this.state.user_?.username, 
                            'Vous a envoyé un message pour resérvation.', 'hotboy://reservation')
                    })
                    .catch(error => alert(error))
      //  }).catch(err => console.log(err));
    }




    firstCarousel = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                enabled={false}
                onPress={() => this.setState({ etat: true })}
            >
                <FastImage
                    source={{ uri: item.image_url || item.image }}
                    imgStyle={styles.images}
                    style={styles.images}
                    enableScaling={false}
                    easingFunc={Easing.ease}
                />
            </TouchableOpacity>
        );
    }


    render() {
        const { house, modalVisible2, imageshouses,
            users, userid, houseid
            , likes, selectedEndDate, selectedStartDate,
            dates_occupers, reservation_user, etat } = this.state;

        const startDate = selectedStartDate ? moment(selectedStartDate).format('DD/MM/YYYY') : '';
        const endDate = selectedEndDate ? moment(selectedEndDate).format('DD/MM/YYYY') : '';

        var image = []
        imageshouses?.map(
            ima_ => image.push({ url: ima_.image_url || ima_.image, height: 200, with: 350 })
        )

        var martop = Platform.OS == 'ios' ? DeviceInfo.hasNotch() ? 40 : 15 : DeviceInfo.hasNotch() ? 25 : 0

        return (
            <View style={{ flex: 1 }}>
                <Modal visible={etat} transparent={true}>
                <View style={{backgroundColor: 'black', flexDirection: 'row-reverse' }}>
                        <TouchableOpacity
                            style={{ ...styles.heart, margin: 5, marginTop: 15 }}
                            onPress={() => {
                                this.setState({ etat: false });
                            }}
                        >
                            <Entypo name='cross' size={32}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, backgroundColor: 'white' }}>
                        <ImageViewer imageUrls={image}
                            renderHeader={() => (
                                <Text style={{textAlign:'center' ,borderWidth: 5, borderColor: 'white', color:'white' }}>{""}</Text>
                            )}
                            enableImageZoom={true}
                            onDoubleClick={() => this.setState({ etat: false })}
                        />
                    </View>
                </Modal>

                <ScrollView>
                    <View>
                        <Carousel
                            ref={(c) => { this._carousel = c; }}
                            data={imageshouses}
                            renderItem={this.firstCarousel}
                            sliderWidth={width_}
                            itemWidth={width_}
                            autoplay={true}
                            onSnapToItem={(index) => this.setState({ activeSlide: index })}
                        />
                        {this.pagination(imageshouses)}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', bottom: height_ * 0.4, }} >
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                                style={{ ...styles.heart, margin: 5, marginTop: 15 }}>
                                <Entypo name='cross' size={32} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.save_like(house.id, userid, _.find(users, { id: house?.user }))}
                                style={{ ...styles.heart, margin: 7, marginTop: 15 }}>
                                <FontAwesome
                                    name={!_.find(likes, { house: house?.id, user: userid }) ? 'heart-o' : 'heart'}
                                    size={30} color='#7c3325'
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>

                            <View>
                                <Text numberOfLines={1} style={{ fontWeight: 'bold', fontSize: 18 }}>
                                    {house?.name}
                                </Text>
                            </View>


                            <TouchableOpacity style={{ marginTop: -10 }}
                                onPress={() => { this.generateLink('id', house, _.first(this.state.imageshouses)?.image_url, userid) }
                                }
                            >
                                <View style={{ flexDirection: 'row', }}>
                                    <Entypo style={{ top: 8 }} name='forward' color='grey' size={35} />
                                </View>
                                <Text style={{ marginRight: 5, fontSize: 8, fontWeight: 'bold' }}>Partager</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 18 }}>Le gérant</Text>

                        <TouchableOpacity
                            activeOpacity={0.8}
                            enabled={false}
                            onPress={() => {
                                this.props.navigation.navigate('AglaProfile', {
                                    agla: _.find(users, { id: house?.user }) || house?.user,
                                    countrie: house?.user?.countrie,
                                    houses: _.filter(this.state.allHouses, (value) => value?.user?.id == house?.user?.id),
                                    content_ct: 15
                                })
                            }}>
                            <View style={{ flexDirection: "row", justifyContent: 'space-evenly' }}>
                                {
                                    _.size(this.state.allHouses) == 0 ? null :
                                        <Text style={{ color: '#7c3325', letterSpacing: 2 }}>Voir le profil</Text>
                                }
                            </View>
                            <View style={{ flexDirection: "row", margin: 6, paddingTop: 7 }} >
                                <Avatar
                                    size={40}
                                    rounded
                                    source={{ uri: house?.user?.profile_url || _.find(users, { id: house?.user })?.profile_image }} />
                                <View style={{ width: '85%', marginLeft: 10 }}>
                                    <Text style={{ fontWeight: "bold", color: '#7c3325', fontSize: 14, letterSpacing: 2 }}>
                                        {house?.category}  {!house?.colocation ? 'à ' + house?.publish_type : 'en collocation'} est
                                        géré(e) par {house?.user?.username || _.find(users, { id: house?.user })?.username}
                                    </Text>
                                    <Text style={{ fontWeight: "bold", color: 'red', fontSize: 18, textAlign: 'center' }}>
                                        Prix: {house?.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} {house?.country?.monnaie} {house?.category?.toLowerCase() == "terrain" ? '/ m²' : house?.publish_type == 'vendre' ? '' : house?.is_immeuble == true ? '/ jour' : '/ mois'} </Text>
                                </View>
                            </View >
                        </TouchableOpacity>

                        <View style={{ bottom: 15, marginLeft: 5, marginRight: '10%', marginTop: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginBottom: 5 }}>Description</Text>
                            <Text style={{ color: 'grey' }} >"{house?.description}"</Text>
                        </View>

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        {house?.disponible ?
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name='warning' color="red" size={15} style={{ marginTop: 2 }} />
                                <Text style={{ fontSize: 15, fontWeight: "bold", textAlign: 'center', color: 'red' }}>
                                    Indisponible
                                </Text>

                            </View>
                            : null
                        }

                        {
                            house?.colocation ?
                                <View>
                                    <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 18 }} >Préference</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='user' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />

                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> sexe: {house?.sexe_colocataire} </Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='minus' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />
                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> tranche d'âge: {house?.age_colocataire == '' ? 'Tous' : house?.age_colocataire}</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='institution' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />
                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> réligion: {house?.religion_colocataire == '' ? 'Toutes' : house?.religion_colocataire}</Text>
                                    </View>
                                </View>
                                : null
                        }

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 18 }} >Constitution</Text>

                        {
                            house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                && house?.category.toLowerCase() !== "autre" ?
                                <View style={{ bottom: 12, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10 }}>
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='bed' color='grey' size={20} />
                                            <Text style={{ fontSize: 13, color: 'grey' }}>{house?.nb_room} Chambres </Text>
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{house?.nb_toilet} douche(s)</Text>
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{house?.nb_parlour} salon(s) </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        {house?.nb_kitchen != 0 ?
                                            <View style={{ flexDirection: "row" }}>
                                                <MaterialIcons name='kitchen' color='grey' size={20} style={{ marginLeft: '2%' }} />
                                                <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{house?.nb_kitchen} Cuisine(s)</Text>
                                            </View> : null
                                        }
                                        {house?.fence == true ? (
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='slideshare' color='grey' size={20} />
                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>Dans la barrière</Text>
                                            </View>) : null
                                        }
                                        {house?.garden == true ? (
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='pied-piper-alt' color='grey' size={20} />
                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>jardin</Text>
                                            </View>) : null
                                        }
                                        {house?.parking == true ? (
                                            <View style={{ flexDirection: 'row' }}>

                                                <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>+ parking</Text>
                                            </View>) : null
                                        }
                                    </View>
                                </View> : house?.category.toLowerCase() == "terrain" ?
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='stop' color='grey' size={20} />
                                            <Text style={{ fontSize: 13, color: 'grey' }}>Dimension: {house?.dimension} mètres carrés </Text>
                                        </View>
                                    </View> : null

                        }


                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 18 }} >Fournitures</Text>
                        {
                            house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                && house?.category.toLowerCase() !== "autre" ?
                                <View style={{ bottom: 12, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10 }}>

                                    <View style={{ width: '47%', flexDirection: 'row', marginTop: 7 }}>
                                        <AntDesign name="wifi" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.wifi == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.wifi == true ? "orange" : "silver" }} >Wifi</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <Ionicon name="water" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.hot_water == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.hot_water == true ? "orange" : "silver" }} >Eau chauffante</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <FontAwesome5 name="fan" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.climatisation == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.climatisation == true ? "orange" : "silver" }} >Climatisation</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="fridge" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.freezer == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.freezer == true ? "orange" : "silver" }} >Congélateur</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="toaster-oven" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.cooker == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.cooker == true ? "orange" : "silver" }} >Cuisinière</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <FontAwesome name="tv" size={20} style={{ marginLeft: 8, marginRight: 15, color: house?.tv_all_bedroom == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.tv_all_bedroom == true ? "orange" : "silver" }} >Télévision</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="baby-bottle" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.cooker == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.gym == true ? "orange" : "silver" }} >Salle de sport</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="elevator-passenger" size={20} style={{ marginLeft: 8, marginRight: 15, color: house?.tv_all_bedroom == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.elevator == true ? "orange" : "silver" }} >Ascenceur</Text>
                                    </View>
                                </View>
                                : null
                        }

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ marginLeft: 5, marginBottom: 10, marginTop: 7 }}>
                            <Text style={{ fontSize: 16, fontWeight: "500" }} >Situé à {house?.neighbor + ", " + house?.city?.name + ", " + house?.country?.name}</Text>
                        </View>
                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6, marginTop: 7 }} />
                        {
                            house?.is_immeuble ?
                                house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                    && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                    && house?.category.toLowerCase() !== "autre" ?
                                    <View style={{ bottom: 10, marginLeft: 10, marginTop: 7 }}>
                                        <Text style={{ marginBottom: 8, fontWeight: 'bold', fontSize: 18 }} >Régles à suivre</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialCommunityIcons name="party-popper" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.events_allowed == true ? "Fêtes autorisées" : "Pas de fêtes autorisées"}
                                            </Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialCommunityIcons name="smoking" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.smooking_allowed == true ? "Vous pouvez fumer à l'intérieur" : "Interdit de fumer à l'intérieur"}
                                            </Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialIcons name="pets" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.pets_allowed == true ? "Animaux domestiques autorisés" : "Animaux domestiques interdits"}</Text>
                                        </View>
                                    </View>
                                    : null
                                : null
                        }
                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6, marginBottom: 20 }} />
                    </View>

                    <View style={{ height: 70 }} ></View>
                </ScrollView>

                <View style={styles.footer}>
                    {
                        house ?
                            <View style={{
                                borderRadius: 10,
                                backgroundColor: '#dc7e27'
                                , flexDirection: 'row',
                                shadowColor: "black",
                                shadowOffset: {
                                    width: 0, height: 6,
                                }, paddingVertical: 3,
                                shadowOpacity: 0.05,
                            }}>
                                {
                                    //house?.rent_per_day == true ?
                                    <TouchableOpacity style={{
                                        alignItems: 'center', width: '35%'
                                    }}
                                        disabled={true}
                                        onPress={() => this.setState({ modalVisible2: true, searchelt: "guest" })}
                                    >
                                        <Entypo name='notification' size={25} color='grey' />
                                        <Text style={{ color: "black", fontWeight: "bold", fontSize: 15 }}>{house?.publish_type == 'louer' ? ' Réserver ' : 'Acheter'}</Text>
                                    </TouchableOpacity>
                                    //: null   notification
                                }

                                <TouchableOpacity style={{
                                    alignItems: 'center', width: '35%'
                                }}
                                    onPress={() => {
                                        
                                            new PostData().reservation(house.reserveurs, userid, house.id),
                                            house?.user?.date_experiration == null || moment(house?.user?.date_experiration) >= moment(new Date()) ?
                                                this.calling({
                                                    number:'+' + house?.user?.countrie?.indicatif?.toString() + house?.user?.phone_number.toString(),
                                                    prompt:false
                                                })
                                                : new Notification().sendNotitfications(house?.user?.tokens, 
                                                    this.state.user_?.username, 
                                                    'A essayé de vous joindre, veuillez payer votre abonnement.', 'hotboy://message/'+ house?.id)
                                    }}
                                >

                                    <Entypo name='old-phone' size={25} color='grey' />
                                    <Text style={{ color: house?.user?.date_experiration == null || moment(house?.user?.date_experiration) >= moment(new Date()) ? "black" : '#8e8e8e', fontWeight: "bold", fontSize: 15 }}>Contacter</Text>

                                </TouchableOpacity>
                                {
                                    house?.user?.id !== this.state.userid ?
                                        <TouchableOpacity style={{
                                            alignItems: 'center', width: '35%'
                                        }}
                                            onPress={() => {
                                                this.postLien(house?.user, house),
                                                    this.props.navigation.navigate(
                                                        'MessagesIni', {
                                                        data: house,
                                                        user: house?.user
                                                    })
                                            }}




                                        >

                                            <Entypo name='new-message' color='grey' size={25} />
                                            <Text style={{ color: "black", fontWeight: "bold", fontSize: 15 }}> Ecrire </Text>

                                        </TouchableOpacity> : null
                                }
                            </View>
                            : <View style={styles.preloader}>
                                <ActivityIndicator size="large" color="#7c3325" />
                            </View>
                    }
                </View>

                <Modal animationType="slide"
                    transparent={true}
                    visible={modalVisible2}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setModalVisible2(!modalVisible2);
                    }}>

                    <View style={{ ...styles.centeredView, marginTop: 53 }}>
                        <View style={{ ...styles.modalView, marginTop: this.state.searchelt == "date" ? height_ * 0.28 : height_ * 0.72 }}>
                            <View style={{ flexDirection: 'row' }} >
                                <TouchableOpacity
                                    style={{ width: '10%' }}
                                    onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                                >
                                    <Entypo name="cross" size={25} color='red' />
                                </TouchableOpacity>
                            </View>
                            <ScrollView>
                                {
                                    this.state.searchelt == "date" ?
                                        <View style={{ width: "90%" }} >
                                            <Text style={{ fontSize: 14, fontWeight: 'bold', marginBottom: 5, color: '#dc7227', textAlign: 'center' }}>{startDate == '' ? 'Selectionner une période de location' : 'Date choisie ' + startDate + ' - ' + endDate + ' . '}</Text>
                                            <CalendarPicker
                                                weekdays={['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim']}
                                                months={['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Dècembre']}
                                                previousTitle="Précédent"
                                                nextTitle="Suivant"
                                                startFromMonday={true}
                                                allowRangeSelection={true}
                                                minDate={moment(selectedStartDate).format('DD-MM-YYYY')}
                                                maxDate={moment(selectedEndDate).format('DD-MM-YYYY')}
                                                todayBackgroundColor="orange"
                                                scaleFactor={375}
                                                selectedDayColor="#dc7227"
                                                selectedDayTextColor="#FFFFFF"
                                                selectedDayStyle={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid' }}
                                                disabledDatesTextStyle={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid' }}
                                                disabledDates={date => {
                                                    if (dates_occupers.length !== 0) {
                                                        let value = false;
                                                        dates_occupers.map(da_ =>
                                                            date.isBetween(new Date(new Date(da_.checkin_time).getTime() - (86400 * 1000)).toISOString(), new Date(new Date(da_.checkout_time).getTime() + (86400 * 1000)).toISOString()) ?
                                                                value = true : null
                                                        );
                                                        return value
                                                    } else {
                                                        return false
                                                    }
                                                }}
                                                //scrollable={true}
                                                onDateChange={this.onDateChange}
                                            />

                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginLeft: 10, marginRight: 10, marginBottom: 10 }}>
                                                <TouchableOpacity style={styles.buttonstyle}
                                                    onPress={() => this.setState({ searchelt: "guest" })}
                                                >
                                                    <Text style={{ fontSize: 12, textAlign: "center", color: "white" }}>Retour</Text>
                                                </TouchableOpacity>
                                                {this.state.reservation_user.checkout_time !== null ?
                                                    <TouchableOpacity style={styles.buttonstyle}
                                                        onPress={() => {
                                                            this.props.navigation.navigate('Reservation1', {
                                                                userid: userid, houseid: houseid,
                                                                reservation_user: reservation_user, nbre_personne: this.state.nbre_personne
                                                            }), this.setState({ modalVisible2: false })
                                                        }}
                                                    >
                                                        <Text style={{ fontSize: 12, textAlign: "center", color: "white" }}>Suivant</Text>
                                                    </TouchableOpacity> : null}
                                            </View>
                                        </View>
                                        :
                                        this.state.searchelt == "guest" ?
                                            <>
                                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 5, marginLeft: 10 }}>
                                                    <View style={{ width: "70%" }}>
                                                        <Text style={{ fontSize: 16 }}> Nombre de personnes qui viendront</Text>
                                                    </View>
                                                    <View >
                                                        <NumericInput
                                                            value={this.state.nbre_personne}
                                                            onChange={nbre_personne => { this.setState({ nbre_personne }) }}
                                                            onLimitReached={(isMin, msg) => console.log(isMin, msg)}
                                                            totalWidth={80}
                                                            totalHeight={35}
                                                            iconSize={10}
                                                            step={1}
                                                            minValue={0}
                                                            valueType="real"
                                                            rounded editable={true}
                                                            textColor="#7c3327"
                                                            iconStyle={{ color: "black" }}
                                                            rightButtonBackgroundColor="white"
                                                            leftButtonBackgroundColor="white"
                                                        />
                                                    </View>
                                                </View>

                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginLeft: 10, marginBottom: 10 }}>
                                                    <TouchableOpacity style={styles.buttonstyle}
                                                        onPress={() => this.setState({ nbre_personne: 0, modalVisible2: !modalVisible2 })}
                                                    >
                                                        <Text style={{ fontSize: 12, textAlign: "center", color: "black", fontWeight: 'bold' }}>Réinitialiser</Text>
                                                    </TouchableOpacity>
                                                    {this.state.nbre_personne > 0 ?
                                                        <TouchableOpacity style={styles.buttonstyle}
                                                            onPress={() => { this.setState({ searchelt: "date" }) }}
                                                        >
                                                            <Text style={{ fontSize: 15, textAlign: "center", color: "black", fontWeight: 'bold' }}>Suivant</Text>
                                                        </TouchableOpacity> : null}
                                                </View>
                                            </>
                                            : null}
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    footer: {
        padding: 4,
        position: 'absolute',
        backgroundColor: 'transparent',
        bottom: 0,
        width: '100%',
        marginBottom: Platform.OS === 'ios' ? 20 : 10,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    preloader: {
        //margintop: '50%',
        //bottom: 0,
        //position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalView: {
        backgroundColor: "white",
        borderTopRightRadius: 15, borderTopLeftRadius: 15,
        padding: 8, marginTop: height_ * 0.63,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '100%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    textStyle: {
        color: "#010102",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    buttonstyle1: {
        width: '30%',
        borderRadius: 7,
        backgroundColor: 'white',
        borderWidth: 2,
        // borderColor: '#dc7227',
        textAlign: 'center',
        height: 30,
        justifyContent: 'center'
    },
    buttonstyle: {
        borderRadius: 10, textAlign: "center",
        backgroundColor: 'orange', alignItems: "center"
        , flexDirection: 'row', justifyContent: "center",
        shadowColor: "black",
        shadowOffset: {
            width: 0, height: 6,
        }, paddingBottom: 10, paddingTop: 5,
        shadowOpacity: 0.2, width: "27%", height: 35,
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 24,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    images: {
        width: width_, height: height_ * 0.4,
        borderColor: "white", borderWidth: 0.5,
    }
})

//  https://fireship.io/lessons/pwa-to-play-store/




