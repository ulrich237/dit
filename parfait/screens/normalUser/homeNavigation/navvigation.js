import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Houses from './houses';
import Home from '../home';
import DetailUser from '../DetailUser';
import AglaProfile from '../../profiles/aglaProfile';
import HouseDetails from './houseDetails';
import Profil from '../../auth/socialAuth/update_profiluser';
import Messages from '../Messages';
import MessagesIni from '../Messages_in';
import PostI from '../PostI';
import Feed from '../../feed/feed'
import filter_Feed from '../filtrer_feed';
import Reservation1 from '../reservation/Etape1_reservation_imobbis';
import Reservation2 from '../reservation/Etape2_reservation_imobbis';
import Reservation3 from '../reservation/Etape3_reservation_imobbis';
import Reservation4 from '../reservation/Etape4_reservation_imobbis';
import Reservation5 from '../reservation/Etape5_reservation_imobbis';

const Stack = createStackNavigator();

export default class UserNavigation extends Component {
    constructor(props){
        super(props)
    }

    //<Stack.Screen name='Profil' component={Profil} options={{headerShown: false}}/>
    
    render(){
        return(
        <Stack.Navigator>
            <Stack.Screen name='home' component={Home} options={{headerShown: false}}/>
            <Stack.Screen name='houses' component={Houses} options={{headerShown: false}}/>
            <Stack.Screen name='MessagesIni' component={MessagesIni} options={{headerShown: false}}/>
            <Stack.Screen name='Messages' component={Messages} options={{headerShown: false}}/>
            <Stack.Screen name='AglaProfile' component={AglaProfile} options={{headerShown: false}}/>
            <Stack.Screen name='HouseDetails' component={HouseDetails} options={{headerShown: false}}/>
            <Stack.Screen name='PostI' component={PostI} options={{headerShown: false}}/>
            <Stack.Screen name='filter_Feed' component={filter_Feed} options={{headerShown: false}}/>
            <Stack.Screen name='Reservation1' component={Reservation1} options={{headerShown: false}}/>
            <Stack.Screen name='Reservation2' component={Reservation2} options={{headerShown: false}}/>
            <Stack.Screen name='Reservation3' component={Reservation3} options={{headerShown: false}}/>
            <Stack.Screen name='Reservation4' component={Reservation4} options={{headerShown: false}}/>
            <Stack.Screen name='Reservation5' component={Reservation5} options={{headerShown: false}}/>
            <Stack.Screen name='DetailUser' component={DetailUser} options={{headerShown: false}}/>
        </Stack.Navigator>
        )
    }
    
}
