import React from 'react';
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Switch} from "react-native";
import { Avatar } from 'react-native-elements/dist/avatar/Avatar';
import Icon from "react-native-vector-icons/FontAwesome";
import _ from 'underscore';
import { Divider } from 'react-native-elements'
import moment from 'moment';
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Reservation1 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reservation_user: this.props.route.params.reservation_user,
            userid: Number(this.props.route.params.userid),
            houseid: Number(this.props.route.params.houseid),
            nbre_personne: Number(this.props.route.params.nbre_personne),
            house: null
        }
    }

    componentDidMount() {
        axios.get("https://imobbis.pythonanywhere.com/house/").then(res => {
            this.setState({ house: _.find(res.data, { id: this.state.houseid }) })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(resct => {
            this.setState({ categorys: resct.data })
        }).catch(err => console.log(err));
    }

    getjours = () => {
        let tmp = new Date(this.state.reservation_user.checkout_time) - new Date(this.state.reservation_user.checkin_time);  //Nombre de millisecondes entre les dates
        let diff = {};
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        diff.days = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        return diff.days+1
    }

    render() {
        const { house, houseid, userid, reservation_user, nbre_personne } = this.state;
        return (
            <View >
                <ScrollView >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: '5%', }}>
                        <View>
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: 'red', letterSpacing: 2, marginBottom: '5%' }}>{house?.name} </Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', letterSpacing: 2 }}>
                            {house?.renting_price} {house?.rent_per_day == true ? "XAF/Jour" : "XAF/Mois"}
                            </Text>
                        </View>
                        <Image source={{ uri: house?.photo_1 }} style={{ height: height_*0.15, width: width_*0.4 }} />

                    </View>
                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: '5%' }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12 }}>DU</Text>
                            <Text style={{ marginTop: '1%', color: '#7c3325', fontSize: 12 }}>
                                { moment(reservation_user.checkin_time).format('DD/MM/YYYY') } 
                            </Text>
                        </View>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12 }}>AU</Text>
                            <Text style={{ marginTop: '1%', color: '#7c3325', fontSize: 12 }}>
                            { moment(reservation_user.checkout_time).format('DD/MM/YYYY') }
                            </Text>
                        </View>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 12 }}>pour</Text>
                            <Text style={{ marginTop: '1%', color: '#7c3325', fontSize: 11 }}>{nbre_personne} personne(s) </Text>
                        </View>
                    </View>
                    <Divider style={{ backgroundColor: 'black', marginTop: '3%', marginLeft: '6%', marginRight: '6%' }} />
                    <View style= {{margin: '5%'}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', marginLeft: '3%', marginTop: '3%' }}>Détail sur la reservation</Text>
                        <View style={{ flexDirection: 'row', marginTop: '4%', marginLeft: '5%' }}>
                            <Text style={{fontSize: 12, color: '#7c3325', fontWeight: 'bold'}}>{house?.renting_price} * {this.getjours()} Jour(s)</Text>
                            <Text style={{ marginLeft: '40%', fontSize: 13, fontWeight: 'bold', letterSpacing: 2 }}>{house?.renting_price ? house?.renting_price * this.getjours() : '0'} XAF</Text>
                        </View>

                    </View>
                    <TouchableOpacity 
                       onPress={() => this.props.navigation.navigate('Reservation2', {userid: userid, houseid: houseid, reservation_user: reservation_user})}
                      style={{ backgroundColor: '#dc7e27', marginTop: '18%', height: 50, marginLeft: height_*0.3, marginRight: "5%", borderRadius: 12, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>Suivant</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>

        );
    }
}




