import React from 'react';
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, KeyboardAvoidingView, Button, Modal, ScrollView, StyleSheet, Dimensions, Switch
} from "react-native";
import { Avatar } from 'react-native-paper'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import AutogrowInput from 'react-native-autogrow-input';
import _ from 'underscore';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Divider } from 'react-native-elements'
import moment from 'moment';
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);


export default class Reservation4 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reservation_user: this.props.route.params.reservation_user,
            userid: Number(this.props.route.params.userid),
            houseid: Number(this.props.route.params.houseid),
            house: null,
            user: null,

            content: '',
            target_ct: null,
            target_id: null,
            bailleurs: [],
            agents: [],
            agence_imobieres: [],
            messages: []
        }
    }

    componentDidMount() {
        axios.get("https://imobbis.pythonanywhere.com/house/").then(res => {
            this.setState({ house: _.find(res.data, { id: this.state.houseid }) })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/user/").then(resu => {
            this.setState({ user: _.find(resu.data, { id: this.state.userid }) })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/messaging/`,)
            .then(res => this.setState({ messages: _.filter(res.data, { user: this.state.userid }) }))

        axios.get("https://imobbis.pythonanywhere.com/landlord/").then(res => {
            this.setState({ bailleurs: res.data })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/agent/agent").then(res => {
            this.setState({ agents: res.data })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/agent/company").then(res => {
            this.setState({ agence_imobieres: res.data })
        }).catch(err => console.log(err));

    }

    refresh_messaging() {
        axios.get(`https://imobbis.pythonanywhere.com/messaging/`,)
            .then(res => this.setState({ messages: _.filter(res.data, { user: this.state.userid }) }))
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.messages !== this.state.messages) {
            axios.get("https://imobbis.pythonanywhere.com/messaging/").then(res => {
                this.setState({ messages: res.data });
            }).catch(err => console.log(err));
        }

    }

    post = (user, content, target_ct, target_id) => {
        console.log(target_ct + " " + target_id)
        this.state.content == "" ? null :
            axios.post("https://imobbis.pythonanywhere.com/messaging/",
                {
                    'user': Number(user),
                    'context': content,
                    'is_a_receiver': false,
                    'is_a_sender': true,
                    'target_ct': Number(target_ct),
                    'target_id': Number(target_id),
                    'view': false,
                })
                .then(response => {
                    this.setState({ content: '' })
                    //this.refresh_messaging()
                })
                .catch(error => console.log(error))
    }

    render() {
        const { house, houseid, user, userid, reservation_user, bailleurs, agents, agence_imobieres } = this.state;
        let target_id = null; let target_ct = null;

        _.find(agence_imobieres, { user: Number(house?.user) }) ? (target_ct = 32, target_id = _.find(agence_imobieres, { user: Number(house?.user) })?.id) :
            _.find(agents, { id: Number(house?.user) }) ? (target_ct = 31, target_id = _.find(agents, { id: Number(house?.user) })?.id) :
                _.find(bailleurs, { id: Number(house?.user) }) ? (target_ct = 15, target_id = _.find(bailleurs, { id: Number(house?.user) })?.id) : null;

        let message = _.find(this.state.messages, { target_id: target_id, target_ct: target_ct }) ? _.find(this.state.messages, { target_id: target_id, target_ct: target_ct }) : "";
        return (
            <View style={{ height: height_ }}>
                <ScrollView >
                    <View style={{ marginTop: '5%', marginLeft: '5%' }} >
                        <Text style={{ fontSize: 18, fontWeight: "bold", color: '#7c3325', textAlign: 'center', letterSpacing: 2 }}>Présentez-vous</Text>
                    </View>

                    <View style={{ marginTop: '10%', marginBottom: '6%', marginLeft: '5%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 13, fontWeight: "bold", color: 'black', letterSpacing: 1 }}>
                            Dites au bailleur vos attentes concernant la maison et si vous avez des questions par rapport à la location.
                        </Text>
                    </View>

                    {
                        message !== '' ?
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                <View style={{ alignItems: "center", justifyContent: "center" }}>
                                    <Avatar.Image
                                        size={45}
                                        avatarStyle={{ borderWidth: 1, borderColor: 'grey' }}
                                        rounded
                                        source={{ uri: user?.profile_image }} />
                                </View>

                                <View style={styles.store_row4}>
                                    <View>
                                        <Text style={{ fontSize: 14 }}>{message?.context}</Text>
                                    </View>
                                </View>
                            </View> :

                            <KeyboardAvoidingView style={styles.footer}>
                                <View style={styles.input_}>
                                    <AutogrowInput
                                        defaultHeight={10}
                                        style={styles.textInput}
                                        multiline={true}
                                        placeholder="Taper un message ici"
                                        autoFocus={true}
                                        placeholderTextColor="#666666"
                                        autoCapitalize='none'
                                        value={this.state.content}
                                        onChangeText={(val) => this.setState({ content: val })}
                                    />
                                </View>
                                {
                                    this.state.content != '' ?
                                        <TouchableOpacity style={{ marginTop: -4, marginLeft: "2%" }}
                                            onPress={() => this.post(this.state.userid, this.state.content, target_ct, target_id)}>
                                            <Ionicons size={30} name='send-sharp' color='#7c3325' style={{ marginTop: 6 }}

                                            />
                                        </TouchableOpacity>
                                        : null
                                }
                            </KeyboardAvoidingView>
                    }

                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Reservation5', { userid: userid, houseid: houseid, reservation_user: reservation_user })}
                        style={{ backgroundColor: '#dc7e27', marginTop: '18%', height: 50, marginLeft: height_ * 0.3, marginRight: "5%", borderRadius: 12, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>Suivant</Text>
                    </TouchableOpacity>

                </ScrollView>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    footer: {
        borderTopColor: '#05375a',
        padding: 7, borderTopWidth: 0.5,
        //position: 'absolute',
        //bottom: 0,
        width: '100%',
        height: 55,
        marginTop: 20,
        //marginBottom: 20,
        flexDirection: 'row'

    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        color: '#05375a',
        marginTop: -13
    },
    input_: {
        borderWidth: 1, width: '77%', minHeight: 40, padding: 5,
        borderRadius: 7, borderColor: '#05375a', borderWidth: 1.5,
    },
    store_row4: {
        padding: 10,
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5, marginTop: 3,
        borderRadius: 5, margin: 1,
        //height: 50,
        width: width_ * 0.7,
        marginLeft: "2%"

    },
})