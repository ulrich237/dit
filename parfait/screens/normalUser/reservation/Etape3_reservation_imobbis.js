import React from 'react';
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Switch
} from "react-native";
import _ from 'underscore';
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import { Divider } from 'react-native-elements'
import moment from 'moment';
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Reservation3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reservation_user: this.props.route.params.reservation_user,
            userid: Number(this.props.route.params.userid),
            houseid: Number(this.props.route.params.houseid),
            house: null,
            user: null,
            modalVisible2: false,
            countrys: [],
            provinces: [],
            citys: [],
            pieces: [],
            piece: {
                full_CNI_name: "",
                id_type: "passport",
                ID_number: "",
                user: null
            }
        }
    }

    componentDidMount() {
        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({ countrys: res.data.country,
                     provinces: res.data.province, citys: res.data.city })
            });

        axios.get(`https://imobbis.pythonanywhere.com/identification`)
            .then(res => {

                this.setState({ pieces: res.data.reverse() })
            });

        axios.get("https://imobbis.pythonanywhere.com/user/").then(resu => {
            this.setState({ user: _.find(resu.data, { id: this.state.userid }) })
        }).catch(err => console.log(err));
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.pieces !== this.state.pieces) {
            axios.get("https://imobbis.pythonanywhere.com/identification/").then(res => {
                this.setState({ pieces: res.data.reverse() });
            }).catch(err => console.log(err));
        }

    }

    handleChange = (name, value) => {
        const piece = { ...this.state.piece, [name]: value };
        this.setState({ piece });
    };

    handleChangereservation = (name, value) => {
        let identification = this.state.reservation_user.identification;
        this.state.reservation_user.identification.find(id => id == value) == value ? identification = identification.filter(dent => dent !== value) : identification.push(value);
        const reservation_user = { ...this.state.reservation_user, [name]: identification };
        this.setState({ reservation_user });
    }

    save_profil = piece => {

        if (piece.full_CNI_name !== "" && piece.ID_number !== "") {
            piece.user = this.state.userid;
            let exist = _.find(this.state.pieces, { full_CNI_name: piece.full_CNI_name, ID_number: piece.ID_number, user: piece.user }) ? true : false;
            exist == false ?
                axios.post(`https://imobbis.pythonanywhere.com/identification`, piece).then(res => {
                    this.setState({ modalVisible2: false })
                    //console.log("post effectué")
                }) : alert("Echec! ce profil existe déjà.")
        } else {
            alert("veuillez saisir tous les champs");
        }
        
    }


    render() {
        const { modalVisible2, user, userid, houseid, reservation_user, countrys, citys, pieces } = this.state;

        return (
            <View style={{ height: height_ }}>
                <ScrollView >
                    <View style={{ marginTop: '5%', marginLeft: '5%' }} >
                        <Text style={{ fontSize: 16, fontWeight: "bold", color: '#7c3325', letterSpacing: 2 }}>profil d'invité</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />

                    <View style={{ marginTop: '3%', marginBottom: '2%', marginLeft: '5%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 13, fontWeight: "bold", color: 'black', letterSpacing: 1 }}>
                            pour des raisons de sécurité et de confirmation de votre identité, Nous aimerions avoir votre Nom complet et numéro de CNI ou passport pour la confirmation
                        </Text>
                    </View>

                    {
                        pieces.map((piece_, i) => (
                            <>
                                <View key={i} style={{ flexDirection: 'row', marginLeft: '5%', marginRight: '5%', marginTop: '3%' }}>
                                    <View style={{ width: width_ * 0.8, height: height_ * 0.1, justifyContent: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: "bold", color: '#7c3325', }}>{piece_.full_CNI_name} </Text>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'orangered', marginTop: '1%' }}>{piece_.id_type == "CNI" ? "CNI" : "PASSPORT"}: {piece_.ID_number} </Text>
                                    </View>
                                    <View style={{ height: height_ * 0.1, alignItems: 'center', justifyContent: 'center', }}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                                            thumbColor={this.state.reservation_user.identification.find(id => id == piece_.id) == piece_.id ? "#f5dd4b" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={() => this.handleChangereservation("identification", piece_.id)}
                                            value={this.state.reservation_user.identification.find(id => id == piece_.id) == piece_.id ? true : false}
                                        />
                                    </View>
                                </View>
                                <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />
                            </>
                        ))
                    }

                    <View style={{ height: 100 }} ></View>
                </ScrollView>

                <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />

                {
                    _.find(this.state.pieces, { full_CNI_name: this.state.piece.full_CNI_name, ID_number: this.state.piece.ID_number, user: this.state.piece.user }) ?
                        <View style={styles.footer}>
                            <View style={{ margin: '2%', width: width_ * 0.6, height: height_ * 0.1, justifyContent: 'center', }}>
                                <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#7c3325' }}>Ajouter au moins 1 profil </Text>
                            </View>
                            <View style={{ height: height_ * 0.1, alignItems: 'center', justifyContent: 'center', }}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                                    style={{ margin: '1%', backgroundColor: '#dc7e27', height: 50, width: 100, borderRadius: 12, alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 16, color: '#fff' }}>Creer</Text>
                                </TouchableOpacity>
                            </View>
                        </View> :
                        <View style={styles.footer}>
                            {
                                reservation_user?.identification?.length == 0 ?
                                    <View style={{ margin: '2%', width: width_ * 0.6, height: height_ * 0.1, justifyContent: 'center', }}>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#7c3325' }}>sélectionner au moins un profil  </Text>
                                    </View> :
                                    <>
                                        <View style={{ margin: '2%', width: width_ * 0.6, height: height_ * 0.1, justifyContent: 'center', }}>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#7c3325' }}>Nbre profil(s) sélectionné(s)   {reservation_user.identification.length} </Text>
                                        </View>
                                        <View style={{ height: height_ * 0.1, alignItems: 'center', justifyContent: 'center', }}>
                                            <TouchableOpacity
                                                onPress={() => this.props.navigation.navigate('Reservation4', { userid: userid, houseid: houseid, reservation_user: reservation_user })}
                                                style={{ margin: '1%', backgroundColor: '#dc7e27', height: 50, width: 100, borderRadius: 12, alignItems: 'center', justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 16, color: '#fff' }}>Suivant</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                            }
                            
                        </View>
                }

                <Modal animationType="slide"
                    transparent={true}
                    visible={modalVisible2}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({ modalVisible2: !modalVisible2 });
                    }}>

                    <View style={{ ...styles.centeredView, marginTop: 53 }}>
                        <View style={styles.modalView}>
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }} >
                                <TouchableOpacity
                                    style={{ width: '10%' }}
                                    onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                                >
                                    <Icon name="times" size={25} style={{ marginLeft: 2 }} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.buttonstyle}
                                    onPress={() => this.save_profil(this.state.piece)}
                                >
                                    <Text style={{ fontSize: 16, fontWeight: 'bold', textAlign: "center", color: "#7c3325", textDecorationLine: 'underline', textDecorationStyle: 'solid' }}>Ajouter</Text>
                                </TouchableOpacity>

                            </View>
                            <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />

                            <ScrollView>
                                <View style={{ margin: '5%' }} >
                                    <Text style={{ fontSize: 18, fontWeight: "bold", color: 'black', letterSpacing: 2 }}>Ajouter nouveau profil</Text>
                                    <Text style={{ fontSize: 14, marginTop: '1%', letterSpacing: 1, marginBottom: '2%' }}>Pays/Region:
                                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: 'orangered', marginLeft: '1%' }}> {_.find(countrys, { id: Number(user?.countrie) })?.name}, {_.find(citys, { id: Number(user?.citie) })?.name} </Text>
                                    </Text>
                                </View>

                                <View style={{ margin: '5%', width: '100%', }} >
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: 'black' }}>selectionner type d'identité</Text>
                                    <Picker
                                        selectedValue={this.state.piece}
                                        style={{ height: 50, width: "100%", color: '#7c3325' }}
                                        onValueChange={(val) => this.handleChange('id_type', val)}
                                    >
                                        <Picker.Item key={0} label="passport" value="passport" />
                                        <Picker.Item key={1} label="CNI" value="CNI" />
                                    </Picker>
                                </View>

                                <View style={{ margin: '5%', }}>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: 'black' }}>Numero de {this.state.piece.id_type == "CNI" ? "CNI" : "PASSPORT"}</Text>
                                    <TextInput
                                        onChangeText={val => this.handleChange('ID_number', val)}
                                        value={this.state.piece.ID_number}
                                        style={{ color: '#7c3325' }}
                                        placeholderTextColor='#7c3325'
                                        placeholder="Entrer le numéro de votre pièce ..."
                                        keyboardType="numeric"
                                    />
                                </View>

                                <View style={{ margin: '5%', }}>
                                    <Text style={{ fontSize: 12, fontWeight: "bold", color: 'black' }}>Nom et prenom</Text>
                                    <TextInput
                                        style={{ color: '#7c3325' }}
                                        placeholder="Entrer votre nom complet ..."
                                        placeholderTextColor='#7c3325'
                                        onChangeText={val => this.handleChange('full_CNI_name', val)} />
                                </View>

                            </ScrollView>
                        </View>
                    </View>
                </Modal>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    footer: {
        //padding: 5,
        position: 'absolute',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        //borderColor: '#7c3325',
        //borderWidth: 1,
        backgroundColor: 'silver',
        bottom: 0,
        height: 60,
        marginLeft: '5%',
        width: '92%',
        justifyContent: 'space-between',
        marginBottom: Platform.OS === 'ios' ? 40 : 30,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 15,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '95%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    buttonstyle: {
        //width: '40%',
        backgroundColor: 'white',
        height: 30,
        justifyContent: 'center'
    }
})