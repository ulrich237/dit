import React from 'react';
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Switch
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import _ from 'underscore';
import Icon from "react-native-vector-icons/FontAwesome";
import { Divider } from 'react-native-elements'
import moment from 'moment';
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);


export default class Reservation5 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reservation_user: this.props.route.params.reservation_user,
            userid: Number(this.props.route.params.userid),
            houseid: Number(this.props.route.params.houseid),
            house: null,
            user: null,

            modalVisible2: false,
            countrys: [],
            provinces: [],
            citys: [],
            payments: []
        }
    }

    componentDidMount() {
        axios.get("https://imobbis.pythonanywhere.com/house/").then(res => {
            this.setState({ house: _.find(res.data, { id: this.state.houseid }) })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({ countrys: res.data.country, provinces: res.data.province, citys: res.data.city })
            });

        axios.get(`https://imobbis.pythonanywhere.com/identification/payment`)
            .then(res => {
                this.setState({ payments: res.data })
            });

        axios.get("https://imobbis.pythonanywhere.com/user/").then(resu => {
            this.setState({ user: _.find(resu.data, { id: this.state.userid }) })
        }).catch(err => console.log(err));
    }

    handleChangereservation = (name, value) => {
        const reservation_user = { ...this.state.reservation_user, [name]: value };
        this.setState({ reservation_user });
    }

    getjours = () => {
        let tmp = new Date(this.state.reservation_user.checkout_time) - new Date(this.state.reservation_user.checkin_time);  //Nombre de millisecondes entre les dates
        let diff = {};
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        diff.days = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        return diff.days + 1
    }

    postreservation = () => {
        axios.post("https://imobbis.pythonanywhere.com/house/house_booking",
            {
                'pending': true,
                'confirm': false,
                'error': false,
                'checkin_time': this.state.reservation_user.checkin_time,
                'checkout_time': this.state.reservation_user.checkout_time,
                "bank_ac_num": null,
                "bank_ac_name": null,
                "bank_ac_ca_num": null,
                "Or_num": null,
                "Or_name": null,
                "MTN_name": null,
                "MTN_num": null,
                'payment_mode': this.state.reservation_user.payment_mode,
                'user': Number(this.state.reservation_user.user),
                'house_book': Number(this.state.reservation_user.house_book),
                'identification': this.state.reservation_user.identification
            })
            .then(response => {
                alert("Reservation terminée et en attente de validation")
            })
            .catch(error => console.log(error))
    }

    render() {
        const { modalVisible2, payments, house, houseid, countrys, citys, user, userid, reservation_user } = this.state;

        let date_chek_in = null; let date_chek_out = null; let jour = null; let mois = null;
        let posi_jour = null; let jour1 = null; let mois1 = null; let posi_jour1 = null;
        reservation_user ? (
            date_chek_in = moment(reservation_user.checkin_time).format('LLLL'),
            date_chek_out = moment(reservation_user.checkout_time).format('LLLL'),
            jour = date_chek_in.substring(0, date_chek_in.indexOf(',')),
            mois = ((date_chek_in).substr(jour.length + 2, date_chek_in.length)).substring(0, (date_chek_in).substr(jour.length + 2, date_chek_in.length).indexOf(' ')),
            posi_jour = ((date_chek_in).substr(jour.length + 2 + mois.length, date_chek_in.length)).substring(0, (date_chek_in).substr(jour.length + 2 + mois.length, date_chek_in.length).indexOf(',')),
            jour1 = date_chek_out.substring(0, date_chek_out.indexOf(',')),
            mois1 = ((date_chek_out).substr(jour1.length + 2, date_chek_out.length)).substring(0, (date_chek_out).substr(jour1.length + 2, date_chek_out.length).indexOf(' ')),
            posi_jour1 = ((date_chek_out).substr(jour1.length + 2 + mois1.length, date_chek_out.length)).substring(0, (date_chek_out).substr(jour1.length + 2 + mois1.length, date_chek_out.length).indexOf(','))
        ) : null;

        return (
            <View style={{ height: height_ }}>
                <ScrollView >
                    <View style={{ marginTop: '8%', marginLeft: '5%' }} >
                        <Text style={{ fontSize: 18, fontWeight: "bold", color: '#7c3325', letterSpacing: 2 }}>Confirmer et Payer</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: '5%', }}>
                        <View>
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: 'red', letterSpacing: 2, marginBottom: '5%' }}>{house?.name} </Text>
                            {
                                mois == mois1 ?
                                    <Text style={{ fontSize: 14 }}>
                                        {mois} {posi_jour} - {posi_jour1}
                                    </Text> :
                                    <Text style={{ fontSize: 14 }}>
                                        {mois} {posi_jour} - {mois1} {posi_jour1}
                                    </Text>
                            }
                        </View>
                        <Image source={{ uri: house?.photo_1 }} style={{ height: height_ * 0.15, width: width_ * 0.4 }} />
                    </View>

                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />

                    <View style={{ marginTop: '3%', marginLeft: '5%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 12, fontWeight: "bold", color: 'black' }}>selectionner un moyen de paiement</Text>
                        <Picker
                            selectedValue={this.state.piece}
                            style={{ height: 40, width: "100%", color: '#7c3325' }}
                            onValueChange={(val) => this.handleChangereservation('payment_mode', val)}
                        >
                            <Picker.Item key={0} label="Mode paiement" value="" />
                            <Picker.Item key={0} label="ORANGE money" value="ORANGE money" />
                            <Picker.Item key={1} label="Mtn money" value="Mtn money" />
                            <Picker.Item key={1} label="EU mobile money" value="EU mobile money" />
                            <Picker.Item key={1} label="PAYPAL" value="PAYPAL" />
                            <Picker.Item key={1} label="VISA card" value="VISA card" />
                        </Picker>
                    </View>

                    {
                        reservation_user.payment_mode !== "" ?
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '5%', marginRight: '5%', marginTop: '3%', marginBottom: "3%" }}>
                                <Text style={{ fontSize: 12, color: 'red', fontWeight: 'bold' }}>
                                    {
                                        reservation_user.payment_mode == "ORANGE money" ? payments.find(pay => pay.Or_name !== "").Or_name :
                                            reservation_user.payment_mode == "Mtn money" ? payments.find(pay => pay.MTN_name !== "").MTN_name :
                                                reservation_user.payment_mode == "EU mobile money" ? payments.find(pay => pay.bank_ac_name == "imobbis EU mobile money").bank_ac_name :
                                                    reservation_user.payment_mode == "PAYPAL" ? payments.find(pay => pay.bank_ac_name == "imobbis paypal").bank_ac_name :
                                                        reservation_user.payment_mode == "VISA card" ? payments.find(pay => pay.bank_ac_name == "imobbis visa card").bank_ac_name : null
                                    }
                                </Text>
                                <Text style={{ fontSize: 13, fontWeight: 'bold', color: 'orangered', letterSpacing: 2 }}>
                                    {
                                        reservation_user.payment_mode == "ORANGE money" ? payments.find(pay => pay.Or_name !== "").Or_num :
                                            reservation_user.payment_mode == "Mtn money" ? payments.find(pay => pay.MTN_name !== "").MTN_num :
                                                reservation_user.payment_mode == "EU mobile money" ? payments.find(pay => pay.bank_ac_name == "imobbis EU mobile money").bank_ac_num :
                                                    reservation_user.payment_mode == "PAYPAL" ? payments.find(pay => pay.bank_ac_name == "imobbis paypal").bank_ac_num :
                                                        reservation_user.payment_mode == "VISA card" ? payments.find(pay => pay.bank_ac_name == "imobbis visa card").bank_ac_num : null
                                    }
                                </Text>
                            </View> : null
                    }

                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />
                    <View style={{ flexDirection: 'row', marginLeft: '5%', marginTop: '3%', marginBottom: "3%" }}>
                        <Text style={{ fontSize: 12, color: '#7c3325', fontWeight: 'bold' }}>{house?.renting_price} * {this.getjours()} Jour(s)</Text>
                        <Text style={{ marginLeft: '40%', fontSize: 13, fontWeight: 'bold', letterSpacing: 2 }}>{house?.renting_price ? house?.renting_price * this.getjours() : '0'} XAF</Text>
                    </View>

                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: '5%', marginRight: '8%', marginTop: '3%', marginBottom: "3%" }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Total</Text>
                        <Text style={{ fontSize: 16, color: '#7c3325', fontWeight: 'bold', letterSpacing: 2 }}>{house?.renting_price ? house?.renting_price * this.getjours() : '0'} XAF</Text>
                    </View>

                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%', marginTop: "3%" }} />

                    <View style={{ marginTop: '5%', marginLeft: '5%' }} >
                        <Text style={{ fontSize: 16, fontWeight: "bold", color: '#7c3325', letterSpacing: 2 }}>Politique d'annulation</Text>
                    </View>
                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />

                    <View style={{ marginTop: '3%', marginBottom: '2%', marginLeft: '5%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 13, fontWeight: "bold", color: 'black', letterSpacing: 1 }}>
                            pour des raisons de sécurité et de confirmation de votre identité, Nous aimerions avoir votre Nom complet et numéro de CNI ou passport pour la confirmation
                        </Text>
                    </View>

                </ScrollView>

                <View style={styles.footer}>
                    <TouchableOpacity
                        onPress={() => { reservation_user.payment_mode == "" ? alert("veuillez selectionner un mode de paiement !") : this.setState({ modalVisible2: !modalVisible2 }) }}
                        style={{ backgroundColor: '#dc7e27', height: 50, width: "100%", borderRadius: 5, alignItems: 'center', justifyContent: 'space-evenly', flexDirection: "row" }}>
                        <Entypo name='lock' size={25} style={{ color: "white" }} />
                        <Text style={{ fontSize: 16, color: '#fff' }}>Confirmer reservation - {house?.renting_price ? house?.renting_price * this.getjours() : '0'} XAF</Text>
                    </TouchableOpacity>

                </View>

                <Modal animationType="slide"
                    transparent={true}
                    visible={modalVisible2}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                        this.setState({ modalVisible2: !modalVisible2 });
                    }}>

                    <View style={{ ...styles.centeredView, marginTop: 53 }}>
                        <View style={styles.modalView}>
                            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }} >
                                <TouchableOpacity
                                    style={{ width: '10%' }}
                                    onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                                >
                                    <Icon name="times" size={25} style={{ marginLeft: 2 }} />
                                </TouchableOpacity>

                            </View>
                            <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />

                            <ScrollView>
                                <View style={{ margin: '5%' }} >
                                    <Text style={{ fontSize: 14, fontWeight: "bold", color: 'black' }}>Informations complètes sur cette reservation :</Text>
                                    <Text style={{ fontSize: 14, marginTop: '1%', letterSpacing: 1, marginBottom: '2%' }}>
                                        <Text style={{ fontWeight: 'bold', color: 'orangered', marginLeft: '1%' }}>{house?.name} </Text> , meublé situé à
                                        <Text style={{ fontWeight: 'bold', color: 'orangered', marginLeft: '1%' }}> {_.find(countrys, { id: Number(house?.country) })?.name}, {_.find(citys, { id: Number(house?.city) })?.name} </Text>
                                        , reservé pour la periode du {moment(reservation_user.checkin_time).format('DD/MM/YYYY')} au {moment(reservation_user.checkout_time).format('DD/MM/YYYY')} par {user?.username} via le paiement {reservation_user.payment_mode} du compte imobbis à hauteur de
                                        <Text style={{ fontWeight: 'bold', color: 'orangered', marginLeft: '1%' }}> {house?.renting_price ? house?.renting_price * this.getjours() : '0'} </Text> XAF
                                    </Text>
                                </View>

                                <TouchableOpacity
                                    style={{ backgroundColor: '#dc7e27', marginLeft: '5%', height: 50, width: "50%", borderRadius: 5, alignItems: 'center', justifyContent: 'space-evenly', flexDirection: "row" }}
                                    onPress={() => this.postreservation()}
                                >
                                    <Text style={{ fontSize: 14, fontWeight: "bold", color: 'white' }}>Reservez</Text>
                                </TouchableOpacity>

                            </ScrollView>
                        </View>
                    </View>
                </Modal>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    footer: {
        position: 'absolute',
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        bottom: 0,
        height: 60,
        marginLeft: '5%',
        marginRight: '5%',
        width: '92%',
        justifyContent: 'space-between',
        marginBottom: Platform.OS === 'ios' ? 40 : 30,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 15,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '95%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    buttonstyle: {
        backgroundColor: 'white',
        height: 30,
        justifyContent: 'center'
    }
})