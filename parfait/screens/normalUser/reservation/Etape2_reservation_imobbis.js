import React from 'react';
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Switch
} from "react-native";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import _ from 'underscore';
import Ionicon from "react-native-vector-icons/Ionicons";
import { Divider } from 'react-native-elements'
import moment from 'moment';
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);


export default class Reservation2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            outil_houses: [],
            reservation_user: this.props.route.params.reservation_user,
            userid: Number(this.props.route.params.userid),
            houseid: Number(this.props.route.params.houseid),
            date_recente: {},
            house: null
        }
    }

    componentDidMount() {
        axios.get("https://imobbis.pythonanywhere.com/house/").then(res => {
            this.setState({ house: _.find(res.data, { id: this.state.houseid }) })
        }).catch(err => console.log(err));

        axios.get("https://imobbis.pythonanywhere.com/house/loi_house/").then((reslh) => {
            this.setState({ outil_houses: reslh.data })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/house/house_booking`)
            .then(res => {
                axios.get(`https://imobbis.pythonanywhere.com/house/personal_use/`)
                    .then(resus => {
                        let dates_occupers = []; let date_recente = {}
                        _.filter(res.data, { house_book: this.state.houseid, pending: false, confirm: false, error: false }).map((reserv, i) => { i == 0 ? date_recente = { checkin_time: reserv.checkin_time, checkout_time: reserv.checkout_time } : null; dates_occupers.push({ checkin_time: reserv.checkin_time, checkout_time: reserv.checkout_time }) });
                        _.filter(resus.data, { house: this.state.houseid }).map(reserv => dates_occupers.push({ checkin_time: reserv.checkin_time, checkout_time: reserv.checkout_time }));

                        dates_occupers.map(date => new Date(date_recente.checkout_time).getTime() < new Date(date.checkout_time).getTime() ? date_recente = { checkin_time: date.checkin_time, checkout_time: date.checkout_time } : null);
                        this.setState({ date_recente })
                    });
            })
    }

    render() {
        const { date_recente, outil_houses, house, reservation_user, userid, houseid } = this.state;

        let date_chek_in = null; let date_chek_out = null; let jour = null; let mois = null;
        let posi_jour = null; let jour1 = null; let hour = null; let mois1 = null; let posi_jour1 = null;
        let hour1 = null;
        (date_recente.checkin_time !== undefined) ? (
        date_chek_in = moment(date_recente.checkin_time).format('LLLL'),
        date_chek_out = moment(date_recente.checkout_time).format('LLLL'),
        jour = date_chek_in.substring(0, date_chek_in.indexOf(',')),
        mois = ((date_chek_in).substr(jour.length + 2, date_chek_in.length)).substring(0, (date_chek_in).substr(jour.length + 2, date_chek_in.length).indexOf(' ')),
        posi_jour = ((date_chek_in).substr(jour.length + 2 + mois.length, date_chek_in.length)).substring(0, (date_chek_in).substr(jour.length + 2 + mois.length, date_chek_in.length).indexOf(',')),
        jour1 = date_chek_out.substring(0, date_chek_out.indexOf(',')),
        hour = (date_chek_in).substr(jour.length + 2 + mois.length + posi_jour.length + 6, date_chek_in.length),
        mois1 = ((date_chek_out).substr(jour1.length + 2, date_chek_out.length)).substring(0, (date_chek_out).substr(jour1.length + 2, date_chek_out.length).indexOf(' ')),
        posi_jour1 = ((date_chek_out).substr(jour1.length + 2 + mois1.length, date_chek_out.length)).substring(0, (date_chek_out).substr(jour1.length + 2 + mois1.length, date_chek_out.length).indexOf(',')),
        hour1 = (date_chek_out).substr((jour1.length + 2 + mois1.length + posi_jour1.length + 6), date_chek_out.length) ) : null
        
        return (
            <View >
                <ScrollView >
                {date_recente.checkin_time !== undefined ? 
                    <>
                    <View style={{ marginTop: '5%', marginLeft: '5%' }} >
                        <Text style={{ fontSize: 16, fontWeight: "bold", color: '#7c3325', letterSpacing: 2 }}>Reservation précédente </Text>
                    </View>
                    <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />
                    
                    <View style={{ flexDirection: 'row', marginLeft: '5%', marginTop: '2%' }}>
                        <View style={{ width: width_ * 0.15, height: height_ * 0.1, alignItems: 'center', justifyContent: 'center', backgroundColor: "silver" }} >
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: '#7c3325', }}>{mois}</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'orangered' }}>{posi_jour} </Text>
                        </View>
                        <View style={{ marginLeft: "5%", height: height_ * 0.1, justifyContent: 'center' }} >
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: '#7c3325', }}>{jour} (date d'entrée) </Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'orangered' }}>{hour} </Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginLeft: '5%', marginTop: '2%' }}>
                        <View style={{ width: width_ * 0.15, height: height_ * 0.1, alignItems: 'center', justifyContent: 'center', backgroundColor: "silver" }}>
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: '#7c3325' }}>{mois1} </Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'orangered' }}>{posi_jour1}</Text>
                        </View>
                        <View style={{ marginLeft: "5%", height: height_ * 0.1, justifyContent: 'center' }}>
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: '#7c3325' }}>{jour1} (date sortie) </Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'orangered' }}>{hour1} </Text>
                        </View>
                    </View>
                    </> : null
                    }

                    <View>
                        <Text style={{ marginLeft: "5%", marginTop: "5%", fontSize: 16, fontWeight: "bold", color: '#7c3325', letterSpacing: 2 }} >Restrictions :</Text>

                        <Divider style={{ backgroundColor: 'black', marginLeft: '6%', marginRight: '6%' }} />

                        <View style={{marginLeft: "5%", marginTop: '2%', flexDirection: 'row', alignItems: 'center', marginRight: '10%' }}>
                            <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
                                <Ionicon name="logo-no-smoking" size={20} style={{ marginLeft: 8, marginRight: 15, color: _.find(outil_houses, { house: house?.id })?.smooking_allowed == true ? "black" : "silver" }} />
                                <Text style={{ fontSize: 12, fontWeight: "bold", color: _.find(outil_houses, { house: house?.id })?.smooking_allowed == true ? "black" : "silver" }} >Permission de fumer</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginLeft: "5%", flexDirection: 'row', alignItems: 'center', marginRight: '10%' }}>
                        <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
                            <MaterialCommunityIcons name="account-child-circle" size={20} style={{ marginLeft: 8, marginRight: 15, color: _.find(outil_houses, { house: house?.id })?.suitable_for_children == true ? "black" : "silver" }} />
                            <Text style={{ fontSize: 12, fontWeight: "bold", color: _.find(outil_houses, { house: house?.id })?.suitable_for_children == true ? "black" : "silver" }} >Conviviable aux enfants</Text>
                        </View>
                    </View>
                    <View style={{marginLeft: "5%", flexDirection: 'row', alignItems: 'center', marginRight: '10%' }}>
                        <View style={{ width: '60%', flexDirection: 'row', alignItems: 'center' }}>
                            <MaterialCommunityIcons name="cat" size={20} style={{ marginLeft: 8, marginRight: 15, color: _.find(outil_houses, { house: house?.id })?.pets_allowed == true ? "black" : "silver" }} />
                            <Text style={{ fontSize: 12, fontWeight: "bold", color: _.find(outil_houses, { house: house?.id })?.pets_allowed == true ? "black" : "silver" }} >Animaux de compagnie</Text>
                        </View>
                    </View>
                    <View style={{marginLeft: "5%", flexDirection: 'row', alignItems: 'center' }}>
                        <MaterialCommunityIcons name="party-popper" size={20} style={{ marginLeft: 8, marginRight: 15, color: _.find(outil_houses, { house: house?.id })?.gym == true ? "black" : "silver" }} />
                        <Text style={{ fontSize: 12, fontWeight: "bold", color: _.find(outil_houses, { house: house?.id })?.gym == true ? "black" : "silver" }} >Organiser des evennements</Text>
                    </View>
                    <TouchableOpacity 
                       onPress={() => this.props.navigation.navigate('Reservation3', {userid: userid, houseid: houseid, reservation_user: reservation_user})}
                      style={{ backgroundColor: '#dc7e27', marginTop: '18%', height: 50, marginLeft: height_*0.3, marginRight: "5%", borderRadius: 12, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#fff' }}>Suivant</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }

}