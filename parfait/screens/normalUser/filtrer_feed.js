import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
    SafeAreaView, StyleSheet, View, TouchableOpacity, Text, TextInput,
    ScrollView, Image, Dimensions
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage'
import DeviceInfo from 'react-native-device-info';
import { Avatar } from 'react-native-paper';
import Highlighter from 'react-native-highlight-words';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height)

export default class filter_Feed extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            houses: this.props.route.params?.data, users: this.props.route.params?.prestataire, prestataire: this.props.route.params?.prestataire, loading: true, stores: [], restos: [],
            imagesHouses: [], cbs: [], cmencats: [], deal_cats: [], userid: null,
            restomenue: [], initvalue_: '', stores: [], nameListBackup: [], big_cat: [],
            countries:[]
        };
    }


    componentDidMount() {

        this.displayData()

        this.props.route.params?.data ?
            axios.get(`https://imobbis.pythonanywhere.com/new/house4`,)
                .then(response => this.setState({ houses: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }))
            : null

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => {
                var data = response.data.filter((value) => value.typ_user.toLowerCase() != 'client'.toLowerCase())
                this.setState({
                    users: response.data, prestataire: data
                })
            })
            .catch(error => this.setState({ error, isLoading: false }));


        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imagesHouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
            this.setState({
                countries: res.data.country,
            });
        });



    }

    filterItem = (item) => {

        const { houses, users, countries} = this.state;
        
        let us = _.find(users, { id: item.id })

        item.is_a == '#House' ?
            this.props.navigation.navigate('HouseDetails', { data: _.find(houses, { id: item.id }), id: item.id, userid: this.state.userid }) :
            item.is_a == '#Prestataire' ?
                this.props.navigation.navigate('AglaProfile', {
                    agla: _.find(users, { id: item.id }),
                    countrie: _.find(countries, {id: us?.countrie}),
                    houses: _.filter(houses, (value) => value?.user?.id == item.id),
                    content_ct: 15
                }) :
                null

    }


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ userid: json_id })
        } catch { error => alert(error) };
    }



    render() {
        //const { query } = this.props.route.params;
        //var val = this.props.route.params.query != undefined? query:""
        var data = []
        const { houses, users, prestataire, initvalue_, imagesHouses, big_cat,
            stores, resmencats, cbs, cmencats, deal_cats } = this.state //category

        houses.map(list => {
            var user = list?.user
            var image = _.first(list?.image)
            data.push({
                'id': list.id, 'name': list?.name, 'is_a': '#House', 'category': list?.category,
                'img': image?.image_url, 'buil_name': user?.username,
                'build_img': user?.profile_url, 'publish_type': list?.publish_type
            })
        })

        prestataire.map(list => {
            var house = _.find(houses, (value) => (value?.user?.id == list?.id))
            var image = _.find(imagesHouses, { house: house?.id })

            data.push({
                'id': list.id, 'name': list?.username, 'is_a': '#Prestataire', 'category': list?.typ_user,
                'img': image?.image, 'buil_name': house?.name,
                'build_img': list?.profile_image
            })
        })

        var martop = Platform.OS == 'ios' ? DeviceInfo.hasNotch() ? 50 : 20 : DeviceInfo.hasNotch() ? 30 : 0
        var data_ = data ?
            data.filter((value) => {
                return (
                    value.name.toLowerCase().indexOf(initvalue_.toLowerCase()) > -1 &&
                    value.name.toLowerCase().substr(0, initvalue_.length) == initvalue_.toLocaleLowerCase()
                )
            })

            : data


        return (
            <View style={{ paddingTop: martop, backgroundColor: 'white' }}>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ width: '12%', marginTop: 5 }} onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before' size={53} style={{ marginTop: -5, color: '#dc7e27', }} />
                    </TouchableOpacity>
                    <TextInput
                        value={initvalue_}
                        style={{
                            height: 45, margin: 5, borderWidth: 2.5, borderColor: '#dc7e27',
                            borderRadius: 20, padding: 4, fontSize: 18, width: '83%', color: '#7c3325',
                        }}
                        onChangeText={(text) => { this.setState({ initvalue_: text }) }}
                        placeholder="Faire une rechercher"
                        placeholderTextColor='grey'
                    />
                </View>
                {
                    initvalue_ != '' ?
                        <View style={{ paddingHorizontal: 10, }}>
                            <Text style={{ fontSize: 25, fontWeight: 'bold', textAlign: 'center', marginVertical: 7 }}>{_.size(data_)} résultats</Text>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                {
                                    data_?.map(deal => {
                                        return (

                                            <TouchableOpacity style={{
                                                marginVertical: 7, flexDirection: 'row', flexWrap: 'wrap',
                                                borderBottomColor: '#e6e6e6', borderBottomWidth: 1.5, paddingBottom: 10
                                            }}
                                                onPress={() => this.filterItem(deal)}
                                            >
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ width: '80%' }}>
                                                        <Highlighter
                                                            numberOfLines={1}
                                                            highlightStyle={{ backgroundColor: '#dc7e27' }}
                                                            searchWords={[initvalue_]}
                                                            textToHighlight={deal.name}
                                                            style={{ fontSize: 17, fontWeight: 'bold', color: '#05375a' }}
                                                        />
                                                        <View style={{ flexDirection: 'row', margin: 7 }}>
                                                            <Avatar.Image source={{ uri: deal.build_img }} size={40} />
                                                            <View style={{ marginLeft: 10 }}>
                                                                <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 4 }}>{deal.is_a == '#House' ? 'Géré par ' + deal.buil_name : deal.name + ' (' + deal.category + ')'}</Text>
                                                                <Text style={{ fontSize: 16 }}>

                                                                    {deal.is_a == '#House' ? deal.category + ' à ' + deal.publish_type : deal.is_a == '#Prestataire' ?
                                                                        deal.buil_name ? 'A déjà publié: ' + deal?.buil_name : 'N\'a pas encore publié' : null}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <Image source={{ uri: deal.img }} style={{ width: '19%', height: height_ * 0.125, borderRadius: 10 }} />
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }
                                <View style={{ height: height_ * 0.30 }}></View>
                            </ScrollView>
                        </View>
                        :
                        null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({

    autocompleteContainer: {
        flex: 1, maxHeight: 300,
        zIndex: 1,
    },

});