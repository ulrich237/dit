import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, Platform, TouchableOpacity } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import DeviceInfo from 'react-native-device-info';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import AsyncStorage from '@react-native-async-storage/async-storage';

var width_ = Math.round(Dimensions.get('window').width)
var height_ = Math.round(Dimensions.get('window').height);


export default class Me extends Component {

    constructor(props) {

        super(props)
        this.state = {
            user: {}, user_id: null,
            touchHouse: [],
            follows: [], house: [], land: []
        }
    }

    componentDidMount() {

        this.displayData()

        /*axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => this.setState({ user: _.find(response.data, { id: this.state.user_id }) }),)
            .catch(error => this.setState({ error, isLoading: false }));*/

        axios.get("https://imobbis.pythonanywhere.com/new/hous").then(res => {
            this.setState({ house: res.data });
        })
        axios.get(`https://imobbis.pythonanywhere.com/wen/touch_house`,)
            .then(response => this.setState({ touchHouse: _.filter(response.data, { user: this.state.user_id }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/feature/follow").then(res => {
            this.setState({ follows: _.filter(res.data, { user: this.state.user_id }) });
        });
        axios.get("https://imobbis.pythonanywhere.com/landlord/").then(res => {

            this.setState({ land: res.data });
        });
    }



    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.user !== this.state.user) {
            axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/" + this.state.user_id).then(resu => {
                this.setState({ user: resu.data })

            }).catch(err => console.log(err));
        }
    }

    touch() {
        const { touchHouse, users, house } = this.state;
        var t = 0
        touchHouse.map(pers =>
            t = t + _.size(pers.touched)
        )
        return t;
    }


    displayData = async () => {  //myuser
        try {

            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            let users = await AsyncStorage.getItem('myuser');
            this.setState({ user_id: json_id, user: JSON.parse(users) })

        } catch { error => alert(error) };
    }


    clearAsyncStorage = () => {

        AsyncStorage.removeItem('user_data');
        this.props.navigation.navigate('AuthStack')

    }

    render() {
        const { user, user_id, follows, land, touchHouse } = this.state
        var martop = Platform.OS == 'ios' ? DeviceInfo.hasNotch() ? 50 : 20 : DeviceInfo.hasNotch() ? 20 : 0

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{
                    height: height_ / 15, justifyContent: 'space-between', flexDirection:
                        'row', marginBottom: 3, backgroundColor: '#dc7e27'
                }}>
                    <View style={{ paddingLeft: 7 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 3 }}>
                            <TouchableOpacity style={{ marginLeft: 20, marginTop: 14 }}
                                onPress={() => this.props.navigation.openDrawer()}
                            >
                                <FontAwesome name='list' color='black' size={20} />
                            </TouchableOpacity>
                            <Text style={{
                                marginLeft: 50, margin: 10,
                                color: 'black', marginTop: 10, fontSize: 20, fontWeight: 'bold'
                            }}>IMOBBIS</Text>
                        </View>
                    </View>

                </View>

                <View style={{ height: 150, padding: 5 }}>
                    <View style={{ flexDirection: 'row-reverse' }}>
                        <AntDesign name='setting' size={28} onPress={() => this.props.navigation.navigate('Settings')} />
                        <AntDesign name='user' size={28} style={{ marginRight: 20 }}
                            onPress={() => this.props.navigation.navigate('Profil')}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Avatar.Image
                            size={90}
                            source={{ uri: user?.profile_image }}
                        />
                        <View style={{ justifyContent: 'center', marginLeft: 15, marginBottom: 5 }}>
                            <Text style={{ fontWeight: '600', fontSize: 20 }}>{user?.first_name} {user?.last_name}</Text>
                            <Text style={{ color: '#737373' }}>nom d'utilisateur: {user?.username}</Text>
                        </View>
                    </View>
                </View>

                <View>
                    {user?.typ_user == "client" ?null:
                        <TouchableOpacity style={styles.view} onPress={() => this.props.navigation.navigate('NavigationA', { 'user': user })}>
                            <View style={{ width: '95%', flexDirection: 'row' }}>
                                <MaterialIcons style={{ marginTop: -4 }} name={'payment'} size={30} color='#664200' />
                                <Text style={styles.text}>Payez votre abonnement </Text>
                            </View>
                            <MaterialIcons name={'chevron-right'} size={25} color='#222' />
                        </TouchableOpacity>
                    }
                </View>

                <View style={{ padding: 7, marginTop: 30 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('RecentlyTouched', { 'touchHouse': touchHouse, 'user_id': user_id })}>
                            <AntDesign name='home' size={31} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 12, marginTop: 4 }}>{this.touch()} visites</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('Following', {})}>
                            <SimpleLineIcons name='user-following' size={30} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 12, marginTop: 4 }}> {_.size(follows)} followings</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('RecentlyTouched', { 'touchHouse': touchHouse, 'user_id': user_id })}>
                            <MaterialCommunityIcons name='bell-ring' size={31} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 12, marginTop: 4 }}>0 notification</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ ...styles.row3 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity style={{ width: '50%', alignItems: 'center', marginTop: 30 }}
                                onPress={() => this.props.navigation.navigate('Service')}>
                                <Entypo name='add-user' size={40} color='#664200' />
                                <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>Devenir un spécialiste</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ width: '50%', alignItems: 'center', marginTop: 30 }}
                                disabled={user?.typ_user == "client" ? false : true}
                                onPress={() => this.props.navigation.navigate('Compte')}>
                                <Entypo name='add-user' size={40} color='#664200' />
                                <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>
                                    {user?.typ_user == "client" ?
                                        'Devenir un prestataire immobilier' : 'Vous êtes déjà prestataire'}
                                </Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                    <TouchableOpacity style={[styles.logout]}
                        onPress={() => this.clearAsyncStorage()}
                    >
                        <Ionicons name='md-exit-outline' color='white' size={33} />
                        <Text style={{ fontSize: 25, color: 'white', marginLeft: 5 }}>Se déconnecter</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    row3: {
        paddingVertical: 15,
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        padding: 6, elevation: 9,
        marginVertical: 6,
        borderRadius: 20, margin: 5,
        flexDirection: 'row', flexWrap: 'wrap'
    },
    logout: {
        width: '70%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 20,
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        marginTop: 7,
        alignSelf: 'center',
        backgroundColor: '#ff4d4d',
        marginTop: 55,
        flexDirection: 'row'

    },
    view: {
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 15,
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        margin: 5,
        flexDirection: 'row'
    },
    text: {
        color: 'red',
        fontSize: 17,
        //marginTop: 2,
        marginLeft: 5
    },
})