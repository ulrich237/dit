import React, { Component } from 'react'
import {
    Image, Text, StyleSheet, TextInput, Alert,
    View, Modal, ScrollView, TouchableOpacity, Dimensions, Platform
} from 'react-native'
import { Card } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'
//import PhotoUpload from 'react-native-photo-upload'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import { Divider } from 'react-native-elements';
import _ from 'underscore'
import axios from 'axios'
import Icon from "react-native-vector-icons/FontAwesome";
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-async-storage/async-storage'
import ImagePicker from 'react-native-image-crop-picker';
import Toast from 'react-native-simple-toast';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Profil extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            what_modal: '',
            user: {
                id: null,
                profile_image: "a",
                username: "",
                first_name: "",
                last_name: "",
                gender: "M",
                countrie: null,
                citie: null,
                phone_number: null
            },
            countrys: [],
            districts: [],
            provinces: [],
            citys: [],
            neighbors: [],
            location: "", userid: null,
            appel_bout: false,
            photo: {}

        }
    }

    handlerChoosePhoto = () => {

        ImagePicker.openPicker({
            multiple: false,
            mediaType: 'photo',
        }).then(images => {

            console.log(images)
            this.handleChange('profile_image', images)
            this.setState({ photo: images })

        }).catch(error => {
            //alert(error)
            if (error.code === 'E_PICKER_CANCELLED') { // here the solution
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée. Veuillez réessayer!!!')
                return false;
            } else {
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée.')
            }
        });

    }

    componentDidMount() {
        this.displayData()



    }


    dataFetc(id, user1) {
        axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/' + id)
            .then((resd) => {
                this.setState({ user: resd.data });
                axios.get(`https://imobbis.pythonanywhere.com/location/`)
                    .then(res => {
                        let location = "";
                        _.find(res.data.country, { id: resd.data.countrie }) ? location = _.find(res.data.country, { id: resd.data.countrie }).name : null;

                        _.find(res.data.city, { id: resd.data.citie }) ? location = location + ", " + _.find(res.data.city, { id: resd.data.citie }).name : null;
                        //  _.find(res.data.neighbor, { id: user.neighbor }) ? location = location + ", " + _.find(res.data.neighbor, { id: user.neighbor }).name : null;
                        this.setState({
                            countrys: res.data.country, citys: res.data.city,
                            // neighbors: res.data.neighbor, 
                            location: location
                        });
                    })

            })
            .catch(error => {
                let user = {
                    id: user1.id,
                    profile_image: user1.profile_image,
                    username: user1.username,
                    first_name: user1.first_name,
                    last_name: user1.last_name,
                    gender: user1.gender,
                    countrie: user1.countrie,
                    citie: user1.citie,
                    phone_number: user1.phone_number
                }
                this.setState({ user: user, error, isLoading: false })
            });
    }



    save(item) {

        console.log(item)

        axios.patch('https://imobbis.pythonanywhere.com/new/utilisateur/' + Number(this.state.userid), {
            "username": item.username,
            "first_name": item.first_name,
            "last_name": item.last_name,
            "countrie": item.countrie,
            "citie": item.citie,
            "gender": item.gender
        })
            .then(res => {

                AsyncStorage.setItem('myuser', JSON.stringify(res.data)),

                Toast.showWithGravity(
                        "Modifié avec succés",
                        Toast.LONG,
                        Toast.CENTER,
                      
                    );

                //this.props.navigation.goBack();

            }).catch(err => {

                err.toString() == 'Error: Request failed with status code 400' ?
                    alert("Remplissez tous les champs ") :
                    alert("Une erreur s'est produite, verifiez votre connexion ")

                console.log(err)
            })
    }


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ userid: json_id, })

            let us = await AsyncStorage.getItem('myuser');
            this.dataFetc(json_id, JSON.parse(us))


        } catch { error => alert(error) };
    }

    handleChange = (name, value) => {
        const user = { ...this.state.user, [name]: value };
        this.setState({ user });
    };

    addlocation(name) {
        let location = this.state.location;
        location = location == "" ? name : location + ", " + name;
        this.setState({ location: location });
    }

    handleChangeradio = () => {
        let user = this.state.user;
        user.gender == "M" ? user.gender = "F" : user.gender = "M";
        this.setState({ user });
    };

    initlocation() {
        let user = {
            ...this.state.user, countrie: null,
            citie: null,
        };
        this.setState({ location: "", user: user });
    }

    save_photo() {

        console.log('oui')

        const { photo } = this.state
        const form = new FormData();
        var photos = _.toArray(photo)

        for (let i = 0; i < photos.length; i++) {
            form.append('profile_image', {
                name: photo.filename,
                type: photo.mime,
                uri: Platform.OS === 'android' ? photo.path : photo.path,
            }
            );
        }

        fetch(`https://imobbis.pythonanywhere.com/new/utilisateur/${this.state.userid}`, {
            method: 'PUT',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: form
        })
            .then(res => {

                AsyncStorage.setItem('myuser', JSON.stringify(res.data)),
                Toast.showWithGravity(
                        "Votre profil a été  modifié avec succés!",
                        Toast.LONG,
                        Toast.CENTER,
                     
                    );

            }).catch(err => {
                console.log(err)
            })
    }

    render() {
        const user = this.state.user;



        return (
            <View style={styles.detailsContainer}>
                <View style={{
                    height: height_ / 15, flexDirection:
                        'row', marginBottom: 3, backgroundColor: '#dc7e27'
                }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                        style={{ margin: 10 }}>
                        <Icon name="angle-left" size={30}
                            style={{ marginLeft: 10, color: "black" }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <Text style={{
                            color: 'black', marginTop: 2,
                            fontSize: 18, fontWeight: 'bold'
                        }}>Mettez a jour votre Profil</Text>
                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 5, height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'Select' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '', marginLeft: 15 }}> Photo profil </Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', alignItems: 'center' }}>
                        <FastImage
                            style={{
                                paddingVertical: 30,
                                width: 60,
                                height: 30,
                                borderRadius: 50,
                                backgroundColor: 'silver',
                                marginHorizontal: 10
                            }}
                            source={{
                                uri: user?.profile_image?.path ? user?.profile_image?.path : user?.profile_image,
                                headers: { Authorization: 'someAuthToken' },
                                priority: FastImage.priority.normal,
                                // cache:FastImage.cacheControl.cacheOnly
                            }}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                        <Ionicons color="#7c3325" name="camera" size={30} style={{ right: 20, top: 13 }} />
                    </View>
                </TouchableOpacity>
                <Divider style={{ height: 30, marginLeft: 35, marginTop: '5%', fontWeight: 'bold', marginHorizontal: '3%', color: 'black' }} />
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'username' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Nom utilisateur</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.username ? user.username : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'first' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Nom</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.first_name ? user.first_name : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'last' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Prenom</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.last_name ? user.last_name : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => { this.setState({ isVisible: !this.state.isVisible, what_modal: 'countrie' }) }}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Lieu</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ width: width_ * 0.7, alignItems: "flex-end" }}>
                            <Text style={{ fontSize: 14 }}>{user.countrie !== "" && user.countrie !== null ? this.state.location : ""}</Text>
                        </View>
                        <Ionicons color="#7c3325" name="md-location" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                //onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'phone_number' })}
                >
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Numéro telephone</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.phone_number ? user.phone_number : ""}</Text>
                        <Ionicons color="#7c3325" name="ios-call-sharp" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'gender' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Genre</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.gender !== "null" ? user.gender : "......"}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{ marginHorizontal: 10 }} />
                    </View>
                </TouchableOpacity>
                <View style={{ alignItems: "center", marginTop: 10 }}>

                </View>

                {
                    //modal personnalisé
                }

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <FontAwesome5 style={{ marginTop: 50, alignSelf: 'flex-end' }}
                            onPress={() => {
                                this.setState({ isVisible: !this.state.isVisible })
                            }}
                            name='times-circle'
                            size={30}
                            color='#7c3325' />
                        {
                            this.state.what_modal == 'Select' ?
                                (
                                    <ScrollView>
                                        <TouchableOpacity
                                            onPress={() => this.handlerChoosePhoto()}
                                        >
                                            <FastImage
                                                style={{
                                                    paddingVertical: 30,
                                                    width: 150,
                                                    height: 150,
                                                    borderRadius: 75,
                                                    backgroundColor: '#dc7e27',
                                                    marginTop: 12,
                                                    alignItems: 'center'
                                                }}
                                                source={{
                                                    uri: user?.profile_image?.path ? user?.profile_image?.path : user?.profile_image,
                                                    headers: { Authorization: 'someAuthToken' },
                                                    priority: FastImage.priority.normal,
                                                    // cache:FastImage.cacheControl.cacheOnly
                                                }}
                                                resizeMode={FastImage.resizeMode.cover}
                                            />
                                        </TouchableOpacity>
                                        <View style={{}} >
                                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                                <Text style={{ textAlign: "center", color: "black", fontWeight: "bold", marginTop: 15, fontSize: 12 }} >Appuyer sur le cercle pour selectionner une image </Text>
                                                <MaterialCommunityIcons name="folder-image" size={30} style={{ marginTop: 10, marginLeft: 3, color: "#7c3325", }} />
                                            </View>
                                            <View style={{ marginTop: 10 }}>
                                                <TouchableOpacity style={styles.btn}
                                                    onPress={() => { this.save_photo(), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                    <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold", }}>Modifier </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </ScrollView>

                                ) :
                                this.state.what_modal == 'username' ? (
                                    <View style={{ marginTop: 40 }}>
                                        <View style={{ flexDirection: "row" }} >
                                            <View style={{ width: '25%' }}></View>
                                            <View >

                                                <View style={{}}>
                                                    <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold", marginTop: 25 }} >Changer votre nom d'utilisateur  </Text>
                                                </View>

                                            </View>
                                        </View>
                                        <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                            value={user.username}
                                            placeholder="Entrez votre nom d'utilisateur...."
                                            placeholderTextColor="#003f5c"
                                            onChangeText={val => this.handleChange('username', val)}
                                        //style={{ marginTop: 70, textAlign: 'center' }}
                                        />
                                        <Divider style={{ height: 30, marginLeft: 35, marginTop: '5%', fontWeight: 'bold', marginHorizontal: '3%', color: 'black' }} />
                                        <View style={{ marginTop: 70 }}>
                                            <TouchableOpacity style={styles.btn}
                                                onPress={() => { this.save(user), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold", }}>Modifier </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>) :
                                    this.state.what_modal == 'first' ? (
                                        <View style={{ marginTop: 40 }}>
                                            <View style={{}} >
                                                <View style={{}}>
                                                    <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold", marginTop: 25 }} >Changer votre nom</Text>
                                                </View>
                                            </View>
                                            <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                                value={user.first_name}
                                                placeholder="Entrez votre Nom ... "
                                                placeholderTextColor="#003f5c"
                                                onChangeText={val => this.handleChange('first_name', val)}
                                            //style={{ marginTop: 70, textAlign: 'center' }}
                                            />
                                            <View style={{ marginTop: 70 }}>
                                                <TouchableOpacity style={styles.btn}
                                                    onPress={() => { this.save(user), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                    <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold", }}>Modifier </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>) :
                                        this.state.what_modal == 'last' ? (
                                            <View style={{ marginTop: 40 }}>
                                                <View style={{}} >
                                                    <View style={{ width: '25%' }}></View>
                                                    <View style={{}}>
                                                        <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold", marginTop: 25 }} >Changer votre prenom </Text>
                                                    </View>

                                                </View>
                                                <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                                    value={user.last_name}
                                                    placeholder="Entrez votre prénom... "
                                                    placeholderTextColor="#003f5c"
                                                    onChangeText={val => this.handleChange('last_name', val)}
                                                // style={{ marginTop: 70, textAlign: 'center' }}
                                                />
                                                <View style={{ marginTop: 70 }}>
                                                    <TouchableOpacity style={styles.btn}
                                                        onPress={() => { this.save(user), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                        <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Modifier </Text>
                                                    </TouchableOpacity>
                                                </View>


                                            </View>) :
                                            this.state.what_modal == 'phone_number' ? (
                                                <View style={{ marginTop: 40 }}>
                                                    <View style={{}} >
                                                        <View style={{}}>
                                                            <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold", marginTop: 25 }} >Changer de numéro </Text>
                                                        </View>
                                                    </View>
                                                    <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                                        value={user.phone_number}
                                                        placeholder="phone number ... "
                                                        placeholderTextColor="#003f5c"
                                                        keyboardType="numeric"
                                                        onChangeText={val => this.handleChange('phone_number', val)}
                                                    // style={{ marginTop: 70, textAlign: 'center' }}
                                                    />
                                                    <View style={{ marginTop: 70 }}>
                                                        <TouchableOpacity style={styles.btn}
                                                            onPress={() => { this.setState({ isVisible: !this.state.isVisible }) }}>
                                                            <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Modifier </Text>
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>) :
                                                this.state.what_modal == 'gender' ? (
                                                    <View style={{ marginTop: 40 }}>
                                                        <View style={{}} >
                                                            <View style={{ width: '25%' }}></View>
                                                            <View style={{}}>
                                                                <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold", marginTop: 25 }} >Changer de genre </Text>
                                                            </View>

                                                        </View>
                                                        <View style={{ padding: 40, flexDirection: "row", justifyContent: "space-between" }}>
                                                            <View style={{ flexDirection: "row" }}>
                                                                <Text style={{ fontWeight: "bold" }} >
                                                                    M
                                                                </Text>
                                                                <RadioButton
                                                                    value="true"
                                                                    status={user.gender === "M" ? 'checked' : 'unchecked'}
                                                                    onPress={this.handleChangeradio}
                                                                />
                                                            </View>
                                                            <View style={{ flexDirection: "row" }}>
                                                                <Text style={{ fontWeight: "bold" }}>
                                                                    F
                                                                </Text>
                                                                <RadioButton
                                                                    value="false"
                                                                    status={user.gender === "F" ? 'checked' : 'unchecked'}
                                                                    onPress={this.handleChangeradio}
                                                                />
                                                            </View>
                                                        </View>
                                                        <View style={{ marginTop: 70 }}>
                                                            <TouchableOpacity style={styles.btn}
                                                                onPress={() => { this.save(user), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Modifier </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>) :
                                                    this.state.what_modal == 'countrie' ? (
                                                        <View style={{ marginTop: 50 }}>
                                                            <View style={{ flexDirection: "row" }} >
                                                                <View style={{ width: '25%' }}>
                                                                    <TouchableOpacity style={{ backgroundColor: "white", width: "100%", alignSelf: 'flex-start' }} onPress={() => this.initlocation()}>
                                                                        <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Initialiser </Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={{ width: '50%' }}>
                                                                    <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold" }} >Zones d'opérations </Text>
                                                                </View>
                                                                <View style={{ width: '25%' }}>
                                                                    <TouchableOpacity style={{ backgroundColor: "white", width: "100%", alignSelf: 'flex-end' }}
                                                                        onPress={() => { this.setState({ isVisible: !this.state.isVisible }) }}>
                                                                        <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Arrêter </Text>
                                                                    </TouchableOpacity>
                                                                </View>

                                                            </View>
                                                            <View style={{ flexDirection: "row", marginTop: 20, fontSize: 16 }} >
                                                                <View style={{ width: '30%', borderBottomColor: '#dc7e27', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                                    <TouchableOpacity onPress={() => this.initlocation()}>
                                                                        <Text style={{ color: '#dc7e27', marginLeft: 8 }}>Pays</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                                <View style={{ width: '30%', borderBottomColor: user?.countrie !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                                    <TouchableOpacity onPress={() => this.initlocation()}>
                                                                        <Text style={{ color: user?.countrie !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>Ville</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>

                                                            <Card.Divider />

                                                            <ScrollView>
                                                                {
                                                                    user?.countrie == null ?
                                                                        this.state.countrys.map((item, i) => {
                                                                            return (
                                                                                <TouchableOpacity key={i} onPress={() => { this.handleChange('countrie', item.id), this.addlocation(item.name) }}>
                                                                                    <Text style={{ color: 'black', textAlign: "center", fontSize: 20, marginBottom: 10 }}> {item.name} </Text>
                                                                                </TouchableOpacity>

                                                                            )
                                                                        }) :

                                                                        user?.countrie != "" ? (
                                                                            <>
                                                                                {
                                                                                    user?.citie == null ?
                                                                                        this.state.countrys.map(count =>
                                                                                            (count.id === Number(user.countrie)) ?
                                                                                                <ScrollView >
                                                                                                    {this.state.citys.map(prov =>
                                                                                                        count.cities.map((province, i) =>
                                                                                                            (province === prov.id) ? (
                                                                                                                <TouchableOpacity key={i} onPress={() => { this.handleChange('citie', prov.id), this.addlocation(prov.name) }}>
                                                                                                                    <Text key={i} style={{ color: 'black', textAlign: "center", fontSize: 20, marginBottom: 10 }}> {prov.name} </Text>
                                                                                                                </TouchableOpacity>) : null
                                                                                                        )
                                                                                                    )}
                                                                                                </ScrollView> : null
                                                                                        ) :
                                                                                        <View >
                                                                                            <Text style={{ fontSize: 14, marginLeft: "3%" }}>{user?.countrie !== "" && user?.countrie !== null ? _.find(this.state.countrys, { id: user?.countrie })?.name + ', ' + _.find(this.state.citys, { id: user?.citie })?.name : "......"}</Text>
                                                                                        </View>

                                                                                }
                                                                            </>
                                                                        ) : null
                                                                }
                                                                <View style={{ height: 300 }}>

                                                                </View>
                                                            </ScrollView>
                                                            <View style={{ marginTop: 150 }}>
                                                                <TouchableOpacity style={styles.btn}
                                                                    onPress={() => { this.save(user), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                                    <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Modifier </Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>

                                                    ) : null

                        }
                    </View>
                </Modal>

                {
                    //fin modal personnalisé
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    detailsContainer: {
        height: '58%',
    },
    modal: {
        backgroundColor: "silver",
        borderRadius: 10,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.99,
        //marginBottom: height_ * 0.30,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modal1: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        //marginTop: height_ * 0.4,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    btn: {
        backgroundColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
    }
})