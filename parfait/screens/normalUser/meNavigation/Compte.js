import React, { Component } from 'react'
import {
    Image, Text, StyleSheet, TextInput, Alert,
    Switch, View, Modal, ScrollView, TouchableOpacity, Dimensions, Platform
} from 'react-native'
import { Card } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
//import PhotoUpload from 'react-native-photo-upload'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Divider, Badge } from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage'
import _ from 'underscore'
import axios from 'axios'
import DeviceInfo from 'react-native-device-info'
import StepIndicator from 'react-native-step-indicator'
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { lowerFirst } from 'lodash';
import Toast from 'react-native-simple-toast';
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);




const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}


const labels = ["Quel prestataire immobilier ? ", "Complétez ses informations"];
export default class Compte extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            what_modal: '',
            appel_bout: false,
            agence: {
                username: null,
                profile_image: null,
                phone_number: null,
                typ_user: null,
                descriptions: null,
                countrie: null,
                citie: null,
                neighbor: null
            },

            countrys: [],
            citys: [],
            photo: {},
            neighbors: [],
            idagence: null,
            selectedItems: [], currentPosition: 0, ancien: '',
            isLandlord: false, isAgent: false, isAgence: false
        }
    }

    componentDidMount() {

        this.displayData()


    }


    handlerChoosePhoto = () => {

        ImagePicker.openPicker({
            multiple: false,
            mediaType: 'photo',
        }).then(images => {
             console.log(images)
            this.handleChange('profile_image', images)
            this.setState({ photo: images })

        }).catch(error => {
            //alert(error)
            if (error.code === 'E_PICKER_CANCELLED') { // here the solution
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée. Veuillez réessayer!!!')
                return false;
            } else {
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée.')
            }
        });

    }


    getData(id) {
        axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/' + id)
            .then(resag => {

                let agence = resag.data ? resag.data : { countrie: null, citie: null, neighbor: null };

                this.setState({ agence: agence, selectedItems: agence.agent, ancien: agence.typ_user });
                axios.get(`https://imobbis.pythonanywhere.com/location/`)
                    .then(res => {
                        this.setState({
                            countrys: res.data.country,
                            citys: res.data.city, //neighbors: res.data.neighbor
                        });
                    })

            }).catch(err => console.log("err " + err))
    }


    handleChange = (name, value) => {
        const agence = { ...this.state.agence, [name]: value };
        this.setState({ agence });
    };

    initlocation() {
        let agence = { ...this.state.agence, countrie: null, citie: null, neighbor: null };
        this.setState({ agence: agence });
    }


    addMonths(date, months) {
        var f = new Date(date)
        var d = f.getDate();
        f.setMonth(f.getMonth() + +months);
        if (f.getDate() != d) {
            f.setDate(0);
        }
        return f;
    }

    save(item) {
        const { isAgent, isLandlord, isAgence } = this.state
        var type = isAgence ? 'agence immobiliere' : isAgent ? 'agent' : isLandlord ? 'Bailleur' : null
        const formData = new FormData();
        const { photo } = this.state
        formData.append("username", item.username);

        

        formData.append("profile_image", {
            name: photo?.filename,
            type: photo?.mime,
            uri: photo?.path
           
        });

        formData.append("phone_number", item.phone_number);
        formData.append("descriptions", item.descriptions);
        formData.append("countrie", Number(item.countrie));
        formData.append("citie", Number(item.citie));


        formData.append("typ_user", type);
        this.setState({ appel_bout: true });

       

        (
            fetch(`https://imobbis.pythonanywhere.com/new/utilisateur/${item.id}`, {
                method: 'PUT',
                headers: {
                    'content-type': 'multipart/form-data'
                },
                body: formData
            })
                .then(res => {

                    try {
                        AsyncStorage.setItem('myuser', JSON.stringify(res.data))
                    } catch { error => alert(error) }

                    axios.patch("https://imobbis.pythonanywhere.com/new/utilisateur/" + item.id, {
                        'date_prestataire': new Date(),
                        'date_experiration': this.addMonths(new Date(), 2)
                    }).catch(error => { }).then(response => {
                        console.log(response.data)
                        this.setState({ appel_bout: false })
                        Toast.showWithGravity(
                            "Vous venez de changer de statut; " + this.state.ancien + " à " + type,
                            Toast.LONG,
                            Toast.CENTER,
                         
                        );
                        this.props.navigation.navigate('Home')

                    })
                        .catch(error => { })

                }).catch(err => {
                    console.log(err)
                    this.setState({ appel_bout: false })
                    err.toString() == 'Error: Request failed with status code 400' ?
                        (
                            Alert.alert('Attention', "Remplissez tous les champs de votre compte. "),
                            this.props.navigation.navigate('Profil')
                        )
                        : err.toString() == 'Error: Network Error' ?
                            (
                                Alert.alert('Attention', "Une erreur c'est produite, verifiez votre connexion et recommencer. ")

                            )
                            :
                            (
                                Alert.alert('Attention', "Une erreur c'est produite, verifiez votre connexion et recommencer. "),
                                this.props.navigation.navigate('Profil')
                            )
                    console.log(err)
                })
        )




    }

    onPageChange(position) {
        this.setState({ currentPosition: position });

    }


    onSelectedItemsChange = selectedItems => {
        const agence = { ...this.state.agence, agent: selectedItems };
        this.setState({ agence });
        this.setState({ selectedItems });
    };

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.getData(json_id)

            this.setState({
                idagence: json_id
            })
        } catch { error => alert(error) };
    }

    render() {
        const { agence, selectedItems, countrys, citys, neighbors,
            isLandlord, isAgence, isAgent } = this.state;
        let items = [];




        return (
            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 45 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={2}
                />

                <Divider style={{ backgroundColor: 'silver', height: 8, width: "100%" }} />
                {this.state.currentPosition == 0 ?
                    <View style={{
                        backgroundColor: 'white', height: hp('80%')
                    }}>
                        <Text style={{
                            marginTop: '5%', fontSize: 13,
                            color: 'black', fontWeight: 'bold', textAlign: 'center'
                        }}>Bailleur, agent immobilier ou angence immobiliere</Text>

                        <View style={{
                            backgroundColor: 'white', height: hp('80%')
                        }}>
                            <View style={{ marginTop: '40%' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold' }}>Bailleur </Text>
                                    <View style={{ marginLeft: '70%' }}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isLandlord ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ isLandlord: itemValue, isAgent: false, isAgence: false })}
                                            value={isLandlord}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold' }}>Agent immobilier </Text>
                                    <View style={{ marginLeft: '50%' }}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isAgent ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ isAgent: itemValue, isLandlord: false, isAgence: false })}
                                            value={isAgent}
                                        />
                                    </View>
                                </View>


                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                                    <Text style={{ fontWeight: 'bold' }}>Agence immobilière </Text>
                                    <View style={{ marginLeft: '50%' }}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isAgence ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ isAgence: itemValue, isLandlord: false, isAgent: false })}
                                            value={isAgence}
                                        />
                                    </View>
                                </View>
                            </View>
                            {isAgent || isLandlord || isAgence ?
                                <TouchableOpacity style={[styles.loginBtn]}
                                    onPress={() => this.onPageChange(1)}>
                                    <Text style={{ fontSize: 22, color: 'white', fontWeight: '600' }}>Suivant</Text>
                                </TouchableOpacity> : null
                            }
                        </View>
                    </View>
                    :
                    this.state.currentPosition == 1 ?
                        <View style={{
                            backgroundColor: 'white', height: hp('80%')
                        }}>
                            <ScrollView>
                                <TouchableOpacity style={{ marginTop: 5, height: 100, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'Select' })}>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}> Profil</Text>
                                    </View>

                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <FastImage
                                            style={{
                                                paddingVertical: 30,
                                                width: 60,
                                                height: 50,
                                                borderRadius: 20,
                                                backgroundColor: '#dc7e27'
                                            }}
                                            source={{
                                                uri: agence?.profile_image?.uri ? agence?.profile_image?.uri : agence?.profile_image,
                                                headers: { Authorization: 'someAuthToken' },
                                                priority: FastImage.priority.normal,

                                            }}
                                            resizeMode={FastImage.resizeMode.cover}
                                        />
                                        <Feather color="#7c3325" name="chevron-right" size={30} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ height: 100, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'username' })}>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}>Nom à afficher</Text>
                                    </View>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 14 }}>{agence?.username ? agence?.username : "......"}</Text>
                                        <Feather color="#7c3325" name="chevron-right" size={30} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ height: '8.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'countrie' })}>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}>Votre lieu</Text>
                                    </View>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Feather color="#7c3325" name="chevron-right" size={30} />
                                    </View>
                                </TouchableOpacity>
                                <View >
                                    <Text style={{ color: agence?.countrie ? "black" : "red", fontSize: 14, marginLeft: "3%" }}>{agence?.countrie ? _.find(countrys, { id: agence?.countrie })?.name + ', ' + _.find(citys, { id: agence?.citie })?.name : "Selectionner vos zones d'opérations"}</Text>
                                </View>
                                <TouchableOpacity style={{ height: 100, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                //onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'phone_number' })}
                                >
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}>Numéro telephone</Text>
                                    </View>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 14 }}>{agence?.phone_number ? agence?.phone_number : "......"}</Text>
                                        <Feather color="#7c3325" name="chevron-right" size={30} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ height: '8.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'descriptions' })}>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}>Descriptions</Text>
                                    </View>
                                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text numberOfLines={1} style={{ fontSize: 14 }}>{agence?.descriptions ? agence?.descriptions : "......"}</Text>
                                        <Feather color="#7c3325" name="chevron-right" size={30} />
                                    </View>
                                </TouchableOpacity>


                                <View style={styles.row}>
                                    <View style={{ width: "50%" }}>
                                        <TouchableOpacity style={styles.loginBtn1}
                                            onPress={() => this.onPageChange(0)}>
                                            <Text style={{ fontSize: 18, color: '#7c3325', fontWeight: '600' }}>Précédent</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: "50%" }}>
                                        <TouchableOpacity style={styles.loginBtn}
                                            onPress={() => { this.state.appel_bout == false ? (this.save(this.state.agence), this.setState({ appel_bout: true })) : null }} >
                                            <Text style={{ fontSize: 18, textAlign: 'center', fontWeight: 'bold', }}>{this.state.appel_bout == false ? 'Terminer' : 'Patienter'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>


                            </ScrollView>
                        </View> : null

                }


                {
                    //modal personnalisé
                }
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <FontAwesome5 style={{ marginTop: 50, alignSelf: 'flex-end' }}
                            onPress={() => {
                                this.setState({ isVisible: !this.state.isVisible })
                            }}
                            name='times-circle'
                            size={30}
                            color='#7c3325' />
                        {
                            this.state.what_modal == 'Select' ?
                                (
                                    <ScrollView>
                                        <View style={{ flexDirection: "row" }} >
                                            <View style={{ width: '25%' }}></View>
                                            <View style={{ width: '50%' }}>
                                                <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold" }} >Changer de profil </Text>
                                            </View>
                                            <View style={{ width: '25%' }}>

                                            </View>
                                        </View>
                                        <TouchableOpacity
                                            onPress={() => this.handlerChoosePhoto()}
                                        >

                                            <FastImage
                                                style={{
                                                    paddingVertical: 30,
                                                    width: 150,
                                                    height: 150,
                                                    borderRadius: 75,
                                                    backgroundColor: '#dc7e27'
                                                }}
                                                source={{
                                                    uri: agence?.profile_image?.path ? agence?.profile_image.path : agence?.profile_image,
                                                    headers: { Authorization: 'someAuthToken' },
                                                    priority: FastImage.priority.normal,
                                                    // cache:FastImage.cacheControl.cacheOnly
                                                }}
                                                resizeMode={FastImage.resizeMode.cover}
                                            />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={styles.btn} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                            <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                        </TouchableOpacity>
                                    </ScrollView>

                                ) :
                                this.state.what_modal == 'username' ? (
                                    <View style={{ marginTop: 40 }}>
                                        <View style={{ flexDirection: "row" }} >
                                            <View style={{ width: '25%' }}></View>
                                            <View style={{ width: '50%' }}>
                                                <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold" }} >Changer de profil </Text>
                                            </View>
                                            <View style={{ width: '25%' }}>

                                            </View>
                                        </View>
                                        <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                            value={agence?.username}
                                            placeholder="name ... "
                                            placeholderTextColor="#003f5c"
                                            onChangeText={val => this.handleChange('username', val)}
                                        />

                                        <View style={{}}>
                                            <TouchableOpacity style={styles.btn}
                                                onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>) :
                                    this.state.what_modal == 'descriptions' ? (
                                        <View style={{ marginTop: 40 }}>
                                            <View style={{ flexDirection: "row" }} >
                                                <View style={{ width: '25%' }}></View>
                                                <View style={{ width: '50%' }}>
                                                    <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold" }} >Votre descriptions </Text>
                                                </View>
                                                <View style={{ width: '25%' }}>

                                                </View>
                                            </View>
                                            <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                                value={agence?.descriptions}
                                                placeholder="descriptions ... "
                                                placeholderTextColor="#003f5c"
                                                onChangeText={val => this.handleChange('descriptions', val)}
                                            />

                                            <TouchableOpacity style={styles.btn} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                            </TouchableOpacity>
                                        </View>) :
                                        this.state.what_modal == 'countrie' ? (
                                            <View style={{ marginTop: 40 }}>
                                                <View style={{ flexDirection: "row" }} >
                                                    <View style={{ width: '25%' }}>
                                                        <TouchableOpacity style={{ backgroundColor: "white", width: "100%", alignSelf: 'flex-start' }} onPress={() => this.initlocation()}>
                                                            <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Initialiser </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{ width: '50%' }}>
                                                        <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold" }} >Zones d'opérations </Text>
                                                    </View>
                                                    <View style={{ width: '25%' }}>
                                                        <TouchableOpacity style={{ backgroundColor: "white", width: "100%", alignSelf: 'flex-end' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                            <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: "row", marginTop: 20, fontSize: 16 }} >
                                                    <View style={{ width: '30%', borderBottomColor: '#dc7e27', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                        <TouchableOpacity onPress={() => this.initlocation()}>
                                                            <Text style={{ color: '#dc7e27', marginLeft: 8 }}>Pays</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{ width: '30%', borderBottomColor: agence?.countrie !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                        <TouchableOpacity onPress={() => this.initlocation()}>
                                                            <Text style={{ color: agence?.countrie !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>Ville</Text>
                                                        </TouchableOpacity>
                                                    </View>

                                                </View>
                                                <Card.Divider />
                                                <ScrollView>
                                                    {
                                                        agence?.countrie == null ?
                                                            this.state.countrys.map((item, i) => {
                                                                return (
                                                                    console.log(item.name),

                                                                    <TouchableOpacity key={i} onPress={() => { this.handleChange('countrie', item.id) }}>
                                                                        <Text style={{ color: 'black', textAlign: "center", fontSize: 20, marginBottom: 10 }}> {item.name} </Text>
                                                                    </TouchableOpacity>

                                                                )
                                                            }
                                                            ) :

                                                            agence?.countrie !== null ? (
                                                                <>
                                                                    {
                                                                        agence?.citie == null ?
                                                                            this.state.countrys.map(count =>
                                                                                (count.id === Number(agence?.countrie)) ?
                                                                                    <ScrollView >
                                                                                        {this.state.citys.map(citie =>
                                                                                            count.cities.map((ville_, i) =>
                                                                                                (ville_ === citie.id) ? (
                                                                                                    <ScrollView>
                                                                                                        <TouchableOpacity key={i} onPress={() => { this.handleChange('citie', citie.id) }}>
                                                                                                            <Text key={i} style={{ color: 'black', textAlign: "center", fontSize: 20, marginBottom: 10 }}> {citie.name} </Text>
                                                                                                        </TouchableOpacity>
                                                                                                    </ScrollView>
                                                                                                ) : null
                                                                                            )
                                                                                        )}
                                                                                    </ScrollView> : null
                                                                            ) :

                                                                            <View >
                                                                                <Text style={{ fontSize: 14, marginLeft: "3%" }}>{agence?.countrie !== "" && agence?.countrie !== null ? _.find(countrys, { id: agence?.countrie })?.name + ', ' + _.find(citys, { id: agence?.citie })?.name : "......"}</Text>
                                                                            </View>
                                                                    }
                                                                </>
                                                            ) : null
                                                    }
                                                    <View style={{ height: 300 }}>

                                                    </View>
                                                </ScrollView>
                                            </View>
                                        ) : null

                        }
                    </View>
                </Modal>


                {
                    //fin modal personnalisé
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    /*detailsContainer: {
        height: '58%',
    },*/
    modal: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_,

        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modal1: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.4,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    loginBtn: {
        backgroundColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        // marginTop: 40,
        // marginBottom: 10
    },
    loginBtn1: {
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        //marginTop: 40,
        // marginBottom: 10
    },
    row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
    btn: {
        marginTop: 20,
        backgroundColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
    }
})