import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import UserProfile from '../../profiles/userProfile';
import Me from './me';
import Compte from './Compte';
import Settings from '../../settings/settings';
import App from '../../../App';
import Service from '../../specialiste/Service';
import Specialiste from '../../specialiste/Specialiste';
//import Appsecond from '../../../Appsecond';
import AuthStack from '../../auth/authStack';
import Home from '../home';
import Profil from './updateMe';
import NavigationA from '../abonnement/navigationA'
import Entreprise from './Entreprise';
import RecentlyTouched from './recentlytouched';


const Stack = createStackNavigator();

export default class MeNavigation extends Component {
    constructor(props){
        super(props)
    }

    render(){
      return(
        <Stack.Navigator>
            
            <Stack.Screen name='Me' component={Me} options={{headerShown: false}}/>
            <Stack.Screen name='UserProfile' component={UserProfile} options={{headerShown: false}}/>
            <Stack.Screen name='Settings' component={Settings} options={{headerShown: false}}/>
            <Stack.Screen name='Compte' component={Compte} options={{headerShown: false}}/>
            <Stack.Screen name='Service' component={Service} options={{headerShown: false}}/>
            <Stack.Screen name='AuthStack' component={AuthStack} options={{ headerShown: false }} />
            <Stack.Screen name='Profil' component={Profil} options={{headerShown: false}}/>
           
            <Stack.Screen name='Entreprise' component={Entreprise} options={{headerShown: false}}/>
            <Stack.Screen name='Home' component={Home} options={{headerShown: false}}/>
            <Stack.Screen name='Specialiste' component={Specialiste} options={{headerShown: false}}/>
            <Stack.Screen name='RecentlyTouched' component={RecentlyTouched} options={{headerShown: false}}/>
            <Stack.Screen name='NavigationA' component={NavigationA} options={{headerShown: false}}/>
        
        </Stack.Navigator>
        )
    }
    
}