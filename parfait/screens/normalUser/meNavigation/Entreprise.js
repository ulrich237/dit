import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, Modal, Dimensions, TextInput, TouchableOpacity, Platform, Image, ScrollView, Picker, Switch } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import Form from 'react-native-form'
import { Divider, Badge } from 'react-native-elements'
import _ from 'underscore'
//import PhotoUpload from 'react-native-photo-upload'
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { RadioButton } from 'react-native-paper';
import axios from 'axios'
import MultiSelect from 'react-native-multiple-select';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-async-storage/async-storage'
//import Carousel from 'react-native-snap-carousel';
import Toast from 'react-native-simple-toast';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
const options = {
  title: 'Photo',
  takePhotoButtonTitle: 'prendre une photo avec votre camera',
  chooseFromLibraryButtonTitle: 'Choisir une photo dans votre galerie',
}

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);
export default class Service extends Component {
  constructor(props) {
    super(props);
    this.state = {
      withFlag: true,
      enterCode: false,
      codeconfirm: '',
      phone: '',
      modalVisible2: false,
      country: {
        cca2: 'CM',
        callingCode: '237'
      },
      users: [],
      countries: [],
      districts: [],
      cities: [],
      provinces: [],
      neighbours: [],
      location: '',
      specialiste: {

        description: null,
        nom: null,
        contact: null,
        mail: null,
      },
      etape: 1,
      selectedItems: [],
      multiImage: [],
      avatarSource: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyuNFyw05KSucqjifL3PhDFrZLQh7QAS-DTw&usqp=CAU',
      image: null,
      modalopen: false,
      touch: true, services: [],
      save: false,
      userid: null, photo: [],



    }
  }

  componentDidMount() {
    this.displayData()
    axios.get("https://imobbis.pythonanywhere.com/new/utilisateur").then(res => {
      this.setState({ users: res.data })
    }).catch(err => console.log(err));


    axios.get("https://imobbis.pythonanywhere.com/user/service").then(res => {
      this.setState({ services: res.data });
    });

    axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
      this.setState({ countries: res.data.country, districts: res.data.district, cities: res.data.city, neighbours: res.data.neighbor, provinces: res.data.province });
    });
  }



  handleChange = (name, value) => {
    const specialiste = { ...this.state.specialiste, [name]: value };
    this.setState({ specialiste });
  };

  save(item) {
    console.log(item.nom)
    console.log(item.description)
    console.log(item.contact)
    console.log(item.mail)
    console.log(this.state.userid)
    console.log(this.state.selectedItems)
    const { photo } = this.state;
    var photos = _.toArray(photo)
    this.setState({ save: true })
    const formData = new FormData();
    formData.append("last_name", item.nom);
    formData.append("description_specialiste", item.description);
    formData.append("phone_number", Number(item.contact));
    formData.append("email", Number(item.mail));
    formData.append("is_specialiste", true);
    this.state.selectedItems.map(wh => {
      formData.append("service", wh);
    });
    axios
      .patch(`https://imobbis.pythonanywhere.com/new/utilisateur/${this.state.userid}`, formData, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      })

      .
      then(response => {

        Toast.showWithGravity(
          "veuillez patienter un moment vous y êtes presque",
          Toast.LONG,
          Toast.CENTER,
         
        );
        for (let i = 0; i < photo.length; i++) {

          formData.append('specialist', response.data.id);
          formData.append('image', {
            name: photo[i].path.split('/').pop(),
            type: photo[i].mime,
            uri: Platform.OS === 'android' ? photo[i].path : photo[i].path.replace('file://', ''),
          });
          axios.post(`https://imobbis.pythonanywhere.com/wen/specialist_image`, formData, {
            headers: {
              'Content-type': 'multipart/form-data',
            }
          })
            .then(res => {
              Toast.showWithGravity(
                "Image " + (i + 1) + " postée avec succès!",
                Toast.LONG,
                Toast.CENTER,
               
              );
              (i + 1) == photos.length ? (

                this.props.navigation.navigate('Specialiste'),
                this.setState({ save: false })
              ) : null
            }).catch(err => {
              alert(err)
              this.setState({ save: false })
            })
        };
      }).catch(err => {
        alert('err' + err)
        console.log(err)
        this.setState({ save: false })
      })
  }


  displayData = async () => {
    try {
      let user = await AsyncStorage.getItem('user_data');
      let json_id = JSON.parse(user)

      this.setState({ userid: json_id })
    } catch { error => alert(error) };
  }

  handlerChoosePhoto = () => {

    ImagePicker.openPicker({
      multiple: true,
      mediaType: 'photo',
    }).then(images => {
      this.setState({ photo: images })
    });

  }



  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };
  myFunction = () => {
    ImagePicker.showImagePicker(options, (response) => {
      //console.log('Response = ', response.uri);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('Image Picker Error: ', response.error);
      }

      else {
        let source = response;
        console.log(source);
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source.uri,
          image: source
        });
      }
    });
  }

  goToPikImage = () => {
    let imageList = [];
    ImagepikerM.openPicker({
      multiple: true,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      maxFiles: 5,
      mediaType: 'any',
      includeBase64: true,
    }).then(response => {
      //console.log('image piker = ', response);
      response.map(image => {
        imageList.push({
          path: image.path,
          mine: image.mine,
          data: image.data
        })
      })
      this.setState({ multiImage: imageList })
    })
      .catch(e => console.log('Error: ', e.message));
  }




  render() {
    const { specialiste, selectedItems, modalopen, touch, services } = this.state;

    console.log(selectedItems)
    return (
      <ScrollView>
        <View style={{ marginLeft: '15%', marginTop: '5%' }}><Text style={{ fontSize: 20, fontFamily: '' }}>Proposez un service</Text></View>
        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
        <View >
          <Text style={{ marginLeft: '3%', marginVertical: '3%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: 'brown' }}>Quel service offrez vous?</Text>
        </View>
        <MultiSelect
          hideTags
          items={this.state.services}
          uniqueKey="id"
          ref={(component) => { this.multiSelect = component }}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={selectedItems}
          selectText="selectionner votre service ..."
          searchInputPlaceholderText="Chercher un service..."
          onChangeInput={(text) => console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="brown"
          tagTextColor="black"
          selectedItemTextColor="brown"
          selectedItemIconColor="#orange"
          itemTextColor="#000"
          displayKey="name"
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="brown"
          submitButtonText="Submit"
        />
        <View>
          {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}

        </View>
        <View >
          <Text style={{ marginLeft: '3%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: 'brown' }}>Description :</Text>
        </View>
        <TextInput
          style={
            styles.touch
          }
          value={this.state.specialiste.description}
          placeholder="Entrez  une description"
          onChangeText={(val) => this.handleChange('description', val)}

        ></TextInput>
        <View >
          <Text style={{ marginLeft: '3%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: 'brown' }}>Email : </Text>
        </View>
        <TextInput
          style={
            styles.touch
          }
          value={this.state.specialiste.mail}
          placeholder="Entrez l'adresse mail de votre entreprise..."
          onChangeText={(val) => this.handleChange('mail', val)}
        ></TextInput>
        <View >
          <Text style={{ marginLeft: '3%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: 'brown' }}>Nom : </Text>
        </View>
        <TextInput
          style={
            styles.touch
          }
          value={this.state.specialiste.nom}
          placeholder="Entrez le Nom de votre entreprise..."
          onChangeText={(val) => this.handleChange('nom', val)}
        ></TextInput>
        <View>
          <Text style={{
            marginLeft: '3%', fontSize: 13, textDecorationLine: 'underline',
            textDecorationStyle: 'solid', color: 'brown'
          }}>Contact :</Text>
        </View>
        <TextInput
          style={
            styles.touch
          }
          value={this.state.specialiste.contact}
          placeholder="Entrez  votre Contact..."
          onChangeText={(val) => this.handleChange('contact', val)}
        ></TextInput>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ marginTop: '6%', marginLeft: '3%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: 'brown' }}>photos de vos realisations :</Text>
        </View>
        <View style={{}}>
          <TouchableOpacity style={{ flexDirection: 'row' }} onPress={this.handlerChoosePhoto}>
            <Ionicons name='add-circle-outline' size={60} color='#7c3325' />
            <Text style={{ fontWeight: "bold", marginTop: '5%' }}> Ajouter vos réalisations</Text>
          </TouchableOpacity>
        </View>

        <ScrollView horizontal={true}>
          {
            this.state.photo && (
              this.state.photo.map(im => {
                return (
                  <Image
                    source={{ uri: im.path }}
                    style={{
                      width: width_ / 2, height: height_ / 4, margin: 10,
                      borderRadius: 2
                    }} />
                )
              })
            )
          }
        </ScrollView>
        <View style={{ marginVertical: 10 }}>
          <TouchableOpacity style={{
            backgroundColor: '#7c3325', justifyContent: 'center',
            alignItems: 'center', marginHorizontal: 25, height: 45, borderRadius: 10
          }} onPress={() => this.save(this.state.specialiste)}>
            <Text style={{ color: "white", fontWeight: "bold" }}>{!this.state.save ? 'Valider le service' : 'Patienter'}</Text>
          </TouchableOpacity>
        </View>

      </ScrollView>


    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  modalView: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 15,
    alignItems: "flex-start",
    shadowColor: "#000000",
    width: '95%',
    height: '95%',
    shadowOffset: {
      width: 30,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 15
  },
  row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
  buttonstyle: {
    backgroundColor: 'white',
    height: 30,
    justifyContent: 'center'
  },
  touch: {
    color: 'black', marginLeft: '5%',

  },
  viewprofil: {
    height: 150, flexDirection: 'row',
    alignItems: 'center', justifyContent: 'space-between'
  },
  buttonProfil: {
    right: '20%', top: '10%', backgroundColor: 'grey', height: 50, width: 50,
    borderRadius: 50, justifyContent: 'center', alignItems: 'center'
  },
  //carousel: {
  // margin: 5, height: 200, width: 250, backgroundColor: 'grey', borderRadius: 15,
  //justifyContent: 'center', alignItems: 'center'
  // },
  modal: {
    flex: 1, backgroundColor: '#fff', width: '100%', borderBottomRightRadius: 10,
    borderTopLeftRadius: 10, borderTopRightRadius: 10, maxHeight: height_ * 0.7
  },

})

