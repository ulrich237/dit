import React, { Component } from "react";
import _ from 'underscore'
import axios from 'axios'
import {
  View, Text, StyleSheet, Dimensions, Image, TouchableOpacity,
  ScrollView
} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import FastImage from 'react-native-fast-image';

export default class RecentlyTouched extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {}, houses: [], isloading: false,
      TouchItem: {}, user_id: null, imageshouses: []
    }
  }

  componentDidMount() {
    this.displayData()

    /* axios.get(`https://imobbis.pythonanywhere.com/new/hous`,)
       .then(response =>
         this.setState({
           houses: response.data,
         }),
       )
       .catch(error => this.setState({ error,}));*/

    axios.get("https://imobbis.pythonanywhere.com/new/hous").then(resct => {
      this.setState({ houses: resct.data })
    }).catch(err => err.Error == 'Network Error' ? (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null);


    axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
      .then(response => this.setState({ imageshouses: response.data }),)
      .catch(error => this.setState({ error, isLoading: false }));


  }

  displayData = async () => {
    try {

      let user_id_ = await AsyncStorage.getItem('user_data');
      this.setState({ user_id: JSON.parse(user_id_) })
    } catch { }
  }

  render() {
    const { imageshouses, houses, user_id } = this.state;

    const { touchHouse } = this.props.route.params


    return (
      <View style={{ flex: 1, padding: 5, backgroundColor: 'white' }}>
        <ScrollView>
          {
            touchHouse.map(Touchitem_ => {
              return (
                <View style={{ marginTop: 20, borderColor: '#dc7e27', borderWidth: 2, padding: 5, borderRadius: 9 }}>
                  <Text style={{ fontWeight: '700', fontSize: 16 }}>{Touchitem_.timestamp.substring(0, 10)}</Text>
                  <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {
                      Touchitem_.touched.map(touch => {
                        return (
                          _.filter(houses, { id: touch }).map(hous_ => {
                            return (
                              <TouchableOpacity
                                style={{
                                  backgroundColor: '#fff',
                                  shadowColor: "black",
                                  shadowOffset: {
                                    width: 0, height: 6,
                                  }, //width: '22,85%',
                                   shadowOpacity: 0.1,
                                   shadowRadius: 8.30,
                                   marginBottom: 6, marginTop: 5,
                                   borderRadius: 8, marginRight: 10
                                }}
                                onPress={() => {
                                  this.props.navigation.navigate('HouseDetails',
                                    {
                                      id: hous_.id, userid: user_id,
                                      imageshouses: _.filter(imageshouses, { house: hous_.id })
                                    })
                                }}>
                                <FastImage
                                  style={{
                                    width: '100%', height: 90, borderTopLeftRadius: 8,
                                    resizeMode: 'stretch', borderTopRightRadius: 8
                                  }}
                                  source={{
                                    uri: _.find(imageshouses, { house: hous_.id })?.image,
                                    headers: { Authorization: 'someAuthToken' },
                                    priority: FastImage.priority.normal,
                                    // cache:FastImage.cacheControl.cacheOnly
                                  }}
                                    //resizeMode={FastImage.resizeMode.cover}
                                />
                                <Text style={{
                                  color: "red", fontSize: 12, fontWeight: '400',
                                  margin: 5, alignSelf: 'center'
                                }}>{hous_.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                              </TouchableOpacity>
                            )
                          })
                        )
                      })
                    }

                  </View>
                </View>
              )
            }

            )
          }
        </ScrollView>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  row_modal: {
    backgroundColor: '#fff',
    shadowColor: "black",
    shadowOffset: {
      width: 0, height: 6,
    }, width: '22,85%',
    shadowOpacity: 0.1,
    shadowRadius: 8.30,
    marginBottom: 6, marginTop: 5,
    borderRadius: 8, marginRight: 10
  },
})