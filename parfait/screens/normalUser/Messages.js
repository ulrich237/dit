import axios from 'axios'
import _ from 'underscore'
import React, { Component } from 'react'
import { View, Text, ScrollView, Image, StyleSheet, TouchableOpacity, Dimensions, FlatList, ActivityIndicator } from 'react-native'
import { Avatar } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import { Badge, Divider } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Date_heure from '../../functions/date_heure';



var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class MessagesI extends Component {
    constructor(props) {
        super(props),
            this.state = {
                users: [], senrec: [], user_id: null, refresh: false,
                item_start: 0, un_items: [], on_items: [], mom_beg: true,
                user_:{}, Allsenrec:[]
            }
    }

    componentDidMount() {
        this.dataFetcher()
        this.displayData()
        this.WillFocusSubscription = this.props.navigation.addListener(
            'focus',
            () => {
                this.dataFetcher();
            }
        );

            //  https://imobbis.pythonanywhere.com/new/messagin/

            axios.get('https://imobbis.pythonanywhere.com/new/messagin' ,)
            .then(response =>
                this.setState({
                    Allsenrec: response.data,
            
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));





    }

    /* componentWillUnmount() {
         this.WillFocusSubscription() ?
             this.WillFocusSubscription.remove()
             : null
     }*/

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.senrec !== this.state.senrec) {
            axios.get('https://imobbis.pythonanywhere.com/new/message_liste/' + this.state.user_id,)
            .then(response =>
                this.setState({
                    senrec: response.data,
                    isLoading: false,
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));
        }

        if (prevState.users !== this.state.users) {
            axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
                this.setState({ users: resu.data })
            }).catch(err => console.log(err));
        }

    }


    itemsToRender = async () => {
        try {
            const { senrec } = this.state
            var union_items = senrec
            this.setState({ un_items: union_items, on_items: union_items.slice(0, 10), item_start: 10 })
        } catch (er) {
            alert(er)
        }
    }

    itemsToRender2 = async () => {
        setTimeout(() => {
            if (!this.state.mom_beg) {
                try {
                    const { item_start, un_items, on_items } = this.state
                    var mod_items = on_items
                    var show = mod_items.concat(un_items.slice(item_start, item_start + 10))
                    this.setState({ item_start: item_start + 10, on_items: show, mom_beg: true })
                } catch (er) {
                    console.log(er)
                }
            }
        }, 1000)

    }

    renderLoader = () => {
        const { on_items, un_items } = this.state
        return (
            _.size(un_items) == 0 || _.size(un_items) <= _.size(on_items) ?
                <View style={styles.loaderStyle}>
                    <ActivityIndicator size="large" color="#7c3325" />
                </View> : null
        );
    };

    dataFetcher = () => {

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response =>
                this.setState({
                    users: response.data,
                    isLoading: false,
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));

    }

    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })

    };


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            let users = await AsyncStorage.getItem('myuser');

            let json_ = JSON.parse(users)

            console.log(json_)

            this.setState({ user_id: json_id, user_: json_ })

            axios.get('https://imobbis.pythonanywhere.com/new/message_liste/' + json_id,)
                .then(response =>
                    this.setState({
                        senrec: response.data,
                        isLoading: false,
                    }),
                )
                .catch(error => this.setState({ error, isLoading: false }));


        } catch { error => alert(error) };
    }

    // fonction des calculs d heures

    You_received = (data) => {

     return _.size(_.filter(data, (num) => ((num?.view == false && num?.user_receiver == this.state.user_id))))
   
    }

    // recupere le message le plus recent des messages
    recently_message = (data) => {
        var j = 0, recent = data[0];
        for (let i = 0; i < data.length; ++i) {
            if (recent.time_sent < data[i].time_sent) {
                j = i,
                    recent = data[i];
            }
        }
        return data[j]
    }

    // verifis quoi afficher selon le message photo ou lien ou message simple

    affiche = (data) => {
        if (this.recently_message(data)?.data?.text != '') {
            return this.recently_message(data)?.data?.text
        } else {
            if (this.recently_message(data)?.data?.house != '') {
                return "Lien"
            } else {
                return "photo"
            }
        }
    }

    render() {
        var id_second_user = []
        var sms = []
        var messages = []
        var store_ = {}
        var message
        var m = 0
        const { senrec, users, Allsenrec, user_id } = this.state
    
    

        return (
            <View style={{ flex: 1 }}>
                <View style={{
                    height: height_ / 15, justifyContent: 'space-between', flexDirection:
                        'row', marginBottom: 3, backgroundColor: '#dc7e27'
                }}>
                    <View style={{ paddingLeft: 7 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', }}>
                            <TouchableOpacity style={{ marginLeft: 20, marginTop: 14 }}
                                onPress={() => this.props.navigation.openDrawer()}
                            >
                                <FontAwesome name='list' color='black' size={20} />
                            </TouchableOpacity>
                            <Text style={{
                                marginLeft: 50, margin: 10,
                                color: 'black', marginTop: 10, fontSize: 20, fontWeight: 'bold'
                            }}>
                                IMOBBIS</Text>
                        </View>
                    </View>

                </View>


                <FlatList
                    numColumns={1}
                    data={senrec}
                    showsVerticalScrollIndicator={false}
                    onMomentumScrollBegin={() => this.setState({ mom_beg: false })}
                    renderItem={({ item, index, separators }) => (
                        sms = [],
                        sms = _.filter(Allsenrec, (num) => ((num?.user_receiver == item?.user_receiver?.id && num?.user_sender == this.state.user_id) || (num?.user_sender == item?.user_sender?.id && num?.user_receiver == this.state.user_id))),
                      
                        store_ = item?.user_receiver?.id == user_id? item.user_sender : item.user_receiver,

                        console.log(sms),

                       <View style={{ marginTop: 10 }}>
                            {
                                //ici                         
                                <View>
                                    <TouchableOpacity style={{ flexDirection: 'row' }}
                                        onPress={() => {
                                            this.props.navigation.navigate(
                                               'MessagesIni', {
                                                user: item?.user_receiver?.id == user_id? item.user_sender : item.user_receiver,
                                                sms: sms,
                                            })
                                        }}
                                    >
                                        <View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Avatar.Image size={45} source={{ uri: store_?.profile_url }} />
                                                <View style={{ marginLeft: 15 }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <View style={{ width: width_ * 0.6, }}>
                                                            <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: item?.user_receiver?.id == user_id && !item.view  ?  'bold' :'200' }}>
                                                                {store_?.username}
                                                            </Text>
                                                        </View>
                                                        <Text style={{ fontSize: 12, fontWeight: '200', color: item?.user_receiver?.id == user_id && !item.view ? 'red': 'black'}}> {new Date_heure().time(item?.time_sent)}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                        <View style={{ width: width_ * 0.6, }}>
                                                            <View style={{ flexDirection: 'row', }}>
                                                                <View style={{ marginRight: 5, marginTop: 5 }}>
                                                                    {
                                                                      item?.user_sender?.id == this.state.user_id ?
                                                                            <FontAwesome5 style={{}}
                                                                                name='check-double'
                                                                                size={10}
                                                                                color={!item?.view ? 'black' : '#dc7e27'}
                                                                            />
                                                                            : null

                                                                    }
                                                                </View>
                                                                <View>
                                                                    <Text style={{ fontSize: 14, fontWeight: item?.user_receiver?.id == user_id && !item.view  ? 'bold' : '200' }} numberOfLines={1}>
                                                                        {
                                                                            item?.image !== null ? <View style={{ marginTop: 2, flexDirection: 'row' }}><FontAwesome name='photo' size={10} /><Text> Photo</Text></View> :
                                                                                item?.house !== null ? <View style={{ flexDirection: 'row' }}><FontAwesome style={{ marginTop: 5 }} name='openid' size={10} /><Text> Lien</Text></View> :
                                                                                item?.context
                                                                        }
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                        <Text>
                                                            {
                                                                this.You_received(sms) != 0 ?
                                                                    <Badge
                                                                        status="error"
                                                                        value={this.You_received(sms)}
                                                                        size={20}
                                                                        containerStyle={{}}
                                                                    />
                                                                : null
                                                            }
                                                        </Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }
                            <View style={{ marginTop: 10 }}>
                                <Divider style={{ backgroundColor: 'black' }} />
                            </View>
                        </View>
                    )}
                    ListFooterComponent={this.renderLoader}
                    onEndReachedThreshold={0.7}
                    keyExtractor={item => item}
                    onEndReached={this.itemsToRender2}
                    refreshing={this.state.refresh}

                    onRefresh={this.handleRefesh}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    store_row4: {
        padding: 5,
        width: '99%',
        backgroundColor: "#ffffff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.84,
        elevation: 5,
        marginBottom: 10,
        borderRadius: 30

    },
})