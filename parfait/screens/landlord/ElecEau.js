import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';

//import ImagePicker from 'react-native-image-crop-picker';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import StepIndicator from 'react-native-step-indicator'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
    Picker,
    StatusBar,
    DrawerLayoutAndroid,
    
    Switch
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Avatar } from 'react-native-paper';
import Toast from 'react-native-simple-toast';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);


const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#FF0000',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#FF0000'
}
const labels = ["Electricité", "Eaux "];


export default class ElecEau extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentPosition: 0, save: false, post: false, mois: 0, renter: {}, users: [],
            priceElect: 0, priceEau: 0, house_: {}, payeEau: false, payeElec: false,
            modal: false, renterChoice: {}, us: false, filter: ''
        };
    }



    componentDidMount() {
        const { house } = this.props.route.params;
        axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/')
            .then((res) => {
                this.setState({
                    house_: _.find(res.data, { house: house?.id }),
                })
            })
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get('https://imobbis.pythonanywhere.com/wen/locataires')
            .then((res) => {
                this.setState({
                    renter: _.find(res.data, { current_house: house?.id }),
                })
            })
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get('https://imobbis.pythonanywhere.com/new/utilisateur/')
            .then((res) => {
                this.setState({
                    users: res.data,
                })
            })
            .catch(error => this.setState({ error, isLoading: false }));

    }


    add() {
        const { house } = this.props.route.params;
        const { renterChoice } = this.state;
        var a
        axios.get('https://imobbis.pythonanywhere.com/wen/locataires')
            .then((res) => {
                a = _.find(res.data, { current_house: house?.id }),
                    a ? (
                        this.jours(a?.date_joined, a?.cr_hs_st_dt) >= 10 ? (
                            axios.patch("https://imobbis.pythonanywhere.com/wen/locataires" + a?.id, {
                                "has_rented_before": false,
                                "looking_for_house": false,
                                "cr_hs_st_dt": new Date(),
                                "user": renterChoice?.id,
                            }).catch(error => alert(error)).then(response => {
                                this.setState({ modal: false, post: true })

                                Toast.showWithGravity(
                                    "Enregistré avec succés!",
                                    Toast.LONG,
                                    Toast.BOTTOM,
                                  
                                );
                            })
                                .catch(error => alert(error))

                        ) :( alert("Vous devez patienter 10 jours apres expiration du contrat avant de pouvoir modifier"),
                        this.setState({ modal: false, post: true }))
                    )
                        : (
                            axios.post("https://imobbis.pythonanywhere.com/wen/locataires",
                                {
                                    "has_rented_before": false,
                                    "looking_for_house": false,
                                    "cr_hs_st_dt": new Date(),
                                    "user": renterChoice?.id,
                                    "current_house": house?.id
                                })
                                .then(response => {
                                    this.setState({ modal: false, post: true, us:false
                                    
                                    
                                    
                                    })
                                    Toast.showWithGravity(
                                        "Enregistré avec succés!",
                                        Toast.LONG,
                                        Toast.BOTTOM,
                                       
                                    );
                                })
                                .catch(error => alert(error))
                        )

            })
            .catch(error => this.setState({ error, isLoading: false }));
    }

    jours(dateDebut, dataFin) {

        var d = new Date(dataFin), f = new Date(dateDebut), diff = {};

        var tmp = d - f;  //Nombre de millisecondes entre les dates

        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes

        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes

        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures

        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants

        return tmp

    }


    hoursDate(dateHours) {

        var d = new Date(Date.now()), f = new Date(dateHours), diff = {};
        var tmp = d - f;  //Nombre de millisecondes entre les dates

        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes

        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes

        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)

        return tmp;
    }


    onPageChange(position) {
        this.setState({ currentPosition: position });

    }


    payeEau = () => {
        const { house } = this.props.route.params;
        const { house_ } = this.state;
        axios.patch("http://imobbis.pythonanywhere.com/wen/facture_eau_lumiere" + house_?.id, {
            'm3_consumed': 0,

        }).catch(error => alert(error)).then(response => {
            this.setState({ payeEau: true })
            Toast.showWithGravity(
                "Enregistré avec succés!",
                Toast.LONG,
                Toast.BOTTOM,
               
            );

        })
            .catch(error => alert(error))

    }

    payeElec = () => {
        const { house } = this.props.route.params;
        const { house_ } = this.state;
        axios.patch("http://imobbis.pythonanywhere.com/wen/facture_eau_lumiere" + house_?.id, {
            'kw_consumed': 0,

        }).catch(error => alert(error)).then(response => {
            this.setState({ payeElec: true })
            Toast.showWithGravity(
                "Enregistré avec succés!",
                Toast.LONG,
                Toast.BOTTOM,
               
            );

        })
            .catch(error => alert(error))

    }


    addMonths(date, months) {
        var f = new Date(date)
        var d = f.getDate();
        f.setMonth(f.getMonth() + +months);
        if (f.getDate() != d) {
            f.setDate(0);
        }
        return f;
    }



    update = () => {
        const { mois } = this.state;
        const { house } = this.props.route.params;
        var renter

        axios.get('http://imobbis.pythonanywhere.com/wen/locataires')
            .then((res) => {
                renter = _.find(res.data, { current_house: house?.id }),
                    renter ? (
                        axios.patch('http://imobbis.pythonanywhere.com/wen/locataires' + renter?.id, {
                            'cr_hs_st_dt': this.addMonths(renter?.cr_hs_st_dt, mois),

                        }).catch(error => alert(error)).then(response => {
                            this.setState({ post: false })
                            Toast.showWithGravity(
                                "Modification éffectué avec succés!",
                                Toast.LONG,
                                Toast.BOTTOM,
                               
                            );

                        })
                            .catch(error => alert(error))
                    ) : null
            })
            .catch(error => this.setState({ error, isLoading: false }));

    }



    save = () => {

        const {
            priceEau, priceElect, payeEau, payeElec, renter
        } = this.state;

        const { house } = this.props.route.params;


        axios.get("http://imobbis.pythonanywhere.com/wen/facture_eau_lumiere")
            .then((res) => {

                var data_ = _.find(res.data, { house: house?.id })
                data_ ? (
                    this.hoursDate(data_?.last_update) > 48 ? (
                        axios.patch("http://imobbis.pythonanywhere.com/wen/facture_eau_lumiere" + data_?.id, {
                            'kw_consumed': priceElect,
                            'm3_consumed': priceEau,

                        }).catch(error => alert(error)).then(response => {
                            this.setState({ save: false })

                            Toast.showWithGravity(
                                "Enregistré avec succés!",
                                Toast.LONG,
                                Toast.BOTTOM,
                              
                            );
                        })
                            .catch(error => alert(error))

                    ) : (
                            this.setState({ save: true })
                        )
                ) : (
                        axios.post("http://imobbis.pythonanywhere.com/wen/facture_eau_lumiere",
                            {
                                'kw_consumed': priceElect,
                                'm3_consumed': priceEau,
                                'house': house?.id
                            })
                            .then(response => {
                                this.setState({ save: false })
                                Toast.showWithGravity(
                                    "Enregistré avec succés!",
                                    Toast.LONG,
                                    Toast.BOTTOM,
                                  
                                );
                            })
                            .catch(error => alert(error))
                    )
            })
            .catch(error => this.setState({ error, isLoading: false }));

    }

    render() {
        const {
            save, payeElec, payeEau, priceEau, priceElect, house_,
            mois, renter, modal, users, renterChoice, filter, us
        } = this.state;
        const { house } = this.props.route.params;
        var dat = 48 - this.hoursDate(house_?.last_update)
        return (

            <ScrollView style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 45 : 0, backgroundColor: "#dc7e27" }}>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.us}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <TouchableOpacity style={{ flexDirection: 'row', }}
                            onPress={() => this.setState({ us: false })}>
                            <FontAwesome style={{ marginLeft: '90%', }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <TextInput
                            style={{}}
                            onChangeText={(val) => this.setState({ filter: val })}
                            value={this.state.filter}
                            placeholder="Rechercher..."

                        />
                        <ScrollView style={{

                        }}>
                            {
                                users.filter((value) => {
                                    return (
                                        (
                                            value.username.toLowerCase().indexOf(filter.toLowerCase()) > -1 ||
                                            value.first_name.toLowerCase().indexOf(filter.toLowerCase()) > -1 ||
                                            value.last_name.toLowerCase().indexOf(filter.toLowerCase()) > -1
                                        )
                                    )
                                }).map((user) => {
                                    return (
                                        <TouchableOpacity style={{ margin: 10 }}
                                            onPress={() => this.setState({ renterChoice: user, modal: true })}>

                                            <View style={{ flexDirection: 'row' }}>
                                                <Avatar.Image size={45} source={{ uri: user.profile_image }} />
                                                <View>
                                                    <Text>{user.username} {user.first_name} {user.last_name}</Text>
                                                    <Text>{user.email}</Text>
                                                </View>
                                            </View>

                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                </Modal>

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.post}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modalView}>
                        <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => this.setState({ post: false })}>
                            <FontAwesome style={{ marginLeft: '90%', }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <View>
                            <Text>Nombre de mois:</Text>
                            <TextInput
                                style={styles.input}
                                onChangeText={(val) => this.setState({ mois: val })}
                                value={this.state.mois}
                                placeholder="Exemple: 24"
                                keyboardType="numeric"
                            />
                        </View>
                        <View style={{ flexDirection: 'row-reverse' }}>
                            <TouchableOpacity style={{}}
                                onPress={() => {
                                    this.update()
                                }}>
                                <View style={{ flexDirection: 'row', margin: 10 }}>

                                    <Text style={{color:'#7c3325',fontWeight:'bold'}}>Sauvegarder</Text>

                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.modal}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modalView}>
                        <View>
                            <Text>Voullez vous vraiment mettre {renterChoice?.username} comme locataire de {house?.name}
                            </Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Image
                                    source={{
                                        uri: house?.photo_1
                                    }}
                                    style={{ height: 20, resizeMode: 'stretch', width: 20 }}
                                />
                                <Image
                                    source={{
                                        uri: house?.photo_2
                                    }}
                                    style={{ height: 20, resizeMode: 'stretch', width: 20 }}
                                />
                                <Image
                                    source={{
                                        uri: house?.photo_3
                                    }}
                                    style={{ height: 20, resizeMode: 'stretch', width: 20 }}
                                />
                                <Image
                                    source={{
                                        uri: house?.photo_4
                                    }}
                                    style={{ height: 20, resizeMode: 'stretch', width: 20 }}
                                />

                            </View>

                        </View>


                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={styles.button}
                                onPress={() => {
                                    this.setState({ modal: false })
                                }}>
                                <View style={{}}>
                                    <Text style={{color:'white'}}>Non</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button}
                                onPress={() => {
                                    this.add()
                                }}>
                                <View style={{}}>
                                    <Text style={{color:'white'}}>Oui</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                { renter ?
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity style={[styles.button, { backgroundColor: '#7c3325', }]}
                                onPress={() => this.setState({ post: true })}>
                                <Text style={{ fontSize: 12, color: 'white', fontWeight: '600' }}> le nombre de mois payé</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.button, { backgroundColor: '#7c3325', }]}
                                onPress={() => this.setState({ us: true })}>
                                <Text style={{ fontSize: 12, color: 'white', fontWeight: '600' }}>Changer de locataire</Text>
                            </TouchableOpacity>
                        </View>

                        <StepIndicator
                            customStyles={customStyles}
                            currentPosition={this.state.currentPosition}
                            labels={labels}
                            stepCount={2}
                        />
                    </View> : null
                }

                {
                    renter ? (
                        this.state.currentPosition == 0 ?
                            <View>
                                <Text style={{
                                    marginTop: '5%', fontSize: 20,
                                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                                }}>Quantité d'électricité consommé en kilowatt</Text>

                                <View style={{
                                    backgroundColor: 'white', height: hp('80%')
                                }}>
                                    <View style={{ marginTop: 20 }}>
                                        {
                                            house_ && house_?.m3_consumed != 0 && payeEau == false ?
                                                <TouchableOpacity style={[styles.button, { backgroundColor: '#7c3325', }]}
                                                    onPress={() => this.payeEau()}>
                                                    <Text style={{ fontSize: 15, color: 'white', fontWeight: '600' }}>Mettre le compteur à 0</Text>
                                                </TouchableOpacity>
                                                : <Text style={{
                                                    textAlign: 'center', color: 'red',
                                                    fontWeight: 'bold'
                                                }}>Quantité d'eau pas encore enregistrée</Text>

                                        }
                                    </View>
                                    <View style={{ marginTop: '25%' }}>
                                        <View style={{ margin: 10 }}>
                                            <Text style={{ fontWeight: 'bold',margin:5 }}>quantité :
                                              </Text>
                                            <TextInput
                                                style={styles.input}
                                                onChangeText={(val) => this.setState({ priceElect: val })}
                                                value={this.state.priceElect}
                                                placeholder="Taper la quantité ici"
                                                keyboardType="numeric"
                                            />
                                        </View>

                                    </View>
                                    {priceElect != 0 ?
                                        <TouchableOpacity style={ { height:'8%',backgroundColor: '#7c3325',width:'40%',marginLeft:'50%',justifyContent:'center',alignItems:'center',borderRadius:15,marginTop:'4%'}}
                                            onPress={() => this.onPageChange(1)}>
                                            <Text style={{ fontSize: 17, color: 'white', fontWeight: '600' }}>Suivant</Text>
                                        </TouchableOpacity> : null
                                    }

                                </View>
                            </View>
                            :
                            <View>
                                <Text style={{
                                    color: 'black', fontWeight: 'bold',
                                }}>Quantité d'eaux consommé en métre cube</Text>
                                <View style={{
                                    //alignItems: "center",
                                    //justifyContent: "center",
                                    backgroundColor: 'white', height: hp('80%')
                                }}>
                                    <ScrollView>
                                        <View style={{ flexDirection: 'row',marginTop:'5%',marginLeft:'4%' }}>
                                            <TouchableOpacity style={[ { backgroundColor: '#f9d7a7',height:'130%',borderRadius:15 ,width:'30%'}]}
                                                onPress={() => this.onPageChange(0)}>
                                                <Text style={{ fontSize: 15, color: 'white', fontWeight: '600',margin:5}}>Précédent</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={{ marginTop: 20 }}>
                                            {
                                                house_ && house_?.kw_consumed != 0 && payeElec == false ?
                                                    <TouchableOpacity style={[styles.button, { backgroundColor: '#7c3325', }]}
                                                        onPress={() => this.payeElec()}>
                                                        <Text style={{ fontSize: 15, color: 'white', fontWeight: '600' }}>Ce bien a t-il payé l'électricité?</Text>
                                                    </TouchableOpacity>
                                                    : <Text style={{
                                                        textAlign: 'center', color: 'red',
                                                        fontWeight: 'bold'
                                                    }}>Quantité d'électricité pas encore enregistrée</Text>

                                            }
                                        </View>

                                        <View style={{}}>
                                            <View style={{ marginTop: '25%' }}>
                                                <View style={{ margin: 10 }}>
                                                    <Text style={{ fontWeight: 'bold' }}>quantité :
                                              </Text>
                                                    <TextInput
                                                        style={styles.input}
                                                        onChangeText={(val) => this.setState({ priceEau: val, currentPosition: 2 })}
                                                        value={this.state.priceEau}
                                                        placeholder="Taper la quantité ici"
                                                        keyboardType="numeric"
                                                    />
                                                </View>


                                            </View>
                                            {priceEau != 0 ?
                                                house_ ?
                                                    this.hoursDate(house_?.last_update) > 48 && save == false ?
                                                        <TouchableOpacity  style={{ backgroundColor:'#7c3325', width:'30%', marginLeft:'40%', borderRadius:15, marginTop:'4%'}}
                                                            onPress={() => this.save()}>
                                                            <Text style={{ fontSize: 10, color: 'white' }}>Sauvegarder</Text>
                                                        </TouchableOpacity>
                                                        : <Text style={{
                                                            textAlign: 'center', color: 'red',
                                                            fontWeight: 'bold', fontSize: 19,
                                                        }}>Vous ne pouvez plus modifier, patientez {dat} {dat == 1 ? ' heure' : ' heures'}</Text>
                                                    : <TouchableOpacity style={ { backgroundColor:'#7c3325', width:'45%', marginLeft:'40%', borderRadius:15, marginTop:'4%',justifyContent:'center',alignItems:'center',height:50 }}
                                                        onPress={() => this.save()}>
                                                        <Text style={{ fontSize: 17, color: 'white', fontWeight: '500' }}>Sauvegarder</Text>
                                                    </TouchableOpacity>
                                                : null

                                            }
                                        </View>

                                    </ScrollView>
                                </View>

                            </View>

                    ) : (  <View>
                            <View>
                                <Text style={{
                                    marginTop: '5%', fontSize: 20,
                                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                                }}>Ajouter un locataire</Text>

                                <TouchableOpacity style={{ backgroundColor: '#7c3325'}}
                                    onPress={() => this.setState({ us: true })}>
                                    <Text style={{ fontSize: 19, color: 'white', fontWeight:'400',height:'80%',marginTop:'5%'}}>Selectionner le locataire</Text>
                                </TouchableOpacity>
                            </View>
                            </View>
                        )

                }


            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "azure",

    },
    input: {
        height: 40,
        borderWidth: 2,
        width: '80%',
        color: '#05375a',
    },

    button: {
        height: 40, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 8, borderRadius: 7,// marginTop: '5%'
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 4.0,
        marginTop: height_ * 0.40,
        marginLeft: width_ / 3,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modal: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_,
        height: height_,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },



});
