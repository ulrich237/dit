import React, { Component } from 'react';
import { Text, Dimensions, SafeAreaView } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import MyHouses from './myHouses';
import MyHousesReserve from './myHousesReserve';
import Feather from 'react-native-vector-icons/Feather'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
const TopTab = createMaterialTopTabNavigator();

var height_ = Math.round(Dimensions.get('window').height)


export default class NavigationGcel extends Component {
    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#fdf7e8', flex: 1 }}>
                <Text style={{ fontWeight: '700', color: '#7c3325', fontSize: 30, margin: 10 }}>Mes biens</Text>
                <TopTab.Navigator
                    tabBarOptions={{
                        activeTintColor: '#7c3325', inactiveTintColor: '#e9deb5',
                        indicatorStyle: { backgroundColor: '#7c3325' }
                    }}
                >
                    <TopTab.Screen name="MyHouses" children={props =>
                        <MyHouses navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Bien publié',
                            tabBarIcon: ({ color, size }) => (
                                <Feather name="list" color={color} size={21} />
                            ),
                        }}
                    />
                    <TopTab.Screen name="MyHousesReserve" children={props =>
                        <MyHousesReserve navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Bien non publié',
                            tabBarIcon: ({ color, size }) => (
                                <MaterialIcons name="local-mall" color={color} size={26} />
                            ),
                        }}
                    />
                </TopTab.Navigator>
            </SafeAreaView>
        )
    }
}


