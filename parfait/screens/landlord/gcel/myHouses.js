import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, FlatList, ActivityIndicator,
    TouchableOpacity, ScrollView, Alert, Dimensions
} from 'react-native'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import AntDesign from "react-native-vector-icons/AntDesign";
import Toast from 'react-native-simple-toast';

import AsyncStorage from '@react-native-async-storage/async-storage';

var width_ = Math.round(Dimensions.get('window').width)
const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};
export default class MyHouses extends Component {
    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: [], landlords: [], houses: [], user_id: null,
            imageshouses: [], gcel: [], touch: [], refresh: false
        }
    }

    componentDidMount() {
        this.displayData()
        this.mess()
        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/hous/`,)
            .then(response => this.setState({ houses: _.filter(response.data, { user: this.state.user_id }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
            .then(response => this.setState({ renters: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

    }



    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.houses !== this.state.houses) {
            axios.get('https://imobbis.pythonanywhere.com/new/hous/')
                .then((res) => {
                    this.setState({
                        houses: _.filter(res.data, { user: this.state.user_id }),
                        isLoading: false
                    });
                })
                .catch(error => this.setState({ error, isLoading: false }));
        }
        if (prevState.mess !== this.state.mess) {
            this.mess
        }
    }


    mess = () => {
        axios.get(`https://imobbis.pythonanywhere.com/wen/touch_house`,)
            .then(response =>
                this.setState({
                    touch: response.data,
                    isLoading: false,
                }),
            )
            .catch(error => this.setState({ error, isLoading: false }));
    }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }


    createAlert = (house) => {
        Alert.alert(
            'Suppression',
            'Voulez vous vraiment supprimer cette publication (' + house?.name + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.delete('https://imobbis.pythonanywhere.com/new/hous/' + house?.id).then((res) => {
                            Toast.showWithGravity(
                                "Suppression effectuée!",
                                Toast.LONG,
                                Toast.CENTER,
                                )
                        })
                            .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]
        )
    }




    indisponible = (house) => {
        var etat = house?.disponible

        Alert.alert(
            !etat ? 'Indisponiblité' : 'Disponiblité',
            'Voulez vous mettre cette publication (' + house?.name + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.patch('https://imobbis.pythonanywhere.com/new/hous/' + house?.id,
                            {
                                'disponible': !etat
                            }).then((res) => {

                                Toast.showWithGravity(
                                    "Opération effectuée!",
                                    Toast.LONG,
                                    Toast.CENTER,
                                   )
                            })
                            .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]

        )

    }

    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })

    };

    getDaysFromDays = (datein, dateout, day_or_month) => {

        let d = new Date(dateout); let f = new Date(Date.now()); let diff = {}; let take = ""
        let tmp = d - f;  //Nombre de millisecondes entre les dates
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        diff.days = tmp;
        var months = Math.floor(diff.days / 30)

        take = day_or_month ?
            diff.days + " jour(s) restant(s)" :
            months == 0 ? diff.days + " jour(s) restant(s)" : months + "mois restant(s)"

        return take

    }

    render() {
        const { users, landlords, houses, renters, user_id, imageshouses, touch } = this.state
        const { navigate } = this.props
        let renter
        let user
        var datas = []
        var labels = []
        var s = 0
        houses.map(hous_ => {
            labels.push(hous_?.name),
                touch.map(touch_ => touch_.touched.includes(hous_?.id) ? s = s + 1 : null),
                datas.push(s)
        })

        const data_ = {
            labels: _.size(labels) !== 0 ? labels : [10, 10],
            datasets: [
                {
                    data: _.size(datas) !== 0 ? datas : [10, 10],
                    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`, // optional
                    strokeWidth: 2 // optional
                }
            ],
            // legend: ["Plus visités"] // optional
        };



       
        return (

            <ScrollView style={{ backgroundColor: 'white' }}>
               { /*<View style={styles.imgContainer}>
                    <Text style={{ color: "black", fontWeight: 'bold', fontSize: 22 }}>
                        Les personnes touchées.
                    </Text>

        </View>*/}
        
                <FlatList
                    numColumns={1}
                    data={_.filter(houses, {gcel:false})}
                    showsVerticalScrollIndicator={false}
                    onMomentumScrollBegin={() => this.setState({ mom_beg: false })}
                    renderItem={({ item, index, separators }) => (
                        <View
                            containerStyle={{ flex: 1, }}
                            isLoading={true}
                            layout={[
                                { key: 'someId', width: 220, height: 20, marginBottom: 6 },
                                { key: 'someOtherId', width: 180, height: 20, marginBottom: 6 }
                            ]}
                        >
                            <View style={{ ...styles.row3 }}>
                                <TouchableOpacity style={{ height: 110, width: '34%', borderRadius: 15 }}
                                    onPress={() => navigate('MyDetails', {'user_': _.find(users, { id: _.find(renters, { current_house: item.id })?.user }),'house': item, 'images': imageshouses, 'renters': _.find(renters, { current_house: item.id })})}>
                                    <Image source={{ uri: _.find(imageshouses, { house: item.id })?.image }}
                                        style={{ height: 110, width: '100%', borderRadius: 15 }}
                                    />
                                </TouchableOpacity>
                                <View style={{ marginLeft: 10, width: '64%' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }} numberOfLines={1}>{item.name}</Text>
                                    <Text style={{ color: 'grey' }} numberOfLines={1}>
                                        {item.nb_room + ' chambre(s) / ' + item.nb_parlour + ' salon(s) / ' + item.nb_toilet + ' douche(s)'}
                                    </Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ marginRight: 5, fontSize: 10, color: 'grey', fontWeight: 'bold' }}>Locataire</Text>
                                        <AntDesign style={{ marginTop: 2 }} name='caretdown' color='grey' size={10} />
                                    </View>
                                    {
                                        renter = _.find(renters, { current_house: item.id }),
                                        user = _.find(users, { id: renter?.user }),
                                        renter ?
                                            <View style={{ padding: 4,flexDirection: 'row', marginTop: 3 }}>
                                                {user ?
                                                    <View style={{ flexDirection: 'row', }}>
                                                        <Avatar.Image size={25}
                                                            source={{ uri: user?.profile_image }} />
                                                        <View style={{ marginLeft: 5, width: '100%' }}>
                                                            <Text numberOfLines={1} style={{ fontSize: 11, fontWeight: 'bold' }}>{user?.username}</Text>
                                                            <Text style={{ fontSize: 11, color: 'red' }}>
                                                                {this.getDaysFromDays(renter.date_in, renter.date_out, item.is_immeuble)}
                                                            </Text>
                                                        </View>
                                                    </View> :
                                                    <View style={{}}>
                                                        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{renter?.prenom + " " + renter?.nom}</Text>
                                                        <Text style={{ fontSize: 11, color: 'red' }}>
                                                            {this.getDaysFromDays(renter.date_in, renter.date_out, item.is_immeuble)}
                                                        </Text>
                                                    </View>
                                                }
                                            </View>
                                            :
                                            <View style={{ borderWidth: 1, borderColor: 'grey', padding: 4, borderRadius: 10, flexDirection: 'row', marginTop: 3 }}>
                                                <Text style={{ marginRight: 5, color: 'red', fontWeight: 'bold' }}>
                                                    Cette maison n'a pas de locataire
                                                </Text>
                                            </View>


                                    }

                                    <View style={{ flexDirection: 'row-reverse', }}>
                                        <TouchableOpacity style={{ ...styles.bt1, backgroundColor: '#7c3325' }}
                                            onPress={() => { _.find(renters, { current_house: item.id }) ? Alert.alert('Attention', 'Vous ne pouvez pas modifier, elle posséde un locataire.') : this.indisponible(item) }}>
                                            <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>{!item?.disponible || _.find(renters, { current_house: item.id }) ? 'Indisponible' : 'Disponible'}</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ ...styles.bt1, marginHorizontal: 7, backgroundColor: 'red' }}
                                            onPress={() => this.createAlert(item)}>
                                            <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Supprimer</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ ...styles.bt1, backgroundColor: 'green' }}
                                            onPress={() => navigate('UpdateEtape', { 'data': item })}>
                                            <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Modifier</Text>
                                        </TouchableOpacity>
                                    </View>


                                </View>
                            </View>
                        </View>
                    )}
                    //ListFooterComponent={this.renderLoader}
                    onEndReachedThreshold={0.7}
                    keyExtractor={item => item.id}
                    //onEndReached={this.itemsToRender2}
                    refreshing={this.state.refresh}
                    onRefresh={this.handleRefesh}
                />
               
            </ScrollView>

        )
    }

}



const styles = StyleSheet.create({
    row3: {
        marginTop: 25,
        backgroundColor: '#fdf7e8',
        shadowColor: "orange",
        shadowOffset: {
            width: 3,
            height: 3,
        }, marginVertical: 10,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7, elevation: 3,
        marginVertical: 6,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: 80,
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    },
    imgContainer: {
        backgroundColor: '#dc7e27',
        alignItems: 'center',
        justifyContent: 'center', margin: 6,
        height: '40%', borderRadius: 15,
        shadowOffset: {
            width: 0, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8.30,
        shadowColor: '#05375a'
    },
})