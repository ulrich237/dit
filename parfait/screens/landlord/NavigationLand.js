import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LandHome from './landHome';
import MyRenters from './rent/myRenters'
import ElecEau from './ElecEau';
import DetailRenter from './DetailRenter'
import Allreservation from './Allreservation';
import Etape from './post/Etape';
import Etape1 from './post/Etape1';
import Etape2 from './post/Etape2';
import Etape3 from './post/Etape3';
import MyDetails from './details';
import Home from '../normalUser/home';
import MyHouses from './gcel/myHouses'
import Notrenter from './Notrenter';
import UpdateEtape from '../updatePost/UpdateEtape';
import UpdateEtape1 from '../updatePost/UpdateEtape1';
import UpdateEtape2 from '../updatePost/UpdateEtape2';
import UpdateEtape3 from '../updatePost/UpdateEtape3';
import UpdateEtape4 from '../updatePost/UpdateEtape4';

import NavigationGcel from './gcel/navigationGcel'
import NavigationRent from './rent/navigationRent'
import MessagesIni from '../normalUser/Messages_in'
import AddLocataire from './AddLocaraire';

const Stack = createStackNavigator();

export default class NavigationLand extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
        <Stack.Navigator>
            <Stack.Screen name='  ' component={LandHome} options={{headerShown: false}}/>
            <Stack.Screen name='ElecEau' component={ElecEau} options={{headerShown: false}}/>
            <Stack.Screen name='Notrenter' component={Notrenter} options={{headerShown: false}}/>
            <Stack.Screen name='Home' component={Home} options={{headerShown: false}}/>
            <Stack.Screen name='Allreservation' component={Allreservation} options={{headerShown: false}}/>
            <Stack.Screen name='Etape' component={Etape} options={{headerShown: false}}/>
            <Stack.Screen name='Etape1' component={Etape1} options={{headerShown: false}}/>
            <Stack.Screen name='Etape2' component={Etape2} options={{headerShown: false}}/>
            <Stack.Screen name='Etape3' component={Etape3} options={{headerShown: false}}/>
            <Stack.Screen name='MyDetails' component={MyDetails} options={{title: 'Detail bien',}}/>
            <Stack.Screen name='MyHouses' component={MyHouses}/>
            <Stack.Screen name='MyRenters' component={MyRenters}/>
            <Stack.Screen name='UpdateEtape' component={UpdateEtape} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape1' component={UpdateEtape1} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape2' component={UpdateEtape2} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape3' component={UpdateEtape3} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape4' component={UpdateEtape4} options={{headerShown: false}}/>  
            <Stack.Screen name='NavigationGcel' component={NavigationGcel} options={{headerShown: false}}/>
            <Stack.Screen name='NavigationRent' component={NavigationRent} options={{headerShown: false}}/>
            <Stack.Screen name='DetailRenter' component={DetailRenter} options={{title: 'Detail locataire',}}/>
   
            <Stack.Screen name='MessagesIni' component={MessagesIni} options={{headerShown: false}}/>
            <Stack.Screen name='AddLocataire' component={AddLocataire} options={{title: 'Ajouter un locataire',}}/>
        
        
        </Stack.Navigator>
        )
    }
    
}