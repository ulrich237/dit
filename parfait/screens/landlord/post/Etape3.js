import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
    StatusBar,
    DrawerLayoutAndroid,
    Alert,
   
    Switch, ActivityIndicator
} from 'react-native';
import Toast from 'react-native-simple-toast';
import FastImage from 'react-native-fast-image';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Avatar } from 'react-native-paper';
//import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-crop-picker';
import StepIndicator from 'react-native-step-indicator'
import AsyncStorage from '@react-native-async-storage/async-storage';

import Notification from '../../../functions/notification';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

var dat = {}
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];

export default class Etape3 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            photo: [], currentPosition: 3, price: 0, animating: false,
            priceEau: 0, priceElect: 0, disc: '', user_id: null, save: false, mes_likes: [],
            lois: {}, loi: false, publier: false, tokens: [],
            users: []
        };

    }



    componentDidMount() {

        this.displayData()

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/")
            .then(resu => {
                this.setState({ users: resu.data })
                axios.get('https://imobbis.pythonanywhere.com/new/follow')
                    .then(res => {
                        var data = []
                        _.filter(res.data, { follower: this.state.user_id }).map(foll => {
                            data.push(_.find(resu.data, { id: foll?.user })?.tokens)
                        })
                        this.setState({ tokens: data })
                    });
            }).catch(err => err.Error == 'Network Error' ?
                (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null);

    }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }

    save = () => {
        const { description, valueModal,
            selectedValue, isMeuble, colloc,
            isModerne, chambre, salon,
            cuisine, douche, terasse, gcel,
            metreCarre, isBarriere, isJardin, name,
            country, city, neighbor, isParking, quartier } = this.props.route.params;

        const { photo, price, priceEau, priceElect, disc } = this.state;
        var photos = _.toArray(photo)
        var form = new FormData()

        this.setState({ save: true, animating: true })


        axios.post("https://imobbis.pythonanywhere.com/new/hous/",
            {
                "nb_room": chambre,
                "name": name,
                "nb_parlour": salon,
                "nb_toilet": douche,
                "nb_kitchen": cuisine,
                "parking": isParking,
                "fence": isBarriere,
                "garden": isJardin,
                "description": description,
                "publish_type": selectedValue,
                "is_negotiable": false,
                "is_furnished": isMeuble,
                "is_finished": false,
                "renting_price": parseFloat(price),
                "dimension": metreCarre,
                "is_immeuble": isMeuble,
                "is_moderne": isModerne,
                "user": this.state.user_id,
                "country": country.id,
                "city": city.id,
                "neighbor": quartier,
                "category": valueModal.name,
                "water_m3_price": priceEau,
                "electricity_Kw_price": priceElect,
                "is_negotiable": disc,
                "is_finished": true,
                "gcel": gcel,
                "colocation": colloc,
            }
        ).then(response => {

            Toast.showWithGravity(
                "Veuillez patienter, vous y êtes presque!",
                Toast.LONG,
                Toast.CENTER,
              
            );
            //alert(response.data.id)
            this.setState({ lois: response.data })

            for (let i = 0; i < photo.length; i++) {
                form = new FormData()
                form.append('house', response.data.id);
                form.append('hauteur', photo[i].height);
                form.append('image', {
                    name: photo[i].filename,
                    type: photo[i].mime,
                    uri: Platform.OS === 'android' ? photo[i].path : photo[i].path,
                });

                fetch(`https://imobbis.pythonanywhere.com/new/image`, {
                    method: 'post',
                    headers: {
                        'Content-type': 'multipart/form-data',
                    },
                    body: form
                })
                    .then(res => {
                        Toast.showWithGravity(
                            "Image " + (i + 1) + " postée avec succès!",
                            Toast.LONG,
                            Toast.CENTER,
                           
                        );
                        (i + 1) == photos.length ? (
                            this.setState({ animating: false, loi: true, publier: true })
                        ) : null
                    }).catch(err => {
                        Alert.alert('Attention',
                            'Publication éffectuée mais, l\'image ' + (i + 1) + 'n \' a pas pu être sauvegardé. Vous pouvez effectuer une modification et ajouter les images.')
                        this.setState({ save: false })
                    })
            };

            new Notification().sendNotitficationsAll(JSON.stringify(this.state.tokens), _.find(this.state.users, { id: this.state.user_id })?.username, 'Nouvelle publication: ' + name, dat)
        })
            .catch(error => { this.setState({ save: false, animating: false }), Alert.alert('Attention', error.toString()) }
            )


    }


    closeActivityIndicator = () => setTimeout(() => this.setState({
        animating: false
    }))


    handlerChoosePhoto = () => {
        ImagePicker.openPicker({
            multiple: true,
            mediaType: 'photo',

        }).then(images => {
            this.setState({ photo: images })
        }).catch(error => {
            //alert(error)
            if (error.code === 'E_PICKER_CANCELLED') { // here the solution
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée. Veuillez réessayer!!!')
                return false;
            } else {
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée.')
            }
        });

    }

    handlerChooseOnePhoto = () => {
        ImagePicker.openPicker({
            mediaType: 'photo',
        }).then(images => {
            var img = this.state.photo
            img.push(images)
            this.setState({ photo: img })
        }).catch(error => {
            if (error.code === 'E_PICKER_CANCELLED') {
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée. Veuillez réessayer!!!')
                return false;
            } else {
                Alert.alert('Attention',
                    'Désolé, un problème est survenu lors de la tentative d\'obtention de l\'image sélectionnée.')
            }
        });

    }


    render() {
        const { photo, disc, priceElect, priceEau, animating, loi, publier, lois } = this.state;

        const { description, valueModal,
            selectedValue, isMeuble, colloc,
            isModerne, chambre, salon, gcel,
            cuisine, douche, terasse, country, city, neighbor,
            metreCarre, isBarriere, isJardin, name, } = this.props.route.params;


        return (
            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 25 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />
                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>Enregistrer les photos et sauvegarder</Text>

                <View style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('90%')
                }}>

                    <View style={{ marginTop: 10, }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>prix
                            {selectedValue == 'vendre' ? ' :' : valueModal?.id == 8 ? ' du m² et non le prix total' :
                                isMeuble ? ' par jour:' : ' par mois:'}</Text>
                        <TextInput
                            style={{ color: '#7c3325', }}
                            onChangeText={(val) => this.setState({ price: val })}
                            value={this.state.price}
                            placeholder="Taper le prix ici, sans espace ni virgule..."
                            placeholderTextColor="red"
                            keyboardType="numeric"
                        />
                    </View>




                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-evenly' }}>
                        <Text style={{ fontWeight: 'bold' }}>Discutable </Text>
                        <View style={{ marginLeft: '60%' }}>
                            <Switch
                                trackColor={{ false: "#767577", true: "#7c3325" }}
                                thumbColor={disc ? "#7c3325" : "#f4f3f4"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={(itemValue) => this.setState({ disc: itemValue })}
                                value={disc}
                            />
                        </View>
                    </View>



                    <Button style={styles.button}
                        title="Sélectionner des photos"
                        onPress={this.handlerChoosePhoto} />

                    {
                        _.size(photo) != 0 ?
                            <TouchableOpacity style={styles.button}
                                onPress={() => !this.state.save ? (this.save(), this.setState({ save: true })) : (this.state.publier == true ?
                                    (this.props.navigation.navigate('Home')) : null)}>
                                <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>{!this.state.save ? 'Publier' : (this.state.publier == true ? 'Quitter pour les publications' : 'Patienter')}</Text>
                            </TouchableOpacity> : null
                    }


                    {
                        loi ?
                            lois?.category.toLowerCase() !== "terrain" && lois?.category !== "" && lois?.category.toLowerCase() !== "boutique"
                                && lois?.category.toLowerCase() !== "local administratif" && lois?.category.toLowerCase() !== "bureau"
                                && lois?.category.toLowerCase() !== "autre" ?
                                <TouchableOpacity style={[styles.button, { backgroundColor: '#d3350e', height: 45, }]}
                                    onPress={() => { this.props.navigation.navigate('Etape2', { 'lois': this.state.lois }) }}>
                                    <Text style={{ fontSize: 14, textAlign: 'center', color: 'white', fontWeight: '600' }}>Ajouter des élèments et des restrictions</Text>
                                </TouchableOpacity> : null
                            : null
                    }


                    <ActivityIndicator style={styles.preloader} animating={animating} size="large" color="#7c3325" />

                    <ScrollView horizontal={true}>
                        {
                            photo.map(im => {
                                return (
                                    <FastImage
                                        style={{
                                            width: width_ / 2, height: height_ / 4, margin: 10,
                                            borderRadius: 2
                                        }}
                                        source={{
                                            uri: im.path,
                                        }}
                                    />
                                )
                            })
                        }
                    </ScrollView>
                </View>


            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: "azure",
    },
    input: {
        height: 30,
        borderWidth: 1,
        width: '90%',
        color: '#7c3325',
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: '2%', borderRadius: 7,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.50,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },

    preloader: {
        left: 0,
        right: 0,
        top: height_ / 2,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    }


});

