import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import StepIndicator from 'react-native-step-indicator'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
    StatusBar,
    DrawerLayoutAndroid,
    
    Switch
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Avatar } from 'react-native-paper';
//import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { stubTrue } from 'lodash';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Divider, Badge } from 'react-native-elements'
import Toast from 'react-native-simple-toast';
let index = 0, doc;


var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);




var reason = [
    { label: "A vendre", value: 0 },
    { label: "A louer", value: 1 },

]

var type = [
    { label: "Meublé", value: 0 },
    { label: "Moderne", value: 1 },

]



const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];


export default class Etape1 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            country: {}, city: {}, province: {}, district: {},
            provinces: [], cities: [], districts: [], neighbors: [], countries: [],
            modalDistrict: false, modalProvince: false, modalCity: false, modalNeighbor: false, modalCountry: true,
            is_country: false, is_province: false, is_city: false, is_neighbor: false, is_district: false,
            currentPosition: 2, save: false, user_id: null, quartier: null
        };
    }



    componentDidMount() {
        this.displayData()
        axios.get('https://imobbis.pythonanywhere.com/location/')
            .then(res => this.setState({
                countries: res.data.country,
                cities: res.data.city,
            }))

    }

    setModalVisible = (visible, country) => {
        this.setState({
            modalCountry: visible,
            country: country, is_country: true, modalCity: true
        });
    }

    setModalVisibleCity = (visible, city,) => {
        this.setState({
            modalCity: visible,
            city: city, is_city: true,
        });
    }


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }


    setModalVisibleDistrict = (visible, district,) => {
        this.setState({
            modalDistrict: visible,
            district: district, is_district: true

        });
    }



    setModalVisibleProvince = (visible, province) => {
        this.setState({
            modalProvince: visible,
            province: province, is_province: true
        });

    }

    save = () => {
        const { description, valueModal,
            price, selectedValue, isMeuble,
            isModerne, genre, chambre, salon, gcel, colloc,
            cuisine, douche, terasse, rent_per_day, isParking,
            metreCarre, isBarriere, isJardin, name } = this.props.route.params;

        const {
            country, city, neighbor,
        } = this.state;


        axios.post("https://imobbis.pythonanywhere.com/house/",
            {

                "nb_room": chambre,
                "name": name,
                "nb_parlour": salon,
                "nb_toilet": douche,
                "nb_kitchen": cuisine,
                "parking": true,
                "fence": isBarriere,
                "garden": isJardin,
                "description": description,
                "publish_type": selectedValue,
                "renting_price": price,
                "is_negotiable": false,
                "is_furnished": isMeuble,
                "is_finished": false,
                "dimension": metreCarre,
                "is_immeuble": isMeuble,
                "is_moderne": isModerne,

                "user": this.state.user_id,
                "country": country.id,
                "city": city.id,
                "neighbor": 0,
                "category": valueModal.name
            }
        ).then(response => {

            console.log(response.data)
            Toast.showWithGravity(
                "Enregistré avec succès! vuillez continuer les étapes",
                Toast.LONG,
                Toast.CENTER,
              
            );

            this.setState({ save: true })



        })
            .catch(error => alert(error))
    }

    render() {
        const { countries, cities, neighbors,
            modalCity, modalCountry, modalNeighbor,
            is_country, is_city, is_neighbor, is_district,
            country, city, quartier

        } = this.state;

        const { description, valueModal,
            selectedValue, isMeuble,
            isModerne, chambre, salon,
            cuisine, douche, terasse, isParking, colloc,
            metreCarre, isBarriere, isJardin, name, gcel,
        } = this.props.route.params;



        return (

            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 45 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalCountry}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}
                >
                    <View style={[styles.modalView]}>
                        <View style={{ flexDirection: 'row-reverse' }}>
                            <TouchableOpacity
                                style={{
                                    ...styles.openButton,
                                    shadowColor: "black",
                                    shadowOffset: {
                                        width: 0, height: 6,
                                    },
                                    shadowOpacity: 0.1, marginTop: 20
                                }}
                                onPress={() => {
                                    this.setState({ modalCountry: false });
                                }}
                            >
                                <FontAwesome style={{}}
                                    name='close'
                                    size={25}
                                    color='red'
                                />
                            </TouchableOpacity>

                        </View>
                        <Text style={{ fontSize: 12, textAlign: 'center', alignSelf: 'center', margin: 5 }}>Sélectionner un pays</Text>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                        <ScrollView style={{ height: 0.25 }}>
                            {
                                countries.map(country => {
                                    return (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setModalVisible(!modalCountry, country);
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>{country.name}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    </View>
                </Modal>


                {
                    is_country ? (
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={modalCity}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                            }}>

                            <View style={[styles.modalView]}>
                                <View style={{ flexDirection: 'row-reverse' }}>
                                    <TouchableOpacity
                                        style={{
                                            ...styles.openButton,
                                            shadowColor: "black",
                                            shadowOffset: {
                                                width: 0, height: 6,
                                            },
                                            shadowOpacity: 0.1, marginTop: 20
                                        }}
                                        onPress={() => {
                                            this.setState({ modalCity: false });
                                        }}
                                    >
                                        <FontAwesome style={{}}
                                            name='close'
                                            size={25}
                                            color='red'
                                        />
                                    </TouchableOpacity>
                                </View>

                                <Text style={{ fontSize: 12, alignSelf: 'center', margin: 5 }}>Sélectionner une ville</Text>
                                <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                                <ScrollView style={{ height: 0.25 }}>
                                    {
                                        cities.map(city => {
                                            return (
                                                country?.cities?.map(idDistrict => {
                                                    return (
                                                        idDistrict == city.id ? (
                                                            <TouchableOpacity
                                                                onPress={() => {
                                                                    this.setModalVisibleCity(!modalCity, city);
                                                                }}
                                                            >
                                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>{city.name}</Text>
                                                            </TouchableOpacity>
                                                        ) : null
                                                    )
                                                })
                                            )
                                        })
                                    }
                                </ScrollView>
                            </View>
                        </Modal>)
                        : null
                }






                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>Localisation</Text>
                <ScrollView style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('80%')
                }}>
                    <ScrollView>
                        <View>
                            <Text style={{
                                fontSize: 18, marginTop: 10,
                                fontWeight: '600', color: '#7c3325'
                            }}>Pays
                            </Text>
                            <TouchableOpacity style={{ flexDirection: 'row', marginTop: 5, justifyContent: 'space-between' }}
                                onPress={() => {
                                    this.setState({ modalCountry: true });
                                }}>
                                <View style={{ width: 240 }}>
                                    <Text style={{ color: 'grey', fontSize: 18 }}>
                                        Cliquer ici...
                                    </Text>
                                </View>
                                <MaterialIcons name='navigate-next' style={{ alignSelf: 'flex-end' }} size={28} color='grey' />
                            </TouchableOpacity>
                            {is_country ?
                                <Text style={{ fontWeight: '500' }}>Pays sélectionné: {country?.name}</Text>
                                : null
                            }
                        </View>

                        {is_country ?
                            <View>
                                <Text style={{
                                    fontSize: 18, marginTop: 10,
                                    fontWeight: '600', color: '#7c3325'
                                }}>Ville
                                </Text>
                                <TouchableOpacity style={{ flexDirection: 'row', marginTop: 5, justifyContent: 'space-between' }}
                                    onPress={() => {
                                        this.setState({ modalCity: true });
                                    }}>
                                    <View style={{ width: 240 }}>
                                        <Text style={{ color: 'grey', fontSize: 18 }}>
                                            Cliquer ici...
                                        </Text>
                                    </View>
                                    <MaterialIcons name='navigate-next' style={{ alignSelf: 'flex-end' }} size={28} color='grey' />
                                </TouchableOpacity>
                                {is_city ?
                                    <Text style={{ fontWeight: '500' }}>Ville sélectionné: {city?.name}</Text>
                                    : null
                                }
                            </View>
                            : null
                        }

                        {is_city ?
                            <View>
                                <Text style={{
                                    fontSize: 18, marginTop: 10,
                                    fontWeight: '600', color: '#7c3325'
                                }}>Quartier
                                </Text>
                                <TouchableOpacity style={{ flexDirection: 'row', marginTop: 5, justifyContent: 'space-between' }}
                                    onPress={() => {
                                        this.setState({ modalNeighbor: true });
                                    }}>
                                    <View style={{ width: 240 }}>
                                        <Text style={{ color: 'grey', fontSize: 12 }}>
                                            Ecrire le quartier en dessous...
                                        </Text>
                                    </View>
                                    <MaterialIcons name='edit' style={{ alignSelf: 'flex-end' }} size={28} color='grey' />
                                </TouchableOpacity>
                                {is_city ?
                                    <View style={{}}>
                                        <TextInput
                                            style={{}}
                                            onChangeText={(val) => this.setState({ quartier: val })}
                                            value={this.state.quartier}
                                            placeholderTextColor="red"
                                            placeholder="Taper le quartier ici"
                                        />
                                    </View>
                                    : null
                                }
                                {
                                    // !this.state.save ?
                                    //  <TouchableOpacity style={styles.button}
                                    //     onPress={() => { alert("changement de cadence") }}>
                                    //     <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Enregistrer</Text>
                                    //</View> </TouchableOpacity>
                                    // : null
                                }
                            </View>
                            : null
                        }


                        {
                            this.state.quartier != null && this.state.quartier != " " ?
                                <TouchableOpacity style={styles.button}
                                    onPress={() => this.props.navigation.navigate('Etape3', {
                                        'description': description, 'name': name, 'isParking': isParking, 'gcel': gcel,
                                        'valueModal': valueModal, 'country': this.state.country,
                                        'isMeuble': isMeuble, 'selectedValue': selectedValue, 'colloc': colloc,
                                        'isModerne': isModerne, 'chambre': chambre, 'salon': salon, 'quartier': quartier,
                                        'cuisine': cuisine, 'douche': douche, 'terasse': terasse, 'city': this.state.city,
                                        'metreCarre': metreCarre, 'isBarriere': isBarriere, 'isJardin': isJardin
                                    })}>
                                    <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Continue</Text>
                                </TouchableOpacity>
                                : null
                        }
                    </ScrollView>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "azure",

    },
    input: {
        height: 50,
        borderWidth: 2,
        width: '80%',
        color: '#05375a',
    },

    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 10, borderRadius: 7,// marginTop: '5%'
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        borderColor: 'orange',
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderTopWidth: 2,
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.30,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5, marginLeft: width_ * 0.25, marginTop: height_ * 0.3,
    },


});

