import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';
import StepIndicator from 'react-native-step-indicator'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Modal,
    Text,
    Switch
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Avatar } from 'react-native-paper';
import { Divider, Badge } from 'react-native-elements'
//import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
let index = 0, doc;


var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);




var reason = [
    { label: "A vendre", value: 0 },
    { label: "A louer", value: 1 },

]

var type = [
    { label: "Meublé", value: 0 },
    { label: "Moderne", value: 1 },

]


const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];


export default class Etape2 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            cat: [],
            isModerne: false, lois: {},
            wifi: false, eau: false, clim: false, sport: false,
            conEnfant: false, amis: false, fume: false, evenement: false,
            all_cham: false, cham: false, cong: false, cuisi: false,
            elever: false, currentPosition: 4, user_id: null, asc: false,
            sexe: 'Feminin', age: '', religion: '', modal: false, what: 0,
        };
    }


    onPageChange(position) {
        this.setState({ currentPosition: position });
    }

    componentDidMount() {


        this.displayData()
        axios.get('https://imobbis.pythonanywhere.com/house/cat')
            .then(res => this.setState({ cat: res.data }))
    }


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }


    save = () => {

        const { lois } = this.props.route.params;
        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + lois?.id,
            {
                "wifi": this.state.wifi,
                "hot_water": this.state.eau,
                "climatisation": this.state.clim,
                "freezer": this.state.cong,
                "cooker": this.state.cuisi,
                "tv_all_bedroom": this.state.all_cham,
                "smooking_allowed": this.state.fume,
                "events_allowed": this.state.evenement,
                "pets_allowed": this.state.elever,
                "elevator": this.state.asc,
                "gym": this.state.sport,
                "age_colocataire": this.state.age,
                "sexe_colocataire": this.state.sexe,
                "religion_colocataire": this.state.religion
            }
        ).then(response => {
            lois?.gcel == true ? this.props.navigation.navigate('MyHouses') : this.props.navigation.navigate('Home')
        })
            .catch(error => alert(error))
    }

    setReligion = (visible, city,) => {
        this.setState({
            modal: visible,
            religion: city,

        });
    }

    setAge = (visible, city,) => {
        this.setState({
            modal: visible,
            age: city,

        });
    }

    setSexe = (visible, city,) => {
        this.setState({
            modal: visible,
            sexe: city,

        });
    }


    render() {
        const { isModerne, wifi, eau, clim, sport,
            conEnfant, amis, fume, evenement, sexe, age, religion,
            all_cham, cham, cong, cuisi, elever, what, currentPosition, asc, modal } = this.state;
        
            const { lois } = this.props.route.params;



        return (
            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 25 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modal}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}>

                    <View style={[styles.modalView]}>
                        <TouchableOpacity
                            style={{
                                ...styles.openButton,
                                shadowColor: "black",
                                shadowOffset: {
                                    width: 0, height: 6,
                                },
                                shadowOpacity: 0.1, marginTop: 20
                            }}
                            onPress={() => {
                                this.setState({ modal: false });
                            }}
                        >
                            <FontAwesome style={{}}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                        <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Faites un choix:</Text>
                        <ScrollView style={{ height: 0.25 }}>
                            {
                                what == 0 ?
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'christianisme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>christianisme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'judaïsme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>judaïsme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'bouddhisme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>bouddhisme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'islam');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>islam</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, '');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Toutes</Text>
                                        </TouchableOpacity>
                                    </View>
                                    : what == 1 ?
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '18-25');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>18-25</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '26-40');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>26-40</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '41-plus');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>41-plus</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Tous</Text>
                                            </TouchableOpacity>
                                        </View>
                                        : what == 2 ?
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setSexe(!modal, 'Masculin');
                                                    }}
                                                >
                                                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Masculin</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setSexe(!modal, 'Feminin');
                                                    }}
                                                >
                                                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Feminin</Text>
                                                </TouchableOpacity>
                                            </View>
                                            : null
                            }
                        </ScrollView>
                    </View>
                </Modal>
                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>Les informations partielles </Text>
                <ScrollView style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('90%')
                }}>
                    <ScrollView>
                        {lois?.is_immeuble ?
                            <View>
                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Animaux domestiques autorisés </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={elever ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ elever: itemValue })}
                                            value={elever}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Peut on fumer à l'intérieur </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={fume ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ fume: itemValue })}
                                            value={fume}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Fêtes autorisées </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={evenement ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ evenement: itemValue })}
                                            value={evenement}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Télévision </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={all_cham ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ all_cham: itemValue })}
                                            value={all_cham}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Congélateur </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={cong ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ cong: itemValue })}
                                            value={cong}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Cuisinière </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={cuisi ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ cuisi: itemValue })}
                                            value={cuisi}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Ascenceur</Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={asc ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ asc: itemValue })}
                                            value={asc}
                                        />
                                    </View>
                                </View>
                                <Text style={{ fontWeight: '200' }}></Text>
                            </View> : null
                        }

                        <View>
                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Wifi </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={wifi ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ wifi: itemValue })}
                                        value={wifi}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Eau chauffante</Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={eau ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ eau: itemValue })}
                                        value={eau}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Climatisation </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={clim ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ clim: itemValue })}
                                        value={clim}
                                    />
                                </View>
                            </View>


                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Salle de sport</Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={sport ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ sport: itemValue })}
                                        value={sport}
                                    />
                                </View>
                            </View>
                        </View>

                        {
                            lois?.colocation ?
                                <View>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 2, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Vous voulez un collocataire de quel sexe</Text>
                                        <View style={{}}
                                        >
                                            <Text>{sexe == '' ? 'Tous' : sexe}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 1, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Tranche d'âge</Text>
                                        <View style={{}}
                                        >
                                            <Text>{age == '' ? 'Tous' : age}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 0, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Vous voulez un collocataire de quelle réligion:</Text>
                                        <View style={{}}
                                        >
                                            <Text>{religion == '' ? 'Toutes' : religion}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                        <TouchableOpacity style={styles.button}
                            onPress={() => this.save()}>
                            <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Ajouter</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: "azure",
    },
    input: {
        height: 30,
        borderWidth: 1,
        width: '90%',
        color: '#7c3325',
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 10, borderRadius: 7,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.30,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5, marginLeft: width_ * 0.25, marginTop: height_ * 0.3,
    },


});

