import React from 'react';
import {
    View, Text, Image, ScrollView, StyleSheet, ImageBackground,
    TouchableOpacity, Button, TextInput, Modal, Dimensions
} from 'react-native';
import axios from 'axios';
import { Divider } from 'react-native-elements';
import _ from 'underscore';
import { Avatar } from 'react-native-paper'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from "react-native-vector-icons/FontAwesome";
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Allreservation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            querry: '', agent: [], users: [], company: {}, houses: [],
            likes: [],
            name: '', description: '', photo: null,
            user: null, cities: [], booking: [],

        }
    }



    componentDidMount() {
        const { type } = this.props.route.params;

         this.displayData()
        axios.get("https://imobbis.pythonanywhere.com/new/hous").then(res => {

            this.setState({ houses: _.filter(res.data, { user: this.state.user }) });
        });
        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
            this.setState({ users: res.data });
        });

        axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
            this.setState({ cities: res.data.city, neighbours: res.data.neighbor });
        })

        axios.get("https://imobbis.pythonanywhere.com/new/house_bookin").then(res => {

            var data = _.filter(res.data,
                type == 'attente' ? { pending: true }
                    : type == 'confirm' ? { confirm: true }
                        : type == 'error' ? { error: true }
                            : null)
            type == 'tous' ? data == res.data : null 

            this.setState({ booking: data })
        });
    }


     displayData = async () => {
        try{
            let user = await  AsyncStorage.getItem('user_data');  
            let json_id = JSON.parse(user)
            
            this.setState({user: json_id})
        } catch{error => alert(error)};
    }

    render() {
        const { houses, likes, cities, booking, users } = this.state;
        const { type } = this.props.route.params;

        return (
            <ScrollView>
                <Text style={{ fontWeight: 'bold',fontSize: 19 }}>   {type == 'attente' ? 'Reservations en attente'
                                                    : type == 'confirm' ? 'Reservations confirmées'
                                                        : type == 'tous' ? 'Toutes les reservations'
                                                            : type == 'error' ? 'Reservations en erreures' : null}
                                                </Text>

                {
                    booking.map((pers) => {
                        return (
                            <View>

                                {
                                    _.filter(houses, { id: pers.house_book, user: this.state.user }).map(houses_ => {
                                        return (    
                                            <View style={{ marginHorizontal: 5, ...styles.row3 }}>

                                             <View style={{borderWidth: 2, borderColor: 'grey', padding: 4, borderRadius: 10, flexDirection: 'row', marginTop: 3}}>
                                                <Text style={{marginRight: 5, color: 'grey', fontWeight: 'bold'}}>Reservé par:</Text>
                                                <Avatar.Image size={35}  
                                                 source={{
                                                     uri: _.find(users, { id: pers.user })?.profile_image
                                                 }}/>
                                                <View style={{marginLeft: 5}}>
                                                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                                                    { _.find(users, { id: pers.user })?.username}</Text>  
                                                </View>
                                            </View>
                                                 
                                                <ImageBackground source={{ uri: houses_.photo_1 }} style={{ width: '100%', height: 200, marginTop:10 }}>
                                                </ImageBackground>    
                                                <View style={{ marginLeft: '2%', paddingVertical: 7 }}>
                                                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                                                        <View style={{ flexDirection: "row" }}>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Icon name='map-marker' color="grey" size={19} style={{marginTop:5}}/>
                                                                <Text style={{ fontSize: 19, marginLeft: 5, color: 'grey', }}>{_.find(cities, { id: houses_.city })?.name} </Text>
                                                            </View>
                                                            <Text style={{ fontSize: 18 }}> - {houses_.name}</Text>
                                                        </View>
                                                    </View>
                                                  
                                                    <Text style={{ fontSize: 17, marginVertical: 4 }}>
                                                        <Text style={{ fontWeight: 'bold', color: 'red' }}>{houses_.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}XAF</Text>
                                                        <Text > / {houses_.rent_per_day ? 'jour' : 'mois'}</Text>
                                                    </Text>
                                                </View>
                                            </View>
                                          
                                        )
                                    })
                                }

                            </View>
                            
                        )
                    })
                }

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        }, margin: 6,
        shadowOpacity: 0.2,
        shadowRadius: 4,
        elevation: 9,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: '46%',
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 5, marginTop: 4, marginLeft: 3
    }
})
