import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import { Avatar, Badge } from 'react-native-paper';
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Fontisto from 'react-native-vector-icons/Fontisto'
import axios from 'axios';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import _ from 'underscore';
import AsyncStorage from '@react-native-async-storage/async-storage';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Date_heure from '../../functions/date_heure';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);
export default class LandHome extends Component {
    _isMounted = false;
    constructor(props) {
        super(props)
        this.state = {
            follow: [],
            users: [],
            land: {},
            user: null,
            Touchedland: [],
            reviews: [],
            house: [],
            house_booking: [],
            category: [], touched: [], houses: [],
            renters: [],
            userland: [],
            likes: []

        }
    }
    componentDidMount() {
        this._isMounted = true;
        this.displayData()

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
            .then(response => this.setState({ renters: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {

            this.setState({ userland: _.find(res.data, { id: this.state.user }) });
        });
        axios.get("https://imobbis.pythonanywhere.com/wen/touch_house").then(res => {

            this.setState({ Touchedland: res.data, });
        });
        axios.get("https://imobbis.pythonanywhere.com/new/follow").then(res => {
            this.setState({follow: res.data});
        });
        axios.get("https://imobbis.pythonanywhere.com/new/hous").then(res => {
            this.setState({ house: _.filter(res.data, { user: this.state.user }) });
        })

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
            this.setState({ users: res.data });
        })
        axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
            this.setState({ cities: res.data.city, neighbours: res.data.neighbor });
        })
        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(res => {
            this.setState({ category: res.data });
        })
        axios.get("https://imobbis.pythonanywhere.com/new/like").then(res => {
            this.setState({ likes: res.data });
        })
        axios.get("https://imobbis.pythonanywhere.com/new/house_bookin").then(res => {
            this.setState({ house_booking: res.data });
        })

        axios.get('https://imobbis.pythonanywhere.com/touched/')
            .then((res) => {
                this.setState({
                    touched: res.data,
                    isLoading: false
                });
            })
            .catch(error => this.setState({ error, isLoading: false }));

    }


    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.renters !== this.state.renters) {
            axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
            .then(response => this.setState({ renters: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));
        }

    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    touch() {
        const { house } = this.state;
        var n = 0
        house.map((per) => {
            const { Touchedland, house } = this.state;
            Touchedland.map(pers =>
                pers.touched.includes(per?.id) ? n = n + 1 : null
            )
        })
        return n;
    }

    pending() {
        const { house_booking, house } = this.state;
        var s = 0;
        house.map((house_) => {
            s = _.size(_.filter(house_booking, { house_book: house_.id, pending: true })) + s
        })
        return s
    }

    confirmation() {
        const { house_booking, house } = this.state;
        var s = 0;
        house.map((house_) => {
            s = _.size(_.filter(house_booking, { house_book: house_.id, confirm: true })) + s
        })
        return s
    }

    error() {
        const { house_booking, house } = this.state;
        var s = 0;
        house.map((house_) => {
            s = _.size(_.filter(house_booking, { house_book: house_.id, error: true })) + s
        })
        return s
    }

    All() {
        const { house_booking, house } = this.state;
        var s = 0;
        house.map((house_) => {
            s = _.size(_.filter(house_booking, { house_book: house_.id })) + s
        })
        return s
    }



    displayData = async () => {

        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user: json_id })
        } catch { error => alert(error) };
    }

    sum() {
        const { house, likes } = this.state;
        var somme = 0;
        house.map((pers) => {
            somme = _.size(_.filter(likes, { house: pers.id })) + somme
        })
        return somme;
    }

    render() {
        const { follow, house, touched, renters, userland } = this.state
        var rent_ar = []
        renters.map(renter => { return (house.map(id => renter.current_house == id.id ? rent_ar.push(renter) : null)) })
        var visit = []


        return (

            <ScrollView style={{}}>
                <View style={{
                    height: height_ / 15, justifyContent: 'space-between', flexDirection:
                        'row', backgroundColor: '#dc7e27'
                }}>
                    <View style={{ paddingLeft: 7 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 3 }}>
                            <TouchableOpacity style={{ marginLeft: 20, marginTop: 14 }}
                                onPress={() => this.props.navigation.openDrawer()}
                            >
                                <FontAwesome name='list' color='black' size={20} />
                            </TouchableOpacity>
                            <Text style={{
                                marginLeft: 50, margin: 10,
                                color: 'black', marginTop: 10, fontSize: 20, fontWeight: 'bold'
                            }}>IMOBBIS</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 10, marginTop: 15 }}>
                    <Avatar.Image size={60} source={{ uri: userland?.profile_image }} />
                    <View style={{ marginLeft: 10, marginTop: 7, marginRight: 20 }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{userland?.username} </Text>
                        <Text style={{ color: 'grey', fontSize: 12 }}>Membre {new Date_heure().getDate_publication(userland?.date_joined)}</Text>
                    </View>
                </View>

                <Text style={{ alignSelf: 'center', marginTop: 4, fontSize: 15, color: '#05375a' }}> </Text>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 4 }}> {_.size(_.filter(follow, { follower: userland?.id }))}</Text>
                            <Text style={{ fontSize: 14, marginTop: 4, color: 'grey' }}>Abonné(s)</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 4 }}>{this.touch()}</Text>
                            <Text style={{ fontSize: 14, marginTop: 4, color: 'grey' }}>Vue(s)</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '32%', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: 'bold', fontSize: 18, marginTop: 4 }}>{this.sum()}</Text>
                            <Text style={{ fontSize: 14, marginTop: 4, color: 'grey' }}>Aime(s)</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.row4, { marginTop: 10 }]}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ borderBottomWidth: 3, width: '32%', paddingBottom: 3, borderBottomColor: '#cc8500', marginVertical: 10, marginLeft: 10 }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#cc8500' }}>Mes actions</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={{ width: '20%', alignItems: 'center', margin: 10 }}
                            onPress={() => this.props.navigation.navigate('Etape', { gcel: 'false' })}>
                            <MaterialCommunityIcons name='home-plus-outline' size={40} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>Faire une publication</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '20%', alignItems: 'center', margin: 10 }}
                            onPress={() => this.props.navigation.navigate('AddLocataire', {'houses': _.filter(house, {disponible:false})})}>
                            <MaterialCommunityIcons name='account-multiple-plus' size={40} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>Ajouter un locataire</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '20%', alignItems: 'center', margin: 10 }}
                            onPress={() => this.props.navigation.navigate('Etape', { gcel: 'true' })}>
                            <MaterialCommunityIcons name='home-plus' size={40} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>Ajouter un bien pour gérer </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '20%', alignItems: 'center', margin: 10 }}
                            onPress={() => this.props.navigation.navigate('')}>
                            <MaterialCommunityIcons name='newspaper-variant-outline' size={40} color='#664200' />
                            <Text style={{ fontWeight: 'bold', fontSize: 10, marginTop: 4 }}>Mes Documents</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={{ marginTop: 15, ...styles.row4 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{borderBottomWidth: 3, width: '40%', paddingBottom: 3, borderBottomColor: '#cc8500', marginVertical: 10, marginLeft: 10 }}>
                            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#cc8500'}}>A propos des reservations</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <TouchableOpacity style={{ width: '24%', alignItems: 'center', top: -3 }}
                            onPress={() => this.props.navigation.navigate('Allreservation', { type: 'attente' })}
                        >
                            <MaterialIcons name='pending' size={35} color='#05375a' />
                            <Badge
                                status="error"
                                style={{ position: 'absolute', right: 25, top: -5 }}
                            >{this.pending()}</Badge>
                            <Text style={{ fontSize: 12, color: '#737373', marginTop: 2 }}>En attente</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '24%', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('Allreservation', { type: 'confirm' })}
                        >
                            <AntDesign name='checkcircle' size={30} color='#05375a' />
                            <Badge
                                status="error"
                                style={{ position: 'absolute', right: 25, top: -5 }}
                            >{this.confirmation()}</Badge>
                            <Text style={{ fontSize: 12, color: '#737373', marginTop: 5 }}>Confirme</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '24%', alignItems: 'center', top: -3 }}
                            onPress={() => this.props.navigation.navigate('Allreservation', { type: 'error' })}
                        >
                            <MaterialIcons name='error' size={35} color='#05375a' />
                            <Badge
                                status="error"
                                style={{ position: 'absolute', right: 25, top: -5 }}
                            >{this.error()}</Badge>
                            <Text style={{ fontSize: 12, color: '#737373', marginTop: 2 }}>Erreure</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: '23%', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('Allreservation', { type: 'tous' })}
                        >
                            <Fontisto name='world' size={30} color='#05375a' />
                            <Badge
                                status="error"
                                style={{ position: 'absolute', right: 25, top: -5 }}
                            >{this.All()}</Badge>
                            <Text style={{ fontSize: 12, color: '#737373', marginTop: 5 }}>Tous</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={[styles.row3, { marginTop: 15 }]}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ borderBottomWidth: 3, width: '40%', paddingBottom: 3, borderBottomColor: '#cc8500', marginVertical: 10, marginLeft: 10 }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#cc8500' }}>Gestion locative</Text>
                        </View>
                    </View>
                    <View style={[{ marginTop: 5 }]}>
                        <TouchableOpacity style={styles.view} onPress={() => { this.props.navigation.navigate('NavigationGcel') }}>
                            <View style={{ width: '95%', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{flexDirection: 'row',}}>
                                    <FontAwesome name='sitemap' size={25} color='#05375a' />
                                    <Text style={{
                                        fontSize: 17,
                                        //marginTop: 2,
                                        marginLeft: 5
                                    }}> Mes biens </Text>
                                    {
                                        _.size(house) !== 0 ?

                                            <Badge
                                                status="error"
                                                style={{ position: 'absolute', top: -10 }}
                                            >{_.size(house)}</Badge>
                                            : null
                                    }
                                </View>

                                <FontAwesome name='dashboard' size={25} color='#05375a' />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={[{ marginTop: 15 }]}>
                        <TouchableOpacity style={styles.view} onPress={() => { this.props.navigation.navigate('NavigationRent') }}>
                            <View style={{ width: '95%', flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{flexDirection: 'row',}}>
                                    <FontAwesome name='street-view' size={25} color='#05375a' />
                                    <Text style={{
                                        fontSize: 17,
                                        //marginTop: 2,
                                        marginLeft: 5
                                    }}> Mes Locataires </Text>
                                    {
                                        _.size(rent_ar) !== 0 ?
                                            <Badge
                                                status="error"
                                                style={{ position: 'absolute', top: -10 }}
                                            >{_.size(rent_ar)}</Badge>
                                            : null
                                    }
                                </View>
                                <FontAwesome name='dashboard' size={25} color='#05375a' />
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>

            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    row3: {
        marginTop: 20, paddingVertical: 15,
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.25,
        shadowRadius: 8,
        padding: 6, elevation: 7,
        marginVertical: 6,
        borderRadius: 15,
    },
    row4: {
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 15,
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        margin: 5
    },
    view: {
        height: 37, borderRadius: 7,
        backgroundColor: '#fff',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 7, marginBottom: 10
    },
})
