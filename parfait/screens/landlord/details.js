import React, { Component } from 'react'
import { Text, View, StyleSheet, Easing, Modal, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import call from 'react-native-phone-call';
import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import { Checkbox, Avatar } from 'react-native-paper';
import _ from 'underscore';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import axios from 'axios'
import Toast from 'react-native-simple-toast';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);


const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};
export default class MyDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            etat: false,
            countrys: [], neighbours: [],
            provinces: [],
            citys: [],
        }
    }


    componentDidMount() {
        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    countrys: res.data.country, neighbours: res.data.neighbor,
                    citys: res.data.city
                })
            });
    }
    pagination(entries) {
        const { activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{ backgroundColor: 'white' }}
                dotStyle={{
                    width: 5,
                    height: 5,
                    borderRadius: 2,
                    marginHorizontal: 2,
                    backgroundColor: '#dc7e27'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.4}
            />
        );
    }



    indisponible = (house) => {
        var etat = house?.disponible

        Alert.alert(
            !etat ? 'Indisponiblité' : 'Disponiblité',
            'Voulez vous mettre cette publication (' + house?.name + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.patch('https://imobbis.pythonanywhere.com/new/hous/' + house?.id,
                            {
                                'disponible': !etat
                            }).then((res) => {

                                Toast.showWithGravity(
                                    "Opération effectuée!",
                                    Toast.LONG,
                                    Toast.CENTER,
                                    )
                            })
                            .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]

        )

    }



    createAlert = (house) => {
        Alert.alert(
            'Suppression',
            'Voulez vous vraiment supprimer cette publication (' + house?.name + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.delete('https://imobbis.pythonanywhere.com/new/hous/' + house?.id).then((res) => {
                            Toast.showWithGravity(
                                "Suppression effectuée!",
                                Toast.LONG,
                                Toast.CENTER,
                                ),
                                this.props.navigation.navigate('LandHome')
                        })
                            .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]
        )
    }


    getDaysFromDays = (dateout, dateint) => {
        let d = new Date(dateout); let f = new Date(Date.now());
        let k = new Date(dateint)
        let tmp = d - f;  // uttilise
        let tmps = d - k
        let res = tmp / tmps
        return 1 - res
    }


    firstCarousel = ({ item, index }) => {
        return (
            // _.filter(this.state.imageshouses, { house: pers.id }).map(image_ =>
            <TouchableOpacity
                activeOpacity={0.8}
                enabled={false}
                onPress={() => this.setState({ etat: true })}
            >
                <FastImage
                    source={{ uri: item.image }}
                    imgStyle={styles.images}
                    style={styles.images}
                    enableScaling={false}
                    easingFunc={Easing.ease}
                />
            </TouchableOpacity>
            //)
        );
    }



    render() {
        const { house, images, renters, user_ } = this.props.route.params
        const { etat, countrys, citys } = this.state

       
        return (
            <ScrollView>
                <View style={{ flex: 1 }}>
                    <Modal visible={etat} transparent={true}>
                        <View style={{ flex: 1, backgroundColor: 'white' }}>
                            <ImageViewer imageUrls={_.filter(images, {house:house?.id})}
                                renderHeader={() => (
                                    <Text style={{ borderWidth: 5, borderColor: 'white' }}>{image.length}</Text>
                                )}
                                enableImageZoom={true}
                                onDoubleClick={() => this.setState({ etat: false })}
                            />
                        </View>
                    </Modal>
                    <ScrollView>
                        <View>
                            <Text style={{marginBottom:10, fontSize: 12, fontWeight: "bold", textAlign: 'center' }}>
                                {house?.category?.toUpperCase()}
                                {!house?.colocation ? " A " + house?.publish_type?.toUpperCase() : ' EN COLLOCATION'}</Text>
                            <Carousel
                                ref={(c) => { this._carousel = c; }}
                                data={ _.filter(images, {house:house?.id})}   
                                renderItem={this.firstCarousel}
                                sliderWidth={width_}
                                itemWidth={width_}
                                autoplay={true}
                                onSnapToItem={(index) => this.setState({ activeSlide: index })}
                            />
                            {this.pagination( _.filter(images, {house:house?.id}))}
                        </View>

                        <View style={{ bottom: 15, marginLeft: 5, marginRight: '10%', marginTop: 10 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 12, marginBottom: 5 }}>Description</Text>
                            <Text style={{ color: 'grey' }} >"{house?.description}"</Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 12 }} numberOfLines={1}>{house.name}</Text>
                        </View>

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 12 }} >Locataire</Text>

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ marginTop: 10, }}>
                            {
                                renters ?
                                    <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity style={{ width: '50%', marginTop: 20, alignItems: 'center' }}
                                        onPress={() => this.props.navigation.navigate('DetailRenter', {'user_':user_, 'renter':renters, 'images':images, 'house':house })}>
                                             <View style={{ flexDirection: "row", justifyContent: 'space-evenly' }}>
                                                 <Text style={{ color: '#7c3325', letterSpacing: 2 }}>Voir plus de detail</Text>
                                             </View>
                                            <View>
                                                {
                                                    user_ ?
                                                        <Avatar.Image source={{ uri: user_?.profile_image }} size={50} />
                                                        : null
                                                }
                                            </View>
                                            <Text style={{ textAlign: 'center', fontWeight: 'bold', color: 'black', fontSize: 20 }}>
                                                {renters?.nom + ' ' + renters?.prenom}
                                            </Text>
                                        </TouchableOpacity>
                                       <View style={{ width: '50%', marginTop:25}}>
                                            <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', }} >Pourcentage d'utilisation</Text>
                                            <Text style={{ textAlign: 'center', fontSize: 12, color: 'red', marginTop: 5}}>
                                                {(this.getDaysFromDays(renters?.date_out, renters?.date_in).toFixed(2) * 100)}%
                                            </Text>
                                        </View>
                                    </View>
                                    : <TouchableOpacity
                                        style={{ borderBottomColor: '#e6e6e6', borderBottomWidth: 1, marginBottom: 7 }}
                                        onPress={() => {
                                            this.props.navigation.navigate('AddLocataire', {'house': house,})
                                        }}>
                                        <View style={{ padding: 7, flexDirection: 'row', }}>
                                            <View style={{ width: '75%' }}>
                                                <Text style={{ fontSize: 18, fontWeight: '500', color: '#dc7e27' }}>Ajouter un locataire</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 12, color: '#848484' }}>Tapez ici</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                            }
                        </View>
                        {
                            house?.colocation ?
                                <View>
                                    <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 12 }} >Préference</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='user' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />

                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> sexe: {house?.sexe_colocataire} </Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='minus' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />
                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> tranche d'âge: {house?.age_colocataire == '' ? 'Tous' : house?.age_colocataire}</Text>
                                    </View>
                                    <View style={{ flexDirection: "row" }}>
                                        <Icon name='institution' color='#7c3325' size={20} style={{ marginLeft: '4%' }} />
                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'black' }}> réligion: {house?.religion_colocataire == '' ? 'Toutes' : house?.religion_colocataire}</Text>
                                    </View>
                                </View>
                                : null
                        }

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 12 }} >Constitution</Text>

                        {
                            house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                && house?.category.toLowerCase() !== "autre" ?
                                <View style={{ bottom: 12, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10 }}>
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='bed' color='grey' size={20} />
                                            <Text style={{ fontSize: 13, color: 'grey' }}>{house?.nb_room} Chambres </Text>
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{house?.nb_toilet} douche(s)</Text>
                                        </View>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                            <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{house?.nb_parlour} salon(s) </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        {house?.nb_kitchen != 0 ?
                                            <View style={{ flexDirection: "row" }}>
                                                <MaterialIcons name='kitchen' color='grey' size={20} style={{ marginLeft: '2%' }} />
                                                <Text style={{ fontSize: 12, marginLeft: '1%', color: 'grey' }}>{house?.nb_kitchen} Cuisine(s)</Text>
                                            </View> : null
                                        }
                                        {house?.fence == true ? (
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='slideshare' color='grey' size={20} />
                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>Dans la barrière</Text>
                                            </View>) : null
                                        }
                                        {house?.garden == true ? (
                                            <View style={{ flexDirection: 'row' }}>
                                                <Icon name='pied-piper-alt' color='grey' size={20} />
                                                <Text style={{ fontSize: 12, color: 'grey', marginLeft: '2%' }}>jardin</Text>
                                            </View>) : null
                                        }
                                        {house?.parking == true ? (
                                            <View style={{ flexDirection: 'row' }}>

                                                <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>+ parking</Text>
                                            </View>) : null
                                        }
                                    </View>
                                </View> : house?.category.toLowerCase() == "terrain" ?
                                    <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='stop' color='grey' size={20} />
                                            <Text style={{ fontSize: 13, color: 'grey' }}>Dimension: {house?.dimension} métre carré </Text>
                                        </View>
                                    </View> : null

                        }


                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 12 }} >Fournitures</Text>
                        {
                            house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                && house?.category.toLowerCase() !== "autre" ?
                                <View style={{ bottom: 12, flexDirection: 'row', flexWrap: 'wrap', marginLeft: 10 }}>

                                    <View style={{ width: '47%', flexDirection: 'row', marginTop: 7 }}>
                                        <AntDesign name="wifi" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.wifi == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.wifi == true ? "orange" : "silver" }} >Wifi</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <Ionicon name="water" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.hot_water == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.hot_water == true ? "orange" : "silver" }} >Eau chauffante</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <FontAwesome5 name="fan" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.climatisation == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.climatisation == true ? "orange" : "silver" }} >Climatisation</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="fridge" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.freezer == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.freezer == true ? "orange" : "silver" }} >Congélateur</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="toaster-oven" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.cooker == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.cooker == true ? "orange" : "silver" }} >Cuisinière</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <FontAwesome name="tv" size={20} style={{ marginLeft: 8, marginRight: 15, color: house?.tv_all_bedroom == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.tv_all_bedroom == true ? "orange" : "silver" }} >Télèvision</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="baby-bottle" size={20} style={{ marginLeft: 8, marginRight: 5, color: house?.cooker == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.gym == true ? "orange" : "silver" }} >Salle de sport</Text>
                                    </View>
                                    <View style={{ width: '47%', flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                        <MaterialCommunityIcons name="elevator-passenger" size={20} style={{ marginLeft: 8, marginRight: 15, color: house?.tv_all_bedroom == true ? "black" : "silver" }} />
                                        <Text style={{ fontSize: 16, color: house?.elevator == true ? "orange" : "silver" }} >Ascenceur</Text>
                                    </View>
                                </View>
                                : null
                        }

                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <View style={{ marginLeft: 5, marginBottom: 10, marginTop: 7 }}>
                            <Text style={{ fontSize: 16, fontWeight: "500" }} >Situé à {house?.neighbor + ", " + _.find(citys, { id: house?.city })?.name + ", " + _.find(countrys, { id: house?.country })?.name}</Text>
                        </View>
                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6, marginTop: 7 }} />
                        {
                            house?.is_immeuble ?
                                house?.category.toLowerCase() !== "terrain" && house?.category !== "" && house?.category.toLowerCase() !== "boutique"
                                    && house?.category.toLowerCase() !== "local administratif" && house?.category.toLowerCase() !== "bureau"
                                    && house?.category.toLowerCase() !== "autre" ?
                                    <View style={{ bottom: 10, marginLeft: 10, marginTop: 7 }}>
                                        <Text style={{ marginBottom: 8, fontWeight: 'bold', fontSize: 12 }} >Régles à suivre</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialCommunityIcons name="party-popper" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.events_allowed == true ? "Fêtes autorisées" : "Pas de fêtes autorisées"}
                                            </Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialCommunityIcons name="smoking" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.smooking_allowed == true ? "Vous pouvez fumer à l'intérieur" : "Interdit de fumer à l'intérieur"}
                                            </Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                            <View style={{ width: '8%', marginLeft: 5 }}>
                                                <MaterialIcons name="pets" size={23} color='#05375a' />
                                            </View>
                                            <Text style={{ fontSize: 18, color: '#05375a' }} >
                                                {house?.pets_allowed == true ? "Animaux domestiques autorisés" : "Animaux domestiques interdits"}</Text>
                                        </View>
                                    </View>
                                    : null
                                : null
                        }
                    </ScrollView>

                    <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                    <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 12 }}>Actions sur la maison</Text>
                    <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity style={{ ...styles.bt1, backgroundColor: '#7c3325' }}
                            onPress={() => {renters ? Alert.alert('Attention', 'Vous ne pouvez pas modifier, elle posséde un locataire.') : this.indisponible(house) }}>
                            <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>{!house?.disponible || renters? 'Indisponible' : 'Disponible'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ ...styles.bt1, backgroundColor: 'green' }}
                            onPress={() =>  this.props.navigation.navigate('UpdateEtape', { 'data': item })}>
                            <Text style={{}}>Modifier</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ ...styles.bt1, backgroundColor: 'red' }}
                            onPress={() => this.createAlert(renters)}>
                            <Text style={{}}>Supprimer</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 50 }}>

                    </View>
                </View>
            </ScrollView>
        )
    }

}




const styles = StyleSheet.create({
    footer: {
        padding: 4,
        position: 'absolute',
        backgroundColor: 'transparent',
        bottom: 0,
        width: '100%',
        marginBottom: Platform.OS === 'ios' ? 20 : 10,
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    preloader: {
        //margintop: '50%',
        //bottom: 0,
        //position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalView: {
        backgroundColor: "white",
        borderTopRightRadius: 15, borderTopLeftRadius: 15,
        padding: 8, marginTop: height_ * 0.63,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '100%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    textStyle: {
        color: "#010102",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    buttonstyle1: {
        width: '30%',
        borderRadius: 7,
        backgroundColor: 'white',
        borderWidth: 2,
        // borderColor: '#dc7227',
        textAlign: 'center',
        height: 30,
        justifyContent: 'center'
    },
    buttonstyle: {
        borderRadius: 10, textAlign: "center",
        backgroundColor: 'orange', alignItems: "center"
        , flexDirection: 'row', justifyContent: "center",
        shadowColor: "black",
        shadowOffset: {
            width: 0, height: 6,
        }, paddingBottom: 10, paddingTop: 5,
        shadowOpacity: 0.2, width: "27%", height: 35,
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 24,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
    bt1: {
        backgroundColor: '#00394d', width: 80,
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    },
    images: {
        width: width_, height: height_ * 0.4,
        borderColor: "white", borderWidth: 0.5,
    }
})

