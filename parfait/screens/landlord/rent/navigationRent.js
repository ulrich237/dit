import React, { Component } from 'react';
import { Text, Dimensions, SafeAreaView } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import MyRentReserve from './myRentReserve';
import MyRenters from './myRenters';
import Feather from 'react-native-vector-icons/Feather'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
const TopTab = createMaterialTopTabNavigator();

var height_ = Math.round(Dimensions.get('window').height)


export default class NavigationRent extends Component {
    render() {
        return (
            <SafeAreaView style={{ backgroundColor: '#fdf7e8', flex: 1 }}>
                <Text style={{ fontWeight: '700', color: '#7c3325', fontSize: 30, margin: 10 }}>Mes locataires</Text>
                <TopTab.Navigator
                    tabBarOptions={{
                        activeTintColor: '#7c3325', inactiveTintColor: '#e9deb5',
                        indicatorStyle: { backgroundColor: '#7c3325' }
                    }}
                >
                    <TopTab.Screen name="MyRenters" children={props =>
                        <MyRenters navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Actifs',
                            tabBarIcon: ({ color, size }) => (
                                <Feather name="list" color={color} size={21} />
                            ),
                        }}
                    />
                    <TopTab.Screen name="MyRentReserve" children={props =>
                        <MyRentReserve navigate={this.props.navigation.navigate} />}
                        options={{
                            tabBarLabel: 'Compte des locataires',
                            tabBarIcon: ({ color, size }) => (
                                <MaterialIcons name="local-mall" color={color} size={26} />
                            ),
                        }}
                    />
                    
                </TopTab.Navigator>
            </SafeAreaView>
        )
    }
}


