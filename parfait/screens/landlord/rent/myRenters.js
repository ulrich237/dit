import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Share,
    TouchableOpacity, ScrollView, FlatList, Dimensions, 
} from 'react-native'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';
import firebase from '@react-native-firebase/app';
/*import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";*/
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';



const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};

var width_ = Math.round(Dimensions.get('window').width)

export default class MyRenters extends Component {

    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: [], landlords: {},
            houses: [], user_id: null, imageshouses: [], refresh: false
        }
    }


    generateLink = async () => {
        console.log('oui')
        const link_ = await dynamicLinks()
            .buildShortLink(
                {
                    link: `https://imobbis.page.link`,
                    domainUriPrefix: 'https://imobbis.page.link',
                    android: {
                        packageName: 'com.imobbis.app',
                        //minimumVersion: 19
                        //version minmale ici
                        //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                    },
                    /* ios: {
                         bundleId: AppConfig.getIOSBundle(),
                         appStoreId: AppConfig.getIOSAppId(),
                     },*/
                    /* social: {
                         title: `Ma publication pour test`,
                         descriptionText: `Ma publication pour test 237`,
                         imageUrl: `https://imobbis.pythonanywhere.com/media/user_pics/ic_launcher.png`,
                     },*/
                    /* navigation: {
                         forcedRedirectEnabled: true,
                     }*/
                },
                //firebase.dynamicLinks.ShortLinkType.SHORT
            )
            .then((link) => {
                //DynamicLinkStore.currentUserProfileLink = link;
                console.log(link)
                this.onShare(link)
            })
            .catch((err) => {
               console.log(err);
                //  DynamicLinkStore.profileLinkLoading = false;
            });
    }

    onShare = async (data) => {
        try {

            const result = await Share.share({
                message: data+ " J'utilise cette application pour faciliter la gestion locative.",
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            
            console.log(error.message);
        }
    };

    componentDidMount() {

        this.displayData()

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));
        axios.get(`https://imobbis.pythonanywhere.com/new/hous/`,)
            .then(response => this.setState({ houses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
            .then(response => this.setState({ renters: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));
    }


    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.renters !== this.state.renters) {
            axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
                .then(response => this.setState({ renters: response.data }),)
                .catch(error => this.setState({ error, isLoading: false }));
        }

    }




    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })

    };


    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }

    getDaysFromDays = (dateout, dateint) => {
        let d = new Date(dateout); let f = new Date(Date.now());
        let k = new Date(dateint)
        let tmp = d - f;  // uttilise
        let tmps = d - k
        let res = tmp / tmps
        return 1 - res
    }

    render() {

        const { navigate } = this.props
        const { users, landlords, houses, renters, imageshouses, user_id } = this.state
        var my_houses = _.filter(houses, { user: user_id })
        var rent_ar = []
        var user = {}
        var house = {}
        var labels = []
        var datas = []
        renters.map(renter => {
            return (my_houses?.map(id => {
                renter.current_house == id.id ?
                    (labels.push(renter?.nom), datas.push(parseFloat(this.getDaysFromDays(renter?.date_out, renter?.date_in).toFixed(2))), rent_ar.push(renter)) : null
            })
            )
        })

       

       /* const data = {
            labels:  _.size(labels) != 0 ? labels : ['name', 'nom'], // optional
            data: _.size(datas) != 0 ? datas : [10, 10]
        };*/

        return (
            <View style={{ backgroundColor: 'white' }}>
                <ScrollView>
                   {/* <View style={styles.imgContainer}>
                        <Text style={{ textAlign: "center", marginTop: "10%", color: "black", fontWeight: 'bold', fontSize: 22 }}>
                            Progréssion d'utilisation
                        </Text>
                        <ProgressChart
                            data={data}
                            width={width_}
                            height={250}
                            strokeWidth={8}
                            radius={8}
                            chartConfig={chartConfig}
                            hideLegend={true}
                        />
                    </View>*/}
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <FlatList
                            numColumns={2}
                            data={rent_ar}
                            showsVerticalScrollIndicator={false}
                            onMomentumScrollBegin={() => this.setState({ mom_beg: false })}
                            renderItem={({ item, index, separators }) => (
                                <View style={{ ...styles.row3 }}>
                                    {
                                        user = _.find(users, { id: item?.user }),

                                        house = _.find(houses, { id: item.current_house }),

                                        <View>
                                            <View>
                                                <TouchableOpacity style={{}}>
                                                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{item?.prenom + ' ' + item?.nom}</Text>
                                                </TouchableOpacity>
                                                <View style={{ marginTop: 2 }}>
                                                    <View>

                                                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, marginTop: 10 }} />
                                                        <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', }} >Profile sur Imobbis</Text>
                                                        <View style={{ flexDirection: "row", width: '100%', marginTop: 5 }} >
                                                            {
                                                                user ?
                                                                    <View style={{ flexDirection: "row", width: '88%', }} >
                                                                        <Avatar.Image
                                                                            size={30}
                                                                            rounded
                                                                            source={{ uri: user?.profile_image }} />
                                                                        <View style={{ margin: '2%', marginTop: 5 }}>
                                                                            <Text numberOfLines={1} style={{ fontSize: 12, }}>{user?.username}</Text>
                                                                        </View>
                                                                    </View>
                                                                    : <TouchableOpacity>
                                                                        <Text style={{ fontSize: 12, color: 'green' }}>Pas trouvé sur Imobbis</Text>
                                                                    </TouchableOpacity>
                                                            }
                                                        </View>
                                                        <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, marginTop: 10 }} />
                                                        <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', }} >Pourcentage d'utilisation</Text>
                                                        <Text style={{ textAlign: 'center', fontSize: 12, color: 'red', marginTop: 5 }}>
                                                            {(this.getDaysFromDays(item?.date_out, item?.date_in).toFixed(2) * 100)}%
                                                        </Text>
                                                    </View>

                                                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                        <TouchableOpacity style={{
                                                            width: 100, borderWidth: 1.5, width: '46%', padding: 1,
                                                            justifyContent: 'center', alignItems: 'center', borderRadius: 5, marginTop: 4
                                                        }}
                                                            onPress={() => navigate('DetailRenter', { renter: item, house: house, user_: _.find(users, { id: item?.user }), images: imageshouses })}>
                                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>Détail</Text>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{ ...styles.bt1, marginLeft: 10 }}
                                                            onPress={() => {
                                                                _.find(users, { id: item?.user }) ? navigate('MessagesIni', {
                                                                    'user': _.find(users, { id: item?.user }),
                                                                }) : this.generateLink()
                                                            }}>
                                                            <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>{_.find(users, { id: item?.user }) ? 'Ecrire' : 'L\'inviter'}</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, marginTop: 10 }} />
                                            <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', }} >{house?.category}</Text>
                                            <View style={{ marginTop: 5 }}>
                                                <Text style={{ color: '#664200', fontWeight: 'bold', fontSize: 13 }} numberOfLines={1}>{house?.name}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Image source={{ uri: _.find(imageshouses, { house: house.id })?.image }}
                                                        style={{ width: '40%', height: 50, marginTop: 6, borderRadius: 7 }}
                                                    />
                                                    <View style={{ marginTop: 5, marginLeft: 3 }}>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold' }}> {house?.nb_room} chambre(s)</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold' }}> {house?.nb_parlour} salon(s)</Text>
                                                        <Text style={{ color: 'grey', fontWeight: 'bold' }}> {house?.nb_toilet} douche(s)</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    }
                                </View>
                            )}
                            //ListFooterComponent={this.renderLoader}
                            onEndReachedThreshold={0.7}
                            keyExtractor={item => item}
                            refreshing={this.state.refresh}
                            onRefresh={this.handleRefesh}
                        />
                    </View>

                    <View style={{height:150}}></View>
                </ScrollView>
            </View>
        )
    }

}


const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        }, width: '47%', margin: 6,
        shadowOpacity: 0.2,
        shadowRadius: 4,
        padding: 6, elevation: 9,
        borderRadius: 20
    },
    imgContainer: {
        backgroundColor: '#dc7e27',
        //alignItems: 'center',
        justifyContent: 'center', margin: 6,
        height: '30%', borderRadius: 15,
        shadowOffset: {
            width: 0, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8.30,
        shadowColor: '#05375a'
    },

    bt1: {
        backgroundColor: '#7c3325', width: '46%',
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 5, marginTop: 4, marginLeft: 3
    }
})