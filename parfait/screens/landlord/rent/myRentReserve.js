import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, FlatList, ActivityIndicator,
    TouchableOpacity, ScrollView, Alert, Dimensions,
} from 'react-native'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import AntDesign from "react-native-vector-icons/AntDesign";
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
var width_ = Math.round(Dimensions.get('window').width)
const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};
export default class MyRentReserve extends Component {

    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: [], landlords: [], houses: [], user_id: null,
            imageshouses: [], gcel: [], touch: [], refresh:false, countrys: []
        }
    }

    componentDidMount() {

        this.displayData()

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
        .then(response => this.setState({renters: _.filter(response.data, {landlord:this.state.user_id}) }),)
        .catch(error => this.setState({ error, isLoading: false }));


        axios.get(`https://imobbis.pythonanywhere.com/new/hous/`,)
            .then(response => this.setState({ houses: _.filter(response.data, { user: this.state.user_id }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    countrys: res.data.country,
                })
            });
     
    }



    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.users !== this.state.users) {
            axios.get('https://imobbis.pythonanywhere.com/wen/chercheLocataire/' + this.state.user_id)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));
        }

    }


 

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)
           
            axios.get('https://imobbis.pythonanywhere.com/wen/chercheLocataire/' + json_id)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

            this.setState({ user_id: json_id })

        } catch { error => alert(error) };
    }

    join(user, renter){

        axios.patch("https://imobbis.pythonanywhere.com/wen/locataires/" + renter.id, {
            'user': user?.id
        }).catch(error => { }).then(response => {

            Toast.showWithGravity(
                "Effectué avec succés.",
                Toast.LONG,
                Toast.CENTER,
              
            )

        })
        .catch(error => { })
        
        
     
        
   
    }






    handleRefesh = () => {
        this.setState({ refresh: true, }
            , () => { this.setState({ refresh: false }) })

    };


    render() {

        const {countrys, users, landlords, houses, renters, user_id, imageshouses, touch } = this.state
        const { navigate } = this.props
        let renter
        let user
        var datas = []
        var labels = []
        var s = 0
         
      
        return (
            <ScrollView style={{ backgroundColor: 'white' }}>
                     <Text style={{ textAlign: "center", marginTop: "3%", fontWeight: 'bold', color: 'grey', fontSize:12 }}>
                            Faire correspondre le profil de bailleur à l'utilisateur correspondant.
                        </Text>
                <FlatList
                    numColumns={1}
                    data={users}
                    showsVerticalScrollIndicator={false}
                    onMomentumScrollBegin={() => this.setState({ mom_beg: false })}
                    renderItem={({ item, index, separators }) => (
                        <View style={{ ...styles.row3 }}>
                           
                           <View style={{ padding: 10, flexDirection: "row", margin: '4%', justifyContent: "space-between", alignItem: "center", }}>
                                            <View style={{ flexDirection: "row", width: width_ * 0.6, }} >
                                                <Avatar.Image
                                                    size={49}
                                                    rounded
                                                    avatarStyle={{ backgroundColor: 'orange' }}
                                                    source={{ uri: 'https://imobbis.pythonanywhere.com' + item?.profile_image }} />
                                                <View style={{ marginLeft: '5%', }}>
                                                    <Text numberOfLines={1} style={{ fontWeight: "bold", fontSize: 18 }}>{item?.username} {item?.nom}</Text>
                                                    <View>
                                                        {
                                                            _.filter(renters, (value) => value.phone_number == item?.phone_number).map(renter_=>{
                                                                return(
                                                                    <TouchableOpacity
                                                                    style={{borderWidth: 1, borderColor: 'grey', padding: 4, borderRadius: 10, marginTop: 3}}
                                                                    onPress={()=>{this.join(item, renter_)}}>
                                                                         <Text style={{fontSize:12, color:'red' }}>Taper ici pour faire correspondre</Text>
                                                                        <View style={{ flexDirection: 'row', }}>      
                                                                            <View style={{ marginLeft: 5, width: '100%' }}>
                                                                                <Text numberOfLines={1} style={{fontSize: 16,}}>
                                                                                    {renter_?.nom} {renter_?.prenom}</Text>      
                                                                            </View>
                                                                        </View> 
                                                                     </TouchableOpacity>

                                                                )
                                                            })                                                       
                                                        }
                                                    </View>
                                                  
                                                </View>
                                            </View>
                                          
                                        </View>
                        </View>
                    )}
                    //ListFooterComponent={this.renderLoader}
                    onEndReachedThreshold={0.7}
                    keyExtractor={item => item.id}
                    //onEndReached={this.itemsToRender2}
                    refreshing={this.state.refresh}
                    onRefresh={this.handleRefesh}
                />
                <View style={{ height: 30 }}></View>
            </ScrollView>
        )
    }

}



const styles = StyleSheet.create({
    row3: {
        marginTop: 25,
        backgroundColor: '#fdf7e8',
        shadowColor: "orange",
        shadowOffset: {
            width: 3,
            height: 3,
        }, marginVertical: 10,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7, elevation: 3,
        marginVertical: 6,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: 80,
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    },
    imgContainer: {
        backgroundColor: '#dc7e27',
        alignItems: 'center',
        justifyContent: 'center', margin: 6,
        height: '40%', borderRadius: 15,
        shadowOffset: {
            width: 0, height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8.30,
        shadowColor: '#05375a'
    },
})
