import React, { Component } from 'react'
import {
    Dimensions, TextInput, Image, View, StyleSheet, Alert,
    ScrollView, Picker, Modal, TouchableHighlight, TouchableOpacity,
} from 'react-native';
import { Text, Card, Button, Icon, Badge } from 'react-native-elements';
import _ from 'underscore'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Toast from 'react-native-simple-toast'
import Entypo from 'react-native-vector-icons/Entypo';
import { RadioButton } from 'react-native-paper';
import MultiSelect from 'react-native-multiple-select';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FastImage from 'react-native-fast-image';
import CalendarPicker from 'react-native-calendar-picker';
import DatePicker from 'react-native-datepicker';
import Date_heure from '../../functions/date_heure';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class AddLocataire extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            countrys: [], citys: [], districts: [],
            nebors: [], users: [], houses: this.props.route.params?.houses, user_id: null,
            imageshouses: [], house_: this.props.route.params?.house,
            step: 1,
            bar: {
                country: null, ID_number:0,
                nom: null, prenom: null, profession: null, tel: 0, num1: 0,
                num2: 0, nbMois: 0, nbJour: 0,
                dateIn: null, dateOut: null, current_house: this.props.route.params?.house?.id
            },
            startDate: null,
            endDate: null, post: false, etat: true
        };

    }

    componentDidMount() {
        this.displayData()
        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    countrys: res.data.country,
                })
            });

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/hous/`,)
            .then(response => this.setState({ houses: _.filter(response.data, { user: this.state.user_id, disponible: false }) }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

    }



    addMonths(date, months) {

        var f = new Date(date)
        var d = f.getDate();
        f.setMonth(f.getMonth() + +months);
        if (f.getDate() != d) {
            f.setDate(0);
        }
        return f;

    }

    addDays(date, days) {

        var res = new Date(date);
        res.setDate(res.getDate() + +days);
        return res;

    }




    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }



    handleChange = (name, value) => {

        const bar = { ...this.state.bar, [name]: value };
        this.setState({ bar });

    };






    onChange(date) {
        const { house_ } = this.state
        var dat = house_?.is_immeuble ? this.addDays(date, Number(this.state.bar.nbJour))
            : this.addMonths(date, Number(this.state.bar.nbMois))

        //var dat = this.addMonths(date, Number(this.state.bar.nbMois))

        this.handleChange('dateIn', date)
        this.handleChange('dateOut', dat)

    }


    reinit = () => {
        const bar = {
            country: null,
            nom: null, prenom: null, profession: null, tel: 0, num1: 0,
            num2: 0, nbMois: 0, nbJour: 0,
            dateIn: null, dateOut: null
        };
        this.setState({ bar });
        this.setState({ etat: true })
    };
    

    save = (item) => {
        // https://imobbis.pythonanywhere.com/wen/locataires
        console.log(item)
        this.setState({ post: true })

        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + item?.current_house, {
            'disponible': true
        }).then(response => {

            axios.post("https://imobbis.pythonanywhere.com/wen/locataires",
                {
                    "phone_number": Number(item?.tel),
                    "nom": item?.nom,
                    "prenom": item?.prenom,
                    "numero1": Number(item?.num1),
                    "numero2": Number(item?.num2),
                    "date_in": new Date(item?.dateIn),
                    "date_out": new Date(item?.dateOut),
                    "nb_day": item?.nbJour,
                    "nb_month": item?.nbMois,
                    "ID_number": item?.ID_number,
                    "landlord": this.state.user_id,
                    "current_house": item?.current_house,
                    "countrie": item?.country,
                })
                .then(response => {
                    Toast.showWithGravity(
                        "Locataire ajouté.",
                        Toast.LONG,
                        Toast.CENTER,
                       
                    );

                    this.props.navigation.navigate('LandHome')
                })
                .catch(error => { alert(error), this.setState({ post: false }) })
        })
            .catch(error => { })



    }

    suivant1 = () => {
        if (this.state.house_) {
            if (this.state.bar.nom === null) {
                Alert.alert("Attention", "Veuillez renseigner le nom.")
            } else {
                if (this.state.bar.tel === 0) {
                    Alert.alert("Attention", "Veuillez renseigner le numéro de téléphone.")
                } else {
                    this.setState({ step: 2 })
                }
            }
        } else {
            Alert.alert("Attention", "Veuillez choisir le bien à louer.")
        }

    };

    suivant2 = () => {
        if (this.state.bar.nbJour === 0 && this.state.bar.nbMois == 0) {
            Alert.alert("Attention", "Veuillez remplir le nombre de mois ou de jour.")
        } else {
            this.setState({ step: 3 })
        }
    }



    suivant3 = () => {
        if (this.state.startDate == null) {
            Alert.alert("Attention", "Veuillez remplir la date d'entrée.")
        } else {
            this.save(this.state.bar)
        }
    };


    render() {
        const { imageshouses, houses, users, countrys, etat, house_ } = this.state;
        var hous_ = this.props.route.params?.house != undefined ? this.props.route.params?.house : house_
        var etat_ = this.props.route.params?.house != undefined ? false : etat


        console.log(houses)

        return (
            <ScrollView>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={etat_}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <ScrollView style={styles.modall}>
                        <View style={{ marginTop: 50, marginLeft: '40%' }}>
                            <Text>Pour sortir fermer et quitter</Text>
                            <FontAwesome5 style={{ alignSelf: 'flex-end' }}
                                onPress={() => {
                                    this.setState({ etat: !this.state.etat })
                                }}
                                name='times-circle'
                                size={30}
                                color='#7c3325' />
                        </View>

                        <View style={{ marginTop: 10, borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                        <Text style={styles.fonts}>Choisir la maison à louer </Text>
                        <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                            <Text style={{ fontSize: 10, textAlign: 'center' }}>Vous devez choisir une maison pour ajouter le locataire à l'interieur.</Text>
                        </View>

                        <View style={{ height: 50 }}>

                        </View>

                        {
                            _.size(houses) == 0 ?
                                <View>
                                    <Text style={{ textAlign: 'center', color: 'red', fontSize: 15, fontWeight: 'bold' }}>Pas de maison disponile</Text>
                                </View>
                                : <View>
                                    {
                                        houses.map(item => {
                                            return (
                                                <TouchableOpacity
                                                    containerStyle={{ flex: 1, }}
                                                    onPress={() => { this.handleChange('current_house', item.id), this.setState({ house_: item }), this.setState({ etat: false }) }}
                                                >
                                                    <View style={{ ...styles.row3 }}>
                                                        <View style={{ height: 110, width: '40%', borderRadius: 15 }}
                                                        >
                                                            <Image source={{ uri: _.find(imageshouses, { house: item.id })?.image }}
                                                                style={{ height: 110, width: '100%', borderRadius: 15 }}
                                                            />
                                                        </View>
                                                        <View style={{ marginLeft: 10, width: '60%' }}>
                                                            <Text style={{ fontWeight: 'bold', fontSize: 16 }} numberOfLines={1}>{item.name}</Text>
                                                            <Text style={{ color: 'grey' }} numberOfLines={1}>
                                                                {item.nb_room + ' chambre(s) / ' + item.nb_parlour + ' salon(s) / ' + item.nb_toilet + ' douche(s)'}
                                                            </Text>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ textAlign: 'center', marginTop: 10, marginRight: 25, fontSize: 18, color: 'grey', fontWeight: 'bold' }}>
                                                                    {item?.category?.toUpperCase()}
                                                                    {!item?.colocation ? " A " + item?.publish_type?.toUpperCase() : ' EN COLLOCATION'}
                                                                </Text>

                                                            </View>





                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                            )
                                        })
                                    }
                                </View>
                        }





                    </ScrollView>

                </Modal>
                <View style={styles.container}>
                    {this.state.step == 1 ?
                        <Card>
                            <View style={styles.row}>
                                <View style={{ width: "33%" }}>
                                    <Badge value="1" status="success" />
                                </View>
                                <View style={{ width: "33%" }}>
                                    <Badge value="2" status="warning" />
                                </View>
                                <View style={{ width: "33%" }}>
                                    <Badge value="3" status="warning" />
                                </View>
                            </View>
                            <Text style={styles.fonts}>Information sur le locataire </Text>
                            <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                                <Text style={{ fontSize: 10, textAlign: 'center' }}>Ces informations sont importantes, ils permettrons d'identifier votre locataire et de mieux le gérer...</Text>
                            </View>
                            <Card.Divider />

                            <Text style={{ fontWeight: "bold" }} >
                                Nom :
                            </Text>

                            <TextInput
                                style={{ color: "black" }}
                                placeholder="Taper le nom ici..."
                                value={this.state.bar.nom}
                                placeholderTextColor="#dc7e27"
                                onChangeText={val => this.handleChange('nom', val)}
                            />

                            <Text style={{ fontWeight: "bold" }} >
                                Prenom :
                            </Text>

                            <TextInput
                                style={{ color: "black" }}
                                placeholder="Taper le prenom ici..."
                                value={this.state.bar.prenom}
                                placeholderTextColor="#dc7e27"
                                onChangeText={val => this.handleChange('prenom', val)}
                            />

                            <Text style={{ fontWeight: "bold" }} >
                                Profession :
                            </Text>

                            <TextInput
                                style={{ color: "black" }}
                                placeholder="Taper sa profession ici..."
                                value={this.state.bar.profession}
                                placeholderTextColor="#dc7e27"
                                onChangeText={val => this.handleChange('profession', val)}
                            />

                            <Text style={{ fontWeight: "bold" }} >
                                Numero de téléphone :
                            </Text>

                            <TextInput
                                style={{ color: "black" }}
                                onChangeText={val => this.handleChange('tel', val)}
                                value={this.state.bar.tel}
                                placeholder="Taper son numéro ici..."
                                placeholderTextColor="#dc7e27"
                                keyboardType="numeric"
                            />

                            <View style={styles.row}>
                                <View style={{ width: "50%" }} >
                                    <TouchableOpacity style={styles.loginBtn1} onPress={() => this.reinit()}>
                                        <Text style={{ marginBottom: 10, color: "#7c3325", fontWeight: "bold" }}><Entypo name='chevron-left' color="#7c3325" size={25} style={{ marginTop: -25 }} /> REINITIALISER</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ width: "50%" }}>
                                    <TouchableOpacity style={styles.loginBtn} onPress={() => this.suivant1()}>
                                        <Text style={{ marginBottom: 10, color: "white", fontWeight: "bold" }}>SUIVANT <Entypo name='chevron-right' color="#ffffff" size={25} style={{ marginTop: -100 }} /></Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </Card> :

                        this.state.step == 2 ?
                            <Card>
                                <View style={styles.row}>
                                    <View style={{ width: "33%" }}>
                                        <Badge value="1" status="success" />
                                    </View>
                                    <View style={{ width: "33%" }}>
                                        <Badge value="2" status="success" />
                                    </View>
                                    <View style={{ width: "33%" }}>
                                        <Badge value="3" status="warning" />
                                    </View>
                                </View>
                                <Text style={styles.fonts} h4>Informations : </Text>
                                <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                                    <Text style={{ fontSize: 10, textAlign: 'center' }}>Ces informations sont importantes.</Text>
                                </View>

                                <Card.Divider />


                                <Text style={{ fontWeight: "bold" }} >
                                    Numéro du téléphone 1 (optionnel):
                                </Text>

                                <TextInput
                                    style={{ color: "black" }}
                                    placeholder="Taper son second numéro ici..."
                                    value={this.state.bar.num1}
                                    placeholderTextColor="#dc7e27"
                                    keyboardType="numeric"
                                    onChangeText={val => this.handleChange('num1', val)}
                                />

                                <Text style={{ fontWeight: "bold" }} >
                                    Numéro du téléphone 2 (optionnel):
                                </Text>

                                <TextInput
                                    style={{ color: "black" }}
                                    placeholder="Taper son autre numéro ici..."
                                    value={this.state.bar.num2}
                                    keyboardType="numeric"
                                    placeholderTextColor="#dc7e27"
                                    onChangeText={val => this.handleChange('num2', val)}
                                />
                                {
                                    hous_ ?
                                        <View>

                                            <Text style={{ fontWeight: "bold", marginTop: 10 }}>
                                                Nombre de {hous_?.is_immeuble ? 'jours' : 'mois'}
                                            </Text>

                                            <TextInput
                                                style={{ color: "black" }}
                                                placeholder="Taper ici..."
                                                value={hous_?.is_immeuble ? this.state.bar.nbJour : this.state.bar.nbMois}
                                                placeholderTextColor="#dc7e27"
                                                keyboardType="numeric"
                                                onChangeText={val => this.handleChange(hous_?.is_immeuble ? 'nbJour' : 'nbMois', val)}
                                            />
                                        </View>
                                        : null
                                }

                                <View style={styles.row}>
                                    <View style={{ width: "50%" }} >
                                        <TouchableOpacity style={styles.loginBtn1} onPress={() => this.setState({ step: 1 })}>
                                            <Text style={{ color: "#7c3325", fontWeight: "bold" }}> <Entypo name='chevron-left' color="#7c3325" size={20} iconStyle={{ marginRight: 10 }} /> PRECEDENT </Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ width: "50%" }}>
                                        <TouchableOpacity style={styles.loginBtn} onPress={() => this.suivant2()}>
                                            <Text style={{ color: "white", fontWeight: "bold" }}>SUIVANT <Entypo name='chevron-right' color="#ffffff" size={20} iconStyle={{ marginRight: 10 }} /></Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </Card> :

                            this.state.step == 3 ?
                                <Card>
                                    <View style={styles.row}>
                                        <View style={{ width: "33%" }}>
                                            <Badge value="1" status="success" />
                                        </View>
                                        <View style={{ width: "33%" }}>
                                            <Badge value="2" status="success" />
                                        </View>
                                        <View style={{ width: "33%" }}>
                                            <Badge value="3" status="success" />
                                        </View>
                                    </View>
                                    <Text style={styles.fonts} h4>Informations : </Text>
                                    <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                                        <Text style={{ fontSize: 10, textAlign: 'center' }}>Ces informations sont importantes.</Text>
                                    </View>
                                    <Card.Divider />

                                    {
                                        hous_ ?
                                            <View>


                                                <Text style={{ fontWeight: "bold" }} >
                                                    Il entre en quelle date :
                                                </Text>

                                                <DatePicker
                                                    style={{ width: 200, marginTop: 10 }}
                                                    date={this.state.startDate}
                                                    mode="date"
                                                    placeholder="select date"
                                                    format="YYYY-MM-DD"
                                                    minDate="2019-05-01"
                                                    confirmBtnText="Confirm"
                                                    cancelBtnText="Cancel"
                                                    customStyles={{
                                                        dateIcon: {
                                                            position: 'absolute',
                                                            left: 0,
                                                            top: 4,
                                                            marginLeft: 0
                                                        },
                                                        dateInput: {
                                                            marginLeft: 36
                                                        }
                                                        // ... You can check the source to find the other keys.
                                                    }}
                                                    onDateChange={(date) => { this.setState({ startDate: date }), this.onChange(date) }}
                                                />

                                            </View>
                                            : null
                                    }



                                    <TouchableOpacity onPress={() => { this.setState({ isVisible: true, }) }}
                                        style={{ flexDirection: 'row', marginTop: 7 }}>
                                        <View style={{ width: '10%' }}>
                                            <Text>Lieu </Text>
                                        </View>
                                        <View style={{ width: '84%' }}>
                                            <Text style={{ color: 'red' }}>{this.state.bar.country == null ? 'Sélectionner son pays ?' : _.find(this.state.countrys, { id: this.state.bar.country })?.name}</Text>
                                        </View>
                                        <MaterialIcons name='navigate-next' size={30} color='#05375a' />
                                    </TouchableOpacity>


                                    <Text style={{ fontWeight: "bold" }} >
                                    Numéro de la CNI:
                                </Text>

                                <TextInput
                                    style={{ color: "black" }}
                                    placeholder="Taper ici..."
                                    value={this.state.bar.ID_number}
                                    keyboardType="numeric"
                                    placeholderTextColor="#dc7e27"
                                    onChangeText={val => this.handleChange('ID_number', val)}
                                />

                                    {
                                        //modal personnalisé
                                    }

                                    <View>
                                        <Modal
                                            animationType={"slide"}
                                            transparent={true}
                                            visible={this.state.isVisible}
                                            onRequestClose={() => { console.log("Modal has been closed.") }}>
                                            {
                                                <View style={styles.modal}>
                                                    <View>
                                                        <View style={{ flexDirection: "row" }} >
                                                            <View style={{ width: '25%' }}>
                                                            </View>
                                                            <View style={{ width: '50%' }}>
                                                                <Text style={{
                                                                    fontWeight: "bold", color: "white",
                                                                    textAlign: "center", fontSize: 20, letterSpacing: 2
                                                                }}>
                                                                    LE PAYS
                                                                </Text>
                                                            </View>
                                                            <View style={{ width: '25%' }}>
                                                                <FontAwesome5 style={{ alignSelf: 'flex-end' }}
                                                                    onPress={() => {
                                                                        this.setState({ isVisible: !this.state.isVisible })
                                                                    }}
                                                                    name='times-circle'
                                                                    size={30}
                                                                    color='#05375a' />
                                                            </View>
                                                        </View>

                                                        <View style={{ flexDirection: "row", marginTop: 20, fontSize: 16 }} >
                                                            <View style={{ width: '25%', borderBottomColor: this.state.bar.country == null ? 'yellow' : null, borderBottomWidth: this.state.bar.country == null ? 2 : null, paddingBottom: 10 }}>
                                                                <TouchableOpacity onPress={() => { this.reinit() }}>
                                                                    <Text style={{ color: this.state.bar.country == null ? 'yellow' : 'white' }}>Pays</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                        <Card.Divider />
                                                        <ScrollView>
                                                            {
                                                                this.state.bar.country == null ?
                                                                    this.state.countrys.map(item => (
                                                                        <TouchableOpacity onPress={() => { this.handleChange('country', item.id), this.setState({ isVisible: !this.state.isVisible }) }}>
                                                                            <Text style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10 }}> {item.name} </Text>
                                                                        </TouchableOpacity>
                                                                    ))
                                                                    : null}
                                                        </ScrollView>
                                                    </View>

                                                </View>


                                            }

                                        </Modal>
                                    </View>

                                    {
                                        //fin modal personnalisé
                                    }


                                    {
                                        this.state.bar.dateOut ?
                                            <View>
                                                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6, marginTop: 10 }} />
                                                <Text style={{ marginLeft: 5, marginBottom: 13, fontSize: 12, fontWeight: 'bold', fontSize: 12 }} >Libération</Text>
                                                <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                                                    <Entypo name='resize-full-screen' color={'black'} size={18} />
                                                    <Text style={{ marginLeft: 15 }}>Date sortie: {new Date_heure().format_date(this.state.bar.dateOut)}</Text>
                                                </View>
                                            </View> : null

                                    }


                                    <View style={styles.row}>
                                        <View style={{ width: "50%" }} >
                                            <TouchableOpacity style={styles.loginBtn1} onPress={() => this.setState({ step: 2 })}>
                                                <Text style={{ color: "#7c3325", fontWeight: "bold" }}>  <Entypo name='chevron-left' color="#7c3325" size={20} iconStyle={{ marginRight: 10 }} /> PRECEDENT </Text>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ width: "50%" }}>
                                            <TouchableOpacity style={styles.loginBtn} onPress={() => { this.state.post ? null : this.suivant3() }}>
                                                <Text style={{ color: "white", fontWeight: "bold" }}>ENREGISTRER <Entypo name='chevron-right' color="#ffffff" size={20} iconStyle={{ marginRight: 10 }} /></Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                </Card> : null}

                </View>

            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    container1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fonts: {
        marginBottom: 8,
        color: "#7c3325",
        textAlign: "center"
    },
    row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
    loginBtn: {
        backgroundColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        // marginTop: 40,
        // marginBottom: 10
    },
    loginBtn1: {
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        //marginTop: 40,
        // marginBottom: 10
    },
    modal: {
        backgroundColor: "grey",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.30,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        fontSize: 16,
        textAlign: "center"
    },
    modall: {
        backgroundColor: "white",
        //  borderRadius: 20,
        // padding: 10,
        // shadowColor: "#000",
        alignContent: 'center',

        width: width_,
        height: height_ * 0.85,
        marginBottom: height_ * 0.25,

    },
    row3: {
        marginTop: 25,
        backgroundColor: '#fdf7e8',
        shadowColor: "orange",
        shadowOffset: {
            width: 3,
            height: 3,
        }, marginVertical: 10,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7, elevation: 3,
        marginVertical: 6,
        borderRadius: 20
    },
})