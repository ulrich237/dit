import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import { Avatar } from 'react-native-paper'
import _ from 'underscore'
import axios from 'axios'
import Touched from '../../functions/touched'

import AsyncStorage from '@react-native-async-storage/async-storage';

export default class Notrenter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            renters: [], users: [], landlords: [], houses: [], user_id: null,
            imageshouses: []
        }
    }

    componentDidMount() {
        this.displayData()


        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));
        axios.get(`https://imobbis.pythonanywhere.com/new/hous/`,)
            .then(response => this.setState({ houses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
            .then(response => this.setState({ renters: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
            .then(response => this.setState({ users: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

    }

    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }

    getDaysFromDays = (date) => {
        var today = new Date()
        var end_date = new Date(date.substring(0, 9).replace('-', '/').replace('-', '/'))
        var current_date = new Date(today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate());
        var time_left = end_date.getTime() - current_date.getTime();
        var days_left = time_left / (1000 * 3600 * 24);
        return days_left
    }

    render() {
        const { users, landlords, houses, renters, user_id, imageshouses } = this.state
        let renter
        let user
        return (
            <ScrollView style={{ backgroundColor: 'white' }}>
                {
                    _.filter(houses, { user: user_id }).map(house => {
                        return (
                            <View style={{ ...styles.row3 }}>

                                {
                                    renter = _.find(renters, { current_house: house.id }),
                                    user = _.find(users, { id: renter?.user }),
                                    !renter ?
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    
                                                        <Image source={{ uri: _.find(imageshouses, { house: house.id })?.image }}
                                                            style={{ height: 110, width: '34%', borderRadius: 15 }}
                                                        />
                                                    

                                                    <View  style={{  width: '65%',marginLeft:'3%'}}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 16 }} numberOfLines={1}>{house.name}</Text>
                                                        <Text style={{ color: 'grey' }} numberOfLines={1}>
                                                            {house.nb_room + ' chambres / ' + house.nb_parlour + ' salon / ' + house.nb_toilet + ' douche'}
                                                        </Text>

                                                        <View style={{ borderWidth: 1, borderColor: 'grey', padding: 4, borderRadius: 10, flexDirection: 'row', marginTop: 3,width:'80%' }}>
                                                            <Text style={{ marginRight: 5, color: 'red', fontWeight: 'bold' }}>
                                                                Cette maison n'a pas de locataire
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </View>



                                                <View style={{ flexDirection: 'row-reverse', }}>
                                                    <TouchableOpacity style={{ ...styles.bt1, backgroundColor: '#00394d' }}
                                                        onPress={() => this.props.navigation.navigate('ElecEau', { 'house': house })}>
                                                        <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Gérer</Text>
                                                    </TouchableOpacity>
    
                                                </View>

                                            </View>




                                         : null
                                }
                            </View>
                        )
                    })
                }
            </ScrollView>
        )
    }

}



const styles = StyleSheet.create({
    row3: {
        marginTop: 25,
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 3,
            height: 3,
        }, marginVertical: 10,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7, elevation: 3,
        marginVertical: 6,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: 80,
        justifyContent: 'center', alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    }
})