import React, { Component } from 'react'
import { Image, Text, View, TextInput, StyleSheet, Share, Alert, Easing, Modal, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
//import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import FastImage from 'react-native-fast-image';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
import Icon from "react-native-vector-icons/FontAwesome";
import Ionicon from "react-native-vector-icons/Ionicons";
import { Checkbox, Avatar } from 'react-native-paper';
import _ from 'underscore';
import CalendarPicker from 'react-native-calendar-picker';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import axios from 'axios'
import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';
import firebase from '@react-native-firebase/app';
import Moment from 'moment';
/*import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";*/
import * as Animatable from "react-native-animatable";
import Date_heure from '../../functions/date_heure';
import call from 'react-native-phone-call';
import Toast from 'react-native-simple-toast';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

const chartConfig = {
    backgroundGradientFrom: "#fff",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#fff",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(72, 47, 24, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
};

export default class DetailRenter extends Component {
    constructor(props) {
        super(props)
        this.state = {
            etat: false,
            countrys: [], neighbours: [],
            provinces: [],
            citys: [],
            renters: [], users: [], landlords: [], houses: [], user_id: null,
            imageshouses: [], nb: 0, post_:false
        }
    }


    generateLink = async () => {
        const link_ = await dynamicLinks()
            .buildShortLink(
                {
                    link: `https://imobbis.page.link`,
                    domainUriPrefix: 'https://imobbis.page.link',
                    android: {
                        packageName: 'com.imobbis.app',
                        //minimumVersion: 19
                        //version minmale ici
                        //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                    },
                    /* ios: {
                         bundleId: AppConfig.getIOSBundle(),
                         appStoreId: AppConfig.getIOSAppId(),
                     },*/
                    /* social: {
                         title: `Ma publication pour test`,
                         descriptionText: `Ma publication pour test 237`,
                         imageUrl: `https://imobbis.pythonanywhere.com/media/user_pics/ic_launcher.png`,
                     },*/
                    /* navigation: {
                         forcedRedirectEnabled: true,
                     }*/
                },
                //firebase.dynamicLinks.ShortLinkType.SHORT
            )
            .then((link) => {
                //DynamicLinkStore.currentUserProfileLink = link;
                console.log(link)
                this.onShare(link)
            })
            .catch((err) => {
               console.log(err);
                //  DynamicLinkStore.profileLinkLoading = false;
            });
    }


    calling = (args) =>{
        call(args).catch(console.error)
  }

    onShare = async (data) => {
        try {
            const result = await Share.share({
                message: data+ " J'utilise cette application pour faciliter la gestion locative.",
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    };


    componentDidMount() {
        this.displayData()

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    countrys: res.data.country, neighbours: res.data.neighbor,
                    citys: res.data.city
                })
            });
    }


    createAlert = (house) => {
        Alert.alert(
            'Suppression',
            'Voulez vous returer ce locataire: (' + house?.nom + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.delete('https://imobbis.pythonanywhere.com/wen/locataires/' + house?.id).then((res) => {
                            Toast.showWithGravity(
                                "Suppression effectuée!",
                                Toast.LONG,
                                Toast.CENTER,
                               ),
                                this.props.navigation.navigate('NavigationRent')
                        })
                            .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]
        )
    }




    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }


    getDaysFromDays = (dateout, dateint) => {
        let d = new Date(dateout); let f = new Date(Date.now());
        let k = new Date(dateint)
        let tmp = d - f;  // uttilise
        let tmps = d - k
        let res = tmp / tmps
        return 1 - res
    }


    addMonths(date, months) {
        var f = new Date(date)
        var d = f.getDate();
        f.setMonth(f.getMonth() + +months);
        if (f.getDate() != d) {
            f.setDate(0);
        }
        return f;
    }

    addDays(date, days) {
        var res = new Date(date);
        res.setDate(res.getDate() + +days);
        return res;
    }


    post() {
        const { user_, renter, images, house } = this.props.route.params
        const { nb } = this.state
        var dat = house?.is_immeuble ? this.addDays(renter?.date_out, nb) : this.addMonths(renter?.date_out, nb)


        this.setState({post_:true})
        this.state.nb != 0 ?
            axios.patch('https://imobbis.pythonanywhere.com/wen/locataires/' + renter?.id,
                {
                    'date_out': dat
                }
            )
                .then((res) => {
                    Toast.showWithGravity(
                        "Modification effectuée!",
                        Toast.LONG,
                        Toast.CENTER,
                        ),
                        this.props.navigation.navigate('NavigationRent')
                })
                .catch(error => this.setState({ error, isLoading: false }))

            : Alert.alert("Attention", "Vous ne pouvez pas exévuter cette instruction.")

    }



    render() {
        const { etat, countrys } = this.state
        const { user_, renter, images, house } = this.props.route.params

        return (
            <ScrollView style={{ backgroundColor: 'white' }}>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.etat}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    <View style={styles.modal}>
                        <TouchableOpacity style={{ flexDirection: 'row', }} onPress={() => this.setState({ etat: false })}>
                            <Icon style={{ marginLeft: '85%', marginTop: 50 }}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>

                        <View style={{ marginTop: 10, borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />

                        <View style={{ backgroundColor: "pink", marginTop: 20 }} >
                            <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Veuillez entrer le nombre de {house?.is_immeuble ? 'jours' : 'mois'} payé à nouveau.</Text>
                        </View>

                        <View style={{ height: 50 }}>

                        </View>
                        <Text style={{ fontWeight: "bold", marginTop: 10 }} >
                            Nombre de {house?.is_immeuble ? 'jour' : 'mois'} :
                        </Text>
                        <TextInput style={{ borderWidth: 2, color: "black", borderColor: "#d9d9d9", height: 60 }}
                            placeholder="Taper ici..."
                            keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
                            value={this.state.nb}
                            placeholderTextColor="#003f5c"
                            onChangeText={val => this.setState({ nb: val })}
                        />



                        <View style={styles.row}>
                            <TouchableOpacity style={styles.loginBtn} onPress={() =>{this.state.post_? null: this.post()}}>
                                <Text style={{ color: "white", fontWeight: "bold" }}>SAUVEGARDER <Entypo name='arrow-with-circle-down' color="#ffffff" size={20} iconStyle={{ marginRight: 10 }} /></Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </Modal>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ width: '50%' }}>
                        <View style={{ marginTop: 10, alignItems: 'center' }}>
                            <View>
                                {
                                    user_ ?
                                        <Avatar.Image source={{ uri: user_?.profile_image }} size={50} />
                                        : null
                                }
                            </View>
                            <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>
                                {renter.nom + ' ' + renter.prenom}
                            </Text>
                        </View>

                        <View style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            <TouchableOpacity style={{ ...styles.heart }}
                                onPress={() => this.calling(
                                {
                                    number:'+' + _.find(countrys, { id: renter?.countrie })?.indicatif?.toString() + renter?.phone_number.toString(),
                                    prompt: false 
                                }
                                )}>
                                <Animatable.View animation="shake" iterationCount={5} direction="alternate">
                                    <Entypo name='old-phone' size={20} color='black' />
                                </Animatable.View>
                                <Text style={{ fontSize: 12, fontWeight: '500' }}>Contacter</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styles.heart }}
                                onPress={() =>{renter?.user ?  this.props.navigation.navigate('MessagesIni', {'user':user_,}) : this.generateLink()}}>
                                <Animatable.View animation="shake" iterationDelay={1500} iterationCount='infinite'>
                                    <Entypo name={renter?.user ? 'message' : 'share'} color={renter?.user ? 'black' : 'red'} size={20} />
                                </Animatable.View>
                                <Text style={{ fontSize: 12, fontWeight: '500', color: renter?.user ? 'black' : 'red' }}>
                                    {renter?.user ? 'Ecrire' : 'L\'inviter'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ width: '50%', marginTop:50 }}>
                        <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', }} >Pourcentage d'utilisation</Text>
                        <Text style={{ textAlign: 'center', fontSize: 12, color: 'red', marginTop: 5 }}>
                                    {(this.getDaysFromDays(renter?.date_out, renter?.date_in).toFixed(2) * 100)}%
                        </Text>
                    </View>
                </View>

                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 20 }} >{house?.category}</Text>

                <View>
                    <Text style={{ color: 'grey' }} numberOfLines={1}>
                        {house.nb_room + ' chambre(s) / ' + house.nb_parlour + ' salon(s) / ' + house.nb_toilet + ' douche(s)'}
                    </Text>
                    <ScrollView horizontal={true}>
                        {
                            _.filter(images, { house: house.id }).map(hous => {
                                return (
                                    <View style={{}}>
                                        <FastImage
                                            style={{ width: width_ / 2, height: 100, marginRight: 5, marginTop: 6, borderRadius: 7 }}
                                            source={{
                                                uri: hous?.image,
                                            }}
                                        />
                                    </View>
                                )
                            })
                        }
                    </ScrollView>
                </View>


                <View style={{ marginTop: 15, ...styles.row4 }}>
                    <TouchableOpacity
                        style={{ borderBottomColor: '#e6e6e6', borderBottomWidth: 1, marginBottom: 7 }}
                        onPress={() => this.setState({ etat: true })}>
                        <View style={{ padding: 7, flexDirection: 'row', }}>
                            <View style={{ width: '75%' }}>
                                <Text style={{ fontSize: 18, fontWeight: '500', color: '#dc7e27' }}>Re-Payer</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ fontSize: 12, color: '#848484' }}>Tapez ici</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>


                <View style={{ marginTop: 10, borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />

                <View>
                    <CalendarPicker
                        weekdays={['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim']}
                        months={['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Dècembre']}
                        previousTitle="Précédent"
                        nextTitle="Suivant"
                        startFromMonday={true}
                        allowRangeSelection={true}
                        minDate={renter?.date_in}
                        maxDate={renter?.date_out}
                        todayBackgroundColor="orange"
                        scaleFactor={375}
                        selectedRangeEndTextStyle="#dc7e27"
                        selectedRangeStartTextStyle="#dc7e27"
                        selectedDayColor="#dc7227"
                        selectedDayTextColor="#FFFFFF"
                        selectedDayStyle={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid' }}
                        disabledDatesTextStyle={{ textDecorationLine: 'line-through', textDecorationStyle: 'solid' }}

                    />

                </View>

                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 20 }} >À propos</Text>
                <View style={{ marginTop: 10, }}>
                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='man' color={'black'} size={18} />
                        <Text style={{ marginLeft: 15 }}>Enregistré : {new Date_heure().getDate_publication(renter?.date_joined)}</Text>
                    </View>

                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='arrow-down' color={'black'} size={18} />
                        <Text style={{ marginLeft: 15 }}>Date entrée: {renter?.date_in ? new Date_heure().format_date(renter?.date_in) : null}</Text>
                    </View>

                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='resize-full-screen' color={'black'} size={18} />
                        <Text style={{ marginLeft: 15 }}>Date sortie: {renter?.date_out ? new Date_heure().format_date(renter?.date_out) : null}</Text>
                    </View>
                </View>


                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 20 }} >Actions sur le locataire</Text>
                <View style={{ marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                   {/* <TouchableOpacity style={{ ...styles.bt1, backgroundColor: 'green' }}>
                        <Text style={{}}>Modifier</Text>
                    </TouchableOpacity>*/}

                    <TouchableOpacity style={{ ...styles.bt1, backgroundColor: 'red' }}
                        onPress={() => this.createAlert(renter)}>
                        <Text style={{}}>Supprimer</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ height: 100 }}>

                </View>
            </ScrollView>
        )
    }
}




const styles = StyleSheet.create({
    footer: {
        padding: 4,
        position: 'absolute',
        backgroundColor: 'transparent',
        bottom: 0,
        width: '100%',
        marginBottom: Platform.OS === 'ios' ? 20 : 10,
    },
    modal: {
        backgroundColor: "white",
        //  borderRadius: 20,
        // padding: 10,
        // shadowColor: "#000",

        width: width_,
        height: height_ + 100,
        //marginBottom: height_ * 0.005,
        // marginTop: height_ * 0.12,

    },
    row: { flexDirection: 'row', justifyContent: "center", alignItems: 'center', padding: 12 },
    loginBtn: {
        backgroundColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10,
        borderRadius: 25,
    },
    loginBtn1: {
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#7c3325",
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    centeredView: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    preloader: {
        //margintop: '50%',
        //bottom: 0,
        //position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalView: {
        backgroundColor: "white",
        borderTopRightRadius: 15, borderTopLeftRadius: 15,
        padding: 8, marginTop: height_ * 0.63,
        alignItems: "flex-start",
        shadowColor: "#000000",
        width: '100%',
        shadowOffset: {
            width: 30,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 15
    },
    textStyle: {
        color: "#010102",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
    },
    heart: {
        //textAlign: 'right',
        alignItems: 'center',
        backgroundColor: "#fff6e6", // width: '20%' ,
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },

    row4: {
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 15,
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        margin: 5
    },
    bt1: {
        alignItems: 'center',
        //backgroundColor: "#fff6e6", // width: '20%' ,
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, //alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2
    },
})

