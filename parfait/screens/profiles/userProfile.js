import React, { Component } from 'react'
import { Image, Text, StyleSheet, TextInput, View, Modal, ScrollView, TouchableOpacity, Dimensions, Platform } from 'react-native'
import {Card } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import _ from 'underscore'
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class UserProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            what_modal: '',
            user: {
                id: null,
                profile_image: "a",
                username: "",
                first_name: "",
                last_name: "",
                gender: "M",
                countrie: null,
                province: null,
                citie: null,
                district: null,
                neighbor: null,
                phone_number: null
            },
            countrys: [],
            districts: [],
            provinces: [],
            citys: [],
            neighbors: [],
            location: ""

        }
    }

    componentDidMount() {
        axios.get(`https://imobbis.pythonanywhere.com/user/`)
            .then(resu => {
                let user = _.find(resu.data, { id: 1 })
                this.setState({ user: user });
                axios.get(`https://imobbis.pythonanywhere.com/location/`)
                    .then(res => {
                        let location = "";
                        _.find(res.data.country, {id: user.countrie}) ? location = _.find(res.data.country, {id: user.countrie}).name : null;
                        _.find(res.data.province, {id: user.province}) ? location = location +", "+ _.find(res.data.province, {id: user.province}).name : null;
                        _.find(res.data.district, {id: user.district}) ? location = location +", "+ _.find(res.data.district, {id: user.district}).name : null;
                        _.find(res.data.city, {id: user.citie}) ? location = location +", "+ _.find(res.data.city, {id: user.citie}).name : null;
                        _.find(res.data.neighbor, {id: user.neighbor}) ? location = location +", "+ _.find(res.data.neighbor, {id: user.neighbor}).name : null;
                        this.setState({ countrys: res.data.country, districts: res.data.district, citys: res.data.city, provinces: res.data.province, neighbors: res.data.neighbor, location: location });
                    })
            })

    }

    handleChange = (name, value) => {
        const user = { ...this.state.user, [name]: value };
        this.setState({ user });
    };

    addlocation(name) {
        let location = this.state.location;
        location = location == "" ? name : location+", "+name;
        this.setState({location: location});
    }

    handleChangeradio = () => {
        let user = this.state.user;
        user.gender == "M" ? user.gender = "F" : user.gender = "M";
        this.setState({ user });
    };

    initlocation(){
        let user = { ...this.state.user, countrie: null, province: null, citie: null, district: null, neighbor: null};
        this.setState({location: "", user: user});
    }
    
    save(item) {
        const formData = new FormData();
        formData.append("username", item.username);
        if (item.profile_image.fileName !== undefined) {
            formData.append("profile_image", {
                uri: item.profile_image.uri,
                type: item.profile_image.type,
                name: item.profile_image.fileName,
                data: item.profile_image.data
            });
        };
        formData.append("first_name", item.first_name);
        formData.append("last_name", item.last_name);
        formData.append("gender", item.gender);
        formData.append("countrie", Number(item.countrie));
        formData.append("province", Number(item.province));
        formData.append("citie", Number(item.citie));
        formData.append("district", Number(item.district));
        formData.append("neighbor", Number(item.neighbor));
        formData.append("phone_number", Number(item.phone_number));

        axios
        .patch(`https://imobbis.pythonanywhere.com/user/${item.id}`, formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
            alert("Votre profil a bel et bien été  modifié !");
        }).catch(err => {
            alert('err'+err)
            console.log(err)
        })
    }

    render() {
        const user = this.state.user;
        return (
            <View style={styles.detailsContainer}>

                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    detailsContainer: {
        height: '58%',
    },
    modal: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.50,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modal1: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.4,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
})