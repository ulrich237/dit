import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Share, ImageBackground, ScrollView, Dimensions } from 'react-native'
import { Avatar } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Touched from '../../functions/touched'
import _ from 'underscore'
import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Notification from '../../functions/notification';
import Entypo from 'react-native-vector-icons/Entypo';
import Date_heure from '../../functions/date_heure';
import * as Animatable from "react-native-animatable";
import moment from 'moment';
import PostData from '../../functions/postData';

import dynamicLinks, {
    FirebaseDynamicLinksTypes,
} from '@react-native-firebase/dynamic-links';

import call from 'react-native-phone-call';

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

var dat = {}

export default class AglaProfile extends Component {

    constructor(props) {

        super(props)
        this.state = {

            followers: [], user_id: null,
            imageshouses: [], follow: false,
            citys: [], countrys: [], user: [],

            id: this.props.route.params?.id,

            agla: this.props.route.params?.agla,
            houses: this.props.route.params?.houses,
            countrie: this.props.route.params?.countrie,

            get: false,

        }

    }


    generateLink = async (user, nbhouse, nbfollow) => {
       console.log(nbfollow + " " + nbhouse)
        const link_ = await dynamicLinks()
            .buildShortLink(
                {
                    link: 'https://imobbis.page.link/page/' + user?.id,
                    domainUriPrefix: 'https://imobbis.page.link',
                    android: {
                        packageName: 'com.imobbis.app',
                        minimumVersion: '22'
                        //version minmale ici
                        //fallbackUrl: 'https://play.google.com/store/apps/details?id=com.imobbis.app',
                    },
                    /*ios: {
                         bundleId: AppConfig.getIOSBundle(),
                         appStoreId: AppConfig.getIOSAppId(),
                     }, */
                    social: {
                        title: user?.username,
                        descriptionText: nbhouse.toString() + " publication(s), " + nbfollow.toString()  + " abonné(s) " +  user?.descriptions,
                        imageUrl: user?.profile_url || user?.profile_image
                    }
                    /* navigation: {
                         forcedRedirectEnabled: true,
                     }*/
                },
                //firebase.dynamicLinks.ShortLinkType.SHORT
            )
            .then((link) => {
                //DynamicLinkStore.currentUserProfileLink = link;
                console.log(link)
                this.onShare(link)
            })
            .catch((err) => {
                console.log(err);
                //  DynamicLinkStore.profileLinkLoading = false;
            });
    }

    onShare = async (data) => {
        try {
            const result = await Share.share({
                message: data,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            console.log(error.message);
        }
    };

    componentDidMount() {
        this.displayData()


        axios.get('https://imobbis.pythonanywhere.com/new/follow')
            .then(res => {
                let foll = _.find(res.data, { user: this.state.user_id, follower: this.state.agla?.id }) ? true : false
                this.setState({ followers: res.data, follow: foll })

            });


        axios.get('https://imobbis.pythonanywhere.com/new/house_user4/' + this.state.id)
            .then(res => {
                this.setState({ houses: res.data })

            });




        /*  axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
              this.setState({ user: resu.data })
          }).catch(err => err.Error == 'Network Error' ? (this.setState({ isloading: false }), Alert.alert('Se connecter à un réseau', 'Pour utiliser IMOBBIS, vous devez vous connecter à un reseau ou au wifi')) : null);
  */

        /* axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
             .then(response => this.setState({ imageshouses: response.data }),)
             .catch(error => this.setState({ error, isLoading: false }));*/

    }


    calling = (args) =>{
        call(args).catch(console.error)
  }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.followers !== this.state.followers) {
            axios.get(`https://imobbis.pythonanywhere.com/new/follow`)
                .then(res => {
                    this.setState({ followers: res.data })
                });
        }
    }

    save_follow = () => {
        const { follow } = this.state;
        this.setState({ follow: !follow });
        const { agla } = this.props.route.params;
        let exist = _.find(this.state.followers, { user: this.state.user_id, follower: agla?.id }) ? true : false;
        let item = exist == false ? {
            user: Number(this.state.user_id),
            follower: Number(agla?.id),
        } : { id: _.find(this.state.followers, { user: this.state.user_id, follower: agla?.id })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/follow`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/follow/${item.id}`)
        )
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/follow`)
                    .then(res => {
                        new Notification().sendNotitfications(agla?.tokens, _.find(this.state.user, { id: this.state.user_id })?.username, 'Vous a suivi.', dat)
                        let foll = _.find(res.data, { user: this.state.user_id, follower: agla?.id }) ? true : false
                        this.setState({ followers: res.data, follow: foll })

                    });
            }).catch(error => {
                console.log("error " + error)
            })
    }
    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })

            axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/" + this.state?.id).then(resu => {
                this.setState({ agla: resu.data })
                axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
                    this.setState({
                        countrie: _.find(res.data.country, { id: resu.data?.countrie }),

                    });
                });
            }).catch(err => console.log(err));

        } catch { error => alert(error) };

    }
    render() {

        const { content_ct, imageHouse, } = this.props.route.params

        const { agla, houses, countrie, countrys, followers, user_id, imageshouses, follow } = this.state

        return (
           
          
            <ScrollView style={{ backgroundColor: 'white', }}>
                <View style={{ flexDirection: 'row', width: '100%', backgroundColor: '#dc7e27' }}>
                    <TouchableOpacity style={{ marginLeft: -10, width: '14%' }} onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before' size={45} style={{ marginTop: 3 }} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', width: '80%', alignItems: 'center', marginTop: 3 }}>
                        <Text style={{ fontWeight: "600", fontSize: 18 }}>
                            {agla?.username}    <Text style={{ fontSize: 18 }}>{agla?.typ_user}</Text>
                        </Text>
                    </View>
                </View>
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                    <Avatar.Image source={{ uri: agla?.profile_url || agla?.profile_image }} size={150} />
                    <Text style={{ fontWeight: 'bold', color: 'black', fontSize: 20 }}>
                        {agla?.username}
                    </Text>
                </View>

                <View style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <TouchableOpacity style={{ ...styles.heart }}
                        onPress={() => {
                          
                            agla?.date_experiration == null || moment(agla?.date_experiration) >= moment(new Date()) ?
                                this.calling(
                                    {
                                        number:'+' + countrie?.indicatif?.toString() + agla?.phone_number.toString(),
                                        prompt:false
                                    }
                                )
                                : new Notification().sendNotitfications(agla?.tokens, _.find(this.state.user, { id: this.state.user_id })?.username, 'A essayé de vous joindre, veuillez payer votre abonnement.', dat)
                        }}>
                        <Animatable.View animation="shake" iterationCount={5} direction="alternate">
                            <Entypo name='old-phone' size={20} color={agla?.date_experiration == null || moment(agla?.date_experiration) >= moment(new Date()) ? '' : '#8e8e8e'} />
                        </Animatable.View>
                        <Text style={{ color: agla?.date_experiration == null || moment(agla?.date_experiration) >= moment(new Date()) ? '' : '#8e8e8e', fontSize: 12, fontWeight: '500' }}>Contacter</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ ...styles.heart }}
                        onPress={() => this.save_follow()}>
                        <Animatable.View animation="shake" iterationDelay={1500} iterationCount='infinite'>
                            <Entypo name='bell' color={follow ? 'black' : 'red'} size={20} />
                        </Animatable.View>
                        <Text style={{ fontSize: 12, fontWeight: '500', color: follow ? 'black' : 'red' }}>
                            {follow ? 'Se désabonner' : 'S\'abonner'}</Text>
                    </TouchableOpacity>
                   
                        <TouchableOpacity style={{ ...styles.heart }}
                            onPress={() => { this.generateLink(agla, _.size(houses), _.size(_.filter(followers, { follower: agla?.id }))) }}>
                            <Animatable.View animation="shake" iterationDelay={1500} iterationCount='infinite'>
                                <Entypo name={'share'} color={'red'} size={20} />
                            </Animatable.View>
                            <Text style={{ fontSize: 12, fontWeight: '500', color: 'red' }}>
                                {'Partager'}</Text>
                        </TouchableOpacity>
                  
                </View>
                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 20 }} >À propos</Text>
                <View style={{ marginTop: 10, }}>

                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='database' color={'black'} size={18} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 15 }}>{_.size(houses)}</Text>
                        <Text style={{ marginLeft: 10 }}>Poste(s)</Text>
                    </View>
                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='box' color={'black'} size={18} />
                        <Text style={{ fontWeight: 'bold', marginLeft: 15 }}>{_.size(_.filter(followers, { follower: agla?.id }))}</Text>
                        <Text style={{ marginLeft: 10 }}>Abonné(s)</Text>
                    </View>
                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='man' color={'black'} size={18} />
                        <Text style={{ marginLeft: 15 }}>Membre à Imobbis: {new Date_heure().getDate_publication(agla?.date_joined)}</Text>

                    </View>
                    <View style={{ margin: 7, flexDirection: 'row', alignItems: 'center', }}>
                        <Entypo name='megaphone' color={'black'} size={18} />
                        <Text style={{ marginTop: 8 }}>
                            {agla?.descriptions}
                        </Text>

                    </View>

                </View>


                <View style={{ borderTopColor: '#e6e6e6', borderTopWidth: 2, margin: 6 }} />
                <Text style={{ marginLeft: 5, fontSize: 12, fontWeight: 'bold', fontSize: 20 }} >Les publications</Text>

                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10 }}>
                    {
                        houses?.map(house => {
                            return (
                                <View style={{ width: '48%', margin: 3 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            new Touched().touched_it('house', house?.id, 18)
                                            this.props.navigation.navigate('HouseDetails', { id: house?.id, data: house })
                                        }}
                                    >
                                        <ImageBackground style={{ height: 150, width: '100%', alignItems: 'flex-end' }}
                                            source={{ uri: _.first(house?.image)?.image_url }}>
                                            <View style={{
                                                backgroundColor: 'orange', width: '70%', padding: 3,
                                                borderBottomRightRadius: 8, borderTopLeftRadius: 8, margin: 5
                                            }}>
                                                <Text style={{ fontSize: 14, color: "white", alignSelf: 'center' }}>
                                                    {house.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}
                                                    {' ' + countrie?.monnaie}
                                                    {house?.category?.toLowerCase() == "terrain" ? ' / m²' : house.publish_type == 'vendre' ? '' : house.is_immeuble == true ? ' / jour' : ' / mois'}
                                                </Text>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </View>
            </ScrollView>
          
        )
    }
}

const styles = StyleSheet.create({
    btn: {
        padding: 5, borderColor: '#05375a', borderWidth: 2, borderRadius: 7, height: 35, width: '38%',
        alignItems: 'center', justifyContent: 'center', marginTop: -10, marginLeft: 10
    },
    heart: {
        //textAlign: 'right',
        alignItems: 'center',
        backgroundColor: "#fff6e6", // width: '20%' ,
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84,
        elevation: 5, shadowOpacity: 0.2

    },
})