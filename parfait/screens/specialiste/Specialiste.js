import React from 'react';
import {
    Text,
    TouchableOpacity,
    Modal,
    View,
    StyleSheet,
    Image,
    ScrollView,
    Button,
    TextInput,
    Dimensions
} from 'react-native';
import axios from 'axios';
import _, { uniq } from 'underscore';
import { Avatar, SearchBar, Badge } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-async-storage/async-storage';
import call from 'react-native-phone-call';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/FontAwesome'
var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Specialiste extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            specialistes: [],
            infosspecialistes: [],
            house: [], Services: [],
            search: '',
            userssnew: [],
            querry: '',
            users: [],
            mes_likes: [], likes: [], comment: [], isLoading1: false, userid: null, follow: false, followers: [],
        }
    }
    //affiche le modal


    componentDidMount() {
        this.displayData()
        axios.get("https://imobbis.pythonanywhere.com/user/service").then(res => {
            this.setState({ Services: res.data });
        });

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
            this.setState({ users: res.data, userssnew: res.data });
        });
        axios.get("https://imobbis.pythonanywhere.com/wen/specialist_image").then(res => {
            this.setState({ specialistes: res.data });
        });
        axios.get(`https://imobbis.pythonanywhere.com/wen/like_user`)
            .then(res => {
                let mes_likes = []
                _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.user_liked));
                _.uniq(mes_likes)
                this.setState({ likes: res.data, mes_likes: mes_likes })
            });
        axios.get("https://imobbis.pythonanywhere.com/user/specialist").then(res => {
            this.setState({ infosspecialistes: res.data, });
        });
        axios.get('https://imobbis.pythonanywhere.com/new/follow')
            .then(res => {
                let foll = _.find(res.data, { user: this.state.user_id }) ? true : false

                this.setState({ followers: res.data, follow: foll })

            });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.likes !== this.state.likes) {
            axios.get(`https://imobbis.pythonanywhere.com/wen/like_user`)
                .then(res => {
                    this.setState({ likes: res.data })
                });
        }

        if (prevState.specialistes !== this.state.specialistes) {
            axios.get("https://imobbis.pythonanywhere.com/wen/specialist_image").then(res => {
                this.setState({ specialistes: res.data });
            });
        }

        if (prevState.Services !== this.state.Services) {
            axios.get("https://imobbis.pythonanywhere.com/user/service").then(res => {
                this.setState({ Services: res.data });
            });
        }

        if (prevState.users !== this.state.users) {
            axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
                this.setState({ users: res.data, userssnew: res.data });
            });
        }
    }



    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user);
            this.setState({
                userid: json_id
            })
        } catch { error => alert(error) };

    }

    save_like = (likeuser, user) => {
        let mes_likes = this.state.mes_likes;

        let exist = _.find(this.state.likes, { user: user, user_liked: likeuser }) ? true : false;

        // this.state.mes_likes.map((li, e) => { e == i ? (mes_likes[i] == true ? mes_likes[i] = false : mes_likes[i] = true, console.log("li " + mes_likes[i])) : null });
        exist == true ? mes_likes = _.filter(this.state.likes, (value) => value.user_liked != likeuser)
            : mes_likes.push(likeuser)

        this.setState({ mes_likes: mes_likes });
        let item = exist == false ? {
            user_liked: Number(likeuser),
            user: user,
        }
            : { id: _.find(this.state.likes, { user: user, user_liked: likeuser })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/wen/like_user`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/wen/like_user/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/wen/like_user`)
                    .then(res => {
                        let mes_likes = []
                        _.filter(res.data, { user: this.state.userid }).map(id_ => mes_likes.push(id_.user_liked));
                        uniq(mes_likes)
                        this.setState({ likes: res.data, mes_likes: mes_likes })
                    }).catch(error => {
                        console.log(+ error)
                    });
                console.log("le post a été bien effectué !")
            }).catch(error => {
                console.log("error " + error)
            })


    }
    save_follow = () => {
        const { follow, users } = this.state;
        this.setState({ follow: !follow });
        let spec = _.find(users, {})
        let exist = _.find(this.state.followers, { user: this.state.userid, follower: users?.id }) ? true : false;
        let item = exist == false ? {
            user: Number(this.state.userid),
            follower: Number(users?.id),
        } : { id: _.find(this.state.followers, { user: this.state.userid, follower: users?.id })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/follow`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/follow/${item.id}`)
        )
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/follow`)
                    .then(res => {
                        this.setState({ followers: res.data })
                    });

            }).catch(error => {
                console.log("error " + error)
            })

    }

    updateSearch = (search) => {
        this.setState({ search });
    };


    calling = (args) =>{
        call(args).catch(console.error)
  }
    filter = (val) => {
        const { querry, userssnew } = this.state;
        this.setState({ querry: val });
        if (querry == "") {
            this.setState({ users: userssnew });
        } else {
            var content = userssnew;
            var low = querry.toLowerCase();
            content = content.filter((com) => com.username.toLowerCase().match(low));
            //content = content.filter((com) => com.name.toLowerCase().match(low));
            this.setState({ users: content });
        }
    };

    render() {
        var dat = []
        var cont = []
        const { specialistes, infosspecialistes, Services, search, users, userid } = this.state;


        specialistes.map((pers) => {
            dat.push(pers.specialist)
        })
        dat = _.uniq(dat)
        infosspecialistes.map((pers) => {
            pers.service.map((pe) => {
                cont.push(pe)
            })
        })
        let ser

        return (
            <ScrollView>

                <TextInput
                    style={{
                        padding: 10,
                        borderColor: "#dc7e27",
                        margin: 6,
                        borderWidth: 1
                    }}
                    placeholder="Rechercher un specialiste..."
                    onChangeText={(val) => {
                        this.filter(val);
                    }}
                ></TextInput>
                <View >
                    {
                        users.map(specialiste_ =>
                            specialiste_.is_specialiste ? (
                                ser = "",
                                <View style={{
                                    marginTop: 20,
                                    backgroundColor: 'white',
                                    shadowColor: "orange",
                                    shadowOffset: {
                                        width: 6,
                                        height: 6,
                                    }, margin: 6,
                                    shadowOpacity: 0.2,
                                    shadowRadius: 4,
                                    padding: 6, elevation: 9,
                                    borderRadius: 20,
                                }}>
                                    <View style={{ flexDirection: 'row', shadowColor: '#000', marginVertical: 20 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Infospecialiste', { data: specialiste_ })}>
                                                <View style={{ flexDirection: 'row', }}>
                                                    <Image
                                                        style={{ height: 50, width: 50, marginLeft: 3 }}
                                                        source={{ uri: specialiste_.profile_image }} />
                                                    <Text style={{ marginLeft: '5%' }}
                                                        numberOfLines={1}>{specialiste_.username}
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>

                                    <View style={{}}>
                                        {
                                            specialiste_.service.map((pers) => {
                                                return (
                                                    <View style={{}}>
                                                        {
                                                            _.filter(Services, { id: pers })?.map(serv_ => {
                                                                return (
                                                                    ser = serv_.name + ", " + ser,
                                                                    <View style={{}}>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>

                                    <Text style={{ fontSize: 14, fontWeight: 'bold', marginVertical: '3%' }} >
                                        {ser}
                                    </Text>


                                    <ScrollView horizontal={true}>
                                        {
                                            _.filter(specialistes, { specialist: specialiste_.id })?.map(img_ =>

                                                <View>
                                                    <FastImage
                                                        style={{
                                                            width: width_, height: height_ * 0.5,
                                                            borderColor: "white", borderWidth: 5,
                                                        }}
                                                        source={{
                                                            uri: img_.image,
                                                            headers: { Authorization: 'someAuthToken' },
                                                            //priority: FastImage.priority.normal,
                                                        }}
                                                       
                                                    />
                                                </View>
                                            )
                                        }
                                    </ScrollView>
                                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                        <TouchableOpacity style={styles.heart}
                                            onPress={() => { this.save_like(specialiste_.id, this.state.userid) }}>
                                            <Text style={{
                                                fontSize: 10, left: -0, top: 6, fontWeight: 'bold',
                                                borderRadius: 100, color: "black"
                                            }}>{_.filter(this.state.likes, { user_liked: specialiste_.id }).length}</Text>
                                            <Icon style={{ top: 6, color: "#7c3325" }} size={25} name={this.state.mes_likes.includes(specialiste_.id) == false ? 'heart-o' : 'heart'} />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={styles.heart} onPress={() => this.calling({number:'+' + _.find(countrys, { id:specialiste_?.countrie })?.indicatif?.toString() + specialiste_.phone_number.toString(), prompt:false})
                                        }>
                                            <Feather name='phone-call' size={25} style={{ marginLeft: '7%', marginTop: '5%', }} />
                                        </TouchableOpacity>
                                        {
                                            specialiste_?.id != userid ?
                                                <TouchableOpacity style={{ height: 32, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: "#7c3325", width: 70, marginLeft: '25%', marginTop: '3%' }}
                                                    onPress={() => {
                                                        this.props.navigation.navigate(
                                                            'MessagesIni', {
                                                             user: specialiste_
                                                        })
                                                    }}>
                                                    <Text style={{ color: 'white', }}>Ecrire</Text>
                                                </TouchableOpacity>
                                            : <View></View>
                                        }
                                    </View>
                                </View>
                            ) : null
                        )
                    }
                </View>

            </ScrollView>

        );
    }
}



const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "black",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        //padding: 6,
        elevation: 9,
        marginVertical: 6,
        //margin: 5
    },
    heart: {
        textAlign: 'right',
        backgroundColor: "#fff6e6",
        shadowColor: "black",
        borderRadius: 25,
        padding: 6, alignSelf: 'flex-end',
        shadowRadius: 3.84, margin: 5,
        elevation: 5, shadowOpacity: 0.2, flexDirection: 'row', justifyContent: 'center'

    },
    button1: {
        justifyContent: 'center', flexDirection: 'row', height: 28,
        borderRadius: 8, paddingTop: 3, marginTop: 10
    }
})
