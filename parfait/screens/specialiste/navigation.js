import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Specialiste from './Specialiste';
import Infosspecialiste from './Infospecialiste';
import Service from './Service';
import PostI from '../normalUser/PostI';
import MessagesIni  from '../normalUser/Messages_in';

const Stack = createStackNavigator();

export default class SpecialisteNavigation extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
        <Stack.Navigator>
            <Stack.Screen name='Specialiste' component={Specialiste} options={{headerShown: false}}/>
            <Stack.Screen name='Infospecialiste' component={Infosspecialiste} options={{headerShown: false}}/>
            <Stack.Screen name='Service' component={Service} options={{headerShown: false}}/>
            <Stack.Screen name='MessagesIni' component={MessagesIni} options={{headerShown: false}}/>
            <Stack.Screen name='PostI' component={PostI} options={{headerShown: false}}/>
        </Stack.Navigator>
        )
    }
    
}
