import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, Modal, Dimensions, TextInput, TouchableOpacity, Platform, Image, ScrollView } from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import Form from 'react-native-form'
import { Divider, Badge } from 'react-native-elements'
import _ from 'underscore'
//import PhotoUpload from 'react-native-photo-upload'
import Icon from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RadioButton } from 'react-native-paper';
import axios from 'axios'
import ImagePicker from 'react-native-image-crop-picker';
var height_ = Math.round(Dimensions.get('window').height);

export default class Connexion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      withFlag: true,
      enterCode: false,
      codeconfirm: '',
      phone: '',
      modalVisible2: false,
      country: {
        cca2: 'CM',
        callingCode: '237'
      },
      users: [],
      countries: [],
      districts: [],
      cities: [],
      provinces: [],
      neighbours: [],
      location: '',
      user: {
        profile_image: "a",
        username: "",
        first_name: "",
        last_name: "",
        gender: "M",
        email: "",
        countrie: null,
        province: null,
        citie: null,
        district: null,
        neighbor: null,
        phone_number: null
      },
      etape: 1,
    }
  }

  componentDidMount() {

    axios.get("https://imobbis.pythonanywhere.com/user/").then(res => {
      this.setState({ users: res.data })
    }).catch(err => console.log(err));

    axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
      this.setState({ countries: res.data.country, districts: res.data.district, 
      cities: res.data.city, neighbours: res.data.neighbor, provinces: res.data.province });
    });
  }

  _changeCountry = (country) => {
    console.log(country);
    this.setState({ country });
    //this.refs.form.refs.textInput.focus();
  }

  handleChange = (name, value) => {
    const user = { ...this.state.user, [name]: value };
    this.setState({ user });
  };

  handleChangeradio = () => {
    let user = this.state.user;
    user.gender == "M" ? user.gender = "F" : user.gender = "M";
    this.setState({ user });
  };

  addlocation(name) {
    let location = this.state.location;
    location = location == "" ? name : location + ", " + name;
    this.setState({ location: location });
  }

  effacer = (name) => {
    const user = { ...this.state.user, [name]: null };
    this.setState({ user });
  }

  verify_user = numberphone => {
    _.find(this.state.users, { phone_number: Number(numberphone) }) ? 
    (this.setState({ enterCode: true }), this.displayData(_.find(this.state.users, 
      { phone_number: Number(numberphone) })?.id)) : 
      (this.setState({ modalVisible2: true }), this.handleChange('phone_number', numberphone));
    
  }

  displayData = async (id) => {
    try {
      await AsyncStorage.setItem(
        'user_data',
        id.toString()
      );
    } catch { error => alert(error) };
  }

  save(item) {
    const formData = new FormData();
    formData.append("username", item.username);
    if (item.profile_image.fileName !== undefined) {
      formData.append("profile_image", {
        uri: item.profile_image.uri,
        type: item.profile_image.type,
        name: item.profile_image.fileName,
        data: item.profile_image.data
      });
    };
    formData.append("first_name", item.first_name);
    formData.append("last_name", item.last_name);
    formData.append("username", item.username);
    formData.append("gender", item.gender);
    formData.append("email", item.email);
    formData.append("password", 'maman');
    formData.append("is_landord", false);
    formData.append("is_agent", false);
    formData.append("date_joined", new Date().toISOString());
    formData.append("is_renter", false);
    formData.append("is_house_agent", false);
    formData.append("countrie", Number(item.countrie));
    formData.append("province", Number(item.province));
    formData.append("citie", Number(item.citie));
    formData.append("district", Number(item.district));
    formData.append("neighbor", Number(item.neighbor));
    formData.append("phone_number", Number(item.phone_number));

    this.state.user.username !== "" && this.state.user.first_name !== "" 
    && this.state.user.last_name !== "" && this.state.user.email !== "" 
    && this.state.user.phone_number !== "" ?
      _.find(this.state.users, {phone_number: item.phone_number}) ? alert('ce numéro a déjà été enregistré dans notre base de données !') :
      axios
        .post(`https://imobbis.pythonanywhere.com/user/`, formData, {
          headers: {
            'content-type': 'multipart/form-data'
          }
        })
        .then(res => {
          
          axios.get("https://imobbis.pythonanywhere.com/user/").then(res => {
            var user_in = _.find(res.data, {phone_number: Number(item.phone_number)})

            user_in?( this.displayData(user_in?.id), this.setState({etape:4})):alert("erreur")
          

                 }).catch(err => console.log(err));



        }).catch(err => {
          err.toString() == 'Error: Network Error' ? alert("Image trop lourde !!!") : alert('err' + err);
          console.log(err)
        }) : alert("veuillez remplir tous les champs")
  }

  render() {
    const { modalVisible2, user } = this.state;
    
    return (
      <ScrollView>
        <View style={{ height: "40%", alignItems: 'center', justifyContent: 'center', shadowOpacity: 2, backgroundColor: '#dc7e27', borderRadius: 5 }}>
          <Image source={require('./images/logo.jpeg')} style={{ width: 180, height: 160, marginTop: '10%', borderRadius: 50 }} />
        </View >

        <View style={{ marginLeft: '35%', marginTop: '8%' }}><Text style={{ fontSize: 25, fontFamily: '' }}>Log-In</Text></View>
        {
          !this.state.enterCode ?
            <View style={{ marginLeft: '5%' }}>
              <Text style={{ fontWeight: 'bold', fontSize: 20, }}>Enter your mobile phone</Text>
              <Text style={{ color: 'grey', marginTop: '2%' }}>Please enter your country code</Text>
              <Text style={{ color: 'grey' }}>enter your mobile number </Text>
            </View> : null
        }

        <View>
          <Form  style={{ margin: 20 }}>
            {//ref={'form'}
              !this.state.enterCode ?
                <CountryPicker
                  //ref={'countryPicker'}
                  withFlag={true}
                  withEmoji={true}
                  withFilter={true}
                  withCountryNameButton={true}
                  withAlphaFilter={true}
                  withCallingCode={true}
                  closeable
                  style={{ alignItems: 'center', justifyContent: 'center', borderWidth: 2, borderColor: 'red', color: "yellow" }}
                  onSelect={this._changeCountry}
                  translation='eng'
                /> :
                <View style={{ marginLeft: '5%' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Saisir un code de confirmation</Text>
                </View>
            }

            <View style={{ flexDirection: 'row' }}>
              {
                !this.state.enterCode ?
                  <View style={{ marginTop: 8 }}>
                    <Text >+{this.state.country.callingCode}</Text>
                    <Divider style={{ backgroundColor: "black", marginLeft: '10%', height: 1, width: '290%' }} />
                  </View> : null
              }

              <View style={{ marginLeft: !this.state.enterCode ? '30%' : '10%' }}>
                <TextInput
                  style={{ width: "190%", fontSize: 18 }}
                  //ref={'textInput'}
                  name={this.state.enterCode ? 'code' : 'phoneNumber'}
                  type={'textInput'}
                  underlineColorAndroid={'transparent'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  onChangeText={(val) => !this.state.enterCode ? this.setState({ phone: val }) : this.setState({ codeconfirm: val })}
                  placeholder={this.state.enterCode ? '_ _ _ _ _ _' : 'Phone Number'}
                  keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
                  returnKeyType='go'
                  //autoFocus
                  placeholderTextColor="grey"
                  selectionColor="#dc7e27"
                  maxLength={this.state.enterCode ? 6 : 20}
                  onSubmitEditing={this._getSubmitAction}
                />
              </View>
            </View>
          </Form>

          {
            !this.state.enterCode ?
              <TouchableOpacity
                style={{ backgroundColor: '#7c3325', margin: '5%', borderRadius: 5, height: 70, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => { this.state.phone.length == 9 ? this.verify_user(this.state.phone) : alert("Entrer un numero à 9 chiffres") }} >
                <Text style={{ fontSize: 20, color: '#fff' }}  >Identification</Text>
              </TouchableOpacity> : null
          }

          {
            this.state.codeconfirm.length == 6 ?
              <TouchableOpacity
                style={{ backgroundColor: '#7c3325', marginTop: '5%', marginLeft: height_ * 0.3, marginRight: "5%", borderRadius: 5, height: 70, justifyContent: 'center', alignItems: 'center' }}
                onPress={() => this.props.navigation.navigate('home')} >
                <Text style={{ fontSize: 20, color: '#fff' }}  >Verification</Text>
              </TouchableOpacity> : null
          }


        </View>

        <View style={{ height: 20 }}></View>

        <Modal animationType="slide"
          transparent={true}
          visible={modalVisible2}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setState({ modalVisible2: !modalVisible2 });
          }}>

          <View style={{ ...styles.centeredView, marginTop: 53 }}>
            <View style={styles.modalView}>
              <View style={{ width: '100%', flexDirection: 'row' }} >
                <TouchableOpacity
                  style={{ width: '10%' }}
                  onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                >
                  <Icon name="times" size={25} style={{ marginLeft: 2 }} />
                </TouchableOpacity>
                <View style={{ marginLeft: 20 }}>
                  <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold", color: '#7c3325' }}>Creer un compte :</Text>
                </View>

              </View>
              <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />

              <ScrollView>
                {
                  this.state.etape == 1 ?
                    <>
                      <View style={styles.row}>
                        <View style={{ width: "25%" }}>
                          <Badge value="1" status="primary" />
                        </View>
                        <View style={{ width: "25%" }}>
                          <Badge value="2" status="warning" />
                        </View>
                        <View style={{ width: "25%" }}>
                          <Badge value="3" status="warning" />
                        </View>
                        <View style={{ width: "25%" }}>
                          <Badge value="4" status="warning" />
                        </View>
                      </View>

                      <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold" }}>Localisation</Text>
                        <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                          <Text style={{ fontSize: 10 }}>Ces informations sont importantes, ils permettrons de gerer facilement votre authentification ...</Text>
                        </View>
                      </View>

                      <View style={{ flexDirection: "row", marginTop: '1%', fontSize: 12 }} >
                        <View style={{ width: '20%', borderBottomColor: user.countrie == null ? '#7c3325' : 'black', borderBottomWidth: user.countrie == null ? 2 : null, paddingBottom: 10 }}>
                          <TouchableOpacity onPress={() => { this.effacer('countrie') }}>
                            <Text style={{ color: user.countrie == null ? '#7c3325' : 'black' }}>Pays</Text>
                          </TouchableOpacity>
                        </View>
                        <View style={{ width: '20%', borderBottomColor: user.countrie !== null && user.province == null ? '#7c3325' : user.province !== null ? 'black' : null, borderBottomWidth: user.countrie !== null && user.province == null ? 2 : null, paddingBottom: 10 }}>
                          <TouchableOpacity onPress={() => { this.effacer('province') }}>
                            <Text style={{ color: user.countrie !== null && user.province == null ? '#7c3325' : user.province !== null ? 'black' : null }}>Province</Text>
                          </TouchableOpacity>
                        </View>
                        <View style={{ width: '20%', borderBottomColor: user.province !== null && user.citie == null ? '#7c3325' : user.citie !== null ? 'black' : null, borderBottomWidth: user.province !== null && user.citie == null ? 2 : null, paddingBottom: 10 }}>
                          <TouchableOpacity onPress={() => { this.effacer('citie') }}>
                            <Text style={{ color: user.province !== null && user.citie == null ? '#7c3325' : user.citie !== null ? 'black' : null }}>Ville</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={{ width: '20%', borderBottomColor: user.citie !== null && user.district == null ? '#7c3325' : user.district !== null ? 'black' : null, borderBottomWidth: user.citie !== null && user.district == null ? 2 : null, paddingBottom: 10 }}>
                          <TouchableOpacity onPress={() => { this.effacer('district') }}>
                            <Text style={{ color: user.citie !== null && user.district == null ? '#7c3325' : user.district !== null ? 'black' : null }}>District</Text>
                          </TouchableOpacity>
                        </View>
                        <View style={{ width: '20%', borderBottomColor: user.district !== null ? '#7c3325' : user.neighbor !== null ? 'black' : null, borderBottomWidth: user.district !== null ? 2 : null, paddingBottom: 10 }}>
                          <TouchableOpacity onPress={() => { this.effacer('neighbor') }}>
                            <Text style={{ color: user.district !== null ? '#7c3325' : user.neighbor !== null ? 'black' : null }}>Quartier</Text>
                          </TouchableOpacity>
                        </View>
                      </View>

                      {
                        user.countrie == null ?
                          <ScrollView >
                            {
                              this.state.countries.map((item, i) => (
                                <TouchableOpacity key={item.name} onPress={() => { this.handleChange('countrie', item.id), this.addlocation(item.name) }}>
                                  <Text style={{ color: 'orangered', textAlign: "center", fontSize: 12, marginBottom: 10 }}> {item.name} </Text>
                                </TouchableOpacity>))
                            }
                          </ScrollView>
                          :
                          user.province == null ?
                            this.state.countries.map(count =>
                              (count.id === Number(user.countrie)) ?
                                <ScrollView key={count}>
                                  {this.state.provinces.map(prov =>
                                    count.provinces.map((province, u) =>
                                      (province === prov.id) ? (
                                        <TouchableOpacity key={prov.name} onPress={() => { this.handleChange('province', prov.id), this.addlocation(prov.name) }}>
                                          <Text style={{ color: 'orangered', textAlign: "center", fontSize: 12, marginBottom: 10 }}> {prov.name} </Text>
                                        </TouchableOpacity>) : null
                                    )
                                  )}
                                </ScrollView> : null
                            ) :
                            user.citie == null ?
                              this.state.provinces.map(prov =>
                                (prov.id === Number(user.province)) ?
                                  <ScrollView key={prov}>
                                    {
                                      this.state.cities.map(cit =>
                                        prov.cities.map((idcit, e) =>
                                          (idcit === cit.id) ? (
                                            <TouchableOpacity key={cit.name} onPress={() => { this.handleChange('citie', cit.id), this.addlocation(cit.name) }}>
                                              <Text style={{ color: 'orangered', textAlign: "center", fontSize: 12, marginBottom: 10 }}> {cit.name} </Text>
                                            </TouchableOpacity>
                                          ) : null
                                        )
                                      )
                                    }
                                  </ScrollView> : null
                              ) :

                              user.district == null ?
                                this.state.cities.map(city =>
                                  (city.id === Number(user.citie)) ?
                                    <ScrollView key={city}>
                                      {
                                        this.state.districts.map(dist => (
                                          city.districts.map((idcit, y) =>
                                            (idcit === dist.id) ? (
                                              <TouchableOpacity key={dist.name} onPress={() => { this.handleChange('district', dist.id), this.addlocation(dist.name) }}>
                                                <Text style={{ color: 'orangered', textAlign: "center", fontSize: 12, marginBottom: 10 }}> {dist.name} </Text>
                                              </TouchableOpacity>
                                            ) : null
                                          ))
                                        )
                                      }
                                    </ScrollView> : null
                                ) :
                                user.neighbor == null ?
                                  this.state.districts.map(di =>
                                    (di.id === Number(user.district)) ?
                                      <ScrollView key={di}>
                                        {
                                          this.state.neighbours.map(neig => (
                                            di.neighbors.map((idcit, ee) =>
                                              (idcit === neig.id) ? (
                                                <TouchableOpacity key={neig.name} onPress={() => { this.handleChange('neighbor', neig.id), this.setState({ isVisible: false }), this.addlocation(neig.name) }}>
                                                  <Text style={{ color: 'orangered', textAlign: "center", fontSize: 12, marginBottom: 10 }}> {neig.name} </Text>
                                                </TouchableOpacity>
                                              ) : null
                                            ))
                                          )
                                        }
                                      </ScrollView> : null
                                  ) :
                                  <>
                                    <View style={{ margin: '5%' }}>
                                      <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold", color: '#7c3325' }}>{this.state.location}</Text>
                                    </View>
                                    <TouchableOpacity
                                      style={{ backgroundColor: '#dc7e27', marginLeft: '50%', marginTop: '15%', justifyContent: 'center', height: 50, width: "50%", borderRadius: 5, alignItems: 'center', flexDirection: "row" }}
                                      onPress={() => this.setState({ etape: 2 })}
                                    >
                                      <Text style={{ fontSize: 14, fontWeight: "bold", color: 'white' }}>Suivant</Text>
                                      <Entypo name='chevron-thin-right' size={25} style={{ color: "white", marginLeft: 10, }} />
                                    </TouchableOpacity>
                                  </>
                      }
                    </> :
                    Number(this.state.etape) == 2 ?
                      <>
                        <View style={styles.row}>
                          <View style={{ width: "25%" }}>
                            <Badge value="1" status="primary" />
                          </View>
                          <View style={{ width: "25%" }}>
                            <Badge value="2" status="primary" />
                          </View>
                          <View style={{ width: "25%" }}>
                            <Badge value="3" status="warning" />
                          </View>
                          <View style={{ width: "25%" }}>
                            <Badge value="4" status="warning" />
                          </View>
                        </View>

                        <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                          <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold" }}>Infos personnelles </Text>
                          <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                            <Text style={{ fontSize: 10 }}>Ces informations sont importantes, ils permettrons de gerer facilement votre authentification ...</Text>
                          </View>
                        </View>

                        <View >
                          <Text style={{ textDecorationLine: 'underline', marginLeft: '5%', textDecorationStyle: 'solid', fontSize: 13 }}>Utilisateur</Text>
                        </View>
                        <TextInput
                          style={
                            styles.touch
                          }
                          value={this.state.user.username}
                          placeholder="Entrez  votre username..."
                          onChangeText={(val) => this.handleChange('username', val)}

                        ></TextInput>

                        <View >
                          <Text style={{ textDecorationLine: 'underline', textDecorationStyle: 'solid', marginLeft: '5%', fontSize: 13 }}>Nom</Text>
                        </View>
                        <TextInput
                          style={
                            styles.touch
                          }
                          value={this.state.user.last_name}
                          placeholder="Entrez  votre  Nom..."
                          onChangeText={(val) => this.handleChange('last_name', val)}
                        ></TextInput>

                        <View >
                          <Text style={{ marginLeft: '5%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid' }}>Prenom</Text>
                        </View>
                        <TextInput
                          style={
                            styles.touch
                          }
                          value={this.state.user.first_name}
                          placeholder="Entrez  votre  Prénom..."
                          onChangeText={(val) => this.handleChange('first_name', val)}
                        ></TextInput>

                        <View >
                          <Text style={{ marginLeft: '5%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid' }}>Email</Text>
                        </View>
                        <TextInput
                          style={
                            styles.touch
                          }
                          value={this.state.user.email}
                          placeholder="Entrez  votre adresse électronique..."
                          onChangeText={(val) => this.handleChange('email', val)}
                        ></TextInput>

                        <View >
                          <Text style={{ marginLeft: '5%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid' }}>Phone</Text>
                        </View>
                        <TextInput
                          style={
                            styles.touch
                          }
                          value={user.phone_number}
                          placeholder="numero de téléphone ... "
                          keyboardType="numeric"
                          onChangeText={val => this.handleChange('phone_number', val)}
                        />

                        <View >
                          <Text style={{ marginLeft: '5%', fontSize: 13, textDecorationLine: 'underline', textDecorationStyle: 'solid' }}>Genre</Text>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                          <View style={{ flexDirection: "row", marginLeft: '5%' }}>
                            <Text style={{ fontWeight: "bold" }} >
                              M
                            </Text>
                            <RadioButton
                              value="true"
                              status={user.gender === "M" ? 'checked' : 'unchecked'}
                              onPress={this.handleChangeradio}
                            />
                          </View>
                          <View style={{ flexDirection: "row", marginLeft: '10%' }}>
                            <Text style={{ fontWeight: "bold" }}>
                              F
                            </Text>
                            <RadioButton
                              value="false"
                              status={user.gender === "F" ? 'checked' : 'unchecked'}
                              onPress={this.handleChangeradio}
                            />
                          </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                          <TouchableOpacity
                            style={{ backgroundColor: 'white', borderColor: '#dc7e27', borderWidth: 2, marginTop: '15%', height: 50, width: "50%", alignItems: 'center', justifyContent: 'center', flexDirection: "row" }}
                            onPress={() => this.setState({ etape: 1 })}
                          >
                            <Entypo name='chevron-thin-left' size={25} style={{ color: "#dc7e27" }} />
                            <Text style={{ fontSize: 14, fontWeight: "bold", color: '#dc7e27', marginLeft: 10, }}>Retour</Text>
                          </TouchableOpacity>

                          {
                            this.state.user.username !== "" && this.state.user.first_name !== "" && this.state.user.last_name !== "" && this.state.user.email !== "" ?
                              <TouchableOpacity
                                style={{ backgroundColor: '#dc7e27', marginTop: '15%', height: 50, width: "50%", alignItems: 'center', justifyContent: 'center', flexDirection: "row" }}
                                onPress={() => this.setState({ etape: 3 })}
                              >
                                <Text style={{ fontSize: 14, fontWeight: "bold", color: 'white' }}>Suivant</Text>
                                <Entypo name='chevron-thin-right' size={25} style={{ color: "white", marginLeft: 10, }} />
                              </TouchableOpacity> : null
                          }
                        </View>

                      </> :
                      this.state.etape == 3 ?
                        <>
                          <View style={styles.row}>
                            <View style={{ width: "25%" }}>
                              <Badge value="1" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="2" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="3" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="4" status="warning" />
                            </View>
                          </View>

                          <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                            <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold" }}>Votre profil</Text>
                            <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                              <Text style={{ fontSize: 10 }}>Ces informations sont importantes, ils permettrons de gerer facilement votre authentification ...</Text>
                            </View>
                          </View>

                          <PhotoUpload
                            onResponse={(avatar) => {
                              if (avatar.fileName !== undefined) {
                                //console.log('Image base64 string: ', avatar);
                                this.handleChange('profile_image', avatar)
                              }
                            }}
                          >
                            <Image
                              style={{
                                paddingVertical: 30,
                                width: 150,
                                height: 150,
                                borderRadius: 75,
                                backgroundColor: '#dc7e27'
                              }}
                              resizeMode='cover'
                              source={{ uri: user.profile_image ? user.profile_image.uri : user.profile_image }}
                            />
                          </PhotoUpload>

                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                            <TouchableOpacity
                              style={{ backgroundColor: 'white', borderColor: '#dc7e27', borderWidth: 2, marginTop: '15%', height: 50, width: "50%", alignItems: 'center', justifyContent: 'center', flexDirection: "row" }}
                              onPress={() => this.setState({ etape: 2 })}
                            >
                              <Entypo name='chevron-thin-left' size={25} style={{ color: "#dc7e27" }} />
                              <Text style={{ fontSize: 14, fontWeight: "bold", color: '#dc7e27', marginLeft: 10, }}>Retour</Text>
                            </TouchableOpacity>
                            {
                              this.state.user.profile_image !== "" ?
                                <TouchableOpacity
                                  style={{ backgroundColor: '#dc7e27', marginTop: '15%', height: 50, width: "50%", alignItems: 'center', justifyContent: 'center', flexDirection: "row" }}
                                  onPress={() => this.save(this.state.user)
                                  }
                                >
                                  <Text style={{ fontSize: 14, fontWeight: "bold", color: 'white' }}>Enregistrer</Text>
                                  <Entypo name='chevron-thin-right' size={25} style={{ color: "white", marginLeft: 10, }} />
                                </TouchableOpacity> : null
                            }
                          </View>
                        </> :
                        <>
                          <View style={styles.row}>
                            <View style={{ width: "25%" }}>
                              <Badge value="1" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="2" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="3" status="primary" />
                            </View>
                            <View style={{ width: "25%" }}>
                              <Badge value="4" status="primary" />
                            </View>
                          </View>

                          <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                            <View style={{ height: "25%", alignItems: 'center', justifyContent: 'center', marginBottom: '2%', shadowOpacity: 2, backgroundColor: 'white', borderRadius: 5 }}>
                              <Image source={require('./images/logo.jpeg')} style={{ width: 180, height: 160, marginTop: '10%', borderRadius: 50 }} />
                            </View >
                            <Text style={{ fontSize: 18, marginTop: '3%', color: '#7c3325', fontWeight: "bold" }}>Bienvenue <Text style={{ fontSize: 16, color: 'black', fontWeight: "bold" }}>{user.gender == 'M' ? 'M' : "Mme"} {user.first_name} {user.last_name}  </Text> </Text>

                            <Text style={{ fontSize: 14, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325', marginTop: '5%', marginBottom: '2%', fontWeight: "bold" }}>Qui sommes nous ?  </Text>
                            <Text style={{ fontSize: 12 }}> <Text style={{ fontSize: 14, color: 'orangered', fontWeight: "bold" }}> IMOBBIS </Text> est le tout premier Reseau Social Immobilier au monde. Nous sommes une entreprise dont l'objectif est de valoriser l'immobilier Africain et résoudre le problème de logement en Afrique. </Text>

                            <Text style={{ fontSize: 14, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325', marginTop: '5%', marginBottom: '2%', fontWeight: "bold" }}>Que faisons nous ?  </Text>
                            <Text style={{ fontSize: 12, fontWeight: "bold" }}> Avec <Text style={{ fontSize: 14, color: 'orangered', fontWeight: "bold" }}> IMOBBIS </Text>, vous pourrez</Text>
                            <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text>, Trouver facilement un logement en une recherche</Text>
                            <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text>, Valoriser vos biens immobiliers et avoir plus de visibilité( si vous êtes bailleur, agent imobilier ou une agence immobiliere)</Text>
                            <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text>, Manager vos biens (gestion locative, gérer jusqu'à 100 logements et locataires, générer les contrats de bailles de façon automatique)</Text>
                            <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text>, Trouvez les spécialistes de l'immobilier (ingénieur de Génie Civil, Décorateur d'interieur, peintre, Carreleur, Plombier ...)</Text>
                          </View>

                          <TouchableOpacity
                            style={{ backgroundColor: '#7c3325', marginTop: '2%', marginLeft: height_ * 0.3, marginRight: "5%", borderRadius: 40, height: 50, justifyContent: 'center', alignItems: 'center' }}
                            onPress={() => this.props.navigation.navigate('home')} >
                            <Text style={{ fontSize: 20, color: '#fff' }}  >Allez à l'Accueil</Text>
                          </TouchableOpacity>

                          <View style={{ height: 5 }}></View>
                        </>
                }
              </ScrollView>
            </View>
          </View>
        </Modal>

      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  modalView: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 15,
    alignItems: "flex-start",
    shadowColor: "#000000",
    width: '95%',
    height: '95%',
    shadowOffset: {
      width: 30,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 15
  },
  row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
  buttonstyle: {
    backgroundColor: 'white',
    height: 30,
    justifyContent: 'center'
  },
  touch: {
    color: '#7c3325', marginLeft: '5%',

  },
})