import React from 'react';
import {
    Text,
    TouchableOpacity,
    Modal,
    View,
    StyleSheet,
    Image,
    ScrollView,
    Button,
} from 'react-native';
import axios from 'axios';
import _ from 'underscore';
import { Avatar, Divider } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import Fontisto from "react-native-vector-icons/Fontisto";
import Entypo from "react-native-vector-icons/Entypo";
import Service from './Service';
import AsyncStorage from '@react-native-async-storage/async-storage'




export default class Infosspecialiste extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            land: [],
            user: [],
            house: [],
            cities: [],
            category: [],
            neighbours: [],
            infosspecialistes: [],
            Services: [],
            specialistes: [],
            followers: [], user_id: null, follow: false,
        }
    }

    componentDidMount() {
        this.displayData()
        axios.get("https://imobbis.pythonanywhere.com/user/service").then(res => {
            this.setState({ Services: res.data });
        });
        axios.get("https://imobbis.pythonanywhere.com/house/cat").then(res => {
            this.setState({ category: res.data });
        });
        axios.get("https://imobbis.pythonanywhere.com/wen/specialist_image").then(res => {
            this.setState({ specialistes: res.data });
        });
        axios.get('http://imobbis.pythonanywhere.com/new/follow')
            .then(res => {
                let foll = _.find(res.data, { user: this.state.user_id }) ? true : false

                this.setState({ followers: res.data, follow: foll })

            });

    }
    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.followers !== this.state.followers) {
            axios.get(`https://imobbis.pythonanywhere.com/new/follow`)
                .then(res => {
                    this.setState({ followers: res.data })
                });
        }


    }
    save_follow = () => {
        const { follow } = this.state;
        this.setState({ follow: !follow });
        const { data } = this.props.route.params;
        let exist = _.find(this.state.followers, { user: this.state.user_id, follower: data?.id }) ? true : false;
        let item = exist == false ? {
            user: Number(this.state.user_id),
            follower: Number(data?.id),
        } : { id: _.find(this.state.followers, { user: this.state.user_id, follower: data?.id })?.id };

        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/follow`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/follow/${item.id}`)
        )
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/follow`)
                    .then(res => {
                        this.setState({ followers: res.data })
                    });

            }).catch(error => {
                console.log("error " + error)
            })

    }
    displayData = async () => {
        try {
            let user = await AsyncStorage.getItem('user_data');
            let json_id = JSON.parse(user)

            this.setState({ user_id: json_id })
        } catch { error => alert(error) };
    }

    render() {
        const { data } = this.props.route.params;
        const { house, cities, land, category, neighbours, infosspecialistes, Services, specialistes } = this.state;

        return (




            <ScrollView>
                <View style={{ backgroundColor: '#dc7e27', height: 300, justifyContent: 'center', alignItems: 'center', borderBottomRightRadius: 80 }}>
                    <Text style={{ fontSize: 20, color: 'white', marginVertical: 10 }}>{data.last_name.toUpperCase()} {data.first_name.toUpperCase()}</Text>
                    <Avatar source={{ uri: data.profile_image }} size={90} rounded />
                    <View style={{ flexDirection: "row", marginTop: '2%' }}>
                        <Icon name='phone' color='white' size={20} style={{ marginLeft: '4%' }} />
                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'white' }}>{data.phone_number}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <Fontisto name='email' size={20} style={{ marginLeft: '6%' }} color='white' />
                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'white' }}>{data.email} </Text>
                    </View>
                </View>
                <View>
                    <View style={{ flexDirection: 'row', marginLeft: 100 }}>

                        <TouchableOpacity style={{ ...styles.btn }}
                            onPress={() => this.save_follow()}>
                            <Text style={{ fontSize: 18, fontWeight: '500', color: _.find(this.state.followers, { follower: data?.id, user: this.state.user_id }) ? 'red' : 'black' }}>
                                {_.find(this.state.followers, { follower: data?.id, user: this.state.user_id }) ? 'Abonné' : 'S\'abonner'}</Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection:'row',justifyContent:'center',alignItems:'center',marginLeft:'3%'}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 25 }}>{_.size(_.filter(this.state.followers, { follower: data?.id }))}</Text>
                            <Entypo name='bell' size={25} style={{ marginLeft:'3%'}} color='#7c3325' />
                        </View>
                    </View>
                    <View >
                        <Text style={{ marginLeft: '3%', marginVertical: '3%', fontSize: 20, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325' }}>Description : </Text>
                    </View>
                    <Text style={{ fontSize: 17, marginLeft: 20, fontWeight: 'bold' }}>{data.description_specialiste}</Text>
                    <View >
                        <Text style={{ marginLeft: '3%', marginVertical: '3%', fontSize: 20, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325' }}>Services: </Text>
                    </View>
                    <View style={{ fontSize: 17, marginLeft: 20, fontWeight: 'bold' }}>
                        {
                            data.service.map((pers) => {
                                return (
                                    <View>
                                        {
                                            _.filter(Services, { id: pers })?.map(serv_ => {
                                                return (
                                                    <View>
                                                        <Text style={{ fontSize: 17, marginLeft: 10, fontWeight: 'bold' }} >{serv_.name}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                    </View>
                    <View>
                        <View >
                            <Text style={{ marginLeft: '3%', marginVertical: '3%', fontSize: 20, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325' }}>Realisations : </Text>
                        </View>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            {
                                _.filter(specialistes, { specialist: data.id })?.map(img_ =>

                                    <View>
                                        <Image style={{ width: 150, height: 150, borderRadius: 6, }} source={{ uri: img_.image }} />
                                    </View>

                                )
                            }
                        </View>
                    </View>


                </View>

            </ScrollView>



        );
    }
}



const styles = StyleSheet.create({
    btn: {
        padding: 5, borderColor: '#05375a', borderWidth: 2, borderRadius: 10, height: 45, width: '50%',
        alignItems: 'center', justifyContent: 'center', marginTop: 10, marginLeft: 10,
    }
})