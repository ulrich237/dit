import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, Dimensions} from 'react-native'
import { Avatar } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import _ from 'underscore'
import axios from 'axios'

var width_ = Math.round(Dimensions.get('window').width)

export default class AgentRenters extends Component{
    constructor(props){
        super(props)
        this.state = {
            renters: [], users: [], landlords: [], houses: [] 
        }
    }
    componentDidMount(){

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
        .then(response => this.setState({landlords: response.data}),)
        .catch(error => this.setState({ error, isLoading: false })); 

        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
        .then(response => this.setState({users: response.data}),)
        .catch(error => this.setState({ error, isLoading: false })); 

        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
        .then(response => this.setState({renters: response.data}),)
        .catch(error => this.setState({ error, isLoading: false })); 

    }

    getDaysFromDays = (date) => {
        var today = new Date()
        var end_date = new Date(date.substring(0, 9).replace('-', '/').replace('-', '/'))
        var current_date = new Date(today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate());
        var time_left = end_date.getTime() - current_date.getTime();
        var days_left = time_left / (1000 * 3600 * 24);
        return days_left
    }

    render(){
        const {users} = this.state
        const {houses, renters_} = this.props.route.params

        return(
            <View style={{backgroundColor: 'white'}}>
                <View style={{flexDirection: 'row',}}>
                    <TouchableOpacity style={{marginLeft: -10, width: '14%'}}  onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before'  size={45} style={{marginTop: -5}}/>
                    </TouchableOpacity>
                    <View style={{width: '80%', alignItems: 'center', marginTop: 3}}>
                        <Text style={{ fontWeight: "600", fontSize: 25 }}>
                            Mes locataires
                        </Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                        {
                                renters_.map(renter => {return(
                                    <View style={{}}>
                                    {   
                                        user = _.find(users, {id: renter?.user}),
                                        house = _.find(houses, {id: renter.current_house}),
                                        <View style={{...styles.row3}}>
                                                <Avatar.Image 
                                                    source={{uri: user?.profile_image}}
                                                    size={70}
                                                />
                                                <View style={{ marginLeft: 6}}>
                                                    <View >
                                                        <Text style={{fontWeight: 'bold', fontSize: 20}}>{user?.last_name || user?.username + ' ' + user?.first_name}</Text>
                                                        <Text style={{fontSize: 13, color: 'orange', fontWeight: 'bold' }}>
                                                            {/*this.getDaysFromDays(renter?.cr_hs_st_dt)*/} jours restants
                                                        </Text>
                                                    </View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <TouchableOpacity style={{ width: 100, borderWidth: 1.5, width: '40%', padding: 1,
                                                            justifyContent: 'center', alignItems: 'center', borderRadius: 5, marginTop: 4}}>
                                                            <Text style={{fontSize: 14, fontWeight: 'bold'}}>Details</Text>
                                                        </TouchableOpacity>
                                                        <TouchableOpacity style={{...styles.bt1, marginLeft: 10}}>
                                                            <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Ecrire</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{marginTop: 6}}>
                                                    <Text style={{color: '#664200', fontWeight: 'bold', fontSize: 13}} numberOfLines={1}>{house?.name}</Text>
                                                    <View style={{flexDirection: 'row', borderTopWidth: 2, borderTopColor: '#e6e6e6',
                                                                    paddingTop: 5, width: '90%', marginTop: 5}}>
                                                        <Image source={{uri: house?.photo_1}}
                                                        style={{width: '30%', height: 60, marginTop: 6, borderRadius: 7}}
                                                        />
                                                        <View style={{marginTop: 5, marginLeft: 3}}>
                                                            <Text style={{color: 'grey', fontWeight: 'bold'}}> {house?.nb_room} chambre(s)</Text>
                                                            <Text style={{color: 'grey', fontWeight: 'bold'}}> {house?.nb_parlour} salon(s)</Text>
                                                            <Text style={{color: 'grey', fontWeight: 'bold'}}> {house?.nb_toilet} douche(s)</Text>
                                                        </View>
                                                    </View>
                                            </View>
                                                </View>
                                            
                                        </View>
                                    }
                                </View> 
                                )})
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
    
}


const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        }, width: width_*0.97, margin: 6,
        shadowOpacity: 0.2,
        shadowRadius: 4, flexDirection: 'row',
        padding: 6,elevation: 9,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: '46%',
        justifyContent: 'center',alignItems: 'center',
        height: 30, borderRadius: 5, marginTop: 4, marginLeft: 3
    }
})