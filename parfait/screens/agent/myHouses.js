import React, {Component} from 'react'
import {Text, View, StyleSheet, Image, Alert,
    TouchableOpacity, ScrollView} from 'react-native'
import { Avatar } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import _ from 'underscore'
import axios from 'axios'
import Toast from 'react-native-simple-toast';

var user_id = 2

export default class AgentHouses extends Component{
    constructor(props){
        super(props)
        this.state = {
            renters: [], users: []
        }
    }
    componentDidMount(){
        axios.get(`https://imobbis.pythonanywhere.com/wen/locataires`,)
        .then(response => this.setState({renters: response.data}),)
        .catch(error => this.setState({ error, isLoading: false })); 


        axios.get(`https://imobbis.pythonanywhere.com/new/utilisateur/`,)
        .then(response => this.setState({users: response.data}),)
        .catch(error => this.setState({ error, isLoading: false })); 

    }

    getDaysFromDays = (date) => {
        var today = new Date()
        var end_date = new Date(date.substring(0, 9).replace('-', '/').replace('-', '/'))
        var current_date = new Date(today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate());
        var time_left = end_date.getTime() - current_date.getTime();
        var days_left = time_left / (1000 * 3600 * 24);
        return days_left
    }


    createAlert = (house)=>{
         
        Alert.alert(
            'Suppression',
            'Voulez vous vraiment supprimer cette publication (' + house?.name +') ?',
            [
                {
                    text:'Oui',
                    onPress:()=>{axios.delete('https://imobbis.pythonanywhere.com/new/hous/'+house?.id)},
                    style: 'cancel'
                },
                {
                    text:'Non',
                    onPress:()=>{},
                    style: 'cancel'
                }
            ]
        )

    }

        
    indisponible = (house) => {
        var etat = house?.disponible
    
        Alert.alert(
            !etat?'Indisponiblité':'Disponiblité',
            'Voulez vous mettre cette publication (' + house?.name + ') ?',
            [
                {
                    text: 'Oui',
                    onPress: () => {
                        axios.patch('https://imobbis.pythonanywhere.com/new/hous/' + house?.id,
                         {
                            'disponible':!etat
                         }).then((res) => {
                           
                            Toast.showWithGravity(
                                "Opération effectuée!",
                                Toast.LONG,
                                Toast.CENTER,
                                )
                        })
                        .catch(error => this.setState({ error, isLoading: false }))
                    },
                    style: 'cancel'
                },
                {
                    text: 'Non',
                    onPress: () => { },
                    style: 'cancel'
                }
            ]

        )

    }
    render(){
        const {users, renters} = this.state
        const{houses,imageshouses} = this.props.route.params
        return(
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <View style={{flexDirection: 'row',}}>
                    <TouchableOpacity style={{marginLeft: -10, width: '14%'}}  onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before'  size={45} style={{marginTop: -5}}/>
                    </TouchableOpacity>
                    <View style={{width: '80%', alignItems: 'center', marginTop: 3}}>
                        <Text style={{ fontWeight: "600", fontSize: 25 }}>
                            Mes maisons ({_.size(houses)})
                        </Text>
                    </View>
                </View>
            <ScrollView style={{backgroundColor: 'white'}}>
                {
                    houses.map(house => {return(
                        <View style={{...styles.row3}}>
                            <Image source={{uri: house.photo_1}}
                                style={{height: 110, width: '34%', borderRadius: 15}}
                            />
                            <View style={{marginLeft: 10, width: '64%'}}>
                                <Text style={{fontWeight: 'bold', fontSize: 16}} numberOfLines={1}>{house.name}</Text>
                                <Text style={{ color: 'grey'}} numberOfLines={1}>
                                    {house.nb_room + ' chambres / ' + house.nb_parlour + ' salon / ' + house.nb_toilet + ' douche'}
                                </Text>
                                {
                                    renter = _.find(renters, {current_house: house.id}),
                                    user = _.find(users, {id: renter?.user}),
                                    renter ? 
                                        <View style={{borderWidth: 2, borderColor: 'grey', padding: 4, borderRadius: 10, flexDirection: 'row', marginTop: 3}}>
                                            <Text style={{marginRight: 5, color: 'grey', fontWeight: 'bold'}}>Locataire:</Text>
                                            <Avatar.Image size={25}/>
                                            <View style={{marginLeft: 5}}>
                                                <Text style={{fontSize: 11, fontWeight: 'bold'}}>{user?.last_name + ' ' + user?.first_name}</Text>
                                                <Text style={{fontSize: 10, color: 'orange'}}>
                                                    {this.getDaysFromDays(renter?.cr_hs_st_dt)} jours restants
                                                </Text>
                                            </View>
                                        </View>
                                    : 
                                    <View style={{borderWidth: 1, borderColor: 'grey', padding: 4, borderRadius: 10, flexDirection: 'row', marginTop: 3}}>
                                        <Text style={{marginRight: 5, color: 'red', fontWeight: 'bold'}}>
                                            Cette maison n'a pas de locataire 
                                        </Text>
                                    </View>
                                }
                                <View style={{flexDirection: 'row-reverse'}}>
                                    <TouchableOpacity style={{...styles.bt1, marginHorizontal: 7, backgroundColor: 'red'}}
                                       onPress ={() => this.createAlert(house)}>
                                        <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Supprimer</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{...styles.bt1}}
                                     onPress={() => this.props.navigation.navigate('UpdateEtape', { 'data': house })}>
                                        <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Modifier</Text>
                                    </TouchableOpacity>

                                </View>

                                <TouchableOpacity style={{ ...styles.bt1, backgroundColor: '#f2bb62' }}
                                        onPress={() => this.indisponible(house)}>
                                        <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>{!house?.disponible? 'Indisponible':'Disponible'}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    )})
                }
            </ScrollView>
            </View>
        )
    }
    
}



const styles = StyleSheet.create({
    row3: {
        marginVertical: 7,
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 3,
            height: 3,
        }, marginVertical: 10,
        shadowOpacity: 0.25, marginHorizontal: 4,
        shadowRadius: 3, flexDirection: 'row',
        padding: 7,elevation: 3,
        marginVertical: 6,
        borderRadius: 20
    },
    bt1: {
        backgroundColor: '#00394d', width: 80, 
        justifyContent: 'center',alignItems: 'center',
        height: 30, borderRadius: 7, marginTop: 6
    }
})