import React, {Component} from 'react'
import {Text, View, Image, TouchableOpacity, ScrollView, Dimensions} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import axios from 'axios'
import _ from 'underscore'

var width_ = Math.round(Dimensions.get('window').width)
var height_ = Math.round(Dimensions.get('window').height)

export default class AgentDistricts extends Component{
    constructor(props){
        super(props)
        this.state = {
            districts_: []
        }
    }

    componentDidMount(){
        axios.get(`https://imobbis.pythonanywhere.com/location/`,)
        .then(response => this.setState({districts_: response.data.district}),)
        .catch(error => this.setState({ error, isLoading: false })); 
    }

    render(){
        const {houses, districts} = this.props.route.params
        const {districts_} = this.state
        return(
            <View style={{flex: 1, backgroundColor: 'white', padding: 7}}>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                    <TouchableOpacity style={{marginLeft: -10, width: '14%'}}  onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before'  size={45} style={{marginTop: -5}}/>
                    </TouchableOpacity>
                    <View style={{width: '80%', alignItems: 'center', marginTop: 3}}>
                        <Text style={{ fontWeight: "600", fontSize: 25 }}>
                            Mes districts ({_.size(districts)})
                        </Text>
                    </View>
                </View>
                <ScrollView>
                    <View>
                        {
                            districts_.map(dist => districts.map(distr => {return(
                                dist.id == distr ?
                                    (
                                        homes = _.filter(houses, {district: dist.id}),
                                        <View style={{marginVertical: 9, borderBottomWidth: 2, borderBottomColor: '#e6e6e6', paddingBottom: 6}}>
                                            <View style={{flexDirection: 'row'}}>
                                                <EvilIcons name='location' size={40}  style={{marginTop: 3, marginRight: 5}}/>
                                                <View style={{marginTop: -3}}>
                                                    <Text style={{fontSize: 20, fontWeight: '500'}}>{dist.name}</Text>
                                                    <Text style={{fontSize: 15, color: 'grey'}}>{_.size(homes)} maisons</Text>
                                                </View>
                                            </View>
                                            <ScrollView horizontal={true} contentContainerStyle={{marginLeft: 45}} >
                                                {
                                                    homes.map(home => {return(
                                                        <View style={{height: height_*0.15, width: width_*0.3, alignItems: 'center', margin: 4}}>
                                                            <Image source={{uri: home.photo_1}} style={{width: '100%', height: '75%', borderRadius: 5}}/>
                                                            <Text numberOfLines={1} style={{fontSize: 14}}>{home.name}</Text>
                                                            <Text style={{fontSize: 12, color: 'red', fontWeight: 'bold'}}>{home.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")} XAF</Text>
                                                        </View>
                                                    )})
                                                }
                                            </ScrollView>
                                        </View>
                                    )
                                : null
                            )}))
                        }
                    </View>
                </ScrollView>
            </View>
        )
    }
    
}