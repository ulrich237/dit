import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AgentHome from './agentHome';
import RecentlyTouched from '../helpers/recentTouched';
import Followings from '../helpers/followings';
import AgentHouses from './myHouses';
import AgentRenters from './myRenters';
import AgentDistricts from './myDistricts';
import Etape from '../landlord/post/Etape';
import Etape1 from '../landlord/post/Etape1';
import Etape2 from '../landlord/post/Etape2';
import Etape3 from '../landlord/post/Etape3';
import UpdateEtape from '../updatePost/UpdateEtape';
import UpdateEtape1 from '../updatePost/UpdateEtape1';
import UpdateEtape2 from '../updatePost/UpdateEtape2';
import UpdateEtape3 from '../updatePost/UpdateEtape3';
import UpdateEtape4 from '../updatePost/UpdateEtape4';

import MyDetails from '../landlord/details';
import MyHouses from '../landlord/gcel/myHouses'
import MyRenters from '../landlord/rent/myRenters'

import Home from '../normalUser/home';

import NavigationGcel from '../landlord/gcel/navigationGcel';
import NavigationRent from '../landlord/rent/navigationRent';
import MessagesIni from  '../normalUser/Messages_in';
import AddLocataire from '../landlord/AddLocaraire';
import DetailRenter from '../landlord/DetailRenter'


const Stack = createStackNavigator();

export default class AgentNavigation extends Component {
    constructor(props){
        super(props)
    }


    render(){
        return(
        <Stack.Navigator>
            <Stack.Screen name='AgentHome' component={AgentHome} options={{headerShown: false}}/>
            <Stack.Screen name='RecentlyTouched' component={RecentlyTouched} options={{headerShown: false}}/>
            <Stack.Screen name='Followings' component={Followings} options={{headerShown: false}}/>
            <Stack.Screen name='AgentHouses' component={AgentHouses} options={{headerShown: false}}/>
            <Stack.Screen name='AgentRenters' component={AgentRenters} options={{headerShown: false}}/>
            <Stack.Screen name='AgentDistricts' component={AgentDistricts} options={{headerShown: false}}/>
            <Stack.Screen name='Etape' component={Etape} options={{headerShown: false}}/>
            <Stack.Screen name='Etape1' component={Etape1} options={{headerShown: false}}/>
            <Stack.Screen name='Etape2' component={Etape2} options={{headerShown: false}}/>
            <Stack.Screen name='Etape3' component={Etape3} options={{headerShown: false}}/>

            <Stack.Screen name='UpdateEtape' component={UpdateEtape} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape1' component={UpdateEtape1} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape2' component={UpdateEtape2} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape3' component={UpdateEtape3} options={{headerShown: false}}/>
            <Stack.Screen name='UpdateEtape4' component={UpdateEtape4} options={{headerShown: false}}/>
        
            <Stack.Screen name='NavigationGcel' component={NavigationGcel} options={{headerShown: false}}/>
            <Stack.Screen name='NavigationRent' component={NavigationRent} options={{headerShown: false}}/>
            <Stack.Screen name='DetailRenter' component={DetailRenter} options={{title: 'Detail locataire',}}/>
   
            <Stack.Screen name='MessagesIni' component={MessagesIni} options={{headerShown: false}}/>
            <Stack.Screen name='AddLocataire' component={AddLocataire} options={{title: 'Ajouter un locataire',}}/>
            <Stack.Screen name='MyDetails' component={MyDetails} options={{title: 'Detail bien',}}/>
            <Stack.Screen name='MyHouses' component={MyHouses} options={{headerShown: false}}/>
            <Stack.Screen name='MyRenters' component={MyRenters} options={{headerShown: false}}/>
            <Stack.Screen name='Home' component={Home} options={{headerShown: false}}/>
        
        </Stack.Navigator>
        )
    }
    
}
           