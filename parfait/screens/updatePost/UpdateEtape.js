import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';
import { Divider } from 'react-native-elements'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
   
    StatusBar,
    DrawerLayoutAndroid,

    Switch
} from 'react-native';
import StepIndicator from 'react-native-step-indicator'
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";



var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);




var reason = [
    { label: "A vendre", value: 0 },
    { label: "A louer", value: 1 },

]

var type = [
    { label: "Meublé", value: 0 },
    { label: "Moderne", value: 1 },

]
const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];

export default class UpdateEtape extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            description: this.props.route.params.data?.description, pass: '',
            modal: false, valueModal: {}, selectModal: true,
            cat: [], value: 0, etat: false,
            price: this.props.route.params.data?.publish_type, selectedValue: this.props.route.params.data?.publish_type,
            isMeuble: this.props.route.params.data?.is_immeuble,
            isModerne: this.props.route.params.data?.is_moderne, genre: '',
            chambre: this.props.route.params.data?.nb_room,
            salon: this.props.route.params.data?.nb_parlour,
            cuisine: this.props.route.params.data?.nb_kitchen,
            douche: this.props.route.params.data?.nb_toilet, terasse: 0,
            metreCarre: this.props.route.params.data?.dimension, currentPosition: 0,
            isBarriere: this.props.route.params.data?.fence,
            isJardin: this.props.route.params.data?.garden,
            name: this.props.route.params.data?.name, rent_per_day: this.props.route.params.data?.rent_per_day,
            quartier: this.props.route.params.data?.neighbor, dsc: this.props.route.params.data?.is_negotiable
        };
    }



    componentDidMount() {
        const { data } = this.props.route.params
        axios.get('https://imobbis.pythonanywhere.com/house/cat')
            .then(res => this.setState({ cat: res.data, valueModal: _.find(res.data, { name: data?.category }) }))

    }


    onPageChange(position) {
        this.setState({ currentPosition: position });
    }


    render() {
        const { modal, valueModal, selectModal, cat, value, selectedValue, isMeuble,
            isModerne, genre, chambre, salon, douche, cuisine, terasse, metreCarre, dsc,
            isBarriere, isJardin, description, price, rent_per_day, quartier } = this.state;

        const { data } = this.props.route.params


        return (
            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 25 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />


                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modal}
                    onRequestClose={() => {
                    }}
                >
                    <View style={[styles.modalView, {
                        marginLeft: width_ * 0.25, marginTop: height_ * 0.3,
                        marginBottom: height_ * 0.3
                    }]}>

                        <View style={{ flexDirection: 'row-reverse' }}>
                            <TouchableOpacity
                                style={{
                                    ...styles.openButton,
                                    shadowColor: "black",
                                    shadowOffset: {
                                        width: 0, height: 6,
                                    },
                                    shadowOpacity: 0.1, marginTop: 20
                                }}
                                onPress={() => {
                                    this.setState({ modal: false });
                                }}
                            >
                                <FontAwesome style={{}}
                                    name='close'
                                    size={25}
                                    color='red'
                                />
                            </TouchableOpacity>
                        </View>
                        <ScrollView style={{ marginTop: 10 }}>
                            {
                                cat.map(cat_ => {
                                    return (
                                        <View style={{ flexDirection: 'row' }}>
                                            <TouchableOpacity style={{ flexDirection: 'row' }}
                                                onPress={() => {
                                                    this.setState({
                                                        valueModal: cat_, modal: false,
                                                        selectModal: true, currentPosition: 0
                                                    })
                                                }}>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <Text style={{ color: "black", fontSize: 20, fontWeight: 'bold' }}>{cat_.name}</Text>
                                                    <Image style={{ width: 30, height: 30, borderRadius: 2, margin: 10 }}
                                                        source={{ uri: cat_.image }}
                                                    />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }
                        </ScrollView>

                    </View>
                </Modal>


                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.etat}
                    onRequestClose={() => {
                    }}
                >
                    <View style={[styles.modalView, {
                        marginLeft: width_ * 0.25, marginTop: height_ * 0.3,
                        marginBottom: height_ * 0.3, height: height_ * 0.25
                    }]}>

                        <View style={{ flexDirection: 'row-reverse' }}>
                            <TouchableOpacity
                                style={{
                                    ...styles.openButton,
                                    shadowColor: "black",
                                    shadowOffset: {
                                        width: 0, height: 6,
                                    },
                                    shadowOpacity: 0.1, marginTop: 10
                                }}
                                onPress={() => {
                                    this.setState({ etat: false });
                                }}
                            >
                                <FontAwesome style={{}}
                                    name='close'
                                    size={25}
                                    color='red'
                                />
                            </TouchableOpacity>
                        </View>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 10 }} />
                        <ScrollView style={{ marginTop: 10 }}>
                            <TouchableOpacity style={{}}
                                onPress={() => { this.setState({ selectedValue: 'vendre', etat: false }) }}>

                                <Text style={{ textAlign: 'center' }}>À vendre</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ marginTop: 15 }}
                                onPress={() => { this.setState({ selectedValue: 'louer', etat: false }) }}>

                                <Text style={{ textAlign: 'center' }}>À louer</Text>
                            </TouchableOpacity>
                        </ScrollView>

                    </View>
                </Modal>




                {
                    /* ici
                    */
                }






                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>Information sur le bien à publier</Text>
                <ScrollView style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('90%')
                }}>
                    <TouchableOpacity
                        style={{
                            marginTop: !selectModal ? '60%' : 15,
                            marginLeft: width_ * 0.25
                        }}
                        onPress={() => {
                            this.setState({ modal: true, })
                        }}>
                        <View>
                            <View style={{ flexDirection: 'row' }}>
                                {!selectModal ?
                                    <View>
                                        <Text style={{
                                            textAlign: 'center', color: "black",
                                            color: "black", fontSize: 20, fontWeight: '300'
                                        }}>
                                            {_.find(cat, { name: data?.category })?.name}
                                        </Text>

                                        <Image style={{ width: 30, height: 30, borderRadius: 2, margin: 10 }}
                                            source={{ uri: _.find(cat, { name: data?.category })?.image }}
                                        />
                                    </View>


                                    : <View style={{ flexDirection: 'row' }}>
                                        <Text style={{
                                            textAlign: 'center', color: "black",
                                            color: "black", fontSize: 20, fontWeight: '300'
                                        }}>{valueModal?.name}</Text>
                                        <Image style={{ width: 30, height: 30, borderRadius: 2, margin: 10 }}
                                            source={{ uri: valueModal?.image }} />

                                        <FontAwesome style={{ marginLeft: 20, marginTop: 3 }}
                                            name='edit'
                                            size={35}
                                            color='red'
                                        />
                                    </View>
                                }
                            </View>
                        </View>
                    </TouchableOpacity>
                    <ScrollView>
                        {
                            selectModal ?
                                <View style={{}}>

                                    {
                                        this.state.currentPosition == 0 ?
                                            <View>
                                                <View style={{}}>
                                                    <Text style={{ fontWeight: 'bold' }}>Nom: </Text>

                                                    <TextInput
                                                        style={[styles.input, { width: width_ / 2 }]}
                                                        onChangeText={(val) => this.setState({ name: val })}
                                                        value={this.state.name.toString()}

                                                    />
                                                </View>

                                                <View style={{}}>
                                                    <Text style={{ fontWeight: 'bold' }}>description: </Text>
                                                    <AutogrowInput
                                                        defaultHeight={75}
                                                        style={[styles.input, { height: 200, }]}
                                                        multiline={true}

                                                        placeholderTextColor="#666666"
                                                        autoCapitalize='none'
                                                        value={this.state.description.toString()}
                                                        onChangeText={(val) => this.setState({ description: val })}
                                                    />
                                                </View>


                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <Text style={{ fontWeight: 'bold' }}>Á vendre ou à louer</Text>
                                                    <View style={{ marginLeft: '20%' }}>
                                                        <TouchableOpacity style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'space-evenly' }}
                                                            onPress={() => { this.setState({ etat: !this.state.etat }) }}>
                                                            <Text style={{ fontWeight: 'bold' }}>Á vendre ou à louer:</Text>
                                                            <View style={{ marginLeft: '20%' }}>
                                                                <Text>{this.state.selectedValue}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                                {valueModal?.id != 8 &&
                                                    valueModal?.id != 10 && valueModal?.id != 9 ?
                                                    <View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                                            <Text style={{ fontWeight: 'bold' }}>Meublé </Text>
                                                            <View style={{ marginLeft: '70%' }}>
                                                                <Switch
                                                                    trackColor={{ false: "#767577", true: "#7c3325" }}
                                                                    thumbColor={isMeuble ? "#7c3325" : "#f4f3f4"}
                                                                    ios_backgroundColor="#3e3e3e"
                                                                    onValueChange={(itemValue) => this.setState({ isMeuble: itemValue })}
                                                                    value={isMeuble}
                                                                />
                                                            </View>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                                                            <Text style={{ fontWeight: 'bold' }}>Moderne </Text>
                                                            <View style={{ marginLeft: '70%' }}>
                                                                <Switch
                                                                    trackColor={{ false: "#767577", true: "#7c3325" }}
                                                                    thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                                                    ios_backgroundColor="#3e3e3e"
                                                                    onValueChange={(itemValue) => this.setState({ isModerne: itemValue })}
                                                                    value={isModerne}
                                                                />
                                                            </View>
                                                        </View>
                                                    </View>
                                                    : null
                                                }
                                                {
                                                    this.state.name != '' ?
                                                        <TouchableOpacity style={styles.button}
                                                            onPress={() => this.onPageChange(1)}>
                                                            <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Suivant</Text>
                                                        </TouchableOpacity> : null


                                                }
                                                <View style={{ height: 15 }}></View>

                                                <TouchableOpacity style={styles.button}
                                                    onPress={() => this.props.navigation.navigate('UpdateEtape2', { 'data': data })}>
                                                    <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>A propos des lois et restrictions</Text>
                                                </TouchableOpacity>

                                                <View style={{ height: 15 }}></View>
                                                <Button style={styles.button}
                                                    title="A propos des images"
                                                    onPress={() => this.props.navigation.navigate('UpdateEtape4', {
                                                        'data': data
                                                    })} />
                                            </View>
                                            : <View>
                                                {
                                                    valueModal?.id != 8 && valueModal?.id != 7
                                                        && valueModal?.id != 9 && valueModal?.id != 6 && valueModal?.id != 10
                                                        ?
                                                        <View>
                                                            {
                                                                valueModal?.id != 5
                                                                    ?
                                                                    <View style={{ width: '65%', flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                                        <Text style={{ fontWeight: 'bold' }}>Nombre de Salons:</Text>
                                                                        <View style={{ marginLeft: '60%' }}>
                                                                            <NumericInput
                                                                                onChange={(value) => this.setState({ salon: value })}
                                                                                totalWidth={75}
                                                                                totalHeight={30.5}
                                                                                initValue={salon}
                                                                                iconSize={10}
                                                                                valueType='real'
                                                                                minValue={valueModal?.id != 4 ? 0 : 1}
                                                                                maxValue={valueModal?.id != 4 ? 10 : 1}
                                                                                rounded
                                                                                textColor='#05375a'
                                                                                style={{ marginLeft: '25%' }}
                                                                                iconStyle={{ color: '#05375a', fontSize: 20, }}
                                                                            />
                                                                        </View>
                                                                    </View> : null

                                                            }


                                                            <View style={{ width: '65%', flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Nombre de chambres:</Text>
                                                                <View style={{ marginLeft: '55%' }}>
                                                                    <NumericInput
                                                                        onChange={(value) => this.setState({ chambre: value })}
                                                                        totalWidth={75}
                                                                        totalHeight={30.5}
                                                                        initValue={chambre}
                                                                        iconSize={10}
                                                                        valueType='real'
                                                                        minValue={valueModal?.id == 5 ? 1 : null}
                                                                        maxValue={valueModal?.id == 5 || valueModal?.id == 4 ? 1 : 10}
                                                                        rounded
                                                                        textColor='#05375a'
                                                                        iconStyle={{ color: '#05375a', fontSize: 20 }}
                                                                    />
                                                                </View>
                                                            </View>

                                                            <View style={{ width: '65%', flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Nombre de cuisines:</Text>
                                                                <View style={{ marginLeft: '60%' }}>
                                                                    <NumericInput
                                                                        onChange={(value) => this.setState({ cuisine: value })}
                                                                        totalWidth={75}
                                                                        totalHeight={30.5}
                                                                        initValue={cuisine}
                                                                        iconSize={10}
                                                                        valueType='real'
                                                                        minValue={0}
                                                                        maxValue={valueModal?.id != 5 ? 10 : 1}
                                                                        rounded
                                                                        textColor='#05375a'
                                                                        iconStyle={{ color: '#05375a', fontSize: 20 }}
                                                                    />
                                                                </View>
                                                            </View>
                                                            <View style={{ width: '65%', flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Nombre de douches:</Text>
                                                                <View style={{ marginLeft: '57%' }}>
                                                                    <NumericInput
                                                                        onChange={(value) => this.setState({ douche: value })}
                                                                        totalWidth={75}
                                                                        totalHeight={30.5}
                                                                        initValue={douche}
                                                                        iconSize={10}
                                                                        valueType='real'
                                                                        minValue={0}
                                                                        maxValue={valueModal?.id != 5 ? 10 : 1}
                                                                        rounded
                                                                        textColor='#05375a'
                                                                        iconStyle={{ color: '#05375a', fontSize: 20 }}
                                                                    />
                                                                </View>
                                                            </View>
                                                            <View style={{ width: '65%', flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Nombre de terasse:</Text>
                                                                <View style={{ marginLeft: '60%' }}>
                                                                    <NumericInput
                                                                        onChange={(value) => this.setState({ terasse: value })}
                                                                        totalWidth={75}
                                                                        totalHeight={30.5}
                                                                        initValue={terasse}
                                                                        iconSize={10}
                                                                        valueType='real'
                                                                        minValue={0}
                                                                        maxValue={valueModal?.id != 5 ? 10 : 2}
                                                                        rounded
                                                                        textColor='#05375a'
                                                                        iconStyle={{ color: '#05375a', fontSize: 20 }}
                                                                    /></View>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-evenly' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Dans la barriere </Text>
                                                                <View style={{ marginLeft: '55%' }}>
                                                                    <Switch
                                                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                                                        ios_backgroundColor="#3e3e3e"
                                                                        onValueChange={(itemValue) => this.setState({ isBarriere: itemValue })}
                                                                        value={isBarriere}
                                                                    />
                                                                </View>
                                                            </View>

                                                            <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-evenly' }}>
                                                                <Text style={{ fontWeight: 'bold' }}>Il y'a un jardin </Text>
                                                                <View style={{ marginLeft: '60%' }}>
                                                                    <Switch
                                                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                                                        ios_backgroundColor="#3e3e3e"
                                                                        onValueChange={(itemValue) => this.setState({ isJardin: itemValue })}
                                                                        value={isJardin}
                                                                    />
                                                                </View>
                                                            </View>
                                                        </View>
                                                        : valueModal?.id == 8 ?

                                                            <View style={{}}>
                                                                <Text style={{ fontWeight: 'bold' }}>Nombre de m²
                                                                </Text>
                                                                <TextInput
                                                                    style={{}}
                                                                    onChangeText={(val) => this.setState({ metreCarre: val })}
                                                                    value={this.state.metreCarre}
                                                                    placeholder="500 m²"
                                                                    keyboardType="numeric"
                                                                />
                                                            </View> : null
                                                }

                                                <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                                    <TouchableOpacity style={[styles.button, { backgroundColor: '#f9d7a7', }]}
                                                        onPress={() => this.onPageChange(0)}>
                                                        <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Précédent</Text>
                                                    </TouchableOpacity>


                                                    <TouchableOpacity style={[styles.button, { backgroundColor: '#7c3325', }]}
                                                        onPress={() => this.props.navigation.navigate('UpdateEtape1', {
                                                            'description': description,
                                                            'valueModal': valueModal, 'quartier': quartier,
                                                            'price': price, 'selectedValue': selectedValue, 'isMeuble': isMeuble,
                                                            'isModerne': isModerne, 'genre': genre, 'chambre': chambre, 'salon': salon,
                                                            'cuisine': cuisine, 'douche': douche, 'terasse': terasse, 'rent_per_day': rent_per_day,
                                                            'metreCarre': metreCarre, 'isBarriere': isBarriere, 'isJardin': isJardin, 'name': this.state.name,
                                                            'data': data, 'dsc': dsc
                                                        })}>

                                                        <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Suivant</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                    }
                                </View>
                                : null
                        }
                    </ScrollView>


                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: "azure",
    },
    input: {

        borderWidth: 1,
        width: '90%',
        color: '#7c3325',
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        // alignItems: 'center', alignSelf: 'flex-end',
        margin: 4, borderRadius: 7,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.50,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },


});

