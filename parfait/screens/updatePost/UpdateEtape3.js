import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
    Picker,
    StatusBar,
    DrawerLayoutAndroid,
    
    Switch
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-crop-picker';
import StepIndicator from 'react-native-step-indicator'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];

export default class UpdateEtape3 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            photo: [], currentPosition: 4, price: this.props.route.params.data?.renting_price,
            priceEau: Number(this.props.route.params.data?.water_m3_price),
            priceElect: Number(this.props.route.params.data?.electricity_Kw_price), disc: '',
            images: [], save: false,

        };

    }



    componentDidMount() {

        const { wifi, eau, clim, sport,
            conEnfant, amis, fume, evenement,
            all_cham, cham, cong, cuisi,
            elever, house, data, lois } = this.props.route.params;

        /* this.setState({
             price: data?.renting_price, priceEau: data?.water_m3_price,
             priceElect: data?.electricity_Kw_price
         })*/

        //https://imobbis.pythonanywhere.com/new/image

        axios.get('https://imobbis.pythonanywhere.com/new/image')
            .then(res => this.setState({
                images: _.filter(res.data, { house: data?.id }),

            }))

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { data } = this.props.route.params;
        if (prevState.images !== this.state.images) {

            axios.get('https://imobbis.pythonanywhere.com/new/image')
                .then(res => this.setState({
                    images: _.filter(res.data, { house: data?.id }),

                }))
        }


    }



    save = () => {

        const { description, valueModal,
            selectedValue, isMeuble,
            isModerne, chambre, salon,
            cuisine, douche, terasse,
            metreCarre, isBarriere, isJardin, name, dsc,
            country, city, quartier, isParking, data } = this.props.route.params;

        const { photo, price, priceEau, priceElect, disc } = this.state;
        var photos = _.toArray(photo)
        var form = new FormData()

        this.setState({ save: true })
        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + data?.id,
            {

                "nb_room": chambre,
                "name": name,
                "nb_parlour": salon,
                "nb_toilet": douche,
                "nb_kitchen": cuisine,
                "parking": isParking,
                "fence": isBarriere,
                "garden": isJardin,
                "description": description,
                "publish_type": selectedValue,
                "is_negotiable": false,
                "is_furnished": isMeuble,
                "is_finished": false,
                "renting_price": price,
                "dimension": metreCarre,
                "is_immeuble": isMeuble,
                "is_moderne": isModerne,
                "country": country.id,
                "city": city.id,
                "neighbor": quartier,
                "category": valueModal.name,
                "water_m3_price": priceEau,
                "electricity_Kw_price": priceElect,
                "is_negotiable": dsc,
                "is_finished": true,
            }
        ).then(response => {

            Toast.showWithGravity(
                "Modification effectuée!",
                Toast.LONG,
                Toast.CENTER,
               )

            this.props.navigation.navigate('Home');


        })
            .catch(error => console.log(error)
            )



    }



    handlerChoosePhoto = () => {

        ImagePicker.openPicker({
            multiple: true,
            mediaType: 'photo',
        }).then(images => {
            this.setState({ photo: images, images: [] })
        });

    }




    render() {
        const { photo, disc, priceElect, priceEau, price, images } = this.state;

        const {
            elever, house, data, quartier, dsc } = this.props.route.params;



        return (
            <ScrollView style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 25 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />
                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>sauvegarder</Text>
                <ScrollView style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('90%')
                }}>

                    <ScrollView>

                        <View style={{ marginTop: 10, }}>
                            <Text style={{ fontWeight: 'bold', textAlign: 'center' }}>prix
                                                    {house?.category == 8 ? ' par m²' :
                                    house?.is_immeuble ? ' par jour:' : ' par mois:'}</Text>
                            <TextInput
                                style={{}}
                                onChangeText={(val) => this.setState({ price: val })}
                                value={price.toString()}
                                keyboardType="numeric"
                            />

                        </View>

                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-evenly' }}>
                            <Text style={{ fontWeight: 'bold' }}>Discutable </Text>
                            <View style={{ marginLeft: '60%' }}>
                                <Switch
                                    trackColor={{ false: "#767577", true: "#7c3325" }}
                                    thumbColor={dsc ? "#7c3325" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    onValueChange={(itemValue) => this.setState({ dsc: itemValue })}
                                    value={dsc}
                                />
                            </View>
                        </View>

                        {/*
                            house?.is_immeuble ?
                                <View></View>
                                : <View>
                                    <View style={{}}>


                                    </View>
                                    <View style={{}}>
                                        <Text style={{ fontWeight: 'bold' }}>prix eaux par métre cube
                                                  </Text>
                                        <TextInput
                                            style={{}}
                                            onChangeText={(val) => this.setState({ priceEau: val })}
                                            value={this.state.priceEau.toString()}
                                            placeholder="prix eau"
                                            keyboardType="numeric"
                                        />

                                    </View>

                                    <View style={{}}>
                                        <Text style={{ fontWeight: 'bold' }}>prix élèctricité par kilo watt
                                                  </Text>
                                        <TextInput
                                            style={{}}
                                            onChangeText={(val) => this.setState({ priceElect: val })}
                                            value={this.state.priceElect.toString()}
                                            placeholder="prix"
                                            keyboardType="numeric"
                                        />
                                    </View>

                                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: 'space-between' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Discutable </Text>
                                        <View style={{ marginLeft: '60%' }}>
                                            <Switch
                                                trackColor={{ false: "#767577", true: "#7c3325" }}
                                                thumbColor={disc ? "#7c3325" : "#f4f3f4"}
                                                ios_backgroundColor="#3e3e3e"
                                                onValueChange={(itemValue) => this.setState({ disc: itemValue })}
                                                value={disc}
                                            />
                                        </View>
                                    </View>

                                </View>*/
                        }



                        <TouchableOpacity style={styles.button}
                            onPress={() => this.save()}>
                            <Text style={{ fontSize: 22, color: 'white', fontWeight: '600' }}>Enregistrer les modifications</Text>
                        </TouchableOpacity>






                    </ScrollView>

                </ScrollView>
            </ScrollView>

        );
    }
}

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: "azure",
    },
    input: {
        height: 30,
        borderWidth: 1,
        width: '90%',
        color: '#7c3325',
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 10, borderRadius: 7,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.50,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },


});

