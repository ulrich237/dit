import React, { useRef, useState } from 'react';
import axios from 'axios';
import _, { findLastIndex } from 'underscore';
import ModalSelector from 'react-native-modal-selector';
import AutogrowInput from 'react-native-autogrow-input';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NumericInput from 'react-native-numeric-input';
import StepIndicator from 'react-native-step-indicator'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Colors,
    Dimensions, TouchableHighlight,
    Button,
    TextInput,
    Modal,
    Text,
    Picker,
    StatusBar,
    DrawerLayoutAndroid,
    
    Switch
} from 'react-native';
import DeviceInfo from 'react-native-device-info'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Divider, Badge } from 'react-native-elements'
import Toast from 'react-native-simple-toast';
let index = 0, doc;


var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);




var reason = [
    { label: "A vendre", value: 0 },
    { label: "A louer", value: 1 },

]

var type = [
    { label: "Meublé", value: 0 },
    { label: "Moderne", value: 1 },

]

const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#001f26',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#000000',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#000000',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#000000',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 14,
    currentStepLabelColor: '#001f26'
}
const labels = ["Quoi ? ", "Comment ?", "Où ?", "C'est bien ?", "On peut ?"];


export default class UpdateEtape2 extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            cat: [], is_immeuble: this.props.route.params.data?.is_immeuble,
            isModerne: this.props.route.params.data?.publish_type,
            lois: {}, currentPosition: 0,
            wifi: this.props.route.params.data?.wifi,
            eau: this.props.route.params.data?.wifi,
            clim: this.props.route.params.data?.climatisation,
            sport: this.props.route.params.data?.gym,
            fume: this.props.route.params.data?.smooking_allowed,
            evenement: false,
            all_cham: this.props.route.params.data?.tv_all_bedroom,
            cong: this.props.route.params.data?.freezer,
            cuisi: this.props.route.params.data?.cooker,
            elever: this.props.route.params.data?.pets_allowed,
            asc: this.props.route.params.data?.elevator,
            age: this.props.route.params.data?.age_colocataire,
            sexe: this.props.route.params.data?.sexe_colocataire,
            religion: this.props.route.params.data?.religion_colocataire,
            currentPosition: 4, loi: {}, modal: false, what: 0,
        };
    }

    onPageChange(position) {
        this.setState({ currentPosition: position });
    }
    componentDidMount() {
        const { description, valueModal, country, neighbor,
            price, isMeuble,
            isModerne, chambre, salon,
            cuisine, douche, terasse,
            metreCarre, isBarriere, isJardin, data } = this.props.route.params;

      

    }


    save = () => {
        const { data } = this.props.route.params;
        const { isModerne, lois, wifi, eau, clim, sport,
            conEnfant, asc, fume, evenement,
            all_cham, cham, cong, cuisi, elever, currentPosition, loi } = this.state;

        axios.patch("https://imobbis.pythonanywhere.com/new/hous/" + data?.id,
            {
                "wifi": wifi,
                "hot_water": eau,
                "climatisation": clim,
                "freezer": cong,
                "cooker": cuisi,
                "tv_all_bedroom": all_cham,
                "smooking_allowed": fume,
                "events_allowed": evenement,
                "pets_allowed": elever,
                "elevator": asc,
                "gym": sport,
                "age_colocataire": this.state.age,
                "sexe_colocataire": this.state.sexe,
                "religion_colocataire": this.state.religion
            }
        ).then(response => {

            Toast.showWithGravity(
                "Modification effectuée!",
                Toast.LONG,
                Toast.CENTER,
              )

            this.props.navigation.navigate('Home');

        })
            .catch(error => alert(error))
    }

    setReligion = (visible, city,) => {
        this.setState({
            modal: visible,
            religion: city,

        });
    }

    setAge = (visible, city,) => {
        this.setState({
            modal: visible,
            age: city,

        });
    }

    setSexe = (visible, city,) => {
        this.setState({
            modal: visible,
            sexe: city,

        });
    }


    render() {
        const { isModerne, lois, wifi, eau, clim, sport, age, religion, sexe, what, modal,
            conEnfant, amis, fume, evenement, asc, is_immeuble,
            all_cham, cham, cong, cuisi, elever, currentPosition, loi } = this.state;
        const { data } = this.props.route.params;

        return (
            <View style={{ flex: 1, paddingTop: DeviceInfo.hasNotch() ? 25 : 0, backgroundColor: "#dc7e27" }}>
                <StepIndicator
                    customStyles={customStyles}
                    currentPosition={this.state.currentPosition}
                    labels={labels}
                    stepCount={5}
                />
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modal}
                    onRequestClose={() => {
                        Alert.alert("Modal has been closed.");
                    }}>

                    <View style={[styles.modalView]}>
                        <TouchableOpacity
                            style={{
                                ...styles.openButton,
                                shadowColor: "black",
                                shadowOffset: {
                                    width: 0, height: 6,
                                },
                                shadowOpacity: 0.1, marginTop: 20
                            }}
                            onPress={() => {
                                this.setState({ modal: false });
                            }}
                        >
                            <FontAwesome style={{}}
                                name='close'
                                size={25}
                                color='red'
                            />
                        </TouchableOpacity>
                        <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
                        <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Faites un choix:</Text>
                        <ScrollView style={{ height: 0.25 }}>
                            {
                                what == 0 ?
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'christianisme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>christianisme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'judaïsme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>judaïsme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'bouddhisme');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>bouddhisme</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, 'islam');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>islam</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setReligion(!modal, '');
                                            }}
                                        >
                                            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Toutes</Text>
                                        </TouchableOpacity>
                                    </View>
                                    : what == 1 ?
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '18-25');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>18-25</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '26-40');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>26-40</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '41-plus');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>41-plus</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setAge(!modal, '');
                                                }}
                                            >
                                                <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Tous</Text>
                                            </TouchableOpacity>
                                        </View>
                                        : what == 2 ?
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setSexe(!modal, 'Masculin');
                                                    }}
                                                >
                                                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Masculin</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        this.setSexe(!modal, 'Feminin');
                                                    }}
                                                >
                                                    <Text style={{ fontSize: 20, alignSelf: 'center', margin: 5 }}>Feminin</Text>
                                                </TouchableOpacity>
                                            </View>
                                            : null
                            }
                        </ScrollView>
                    </View>
                </Modal>
                <Text style={{
                    marginTop: '5%', fontSize: 30,
                    color: 'black', fontWeight: 'bold', textAlign: 'center'
                }}>Les informations partielles </Text>
                <ScrollView style={{
                    //alignItems: "center",
                    //justifyContent: "center",
                    backgroundColor: 'white', height: hp('90%')
                }}>
                    <ScrollView>
                        {is_immeuble ?
                            <View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Télèvision </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ all_cham: itemValue })}
                                            value={all_cham}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Peut on fumer à l'intérieur  </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ fume: itemValue })}
                                            value={fume}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Fêtes autorisées </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ evenement: itemValue })}
                                            value={evenement}
                                        />
                                    </View>
                                </View>





                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Congélateur: </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ cong: itemValue })}
                                            value={cong}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Cuisiniére: </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ cuisi: itemValue })}
                                            value={cuisi}
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                    <Text style={{ fontWeight: 'bold' }}>Animaux domestiques autorisés </Text>
                                    <View style={{}}>
                                        <Switch
                                            trackColor={{ false: "#767577", true: "#7c3325" }}
                                            thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                            ios_backgroundColor="#3e3e3e"
                                            onValueChange={(itemValue) => this.setState({ elever: itemValue })}
                                            value={elever}
                                        />
                                    </View>
                                </View>

                            </View> : null
                        }

                        <View>
                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Wifi: </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ wifi: itemValue })}
                                        value={wifi}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Chauffe eau: </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ eau: itemValue })}
                                        value={eau}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Climatisation: </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ clim: itemValue })}
                                        value={clim}
                                    />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Ascenceur:</Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ asc: itemValue })}
                                        value={asc}
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                <Text style={{ fontWeight: 'bold' }}>Salle de sport equipè: </Text>
                                <View style={{}}>
                                    <Switch
                                        trackColor={{ false: "#767577", true: "#7c3325" }}
                                        thumbColor={isModerne ? "#7c3325" : "#f4f3f4"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={(itemValue) => this.setState({ sport: itemValue })}
                                        value={sport}
                                    />
                                </View>
                            </View>


                        </View>

                        {
                            data?.colocation ?
                                <View>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 2, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Vous voulez un collocataire de quel sexe</Text>
                                        <View style={{}}
                                        >
                                            <Text>{sexe == '' ? 'Tous' : sexe}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 1, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Tranche d'âge</Text>
                                        <View style={{}}
                                        >
                                            <Text>{age == '' ? 'Tous' : age}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ what: 0, modal: true })}
                                        style={{ flexDirection: 'row', margin: '2%', justifyContent: 'space-evenly' }}>
                                        <Text style={{ fontWeight: 'bold' }}>Vous voulez un collocataire de quelle réligion:</Text>
                                        <View style={{}}
                                        >
                                            <Text>{religion == '' ? 'Toutes' : religion}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                        <TouchableOpacity style={styles.button}
                            onPress={() => this.save()}>
                            <Text style={{ fontSize: 18, color: 'white', fontWeight: '600' }}>Modifier</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {

        flex: 1,
        backgroundColor: "azure",
    },
    input: {
        height: 30,
        borderWidth: 1,
        width: '90%',
        color: '#7c3325',
    },
    button: {
        height: 45, flexDirection: 'row',
        backgroundColor: '#7c3325', padding: 8,
        justifyContent: 'center',
        alignItems: 'center', alignSelf: 'flex-end',
        margin: 10, borderRadius: 7,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2
        },
        width: width_ / 2.0,
        height: height_ / 2.0,
        marginTop: height_ * 0.30,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5, marginLeft: width_ * 0.25, marginTop: height_ * 0.3,
    },


});

