import React, { Component } from 'react'
import { Image, Text, StyleSheet, TextInput, View, Modal, ScrollView, TouchableOpacity, Dimensions, Platform } from 'react-native'
import {Card } from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
//import PhotoUpload from 'react-native-photo-upload'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { RadioButton } from 'react-native-paper';
import _ from 'underscore'
import axios from 'axios'
import Entypo from 'react-native-vector-icons/Entypo'
import { Divider } from 'react-native-elements';
 import Ionicons from 'react-native-vector-icons/Ionicons'

var width_ = Math.round(Dimensions.get('window').width);
var height_ = Math.round(Dimensions.get('window').height);

export default class Update_profiluser extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            what_modal: '',
            user: {
                id: null,
                profile_image: "a",
                username: "",
                first_name: "",
                last_name: "",
                gender: "M",
                countrie: null,
                province: null,
                citie: null,
                district: null,
                neighbor: null,
                phone_number: null
            },
            countrys: [],
            districts: [],
            provinces: [],
            citys: [],
            neighbors: [],
            location: ""

        }
    }

    componentDidMount() {
        axios.get(`https://imobbis.pythonanywhere.com/user/`)
            .then(resu => {
                let user = _.find(resu.data, { id: 1 })
                this.setState({ user: user });
                axios.get(`https://imobbis.pythonanywhere.com/location/`)
                    .then(res => {
                        let location = "";
                        _.find(res.data.country, {id: user.countrie}) ? location = _.find(res.data.country, {id: user.countrie}).name : null;
                        _.find(res.data.province, {id: user.province}) ? location = location +", "+ _.find(res.data.province, {id: user.province}).name : null;
                        _.find(res.data.district, {id: user.district}) ? location = location +", "+ _.find(res.data.district, {id: user.district}).name : null;
                        _.find(res.data.city, {id: user.citie}) ? location = location +", "+ _.find(res.data.city, {id: user.citie}).name : null;
                        _.find(res.data.neighbor, {id: user.neighbor}) ? location = location +", "+ _.find(res.data.neighbor, {id: user.neighbor}).name : null;
                        this.setState({ countrys: res.data.country, districts: res.data.district, citys: res.data.city, provinces: res.data.province, neighbors: res.data.neighbor, location: location });
                    })
            })

    }

    handleChange = (name, value) => {
        const user = { ...this.state.user, [name]: value };
        this.setState({ user });
    };

    addlocation(name) {
        let location = this.state.location;
        location = location == "" ? name : location+", "+name;
        this.setState({location: location});
    }

    handleChangeradio = () => {
        let user = this.state.user;
        user.gender == "M" ? user.gender = "F" : user.gender = "M";
        this.setState({ user });
    };

    initlocation(){
        let user = { ...this.state.user, countrie: null, province: null, citie: null, district: null, neighbor: null};
        this.setState({location: "", user: user});
    }
    
    save(item) {
        const formData = new FormData();
        formData.append("username", item.username);
        if (item.profile_image.fileName !== undefined) {
            formData.append("profile_image", {
                uri: item.profile_image.uri,
                type: item.profile_image.type,
                name: item.profile_image.fileName,
                data: item.profile_image.data
            });
        };
        formData.append("first_name", item.first_name);
        formData.append("last_name", item.last_name);
        formData.append("gender", item.gender);
        formData.append("countrie", Number(item.countrie));
        formData.append("province", Number(item.province));
        formData.append("citie", Number(item.citie));
        formData.append("district", Number(item.district));
        formData.append("neighbor", Number(item.neighbor));
        formData.append("phone_number", Number(item.phone_number));

        axios
        .patch(`https://imobbis.pythonanywhere.com/user/${item.id}`, formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
        .then(res => {
            alert("Votre profil a bel et bien été  modifié !");
        }).catch(err => {
            alert('err'+err)
            console.log(err)
        })
    }

    render() {
        const user = this.state.user;
        return (
            <View style={styles.detailsContainer}>
                     <View style={{marginVertical:15,marginHorizontal:12,justifyContent:'center',alignItems:'center'}}>
                         <Text style={{fontWeight:'bold',fontSize:20}}>Mettez a jour votre Prrofil</Text>
                     </View>
                <TouchableOpacity style={{ marginTop: 15, height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'Select' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}> Photo profil </Text>
                    </View>

                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Image
                            style={{
                                paddingVertical: 30,
                                width: 60,
                                height: 30,
                                borderRadius: 50,
                                backgroundColor: 'silver',
                                marginHorizontal:10
                            }}
                            resizeMode='cover'
                            source={{ uri: user.profile_image.uri ? user.profile_image.uri : user.profile_image }}
                        />
                         
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                   onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'username' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Nom utilisateur</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14,fontFamily:'Helvetica' }}>{user.username ? user.username : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'first' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Nom</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.first_name ? user.first_name : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'last' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Prenom</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.last_name ? user.last_name : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => {this.setState({ isVisible: !this.state.isVisible, what_modal: 'countrie' }), this.initlocation()}}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Lieu</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ width: width_*0.7, alignItems: "flex-end"}} >
                          <Text style={{ fontSize: 14 }}>{user.countrie !== "" && user.countrie !== null ? this.state.location :null}</Text>
                        </View>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'phone_number' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "", marginLeft: 15 }}>Numéro telephone</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.phone_number ? user.phone_number : ""}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>
                <Divider style={{height:10,marginLeft:35}}/>
                <TouchableOpacity style={{ height: '16.5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                    onPress={() => this.setState({ isVisible: !this.state.isVisible, what_modal: 'gender' })}>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: "#7c3325", marginLeft: 15 }}>Genre</Text>
                    </View>
                    <View style={{ height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 14 }}>{user.gender !== "null" ? user.gender : "......"}</Text>
                        <Ionicons color="#7c3325" name="pencil" size={25} style={{marginHorizontal:10}} />
                    </View>
                </TouchableOpacity>

                <View style={{alignItems: "center", marginTop: 10}} >
                    <TouchableOpacity style={{height: 50, width: width_*0.3, backgroundColor: "#dc7e27", justifyContent: 'center' }}
                       onPress={() => this.save(this.state.user)} >
                            <Text style={{ fontSize: 18, textAlign: 'center', fontWeight: 'bold', color: "#7c3325" }}>Modifier</Text>
                    </TouchableOpacity>
                </View>

                {
                    //modal personnalisé
                }

                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => { console.log("Modal has been closed.") }}>
                    {
                        this.state.what_modal == 'Select' ?
                            (
                                <ScrollView style={styles.modal}>
                                   
                                    <PhotoUpload
                                        onResponse={(avatar) => {
                                            if (avatar.fileName !== undefined) {
                                                //console.log('Image base64 string: ', avatar);
                                                this.handleChange('profile_image', avatar)
                                            }
                                        }}
                                    >
                                        <Image
                                            style={{
                                                paddingVertical: 30,
                                                width: 150,
                                                height: 150,
                                                borderRadius: 75,
                                                backgroundColor: '#dc7e27',
                                                marginTop:12
                                            }}
                                            resizeMode='cover'
                                            source={{ uri: user.profile_image ? user.profile_image.uri : user.profile_image }}
                                        />
                                    </PhotoUpload>
                                    <View style={{ }} >
                                        
                                        <View style={{ flexDirection:'row',justifyContent:'center'}}>
                                            <Text style={{ textAlign: "center", color: "black", fontWeight: "bold",marginTop:15,fontSize:12 }} >Appuyer sur le cercle pour selectionner une option </Text>
                                            <MaterialCommunityIcons name="folder-image" size={30} style={{marginTop:10,marginLeft:3,color: "#7c3325",}}/>
                                        </View>
                                        <View style={{marginTop:10 }}>
                                            <TouchableOpacity style={{ backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold",}}>Terminé </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>

                            ) :
                            this.state.what_modal == 'username' ? (
                                <View style={styles.modal1}>
                                    <View >
                                        
                                        <View style={{}}>
                                            <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:25  }} >Changer votre nom d'utilisateur  </Text>
                                        </View>
                                       
                                    </View>
                                    <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                        value={user.username}
                                        placeholder="Entrez votre nom d'utilisateur.... "
                                        placeholderTextColor="#003f5c"
                                        onChangeText={val => this.handleChange('username', val)}
                                        style={{marginTop:70,textAlign:'center'}}
                                    />
                                     <View style={{marginTop:70 }}>
                                            <TouchableOpacity style={{ backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold",}}>Terminé </Text>
                                            </TouchableOpacity>
                                        </View>
                                </View>
                                
                                
                                ) :
                                this.state.what_modal == 'first' ? (
                                    <View style={styles.modal1}>
                                        <View style={{ }} >
                                            <View style={{}}>
                                                <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:25 }} >Changer votre nom</Text>
                                            </View>
                                           
                                        </View>
                                        <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                            value={user.first_name}
                                            placeholder="Entrez votre Nom ... "
                                            placeholderTextColor="#003f5c"
                                            onChangeText={val => this.handleChange('first_name', val)}
                                            style={{marginTop:70,textAlign:'center'}}
                                        />
                                         <View style={{ marginTop:70 }}>
                                                <TouchableOpacity style={{  backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                    <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold",}}>Terminé </Text>
                                                </TouchableOpacity>
                                            </View>
                                    </View>) :
                                this.state.what_modal == 'last' ? (
                                    <View style={styles.modal1}>
                                        <View style={{  }} >
                                            <View style={{ width: '25%' }}></View>
                                            <View style={{  }}>
                                                <Text style={{  textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:25 }} >Changer votre prenom </Text>
                                            </View>
                                            
                                        </View>
                                        <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                            value={user.last_name}
                                            placeholder="Entrez votre Prénom ... "
                                            placeholderTextColor="#003f5c"
                                            onChangeText={val => this.handleChange('last_name', val)}
                                            style={{marginTop:70,textAlign:'center'}}
                                        />
                                        <View style={{ marginTop:70}}>
                                                <TouchableOpacity style={{  backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center'}} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                    <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                                </TouchableOpacity>
                                            </View>
                                    </View>) :
                                    this.state.what_modal == 'phone_number' ? (
                                        <View style={styles.modal1}>
                                            <View style={{ }} >
                                                <View style={{  }}>
                                                    <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:25}} >Changer de numéro </Text>
                                                </View>
                                               
                                            </View>
                                            <TextInput style={{ borderWidth: 2, borderColor: "#d9d9d9", height: 60 }}
                                                value={user.phone_number}
                                                placeholder="phone number ... "
                                                placeholderTextColor="#003f5c"
                                                keyboardType="numeric"
                                                onChangeText={val => this.handleChange('phone_number', val)}
                                                style={{marginTop:70,textAlign:'center'}}
                                            />
                                             <View style={{ marginTop:70 }}>
                                                    <TouchableOpacity style={{ backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                        <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                                    </TouchableOpacity>
                                                </View>
                                        </View>) :
                                        this.state.what_modal == 'gender' ? (
                                            <View style={styles.modal1}>
                                                <View style={{  }} >
                                                    <View style={{ width: '25%' }}></View>
                                                    <View style={{ }}>
                                                        <Text style={{textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:25}} >Changer de genre </Text>
                                                    </View>
                                                    
                                                </View>
                                                <View style={{ padding: 40, flexDirection: "row", justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Text style={{ fontWeight: "bold" }} >
                                                            M
                                                        </Text>
                                                        <RadioButton
                                                            value="true"
                                                            status={user.gender === "M" ? 'checked' : 'unchecked'}
                                                            onPress={this.handleChangeradio}
                                                           
                                                        />
                                                    </View>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Text style={{ fontWeight: "bold" }}>
                                                            F
                                                        </Text>
                                                        <RadioButton
                                                            value="false"
                                                            status={user.gender === "F" ? 'checked' : 'unchecked'}
                                                            onPress={this.handleChangeradio}
                                                            style={{marginLeft:30}}
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ marginTop:70 }}>
                                                        <TouchableOpacity style={{ backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                            <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold"}}>Terminé </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                            </View>) :
                                            this.state.what_modal == 'countrie' ? (
                                                <View style={styles.modal1}>
                                                    <View style={{ flexDirection: "row" }} >
                                                        <View style={{ width: '25%' }}></View>
                                                        <View style={{ width: '50%' }}>
                                                            <Text style={{ textAlign: "center", color: "#7c3325", fontWeight: "bold",marginTop:20 }} >Changer de pays </Text>
                                                        </View>
                                                       
                                                    </View>
                                                    <View style={{ flexDirection: "row", marginTop: 20, fontSize: 16 }} >
                                                        <View style={{ width: '20%', borderBottomColor: '#dc7e27' , borderBottomWidth: 2 , paddingBottom: 10 }}>
                                                            <TouchableOpacity >
                                                                <Text style={{ color: '#dc7e27', marginLeft: 8 }}>Pays</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        <View style={{ width: '20%', borderBottomColor: user.countrie !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                                <TouchableOpacity >
                                                                    <Text style={{ color: user.countrie !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>Province</Text>
                                                                </TouchableOpacity>
                                                        </View>
                                                        <View style={{ width: '20%', borderBottomColor: user.province !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                                <TouchableOpacity >
                                                                    <Text style={{ color: user.province !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>citie</Text>
                                                                </TouchableOpacity>
                                                        </View>
                                                        <View style={{ width: '20%', borderBottomColor: user.citie !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2, paddingBottom: 10 }}>
                                                                <TouchableOpacity >
                                                                    <Text style={{ color: user.citie !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>District</Text>
                                                                </TouchableOpacity>
                                                        </View>
                                                        <View style={{ width: '20%', borderBottomColor: user.district !== null ? '#dc7e27' : 'silver', borderBottomWidth: 2 , paddingBottom: 10 }}>
                                                                <TouchableOpacity >
                                                                    <Text style={{ color: user.district !== null ? '#dc7e27' : 'silver', textAlign: "center" }}>Quartier</Text>
                                                                </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                    <View style={{ marginTop:150}}>
                                                            <TouchableOpacity style={{backgroundColor: "white", width: 100, alignSelf: 'flex-end',marginTop:25,height:50, marginHorizontal:20,alignItems:'center',justifyContent:'center' }} onPress={() => this.setState({ isVisible: !this.state.isVisible })}>
                                                                <Text style={{ color: "#dc7e27", textAlign: "center", fontSize: 14, fontWeight: "bold" }}>Terminé </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    <Card.Divider />
                                                         {
                                                            user.countrie == null ?
                                                             this.state.countrys.map((item, i) => (
                                                                <TouchableOpacity key={i} onPress={() => {this.handleChange('countrie', item.id), this.addlocation(item.name), this.setState({isVisible: item.id !== 1 ? false : true }) } }> 
                                                                    <Text style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10}}> {item.name} </Text> 
                                                                </TouchableOpacity>
                                                              )) : 

                                                              user.countrie == 1 ? (
                                                                <>
                                                                   {
                                                                      user.province == null ?
                                                                      this.state.countrys.map(count =>
                                                                          (count.id === Number(user.countrie)) ?
                                                                             <ScrollView >
                                                                                  {this.state.provinces.map(prov =>
                                                                                      count.provinces.map((province, i) =>
                                                                                          (province === prov.id) ? (
                                                                                              <TouchableOpacity key={i} onPress={() => {this.handleChange('province', prov.id), this.addlocation(prov.name) } }> 
                                                                                              <Text key={i} style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10}}> {prov.name} </Text> 
                                                                                              </TouchableOpacity> ) : null
                                                                                      )
                                                                                  ) }
                                                                              </ScrollView>: null
                                                                      ) : 
                                                                         user.citie == null ?
                                                                              this.state.provinces.map(prov =>
                                                                                  (prov.id === Number(user.province)) ?
                                                                                      <ScrollView >
                                                                                      {
                                                                                      this.state.citys.map(cit =>
                                                                                          prov.cities.map((idcit, i) =>
                                                                                              (idcit === cit.id) ? (
                                                                                              <TouchableOpacity key={i} onPress={() => {this.handleChange('citie', cit.id), this.addlocation(cit.name) } }> 
                                                                                                  <Text key={i} style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10}}> {cit.name} </Text> 
                                                                                              </TouchableOpacity>
                                                                                              ): null
                                                                                          ) 
                                                                                      )
                                                                                      }
                                                                                      </ScrollView> : null
                                                                              ) : 

                                                                              user.district == null ?
                                                                              this.state.citys.map(city =>
                                                                                  (city.id === Number(user.citie)) ?
                                                                                      <ScrollView >
                                                                                      {
                                                                                      this.state.districts.map(dist =>(
                                                                                          city.districts.map((idcit, i) =>
                                                                                              (idcit === dist.id) ? (
                                                                                              <TouchableOpacity key={i} onPress={() => {this.handleChange('district', dist.id), this.addlocation(dist.name) } }> 
                                                                                                  <Text key={i} style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10}}> {dist.name} </Text> 
                                                                                              </TouchableOpacity>
                                                                                              ): null
                                                                                          ) )
                                                                                      )
                                                                                      }
                                                                                      </ScrollView> : null
                                                                              ) : 
                                                                              user.neighbor == null ?
                                                                              this.state.districts.map(di =>
                                                                                  (di.id === Number(user.district)) ?
                                                                                      <ScrollView >
                                                                                      {
                                                                                      this.state.neighbors.map(neig =>(
                                                                                          di.neighbors.map((idcit, i) =>
                                                                                              (idcit === neig.id) ? (
                                                                                              <TouchableOpacity key={i} onPress={() => {this.handleChange('neighbor', neig.id), this.setState({isVisible: false}), this.addlocation(neig.name) } }> 
                                                                                                  <Text key={i} style={{ color: 'white', textAlign: "center", fontSize: 20, marginBottom: 10}}> {neig.name} </Text> 
                                                                                              </TouchableOpacity>
                                                                                              ): null
                                                                                          ) )
                                                                                      )
                                                                                      }
                                                                                      </ScrollView> : null
                                                                              ) : null
                                                                  }
                                                                </>
                                                              ) : null
                                                         }
                                                </View>) : null

                    }

                </Modal>

                {
                    //fin modal personnalisé
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    detailsContainer: {
        height: '58%',
    },
    modal: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.50,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    modal1: {
        backgroundColor: "silver",
        borderRadius: 20,
        //padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        width: width_,
        height: height_ * 0.90,
        marginTop: height_ * 0.4,
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
})