import React, { Component } from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { Switch} from 'react-native-paper';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Settings extends Component{
    constructor(props){
        super(props)
            this.state={

            }        
    }



    clearAsyncStorage = async () => {        
        AsyncStorage.removeItem('user'); 
        AsyncStorage.removeItem('user_data'); 
        this.props.navigation.navigate('AuthStack')
    }   

    render(){
        return(
            <View>
                <TouchableOpacity style={[styles.signIn]}
                        onPress={() => this.clearAsyncStorage()}
                    >
                        <Ionicons name='md-exit-outline' color='white' size={33}/>
                        <Text style={{fontSize: 25, color: 'white', marginLeft: 5}}>Se deconnecter</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    signIn: {
        width: '70%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        shadowColor: "black", borderRadius: 20,
        shadowOffset: {
          width: 6,
          height: 6,
        },
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        elevation: 7,
        marginTop: 7,
        alignSelf: 'center',
        backgroundColor: '#ff4d4d',
        marginTop: 40,
        flexDirection: 'row'
        
    },
})