import React, {Component} from 'react'
import {Text, View, StyleSheet, Dimensions, Platform, TouchableOpacity} from 'react-native'
import { Avatar } from 'react-native-paper'
import DeviceInfo from 'react-native-device-info';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Fontisto from 'react-native-vector-icons/Fontisto'
var width_ = Math.round(Dimensions.get('window').width)

export default class RentHome extends Component{
    constructor(props){
        super(props)
        this.state = {
            
        }
    }

    render(){
        return(
            <View style={{padding: 5}}>
                <Text style={{fontSize: 25, color: 'orange', fontWeight: 'bold', marginLeft: 10, marginBottom: 15}}>Ma maison</Text>
                <View style={{ ...styles.row3,}}>
                    <View style={{flexDirection: 'row', marginTop: 10, ...styles.row2, width: '100%'}}>
                        <Avatar.Image source={{uri: 'sdsds.jpg'}} size={50}/>
                        <View style={{marginLeft: 10, marginTop: 5}}>
                            <Text style={{fontSize: 18, fontWeight: 'bold'}}>Duplex de fortune haut standing</Text>
                            <Text style={{color: 'grey'}}>4chrs / 2 douches / 1 salon</Text>
                            <View style={{borderBottomWidth: 1, marginVertical: 5, borderBottomColor: '#e6e6e6'}}/>
                            <View style={{...styles.row2}}>
                                <Text style={{color: 'orange', fontWeight: 'bold'}}>Mon bailleur</Text>
                                <View style={{flexDirection: 'row', marginTop: 5}}>
                                    <Avatar.Image source={{uri: 'sdsds.jpg'}} size={30}/>
                                    <View style={{marginLeft: 5}}>
                                        <Text style={{fontSize: 14, fontWeight: 'bold'}}>Agent Brice yang </Text>
                                        <Text style={{color: 'grey', fontSize: 12}}>Menbre depuis 2 ans, 20 maisons postees</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 15}}>
                        <TouchableOpacity style={{width: '32%', alignItems: 'center'}}
                                onPress ={() =>this.props.navigation.navigate('RecentlyTouched' ,{'visited': 'item', user_id: user_id})}>
                            <AntDesign name='home' size={31} color = '#664200'/>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 7}}>90 jours</Text>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 2}}>restants</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width: '32%', alignItems: 'center'}}
                                onPress ={() =>this.props.navigation.navigate('RecentlyTouched' ,{'visited': 'item', user_id: user_id})}>
                            <Fontisto name='lightbulb' size={31} color = '#664200'/>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 7}}>20000 XAF </Text>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 2}}>consommes</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width: '32%', alignItems: 'center'}}
                                onPress ={() =>this.props.navigation.navigate('RecentlyTouched' ,{'visited': 'item', user_id: user_id})}>
                            <Ionicons name='water-outline' size={30} color = '#664200'/>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 7}}>2000 XAF</Text>
                            <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 2}}>consommes</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{color: 'grey', fontSize: 12, marginTop: 10, paddingBottom: 10, alignSelf: 'flex-end', marginRight: 10}}>
                        Derniere mise a jour date de 2 jours
                    </Text>
                </View>
                <View style={{paddingHorizontal: 20, marginTop: 15}}>
                    <TouchableOpacity style={{...styles.button1, backgroundColor: '#b3e0ff'}}>
                        <Text style={styles.text}>Payer mes factures</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{...styles.button1, backgroundColor: '#99ffbb'}}>
                        <Text style={styles.text}>Ecrire au bailleur</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{...styles.button1, backgroundColor: '#ffcc99'}}>
                        <Text style={styles.text}>Contater un specialiste</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{...styles.button1, backgroundColor: 'red'}}>
                        <Text style={styles.text}>Se plaindre du bailleur</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "orange",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        padding: 6,elevation: 9,
        marginVertical: 6,
        borderRadius: 20, margin: 5
    },
    row2: {
        borderRadius: 15, borderColor: '#e6e6e6', borderWidth: 2, marginTop: 10, padding: 5, 
        width: width_*0.75
    },
    button1: {
        borderWidth: 2, flexDirection: 'row', height: 50, marginTop: 30,
        borderRadius: 30, marginLeft: 10, justifyContent: 'center', alignItems: 'center'
    },
    text: {
        fontSize: 16, fontWeight: 'bold'
    }
})