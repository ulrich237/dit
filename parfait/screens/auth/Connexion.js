import React, { Component } from 'react';
import {
  View, Text, Button, StyleSheet, Modal, Alert,
  Dimensions, TextInput, TouchableOpacity, Platform, Image, ScrollView, ActivityIndicator,
} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import Form from 'react-native-form'
import { Divider, Badge } from 'react-native-elements'
import _ from 'underscore'
//import RNHTMLtoPDF from 'react-native-html-to-pdf';
//import RNImageToPdf from 'react-native-image-to-pdf';

import Icon from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RadioButton } from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import axios from 'axios'
import FontAwesome from 'react-native-vector-icons/FontAwesome';

var height_ = Math.round(Dimensions.get('window').height);
var width_ = Math.round(Dimensions.get('window').width);
export default class Connexion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      withFlag: true,
      enterCode: false,
      codeconfirm: '',
      confirm: null,
      phone: '',
      atente: false,
      modalVisible2: false, modalPays: false,
      country: {
        cca2: 'CM',
        callingCode: '237',
        calling: "+237 Cameroon 🇨🇲",
        name: "Cameroon"
      },
      users: [],
      countries: [],
      districts: [],
      cities: [],
      provinces: [],
      neighbours: [],
      nom_pays: '',
      user: {
        username: "",
        countrie: 40,
        phone_number: null
      },
      etape: 1, filter: '',
      block: false,
      animating: false,
      visible: false,
      user_: null

    }
  }
  closeActivityIndicator = () => setTimeout(() => this.setState({
    animating: false
  }))


  componentDidMount() {

    axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
      this.setState({ users: res.data })
    }).catch(err => console.log(err));

    axios.get("https://imobbis.pythonanywhere.com/location/").then(res => {
      this.setState({
        countries: res.data.country,
        cities: res.data.city,
        //neighbours: res.data.neighbor
      });
    });

    this.closeActivityIndicator();



  }

  _changeCountry = (country) => {
    let user = { ...this.state.user, countrie: null }
    /*this.state.countries.map(country_ =>
      country_.indicatif == country.callingCode && country_.name == country.name ?
        user = { ...this.state.user, countrie: country_.id } : null)*/
    user = { ...this.state.user, countrie: _.find(this.state.countries, { name: country?.name })?.id }
    this.setState({ country: country, user: user });

  }

  handleChange = (name, value) => {
    const user = { ...this.state.user, [name]: value };
    this.setState({ user });

  };

  async confirmVerificationCode(code) {
    try {
      await this.state.confirm.confirm(code) ?
        (
          this.state.user_ ?
            (
              this.displayData(this.state.user_),
              this.props.navigation.navigate('home'), this.setState({ modalVisible2: false, confirm: null, animating: false, block: false, })
            ) :
            (this.setState({
              modalVisible2: true, confirm: null,
              nom_pays: _.find(this.state.countries, { id: this.state.user.countrie })?.name
            }))
        ) : null;
    } catch (error) {
      this.setState({ animating: false, block: false, })
      if (error.code == 'auth/invalid-verification-code') {
        Alert.alert(
          'Attention',
          'Invalid code.');
      } else {
        Alert.alert('Error',
          'Account linking error');
      }
    }
    /* this.setState({
       modalVisible2: true, confirm: null,
       nom_pays: _.find(this.state.countries, { id: this.state.user.countrie })?.name
     })*/
  }


  getUsert = (phone_number) => {

    axios.get("https://imobbis.pythonanywhere.com/new/PhoneRequest/" + phone_number).then(res => {
      //existe
      this.setState({ user_: res.data })
      this.verify_user(phone_number, res.data)
    }).catch(err => {
      // existe pas
      if (err) {
        console.log(err)
      }

    });
  }

  async verify_user(phone_number) {
    // mettre ici get en fonction du numero
    try {
      const confirmation = await auth().signInWithPhoneNumber('+' + this.state.country.callingCode + phone_number);
      this.handleChange('phone_number', phone_number);
      this.setState({ confirm: confirmation });
      await auth().onAuthStateChanged((userd) => {
        if (userd) {
          axios.get("https://imobbis.pythonanywhere.com/new/PhoneRequest/" + phone_number).then(res => {
            this.displayData(res?.data),
              this.setState({ user_: res.data }),
              this.props.navigation.navigate('home'), this.setState({ confirm: null, animating: false, block: false, })

          }).catch(err => {
            if (err.toString() == 'Error: Request failed with status code 500') {
              (this.setState({
                modalVisible2: true, confirm: null,
                nom_pays: _.find(this.state.countries, { id: this.state.user.countrie })?.name
              }))
            }
          });
        } else {
          this.setState({ enterCode: true, atente: false, animating: false, block: false, })
          this.handleChange('phone_number', phone_number);
        }
      })
    } catch (error) {
      this.setState({ animating: false, block: false, })
      if (error.code == 'auth/network-request-failed') {
        Alert.alert(
          'Erreur',
          'Vérifier votre connexion internet.');
      } else {
        if (error.code == 'auth/invalid-phone-number') {
          Alert.alert(
            'Erreur',
            'Vérifier votre numéro de téléphone.');
        }
        else {
          if (error.code == 'auth/too-many-requests') {
            alert(error.code),
              this.handleChange('phone_number', phone_number),
              axios.get("https://imobbis.pythonanywhere.com/new/PhoneRequest/" + phone_number).then(res => {
                this.displayData(res?.data),
                  this.setState({ user_: res.data }),
                  this.props.navigation.navigate('home'), this.setState({ confirm: null, animating: false, block: false, })
              }).catch(err => {
                if (err.toString() == 'Error: Request failed with status code 500') {
                  (this.setState({
                    modalVisible2: true, confirm: null,
                    nom_pays: _.find(this.state.countries, { id: this.state.user.countrie })?.name
                  }), Alert.alert('Attention', 'Vôtre numéro est trés utile.'))
                }
              });
          } else {
            Alert.alert(
              'Erreur',
              error.code)
          }
        }
      }
      //alert("Soit vous êtes bloqué pour 24h pour trop de tentative, soit votre connexion n'est pas activée ou votre numero est invalide pour ce pays.");
    }
    //this.setState({ enterCode: true, atente: false })
    //this.handleChange('phone_number', phone_number);  
  }

  displayData = async (id) => {
    try {
      await AsyncStorage.setItem('user_data', id?.id.toString());
      await AsyncStorage.setItem('user', JSON.stringify(id));
      await AsyncStorage.setItem('myuser', JSON.stringify(id));
    } catch { error => alert(error) };
  }

  save(item) {

    item.username = item.username.toLowerCase();
    item.countrie = Number(item.countrie);
    item.phone_number = Number(item.phone_number);
    this.state.users.find(user_ => user_.username.toLowerCase() == item.username.toLowerCase()) ? alert("Nous sommes désolé, ce nom d'utilisateur existe déjà. Veuillez en choisir un autre.") :
      axios
        .post(`https://imobbis.pythonanywhere.com/new/utilisateur/`, item)
        .then(res => {
          this.displayData(res.data),
            //alert("Votre profil a bel et bien été  créé !");
            this.setState({ etape: 2 })
        }).catch(err => {
          err.toString() == 'Error: Network Error' ? alert("Veuillez vérifier votre connexion internet, puis réessayer") : Alert.alert("Attention", err.toString());
          // console.log(err)
        })

  }

  // blocker le boutton
  DisableButtonFunction = () => {
    this.setState({
      block: true,
    })

  }

  render() {
    const { modalVisible2, user, countries, modalPays, filter, visible, country } = this.state;
    const animating = this.state.animating;
    return (
      <ScrollView>
        <View style={{
          height: "40%", alignItems: 'center',
          justifyContent: 'center', shadowOpacity: 2, backgroundColor: '#dc7e27',
          borderRadius: 5, borderBottomRightRadius: 110,
        }}>
          <Image source={require('./images/logo.jpeg')} style={{ width: 180, height: 150, marginTop: '10%', borderRadius: 50 }} />
        </View>
        {
          !this.state.enterCode ?
            <View style={{ marginLeft: '5%' }}>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ marginTop: '10%', fontWeight: 'bold', fontSize: 20, }}>Se Connecter </Text>
              </View>
              <Text style={{ color: 'grey', marginTop: '2%', color: '#7c3325', marginTop: '3%' }}>Entrer le code de votre pays</Text>
              <Text style={{ color: 'grey', color: '#7c3325' }}>Entrer votre numéro de téléphone </Text>
            </View> : null
        }

        <View>
          <Form style={{ margin: 20 }}>
            {//ref={'form'}
              !this.state.enterCode ?
                <View>
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => {
                      this.setState({ visible: true, })
                    }}>
                    <CountryPicker
                      //ref={'countryPicker'}
                      withFlag={true}
                      withEmoji={true}
                      withFilter={true}
                      withCountryNameButton={true}
                      withAlphaFilter={true}
                      withCallingCode={true}
                      //withEmoji={true}
                      //closeable
                      visible={visible}
                      style={{ alignItems: 'center', justifyContent: 'center', borderWidth: 2, borderColor: 'red', color: "yellow" }}
                      onSelect={this._changeCountry}
                      translation='eng'
                    />
                    <FontAwesome name='sort-desc' size={23} style={{ marginTop: -4, marginLeft: 5, color: '#7c3325' }} />
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginLeft: 5, color: '#7c3325' }}>
                        {'+' + country.callingCode + ' ' + country.name}</Text>
                    </View>
                  </TouchableOpacity>

                </View> :
                <View style={{ marginLeft: '5%', marginVertical: 120 }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 20 }}>Saisir le code de confirmation</Text>
                </View>
            }

            <View style={{ flexDirection: 'row' }}>
              {
                !this.state.enterCode ?
                  <View style={{ marginTop: 8 }}>
                    <Text>{this.state.country.callingCode.indexOf('+') == -1 ? '+' : null} {this.state.country.callingCode}</Text>
                    <Divider style={{ backgroundColor: "black", marginLeft: '10%', height: 1, width: '290%' }} />
                  </View> : null
              }

              <View style={{ marginLeft: !this.state.enterCode ? '30%' : '10%', marginTop: this.state.enterCode ? -100 : null, }}>
                <TextInput
                  style={{ width: "190%", fontSize: 15, color: '#7c3325', marginHorizontal: this.state.enterCode ? '30%' : null }}
                  //ref={'textInput'}
                  name={this.state.enterCode ? 'code' : 'phoneNumber'}
                  type={'textInput'}
                  underlineColorAndroid={'transparent'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  onChangeText={(val) => !this.state.enterCode ? this.setState({ phone: val }) : this.setState({ codeconfirm: val })}
                  placeholder={this.state.enterCode ? '_ _ _ _ _ _' : 'Numero de telephone...'}
                  keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
                  returnKeyType='go'
                  //autoFocus
                  placeholderTextColor="grey"
                  selectionColor="#dc7e27"
                  maxLength={this.state.enterCode ? 6 : 20}
                  onSubmitEditing={this._getSubmitAction}
                />
              </View>
            </View>
          </Form>

          {
            !this.state.enterCode ?
              <View>
                <TouchableOpacity
                  style={{ backgroundColor: this.state.block ? '#E9C8B2' : '#dc7e27', margin: '5%', borderRadius: 5, height: 50, justifyContent: 'center', alignItems: 'center', borderBottomRightRadius: 100 }}
                  onPress={() => {
                    this.verify_user(this.state.phone),
                      //blocker le boutton
                      this.DisableButtonFunction(),
                      this.setState({ atente: true, animating: true })
                  }}
                  //propriete de blockage du touvhable
                  disabled={this.state.block}
                >
                  <Text style={{ fontSize: 20, color: '#fff' }} >Suivant</Text>

                </TouchableOpacity>
                <View style={styles.preloader}>
                  <ActivityIndicator animating={animating} size="large" color="#7c3325" />
                </View>
              </View>

              : null
          }


          {
            this.state.atente == true ?
              <View style={{ alignItems: 'center', marginLeft: '-3%', marginTop: -20 }}>
                <Text style={{ fontWeight: "bold", color: "#7c3325" }}>Veuillez patienter ... </Text>
              </View> : null
          }

          {
            this.state.codeconfirm.length == 6 ?
              <View>
                <TouchableOpacity
                  style={{
                    backgroundColor: this.state.block ? '#E9C8B2' : '#dc7e27', margin: '5%',
                    borderRadius: 5, height: 50, justifyContent: 'center', alignItems: 'center', borderBottomRightRadius: 100
                  }}
                  onPress={() => {
                    this.confirmVerificationCode(this.state.codeconfirm),
                      //this.props.navigation.navigate('home')
                      this.DisableButtonFunction(),
                      this.setState({ animating: true })
                  }
                  }

                //disabled={this.state.block}
                >
                  <Text style={{ fontSize: 20, color: '#fff' }}>Vérifier</Text>
                </TouchableOpacity>
                <View style={styles.preloader}>
                  <ActivityIndicator animating={animating} size="large" color="#7c3325" />
                </View>

              </View>
              : null
          }

        </View>

        <View style={{ height: 20 }}></View>



        <Modal animationType="slide"
          transparent={true}
          visible={modalVisible2}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setState({ modalVisible2: !modalVisible2 });
          }}>

          <View style={{ ...styles.centeredView, marginTop: 53 }}>
            <View style={styles.modalView}>
              <View style={{ width: '100%', flexDirection: 'row' }} >
                <TouchableOpacity
                  style={{ width: '10%' }}
                  onPress={() => this.setState({ modalVisible2: !modalVisible2 })}
                >
                  <Icon name="times" size={25} style={{ marginLeft: 2 }} />
                </TouchableOpacity>
                <View style={{ marginLeft: 20 }}>
                  <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold", color: '#7c3325' }}>Création du compte :</Text>
                </View>

              </View>
              <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />

              <ScrollView>
                {
                  this.state.etape == 1 ?
                    <>

                      <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                        <Text style={{ fontSize: 16, textAlign: 'center', fontWeight: "bold" }}> Inscription </Text>
                        <View style={{ backgroundColor: "pink", marginBottom: 10 }} >
                          <Text style={{ fontSize: 10 }}>Votre numéro de téléphone a été confirmé. Veuillez compléter les informations çi-dessous pour créer votre compte. Les champs(*) sont obligatoires.</Text>
                        </View>
                      </View>


                      <View style={{ flexDirection: 'row', }}>
                        <View style={{ flexDirection: 'row' }}>
                          <Text style={{ marginLeft: '5%', textDecorationStyle: 'solid', fontSize: 13, fontWeight: 'bold' }}>
                            Pays :
                          </Text>
                          {
                            this.state.nom_pays !== '' && this.state.nom_pays != undefined ?
                              <Text style={[styles.touch, { marginTop: 0, marginLeft: 6 }]}>{this.state.nom_pays}</Text>
                              : _.find(countries, { id: user.countrie }) != undefined ?
                                <Text style={[styles.touch, { marginTop: 0, marginLeft: 6 }]}>{_.find(countries, { id: user.countrie }).name}</Text>
                                :
                                <TouchableOpacity
                                  onPress={() => this.setState({ modalPays: true })}>
                                  <Text style={[styles.touch, { marginTop: 0, marginLeft: 6, fontSize: 11, color: 'red' }]}>
                                    Appuyer ici pour sélectionner le pays.</Text>
                                </TouchableOpacity>

                          }
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ modalPays: true })}>
                          <FontAwesome style={{ marginTop: -15, marginLeft: 5 }}
                            name='caret-up'
                            size={45}
                            color='#7c3325'
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={{ flexDirection: 'row', marginTop: "7%", marginBottom: '7%' }}>
                        <Text style={{ marginLeft: '5%', fontSize: 13, fontWeight: 'bold' }}>
                          Téléphone :
                        </Text>
                        <Text style={[styles.touch, { marginTop: 0, marginLeft: 6 }]} >
                          {this.state.user.phone_number}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row', }}>
                        <TextInput
                          style={{ marginLeft: '5%', color: 'black' }}
                          value={this.state.user.username}
                          placeholder="Entrez  votre nom d'utilisateur(*)..."
                          onChangeText={(val) => this.handleChange('username', val)}
                          placeholderTextColor="grey"
                          underlineColorAndroid={'transparent'}
                        >
                        </TextInput>
                      </View>
                      <Divider style={{ backgroundColor: 'grey', height: 1, marginTop: 1, width: 350, marginHorizontal: '5%' }} />
                      {
                        this.state.user.username !== "" && this.state.user.countrie ?
                          <View style={{
                            height: 50, marginLeft: 50, marginTop: 30

                          }}>
                            <TouchableOpacity
                              style={{ backgroundColor: '#7c3325', borderRadius: 12, alignItems: 'center', justifyContent: 'center', height: 30, width: 100, marginLeft: 135 }}
                              onPress={() => this.save(this.state.user)}
                            >
                              <Text style={{ fontSize: 15, fontWeight: "bold", color: 'white' }}>Enregistrer</Text>

                            </TouchableOpacity>
                          </View>

                          :
                          <View style={{
                            height: 50, alignItems: 'center', justifyContent: 'center'

                          }}>
                            <Text style={{
                              color: '#7c3325', fontSize: 15, fontWeight: "bold", marginLeft: 0

                            }}>*Insérer le nom d'utilisateur*</Text>
                          </View>
                      }
                    </> :
                    <>

                      <View style={{ marginLeft: '5%', marginTop: '1%', marginBottom: '2%', marginRight: '5%' }} >
                        <View style={{ height: "25%", alignItems: 'center', justifyContent: 'center', marginBottom: '2%', shadowOpacity: 2, backgroundColor: 'white', borderRadius: 5 }}>
                          <Image source={require('./images/logo.jpeg')} style={{ width: 180, height: 160, marginTop: '10%', borderRadius: 50 }} />
                        </View >
                        <Text style={{ fontSize: 18, marginTop: '3%', color: '#7c3325', fontWeight: "bold" }}>Bienvenue
                          <Text style={{ fontSize: 16, color: 'black', fontWeight: "bold" }}>M/Mme {user.username}
                          </Text>
                        </Text>

                        <Text style={{ fontSize: 14, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325', marginTop: '5%', marginBottom: '2%', fontWeight: "bold" }}>Qui sommes nous ?  </Text>
                        <Text style={{ fontSize: 12 }}>
                          <Text style={{ fontSize: 14, color: '#7c3325', fontWeight: "bold" }}> IMOBBIS
                          </Text> est le tout premier Reseau Social Immobilier au monde. Nous sommes une entreprise dont l'objectif est de valoriser l'immobilier Africain et résoudre le problème de logement en Afrique. </Text>

                        <Text style={{ fontSize: 14, textDecorationLine: 'underline', textDecorationStyle: 'solid', color: '#7c3325', marginTop: '5%', marginBottom: '2%', fontWeight: "bold" }}>Que faisons nous ?  </Text>
                        <Text style={{ fontSize: 12, fontWeight: "bold" }}> Avec <Text style={{ fontSize: 14, color: '#7c3325', fontWeight: "bold" }}> IMOBBIS </Text>, vous pourrez</Text>
                        <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text> Trouver facilement un logement en une recherche</Text>
                        <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text> Valoriser vos biens immobiliers et avoir plus de visibilité( si vous êtes bailleur, agent imobilier ou une agence immobiliere)</Text>
                        <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text>Manager vos biens (gestion locative, gérer jusqu'à 100 logements et locataires, générer les contrats de bail de façon automatique)</Text>
                        <Text style={{ fontSize: 10 }}> <Text style={{ fontSize: 12, color: 'orangered', fontWeight: "bold" }}> - </Text> Trouvez les spécialistes de l'immobilier (ingénieur de Génie Civil, Décorateur d'interieur, peintre, Carreleur, Plombier ...)</Text>
                      </View>

                      <TouchableOpacity
                        style={{ backgroundColor: '#7c3325', marginTop: '2%', marginLeft: height_ * 0.2, marginRight: "5%", borderRadius: 20, height: 50, justifyContent: 'center', alignItems: 'center' }}
                        onPress={() => this.props.navigation.navigate('home')} >
                        <Text style={{ fontSize: 15, color: '#fff' }}  >Continuer</Text>
                      </TouchableOpacity>

                      <View style={{ height: 5 }}></View>
                    </>
                }
              </ScrollView>
            </View>
          </View>
        </Modal>



        <Modal animationType="slide"
          transparent={true}
          visible={modalPays}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setState({ modalPays: !modalPays });
          }}>
          <View style={styles.modal}>
            <TouchableOpacity style={{ flexDirection: 'row' }}
              onPress={() => this.setState({ modalPays: false })}>
              <FontAwesome style={{}}
                name='close'
                size={25}
                color='red'
              />
            </TouchableOpacity>
            <TextInput
              style={styles.touch}
              onChangeText={(val) => this.setState({ filter: val })}
              value={this.state.filter}
              placeholder="Rechercher..."
            />
            <Divider style={{ backgroundColor: 'grey', width: '100%', height: 2, marginTop: 8 }} />
            <ScrollView style={{

            }}>
              {
                countries.filter((value) => {
                  return (value.name.toLowerCase().indexOf(filter.toLowerCase()) > -1)
                }).map((user) => {
                  return (
                    <TouchableOpacity style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-evenly' }}
                      onPress={() => { this.setState({ modalPays: false, nom_pays: "" }), this.handleChange('countrie', user.id) }}>
                      <Text>{user.name}</Text>

                    </TouchableOpacity>
                  )
                })
              }
            </ScrollView>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
  modalView: {
    margin: 10,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 15,
    alignItems: "flex-start",
    shadowColor: "#000000",
    width: '95%',
    height: '95%',
    shadowOffset: {
      width: 30,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 15
  },
  modal: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 2
    },
    width: width_,
    height: height_,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
  buttonstyle: {
    backgroundColor: 'white',
    height: 30,
    justifyContent: 'center'
  },
  touch: {
    color: '#7c3325', marginLeft: '5%', marginTop: -13

  },
  preloader: {
    left: 0,
    right: 0,
    top: '50%',
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

