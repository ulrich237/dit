import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeBottomTabs from '../normalUser/homeBotNav';

import Connexion from './Connexion'


const RootStack = createStackNavigator();

const AuthStack = (props) => (
  <RootStack.Navigator>
        <RootStack.Screen name="Connexion" component={Connexion} options={{headerShown: false}}/>
        <RootStack.Screen name='home' component={HomeBottomTabs} options={{headerShown: false}}/>
  </RootStack.Navigator>
);

export default AuthStack;

// <RootStack.Screen name="PhoneNumber" component={PhoneNumber} options={{headerShown: false}}/>