import auth from '@react-native-firebase/auth';

import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import _ from 'underscore'

export default async function signInWithFacebook({navigation}) {
  var user ={
    first_name: null,last_name: null, email: null, 
    user_name: null, image_url: null, id: null
  }

  generateString = (length) => {
    let result = ' '; 
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

  userCheck = (user)=> {
    axios.get('https://imobbis.pythonanywhere.com/user/')
    .then(res => {
        var user_find = _.find(res.data, {email: user.email})
        !user_find
        ?
            (   
                axios.post('https://imobbis.pythonanywhere.com/user/', {
                    "password": "kjljlkklkjlkjl",
                    "username": this.generateString(20),
                    "first_name": user.first_name,
                    "last_name": user.last_name,
                    "email": user.email,
            })
            .then(res => {
                AsyncStorage.setItem( 'user_data', JSON.stringify( res.data ) );
                this.setState({user: res.data})
            })
            .catch(error => alert(error))
                
            )
        : (AsyncStorage.setItem( 'user_data', JSON.stringify( user_find )), this.setState({user: user_find}));
    this.props.navigation.navigate('home')
    }).catch(error => alert(error))
    
  }


  // Attempt login with permissions
  const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

  if (result.isCancelled) {
    alert('You have cancelled ');
  }

  // Once signed in, get the users AccesToken
  const data = await AccessToken.getCurrentAccessToken();

  if (!data) {
    alert('Something went wrong obtaining access token');
  }
  // Create a Firebase credential with the AccessToken
  const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

  // Sign-in the user with the credential
  
  AccessToken.getCurrentAccessToken().then(
    (data) => {
      let accessToken = data.accessToken
      const responseInfoCallback = (error, result) => {
        if (error) {
          console.log(error)
          alert('Error fetching data: ' + error.toString());
        } else {
          user.user_name = generateString(20),
          user.last_name = result.last_name,
          user.email = result.email,
          user.first_name = result.first_name,
          user.image_url = result.picture.data.url
          userCheck(user)
          
        }
      }
      const infoRequest = new GraphRequest(
        '/me',
        {
          accessToken: accessToken,
          parameters: {
            fields: {
              string: 'email,name,first_name,last_name, picture.type(large)'
            }
          }
        },
        responseInfoCallback
      );
      // Start the graph request.
       new GraphRequestManager().addRequest(infoRequest).start()
       return auth().signInWithCredential(facebookCredential);

    }
  )

}