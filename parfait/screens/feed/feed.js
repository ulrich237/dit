import React, { Component, Suspense } from "react";
import {
    View, Image, Text, Picker, TouchableOpacity, Alert, Platform,
    TextInput, Button, Modal, ScrollView, StyleSheet, Dimensions, Share
} from "react-native";
import { Checkbox, Avatar } from 'react-native-paper';
import Entypo from 'react-native-vector-icons/Entypo';
import { Divider } from 'react-native-elements'
import axios from 'axios'
import Icon from "react-native-vector-icons/FontAwesome";
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicon from "react-native-vector-icons/Ionicons";
import _ from 'underscore';
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import NumericInput from 'react-native-numeric-input'
import AsyncStorage from '@react-native-async-storage/async-storage'

var width_ = Math.round(Dimensions.get('window').width)
var height_ = Math.round(Dimensions.get('window').height)

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom;
};

export default class Feed extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cats: [], houses: [], user: [], imageshouses: [], userid: 1,
            neighbors: [], citys: [], likes: [], modiflike: false,
            scrollPosition: 0, isLoading: false,  isLoading1: false
        }
    }

    componentDidMount() {

        axios.get(`https://imobbis.pythonanywhere.com/new/like`)
            .then(res => {
                this.setState({ likes: res.data })
            });

        axios.get(`https://imobbis.pythonanywhere.com/new/home/${this.state.userid}`,)
            .then(response => {
                this.setState({ houses: response.data })
             })
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/house/cat`,)
            .then(response => this.setState({ cats: response.data }),)
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(resu => {
            this.setState({ user: resu.data })
        }).catch(err => console.log(err));

        axios.get(`https://imobbis.pythonanywhere.com/location/`)
            .then(res => {
                this.setState({
                    neighbors: res.data.neighbors, citys: res.data.city
                })
            });
    }

    refresh_depart = () => {
        console.log("depart");
        this.setState({ isLoading1: true });
        axios.get(`https://imobbis.pythonanywhere.com/new/sited/${this.state.userid}`,)
            .then(response => {
                let houses = [];
                response.data.map(hous_ => houses.push(hous_));
                this.state.houses.map(house_ => _.find(houses, {id: house_.id}) ? null : houses.push(house_));
                this.setState({ houses: houses, isLoading1: false })
            })
            .catch(error => this.setState({ error, isLoading1: false }));

            axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error }));
    }

    refresh = () => {
        console.log("bonjour");
        this.setState({ isLoading: true });
        axios.get(`https://imobbis.pythonanywhere.com/new/home/${this.state.userid}`,)
            .then(response => {
                let houses = this.state.houses;
                response.data.map(house_ => houses.push(house_))
                this.setState({ houses: houses, isLoading: false })
            })
            .catch(error => this.setState({ error, isLoading: false }));

        axios.get(`https://imobbis.pythonanywhere.com/new/image`,)
            .then(response => this.setState({ imageshouses: response.data }),)
            .catch(error => this.setState({ error }));
    }

    handleScroll = (event) => {
        const scrollPosition = event.nativeEvent.contentOffset.y;
        scrollPosition == 0 ? this.refresh_depart() : null;
        //this.setState({ scrollPosition: scrollPosition })
    }

    save_like = (homeid, user) => {
        let exist = _.find(this.state.likes, { user: user, house: homeid }) ? true : false;
        let item = exist == false ? {
            house: Number(homeid),
            user: Number(user),
        } : { id: _.find(this.state.likes, { user: user, house: homeid })?.id };
        console.log("id " + item.id);
        (exist == false ?
            axios.post(`https://imobbis.pythonanywhere.com/new/like`, item) :
            axios.delete(`https://imobbis.pythonanywhere.com/new/like/${item.id}`))
            .then(response => {
                axios.get(`https://imobbis.pythonanywhere.com/new/like`)
                    .then(res => {
                        this.setState({ likes: res.data })
                    });
                console.log("le post a été bien effectué !")
            }).catch(error => {
                console.log("error " + error)
            })

    }

    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'https://play.google.com/store/apps/details?id=com.imobbis.app',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    render() {
        const { cats, houses, user, imageshouses, userid, neighbors, citys, likes } = this.state;
        let user_; let imageshouse_; let isLoading = this.state.isLoading;
        let isLoading1 = this.state.isLoading1;

        return (
            <View>
                <View > 
                    <View style={{
                        height: height_ / 15, justifyContent: 'space-between', flexDirection:
                            'row', marginBottom: 3, backgroundColor: '#dc7e27'
                    }}>
                        <View style={{ paddingLeft: 7 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 3 }}>
                                <TouchableOpacity style={{ margin: 10 }}
                                    onPress={() => this.props.navigation.openDrawer()}
                                >
                                    <AntDesign name='bars' color='white' size={19} />
                                </TouchableOpacity>
                                <Text style={{ margin: 10, color: 'white', marginTop: 5, fontSize: 18, fontWeight: 'bold' }}>IMOBBIS</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{
                            padding: 6, flexDirection: 'row', margin: 10, marginTop: 3
                        }}
                            onPress={() => this.props.navigation.navigate('filter_Feed')}
                        >
                            <AntDesign name='search1' color='white' size={17} />
                        </TouchableOpacity>
                    </View>
                </View>

                <View>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 10, }}>
                        {
                            cats.map(cat => {
                                return (
                                    <TouchableOpacity style={{ marginVertical: 5, width: width_ / 5, alignItems: 'center' }}
                                    //onPress={() => this.props.navigation.navigate('houses', { 'cat': cat })}
                                    >
                                        <Avatar.Image size={30} source={{ uri: cat.image }} />
                                        <Text numberOfLines={1} style={{ color: 'grey', marginTop: 5 }}>{cat.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </ScrollView>
                </View>

                <ScrollView scrollEventThrottle={16}
                    onScrollEndDrag={this.handleScroll}
                    onScroll={({ nativeEvent }) => {
                        if (isCloseToBottom(nativeEvent)) {
                            isLoading = true;
                            this.refresh();
                        }
                    }}

                    scrollEventThrottle={400}>
                    
                    {
                        isLoading1 == true ?
                            <View style={{ alignItems: 'center', marginLeft: '5%' }}>
                                <Text style={{ fontWeight: "bold", color: "#dc7e27"}}>Chargement des publications ... </Text>
                            </View> : null
                    }

                    {
                        houses.map((pers, i) =>
                        (
                            user_ = _.find(user, { id: pers.user }),
                            imageshouse_ = _.filter(imageshouses, { house: pers.id }),
                            <View key={i} >
                                <View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 20, fontWeight: "bold", textAlign: 'center' }}>
                                                {pers.category.toUpperCase()} A
                                                {" " + pers.publish_type.toUpperCase()}</Text>
                                        </View>
                                    </View>
                                </View>
                                <TouchableOpacity
                                    /*onPress={() => {
                                        this.props.navigation.navigate('AglaProfile', {
                                            agla: user_
                                            , houses: _.filter(houses, { user: pers.user }),
                                            content_ct: null// _.find(user, { id: pers.user })?.is_landord ? 15 :
                                            // _.find(user, { id: pers.user })?.is_house_agent ? 32 :
                                            // _.find(user, { id: pers.user })?.is_agent ? 31 : null
                                        })
                                    }}*/>

                                    <View style={{ padding: 10, flexDirection: "row", margin: '4%', justifyContent: "space-between", alignItem: "center", }}>
                                        <View style={{ flexDirection: "row" }} >
                                            <Avatar.Image
                                                size={49}
                                                rounded
                                                avatarStyle={{ backgroundColor: 'orange' }}
                                                source={{ uri: user_?.profile_image }} />

                                            <View style={{ alignItems: 'center', marginLeft: '5%', }}>
                                                <Text style={{ fontWeight: "bold", }}>{user_?.username} </Text>
                                                <Text style={{}}>{user_?.typ_user}</Text>
                                                <Text>{this.getDate_publication(pers.date_created)} </Text>
                                            </View>
                                        </View >
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => {this.save_like(pers.id, userid)}} //lineHeight: 32,
                                                style={{ alignItems: 'center', flexDirection: 'row' }}>
                                                <Icon name='heart' size={25} style={{ color: _.find(likes, { house: pers.id, user: userid }) ? "red" : "orange" }} />
                                                <Text style={{ fontSize: 10, left: -6, top: -6, backgroundColor: "purple", borderRadius: 100, color: "white" }}><Text style={{ color: "orangered" }} >.</Text>{_.filter(likes, { house: pers.id }).length} </Text>
                                            </TouchableOpacity>
                                            <Text style={{}}>J'aime</Text>
                                        </View>
                                    </View>

                                </TouchableOpacity >
                                <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: '2%' }}><Text>{pers.description}</Text></View>

                                <ScrollView horizontal={true} >
                                    
                                    {
                                        imageshouse_.length == 0 ?
                                        <TouchableOpacity 
                                           onPress={() => {imageshouse_ = _.filter(imageshouses, { house: pers.id })}}
                                           style={{ alignItems: 'center', borderWidth: 2, justifyContent: 'center', width: width_, borderColor: "#dc7e27" }}>
                                            <Text style={{ fontWeight: "bold", textAlign: 'center', color: "#dc7e27"}}>voir images </Text>
                                        </TouchableOpacity> : 
                                        imageshouse_.map(image_ =>
                                            <TouchableOpacity
                                                onPress={() => { this.props.navigation.navigate('HouseDetails', { id: pers.id, userid: userid }) }}
                                            >
                                                <Image source={{ uri: image_.image }} style={{ width: width_, height: height_ * 0.6, borderColor: "white", borderWidth: 5 }} />
                                            </TouchableOpacity>
                                        )
                                    }

                                </ScrollView>

                                <View style={{ marginBottom: '3%', marginLeft: '2%' }}>

                                    <View style={{ flexDirection: "row", margin: '4%', justifyContent: "space-between", marginTop: '0%' }}>
                                        <View style={{ flexDirection: "row" }}>
                                            <Icon name='map-marker' color="grey" size={19} />
                                            <Text style={{ fontSize: 13, marginLeft: 10, color: 'grey' }}>{_.find(neighbors, { id: pers.neighbor }) ? _.find(neighbors, { id: pers.neighbor })?.name : null + ' ,' + _.find(citys, { id: pers.city }) ? _.find(citys, { id: pers.city })?.name : null} </Text>
                                        </View>
                                        <Text style={{ fontSize: 12, color: "red" }}>{pers.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}XAF {pers.rent_per_day == true ? '/jour' : '/mois'}</Text>
                                    </View>



                                    <View style={{ marginTop: '1%', margin: '5%', flexDirection: 'row', justifyContent: "space-between" }}>
                                        <TouchableOpacity style={{ flexDirection: 'row' }}
                                       
                                        >
                                            <Icon name='phone' size={22} color='orange' />
                                            <Text style={{ fontSize: 8, lineHeight: 32, fontWeight: 'bold' }}>Appeller</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => {
                                                /*this.props.navigation.navigate(
                                                    'MessagesIni', {
                                                    data: pers,
                                                    id:
                                                        _.find(user, { id: pers.user })?.is_landord ? _.find(landlord, { user: _.find(user, { id: pers.user })?.id })?.id
                                                            : _.find(user, { id: pers.user })?.is_agent ? _.find(agent, { user: _.find(user, { id: pers.user })?.id })?.id
                                                                : _.find(user, { id: pers.user })?.is_house_agent ? _.find(company, { user: _.find(user, { id: pers.user })?.id })?.id : null,
                                                    target_ct:
                                                        _.find(user, { id: pers.user })?.is_landord ? 15 :
                                                            _.find(user, { id: pers.user })?.is_house_agent ? 32 :
                                                                _.find(user, { id: pers.user })?.is_agent ? 31 : null
                                                })*/
                                            }}>
                                            <Text>Ecrire</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={() => {
                                                /*this.props.navigation.navigate(
                                                    'PostI', {
                                                    child_ct: 14,
                                                    child_id: pers.id,
                                                    parent_id:
                                                        _.find(user, { id: pers.user })?.is_landord ? _.find(landlord, { user: _.find(user, { id: pers.user })?.id })?.id
                                                            : _.find(user, { id: pers.user })?.is_agent ? _.find(agent, { user: _.find(user, { id: pers.user })?.id })?.id
                                                                : _.find(user, { id: pers.user })?.is_house_agent ? _.find(company, { user: _.find(user, { id: pers.user })?.id })?.id : null,
                                                    parent_ct:
                                                        _.find(user, { id: pers.user })?.is_landord ? 15 :
                                                            _.find(user, { id: pers.user })?.is_house_agent ? 32 :
                                                                _.find(user, { id: pers.user })?.is_agent ? 31 : null
                                                }
                                                )*/
                                            }}>
                                            <Icon name='wechat' size={22} color='grey' />
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{

                                        }}
                                            onPress={() => this.onShare()}
                                        >
                                            <Entypo name='forward' color='orange' size={25} />
                                        </TouchableOpacity>

                                    </View>
                                    {
                                        pers.category.toLowerCase() !== "terrain" && pers.category !== "" && pers.category.toLowerCase() !== "boutique" && pers.category.toLowerCase() !== "local administratif" && pers.category.toLowerCase() !== "bureau" && pers.category.toLowerCase() !== "autre" ?
                                            <>
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%', justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Icon name='bed' color='grey' size={20} />
                                                        <Text style={{ fontSize: 13, color: 'grey' }}>{pers.nb_room} Chambres </Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Icon name='bath' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{pers.nb_toilet}nbre_douches</Text>
                                                    </View>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <Icon name='stop' color='grey' size={20} style={{ marginLeft: '4%' }} />
                                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{pers.nb_parlour} salon </Text>
                                                    </View>

                                                </View>
                                                <View style={{ flexDirection: "row", marginTop: '1%', marginLeft: '4%' }}>
                                                    <View style={{ flexDirection: "row" }}>
                                                        <MaterialIcons name='kitchen' color='grey' size={18} style={{ marginLeft: '4%' }} />
                                                        <Text style={{ fontSize: 13, marginLeft: '1%', color: 'grey' }}>{pers.nb_kitchen} Cuisine(s)</Text>
                                                    </View>
                                                    {pers.parking == true ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Icon name='bus' color='grey' size={20} />
                                                            <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>Parking</Text>
                                                        </View>) : null
                                                    }
                                                    {pers.swimming_pool == true ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ fontSize: 13, color: 'grey', marginLeft: '2%' }}>+  Piscine</Text>
                                                        </View>) : null
                                                    }

                                                </View>
                                            </> : null
                                    }
                                </View>
                                
                                <Divider style={{ backgroundColor: 'grey', height: 15, marginTop: '3%', width: width_ }} />

                            </View>
                        )
                        )

                    }

                    {
                        isLoading == true ?
                            <View style={{ alignItems: 'center', marginLeft: '5%', marginBottom: '10%' }}>
                                <Text style={{ fontWeight: "bold", color: "#dc7e27"}}>Chargement des publications ... </Text>
                            </View> : 
                            <TouchableOpacity style={{ borderColor: '#dc7e27', borderWidth: 2, marginBottom: '5%', marginTop: '2%', marginLeft: '10%', marginRight: '10%' }}
                                onPress={() => (isLoading= true, this.refresh()) }
                            >
                                <Text style={{ fontSize: 13, textAlign: "center", color: "#dc7e27" }}>voir publications </Text>
                            </TouchableOpacity>
                    }
                    <View style={{ height: 250 }} ></View>
                </ScrollView>
            </View>
        )

    }

    getDate_publication = (debut) => {
        let d = new Date(Date.now()); let f = new Date(debut); let diff = {}; let take = ""
        let tmp = d - f;  //Nombre de millisecondes entre les dates
        tmp = Math.floor(tmp / 1000);             // Nombre de secondes entre les 2 dates
        diff.seconds = tmp % 60;                    // Extraction du nombre de secondes
        tmp = Math.floor((tmp - diff.seconds) / 60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes
        tmp = Math.floor((tmp - diff.min) / 60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures
        tmp = Math.floor((tmp - diff.hour) / 24);   // Nombre de jours restants
        diff.days = tmp;

        let seconds = 0;
        if ((diff.min == 0) && (diff.seconds >= 0) && (diff.seconds < 60)) {
            seconds = diff.seconds
            take = "y a moins d'une minute"
        }

        if ((diff.hour == 0) && (diff.min >= 1) && (diff.min < 60)) {
            var minutes = diff.min
            if (minutes == 1) {
                take = "y a " + minutes + " min"
            }
            else {
                take = "y a " + minutes + " min"
            }
        }

        if ((diff.days == 0) && (diff.hour >= 1) && (diff.hour < 23)) {
            var hours = diff.hour
            if (hours == 1) {
                take = "y a " + hours + " h"
            }
            else if (diff.min !== 1) {
                take = "y a " + hours + " h " + diff.min + " min"
            }
            else {
                take = "y a " + hours + " h"
            }
        }

        //1 day to 30 days
        if ((diff.days >= 1) && (diff.days < 30)) {
            var days = diff.days
            if (days == 1) {
                take = "y a " + days + " jour"
            }
            else if (diff.hour !== 1) {
                take = "y a " + days + " jours " + diff.hour + ' h'
            }
            else {
                take = "y a " + days + " jours"
            }
        }

        if (diff.days >= 30 && diff.days < 365) {
            var months = Math.floor(diff.days / 30)
            if (months == 1 && (diff.days - 30) > 0) {
                take = "y a " + months + " mois " + (diff.days - 30) + "jour(s)"
            }
            else if (months == 1) {
                take = "y a " + months + " mois"
            }
            else {
                take = "y a " + months + " mois"
            }
        }

        if (diff.days >= 365) {
            var years = Math.floor(diff.days / 365)
            if (years == 1) {
                take = "y  a " + years + " an"
            }
            else {
                take = "y a " + years + " ans"
            }
        }

        return take
    }

}

const styles = StyleSheet.create({
    row3: {
        backgroundColor: '#fff',
        shadowColor: "black",
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.2,
        shadowRadius: 8,
        //padding: 6,
        elevation: 9,
        marginVertical: 6,
        //margin: 5
    },
    button1: {
        justifyContent: 'center', flexDirection: 'row', height: 28,
        borderRadius: 8, paddingTop: 3, marginTop: 10
    }
})