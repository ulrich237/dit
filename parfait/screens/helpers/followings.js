import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import _ from 'underscore';
import { Avatar } from 'react-native-paper';
import axios from 'axios';


var width_ = Math.round(Dimensions.get('window').width)
export default class Followings extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }
    componentDidMount() {
        axios.get("https://imobbis.pythonanywhere.com/new/utilisateur/").then(res => {
            this.setState({ users: res.data })
        }).catch(err => console.log(err));
    }
    render() {
        const { user, followers } = this.props.route.params
        const { users } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <View style={{ flexDirection: 'row', backgroundColor:'#d97d2a', width:width_ }}>
                    <TouchableOpacity style={{ marginLeft: -8, width: '14%', marginTop:5 }} onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before' size={45} style={{ }} />
                    </TouchableOpacity>
                    <View style={{ width: '80%', alignItems: 'center', marginTop: 10 }}>
                        <Text style={{ fontWeight: "600", fontSize: 18, color:'black' }}>
                            {user.username} ({user.typ_user})
                        </Text>
                    </View>
                </View>
                <ScrollView>
                    <Text style={{ marginVertical: 10, fontWeight: '600', fontSize: 16 }}>Tous mes abonnes ({_.size(followers)})</Text>
                    <View>
                        {
                            _.size(followers) == 0 ?
                                <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: height_ * 0.4, alignSelf: 'center' }}>
                                    Pas d'abonnes pour le moment
                        </Text>
                                :
                                users.map(user_ => followers.map(fol => {
                                    return (
                                        user_.id === fol.user ?
                                            <View style={{ flexDirection: 'row', marginVertical: 6 }}>
                                                <Avatar.Image source={{ uri: user_.profile_image }} size={60} />
                                                <View style={{ marginLeft: 11, marginTop: 7 }}>
                                                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{user_.username}</Text>
                                                    <Text style={{}}>
                                                        {user_.last_name} {user_.first_name} ({user_.typ_user})
                                                    </Text>
                                                </View>
                                            </View>
                                            : null
                                    )
                                }

                                ))
                        }
                    </View>
                </ScrollView>

            </View>
        );
    }
}