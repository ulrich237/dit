import _ from 'underscore'
import axios from 'axios'
import React, {Component} from 'react'
import {View, Text, StyleSheet, Dimensions, Image,TouchableOpacity,
     ScrollView} from 'react-native'
import { Avatar } from 'react-native-paper'
import MaterialIcons from 'react-native-vector-icons//MaterialIcons'

export default class RecentlyTouched extends Component{ 
    constructor(props){
        super(props)
        this.state = {
            users: [], items: [], 
            TouchItem: [], 
        }     
    }

    componentDidMount(){
    const {visited, user_id} = this.props.route.params 

    switch(visited) {
        case 'house': 
          axios.get('https://imobbis.pythonanywhere.com/house/').then(res => {  
            this.setState({items: _.filter(res.data, {user: user_id})});   
            })
          axios.get('https://imobbis.pythonanywhere.com/touched/').then(res => {  
            this.setState({TouchItem: res.data});    
          })   
          axios.get('https://imobbis.pythonanywhere.com/user/').then(res => {  
            this.setState({users: res.data});    
          })   
          
    }   
    }

    render(){
        const {TouchItem, items, users} = this.state  
        var touched_ = []
        TouchItem.map(tit => tit.touched.map(ttt => items.map(it => {
            if(ttt == it.id){
                touched_.push(tit);
            }
        })))
        return( 
             <View style={{flex: 1, padding: 5, backgroundColor: 'white'}}>
             <View style={{flexDirection: 'row'}}>
             <TouchableOpacity style={{marginLeft: -10, width: '14%'}}  onPress={() => this.props.navigation.goBack()}>
                        <MaterialIcons name='navigate-before'  size={50} style={{marginTop: -5}}/>
                    </TouchableOpacity>
             <View style={{width: '80%', alignItems: 'center', marginTop: 3}}>
                <Text style={{fontSize: 25, fontWeight: '600', marginBottom: 15}}> Recemment visitees ({_.size(touched_)})</Text>
             </View>
             </View>
             <ScrollView>
                    {
                        touched_?.map(Touchitem_ => {
                         return(
                              <View style={{marginTop: 20, borderColor: '#e6e6e6', borderWidth: 2, padding: 5,borderRadius: 9}}> 
                                 <Text style={{fontWeight: '700', fontSize: 18, marginVertical: 5, color: 'orange'}}>{Touchitem_.timestamp.substring(0, 10)}</Text>
                                 <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                                 {
                                     Touchitem_.touched.map(touched =>{
                                         return(
                                                user = _.find(users, {id: Touchitem_.user}),
                                                 items.map(item =>{
                                                     return(                                                      
                                                       item.id == touched ?
                                                        <TouchableOpacity style={styles.row_modal}
                                                        onPress={() => alert(5)}
                                                        >   
                                                            <Image
                                                            source={{uri: item.photo_1}}
                                                            style={{width: '100%', height: 120, borderTopLeftRadius: 8,
                                                                 resizeMode: 'stretch', borderTopRightRadius: 8}}
                                                            />
                                                            <Text style={{ fontSize: 14, color: "red", alignSelf: 'center', fontWeight: '900' }}> 
                                                                {item.renting_price?.toString()?.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")}XAF {item?.rent_per_day == true ? '/jour' : '/mois'}
                                                            </Text>
                                                        <View style={{flexDirection: 'row', padding: 4}}>
                                                            <Avatar.Image source={{ uri: user?.photo_profile }} size={35} />
                                                            <View style={{width: '75%', marginLeft: 4}}>
                                                                <Text>Visite par</Text>
                                                                <Text style={{fontWeight: '900', fontSize: 12}} numberOfLines={1}>{user?.username}</Text>
                                                            </View>
                                                        </View>
                                                         </TouchableOpacity>

                                                       :null
                                                     )
                                               })                                              
                                       )
                                     }
                                    )                                
                                 }
                                  </View>
                              </View>                          
                         )}
                         )
                 }
            </ScrollView>  
          </View>
        )
    }
}


const styles = StyleSheet.create({
    row_modal: {
        backgroundColor: '#fff',
        shadowColor: "black",
        shadowOffset: {
          width: 0, height: 6,
        }, width: '47%',
        shadowOpacity: 0.1,
        shadowRadius: 8.30,
        marginBottom: 6, marginTop: 5,
        borderRadius: 8, marginRight: 10
      },
})