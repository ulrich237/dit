import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import _ from 'underscore';
import axios from 'axios';
import { View, Text, Alert, Linking, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

import AuthStack from './screens/auth/authStack'

import HomeBottomTabs from './screens/normalUser/homeBotNav';
//import UserNavigation from './screens/normalUser/homeNavigation/navvigation';
//import MeNavigation from './screens/normalUser/meNavigation/navigation';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
//import VersionNumber from 'react-native-version-number';
//import PushNotification from "react-native-push-notification";
//import Me from './screens/normalUser/meNavigation/me';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Notification from './functions/notification';

const Stack = createStackNavigator();
var dat = {}
var my_fir_ser_key =
  'AAAA2dC4Mq8:APA91bHA0z322vvsu8J7XOrmhHtc0nnTRJ9CLeXxzYZbULtddZoNW_0UlyaQlt47OZURIGNBF9L13G8bEhjY48RuZNvqh6dpqNitt7flcb4GzZXu5l2y1z1gghv4HGbmYAGBrQvUzFFY';
var test_tok =
  'd2zSFc9gQNW_ntoX7ojVu0:APA91bHO1qg7nxMzzcHwciQRu8kWMJ0sm5Mej2cOfuPQSrSCAP5TBp4KHXkUpKo5LB0QOZYucUGFWdJ0-TpAtk4nPPhvGGo_UlgwXFngmv2bDWMcZUUgWZRKoANmZgo1w4tn8w5a8gyV';
var my_app_id = '1:717203538662:ios:7a4aa5b3e84fb22a103127';

var fcmUnsubscribe = null;
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}, userid: 2, user_: {}, data: {}
    }

    /*PushNotification.configure({

      onRegister: function (token) {
        //console.log('APNs Status+++: ', token);
        axios.patch('https://imobbis.pythonanywhere.com/new/utilisateur/' + this.state?.userid, {
          tokens: token,
        }).catch((error) => console.log("error..."));
      },
      
      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);

        this.props.navigation.navigate('Me')

        console.log('data:', notification.data);
       
        //this.scheduleNotfication(notification?.data?.title, notification?.data?.body)
        //notification.finish(PushNotificationIOS.FetchResult.NoData);
      
      },

      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION1:', notification);
      },

      onRegistrationError: function (err) {
        console.error(err.message, err);
      },

      senderID: "935509635759",
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });*/
  }

  componentDidMount() {
    /*messaging().requestPermission()
      .then(authStatus => {
        
        if (authStatus === messaging.AuthorizationStatus.AUTHORIZED
          || authStatus === messaging.AuthorizationStatus.PROVISIONAL) {
          messaging()
            .getToken()
            .then(token => {
              //console.log('APNs Status: ', token);
              axios.patch('https://imobbis.pythonanywhere.com/new/utilisateur/' + this.state?.userid, {
                tokens: token,
              }).catch((error) => console.log("error"));
            });

          messaging().onTokenRefresh(token => {
            console.log('messaging.onTokenRefresh: ', token)
          });

          fcmUnsubscribe = messaging().onMessage(async remoteMessage => {
            remoteMessage?.data?.msgType == 'updateVersion' ?
              VersionNumber.appVersion !== remoteMessage?.data?.version ?
                Alert.alert(
                  remoteMessage.notification.title,
                  remoteMessage.notification.body,
                  [
                    {
                      text: 'METTRE À JOUR',
                      onPress: () => {
                        Linking.openURL(Platform.OS === 'ios' ? '' : 'https://play.google.com/store/apps/details?id=com.imobbis.app');
                      }
                    },
                    {
                      text: 'ANNULER',
                      onPress: () => console.log('Close Pressed'),
                      style: 'cancel'
                    }
                  ]
                ) :
                Alert.alert(
                  remoteMessage.notification.title,
                  "Votre application est à jour",
                )
              : remoteMessage?.data?.msgType == 'message' ?
                Alert.alert(
                  remoteMessage.notification.title,
                  remoteMessage.notification.body,
                )
                : null
          });
        }
      })
      .catch(err => {
        console.log("messaging.requestPermission Error: ", err);
      });*/


    this.displayData()
  }

  displayData = async () => {
    try {
      let user = await AsyncStorage.getItem('user_data');
      let json_user = JSON.parse(user)

      let use = await AsyncStorage.getItem('user');
      let json_user_ = JSON.parse(use)
      this.setState({ user: json_user, userid: json_user, user_: json_user_ })
    } catch { error => alert(error) };
  }

  sed = async () => {

    const message = {
      to: test_tok,
      notification: {
        title: 'title',
        body: 'body',
        vibrate: 1,
        sound: 1,
        show_in_foreground: true,
        priority: "high",
        content_available: true,
        color: '#dc7e27',
      },
      data: {
        title: 'title',
        body: 'body',
        score: 50,
        wicket: 1,
      }
    }

    const headers = {
      "Content-Type": "application/json",
      Authorization: "key=" + my_fir_ser_key,
    }
    let response = await fetch("https://fcm.googleapis.com/fcm/send", {
      method: "POST",
      headers,
      body: JSON.stringify(message),
    }
    ).catch(error => console.log(error)
    )
    response = await response.json()

  }


  scheduleNotfication = (title, message) => {

    PushNotification.localNotification({
      title: title,
      message: message,
    });

  }

  render() {
    var martop = Platform.OS == 'ios' ? DeviceInfo.hasNotch() ? 50 : 20 : DeviceInfo.hasNotch() ? 20 : 0
    const { user } = this.state

    console.log(user)
    return (
      <View style={{ paddingTop: martop, flex: 1 }}>
        <NavigationContainer>
          {
            user == null ?
              <Stack.Navigator>
                <Stack.Screen name='AuthStack' component={AuthStack} options={{ headerShown: false }} />
              </Stack.Navigator>
              :
              <Stack.Navigator
                screenOptions={{
                  headerMode: 'screen',
                  headerTintColor: 'white',
                  headerStyle: { backgroundColor: 'orange'},
                }}>
                <Stack.Screen name='home' component={HomeBottomTabs} options={{ headerShown: false }} />
                {
                  <Stack.Screen name='AuthStack' component={AuthStack} options={{ headerShown: false }} />
                }
                {//<Stack.Screen name='MeNavigation' component={MeNavigation} options={{ headerShown: false }} />
                ///<Stack.Screen name='Me' component={Me} options={{ headerShown: false }} />
              }
              </Stack.Navigator>
          }
        </NavigationContainer>
      </View>
    )
  }

}